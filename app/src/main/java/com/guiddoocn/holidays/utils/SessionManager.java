package com.guiddoocn.holidays.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;


/**
 * Created by Ishwar on 10-06-2019.
 */

public class SessionManager {

    public static DrawerLayout drawer;
    private static final String PREF_NAME = "Holidays";
    private static final String OLDTIMESTAMP = "isOldTimeStamp";
    private static final String NEWTIMESTAMP = "isNewTimeStamp";
    private static final String BASE_URL = "isBaseUrl";
    private static final String BASE_URL_GUIDDOO = "isBaseUrlGuiddoo";
    private static final String NAVIGATION = "isNavigation";
    private static final String LANGUAGE = "isLanguage";
    private static final String NEWPRICE = "isNewPrice";
    private static final String ISMARGIN = "isMargin";

    //ActivityLogin

    private static final String USER_ID = "isUserId";
    private static final String USER_CART_COUNT = "isUserCart";
    private static final String EMAIL_ID = "isEmailId";
    private static final String USER_NAME = "isUserName";
    private static final String HASHKEY = "isHashKey";
    private static final String USER_FNAME = "isUserFName";
    private static final String USER_LNAME = "isUserLName";
    private static final String CONTACT_PERSON = "isContactPerson";
    private static final String COUNTRY_CODE = "isCountryCode";
    private static final String COUNTRY_NAME = "isCountryNAME";
    private static final String CITY_CODE = "isCityCode";
    private static final String CITY_NAME = "isCityNAME";
    private static final String USER_MOBILE_NO = "isUserMobile";
    private static final String USER_PHOTO = "isUserPhoto";
    private static final String LOGED_IN = "logedIn";
    private static final String SORTINGFILTER = "sort";


    //------------Customize packages------------

    private static final String CHECK_IN = "isCheckIn";
    private static final String CHECK_OUT = "isCheckOut";
    private static final String ADULT_MAIN_COUNT = "isAdultMainCount";
    private static final String CHILD_MAIN_COUNT = "isChildMainCount";
    private static final String ADULT_COUNT = "isAdultCount";
    private static final String CHILD_COUNT = "isChildCount";
    private static final String CHILD_AGE = "isChildAge";


    //--------- Hotels Filter--------------------
    private static final String FILTER_MIN = "isFilterMin";
    private static final String FILTER_MAX = "isFilterMaxg";
    private static final String FILTER_HOTEL = "isFilterHotels";

    private static final String AMINITIESFILTER = "isAminities";

    private static final String TVFILTER = "tvfilter";
    private static final String BATHTUBFILTER = "bathfilter";
    private static final String GYMFILTER = "gymfilter";
    private static final String CARFILTER = "carfilter";
    private static final String CURRENCYFILTER = "currencyfilter";
    private static final String FREESHUTTLEFILTER = "freeshuttlefilter";
    private static final String SWIMMINGFILTER = "swimmingfilter";
    private static final String WIFIFILTER = "wififilter";
    private static final String WELLNESSSPAFILTER = "wellnessspafilter";
    private static final String DISCONIGHTCLUBFILTER = "disconightclubfilter";
    private static final String ROOMSERVICEFILTER = "roomservicefilter";

    //--------------Activities Filter--------------
    private static final String FILTER_MIN_ACT = "isFilterMinact";
    private static final String FILTER_MAX_ACT = "isFilterMaxact";
    private static final String SORTINGACTFILTER = "actsort";
    private static final String FILTERACTIVE = "isFilterAct";
    private static final String TRANSFER = "istransfer_included";
    private static final String FILTER_MIN_STAR_ACT = "isFilterMinStarAct";
    private static final String FILTER_MAX_STAR_ACT = "isFilterMaxStarAct";
    private static final String CATFILTER = "isCatFilter";
    private static final String DININGFILTER = "isDinFilter";

    //-------------- Star---------------------

    private static final String FILTER_MIN_STAR = "isFilterMinStar";
    private static final String FILTER_MAX_STAR = "isFilterMaxStar";

    private static final String STARONEFILTER = "staronefilter";
    private static final String STARTWOFILTER = "startwofilter";
    private static final String STARTHREEFILTER = "starthreefilter";
    private static final String STARFOURFILTER = "starfourfilter";
    private static final String STARFIVEFILTER = "starfivefilter";


    private static String TAG = SessionManager.class.getSimpleName();

    Context _context;
    int PRIVATE_MODE = 0;
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public boolean setBaseUrl(String key) {
        editor.putString(BASE_URL, key);
        if (editor.commit()) {
            Log.d(TAG, "BaseUrl key is set!");
            return true;
        }
        return false;
    }

    public boolean setBaseUrlGuiddoo(String key) {
        editor.putString(BASE_URL_GUIDDOO, key);
        if (editor.commit()) {
            Log.d(TAG, "BASE_URL_GUIDDOO key is set!");
            return true;
        }
        return false;
    }


///////////////////////////////////////// set All data //////////////////////////////////////////////////////////


    public boolean setOldTimeStamp(String key) {
        editor.putString(OLDTIMESTAMP, key);
        if (editor.commit()) {
            Log.d(TAG, "Old TimeStamp key is set!");
            return true;
        }
        return false;
    }

    public boolean setNewTimeStamp(String key) {
        editor.putString(NEWTIMESTAMP, key);
        if (editor.commit()) {
            Log.d(TAG, "New TimeStamp key is set!");
            return true;
        }
        return false;
    }

    public boolean setUserName(String key) {
        editor.putString(USER_NAME, key);
        if (editor.commit()) {
            Log.d(TAG, "User Name  is set!");
            return true;
        }
        return false;
    }

    public boolean setHashKey(String key) {
        editor.putString(HASHKEY, key);
        if (editor.commit()) {
            Log.d(TAG, "HashKey is set!");
            return true;
        }
        return false;
    }

    public boolean setUserFirstName(String key) {
        editor.putString(USER_FNAME, key);
        if (editor.commit()) {
            Log.d(TAG, "User FName  is set!");
            return true;
        }
        return false;
    }

    public boolean setUserLastName(String key) {
        editor.putString(USER_LNAME, key);
        if (editor.commit()) {
            Log.d(TAG, "User LName  is set!");
            return true;
        }
        return false;
    }

    public boolean setContactPerson(String key) {
        editor.putString(CONTACT_PERSON, key);
        if (editor.commit()) {
            Log.d(TAG, "Contact person  is set!");
            return true;
        }
        return false;
    }

    public boolean setCountryCode(String key) {
        editor.putString(COUNTRY_CODE, key);
        if (editor.commit()) {
            Log.d(TAG, "User Mobile  is set!");
            return true;
        }
        return false;
    }

    public boolean setCountryName(String key) {
        editor.putString(COUNTRY_NAME, key);
        if (editor.commit()) {
            Log.d(TAG, "User Mobile  is set!");
            return true;
        }
        return false;
    }

    public boolean setCityCode(String key) {
        editor.putString(CITY_CODE, key);
        if (editor.commit()) {
            Log.d(TAG, "User Mobile  is set!");
            return true;
        }
        return false;
    }

    public boolean setCityName(String key) {
        editor.putString(CITY_NAME, key);
        if (editor.commit()) {
            Log.d(TAG, "User Mobile  is set!");
            return true;
        }
        return false;
    }

    public boolean setUserMobile(String key) {
        editor.putString(USER_MOBILE_NO, key);
        if (editor.commit()) {
            Log.d(TAG, "User Mobile  is set!");
            return true;
        }
        return false;
    }

    public boolean setUserPhoto(String key) {
        editor.putString(USER_PHOTO, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setSorting_Position(String key) {
        editor.putString(SORTINGFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setActSortingPosition(String key) {
        editor.putString(SORTINGACTFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setActCatFilter(String key) {
        editor.putString(CATFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setActDiningFilter(String key) {
        editor.putString(DININGFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo is set!");
            return true;
        }
        return false;
    }

    public boolean setCheckInDate(String key) {
        editor.putString(CHECK_IN, key);
        if (editor.commit()) {
            Log.d(TAG, "CHECK_IN is set!");
            return true;
        }
        return false;
    }

    public boolean setCheckOutDate(String key) {
        editor.putString(CHECK_OUT, key);
        if (editor.commit()) {
            Log.d(TAG, "CHECK_OUT is set!");
            return true;
        }
        return false;
    }

    public boolean setAdultMainCount(String key) {
        editor.putString(ADULT_MAIN_COUNT, key);
        if (editor.commit()) {
            Log.d(TAG, "ADULT_COUNT is set!");
            return true;
        }
        return false;
    }

    public boolean setAdultCount(String key) {
        editor.putString(ADULT_COUNT, key);
        if (editor.commit()) {
            Log.d(TAG, "ADULT_COUNT is set!");
            return true;
        }
        return false;
    }

    public boolean setChildMainCount(String key) {
        editor.putString(CHILD_MAIN_COUNT, key);
        if (editor.commit()) {
            Log.d(TAG, "CHILD_COUNT is set!");
            return true;
        }
        return false;
    }


    public boolean setChildCount(String key) {
        editor.putString(CHILD_COUNT, key);
        if (editor.commit()) {
            Log.d(TAG, "CHILD_COUNT is set!");
            return true;
        }
        return false;
    }

    public boolean setChildAge(String key) {
        editor.putString(CHILD_AGE, key);
        if (editor.commit()) {
            Log.d(TAG, "CHILD_AGE is set!");
            return true;
        }
        return false;
    }


//-----------------Sorting With Grid eg. TV,GYM,CAR---------------------

    public boolean setFilterMinAmt(String key) {
        editor.putString(FILTER_MIN, key);
        if (editor.commit()) {
            Log.d(TAG, "FILTER_MIN  is set!");
            return true;
        }
        return false;
    }

    public boolean setFilterMinAmtAct(String key) {
        editor.putString(FILTER_MIN_ACT, key);
        if (editor.commit()) {
            Log.d(TAG, "FILTER_MIN  is set!");
            return true;
        }
        return false;
    }

    public boolean setFilterMaxAmt(String key) {
        editor.putString(FILTER_MAX, key);
        if (editor.commit()) {
            Log.d(TAG, "FILTER_MAX  is set!");
            return true;
        }
        return false;
    }

    public boolean setFilterMaxAmtAct(String key) {
        editor.putString(FILTER_MAX_ACT, key);
        if (editor.commit()) {
            Log.d(TAG, "FILTER_MAX  is set!");
            return true;
        }
        return false;
    }

    public void setFilterHotel(boolean isLoggedIn) {
        editor.putBoolean(FILTER_HOTEL, isLoggedIn);
        editor.commit();
    }

    public void setFilteractive(boolean isLoggedIn) {
        editor.putBoolean(FILTERACTIVE, isLoggedIn);
        editor.commit();
    }

    public boolean setTransferFilter(String key) {
        editor.putString(TRANSFER, key);
        if (editor.commit()) {
            Log.d(TAG, "Transfer key is set!");
            return true;
        }
        return false;
    }

//-------------------------aminities

    public boolean setAminitiesFilter(String key) {
        editor.putString(AMINITIESFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "AMINITIESFILTER  is set!");
            return true;
        }
        return false;
    }

    public boolean setTvFilter(String key) {
        editor.putString(TVFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }



    public boolean setBathtubFilter(String key) {
        editor.putString(BATHTUBFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setGymFilter(String key) {
        editor.putString(GYMFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setCarFilter(String key) {
        editor.putString(CARFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setCurrencyFilter(String key) {
        editor.putString(CURRENCYFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }


    public boolean setFreeShuttleFilter(String key) {
        editor.putString(FREESHUTTLEFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }


    public boolean setSwimmingFilter(String key) {
        editor.putString(SWIMMINGFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setWifiFilter(String key) {
        editor.putString(WIFIFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setWellnessSpaFilter(String key) {
        editor.putString(WELLNESSSPAFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setDiscoNightClubFilter(String key) {
        editor.putString(DISCONIGHTCLUBFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setRoomServiceFilter(String key) {
        editor.putString(ROOMSERVICEFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }


    ///----------- Star  Filter--------------

    public boolean setMinStarFilter(String key) {
        editor.putString(FILTER_MIN_STAR, key);
        if (editor.commit()) {
            Log.d(TAG, "FILTER_MIN_STAR is set!");
            return true;
        }
        return false;
    }

    public boolean setMinStarFilterAct(String key) {
        editor.putString(FILTER_MIN_STAR_ACT, key);
        if (editor.commit()) {
            Log.d(TAG, "FILTER_MIN_STAR is set!");
            return true;
        }
        return false;
    }

    public boolean setMaxStarFilter(String key) {
        editor.putString(FILTER_MAX_STAR, key);
        if (editor.commit()) {
            Log.d(TAG, "FILTER_MAX_STAR is set!");
            return true;
        }
        return false;
    }

    public boolean setMaxStarFilterAct(String key) {
        editor.putString(FILTER_MAX_STAR_ACT, key);
        if (editor.commit()) {
            Log.d(TAG, "FILTER_MAX_STAR is set!");
            return true;
        }
        return false;
    }
    public boolean setStarOneFilter(String key) {
        editor.putString(STARONEFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setStarTwoFilter(String key) {
        editor.putString(STARTWOFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setStarThreeFilter(String key) {
        editor.putString(STARTHREEFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }

    public boolean setStarFourFilter(String key) {
        editor.putString(STARFOURFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }
    public boolean setStarFiveFilter(String key) {
        editor.putString(STARFIVEFILTER, key);
        if (editor.commit()) {
            Log.d(TAG, "User Photo  is set!");
            return true;
        }
        return false;
    }


    public boolean setEmailId(String key) {
        editor.putString(EMAIL_ID, key);
        if (editor.commit()) {
            Log.d(TAG, "User Name  is set!");
            return true;
        }
        return false;
    }

    public boolean setUserID(String key) {
        editor.putString(USER_ID, key);
        if (editor.commit()) {
            Log.d(TAG, "User ID is set!");
            return true;
        }
        return false;
    }


    public boolean setCartCount(String key) {
        editor.putString(USER_CART_COUNT, key);
        if (editor.commit()) {
            Log.d(TAG, "User Card is set!");
            return true;
        }
        return false;
    }

    public boolean setLogedIn(boolean logedIn) {

        editor.putBoolean(LOGED_IN, logedIn);
        if (editor.commit()) {
            Log.d(TAG, "User has loged in !");
            return true;
        }
        return false;
    }


    public boolean setNavigation(String nav) {
        editor.putString(NAVIGATION, nav);
        if (editor.commit()) {
            Log.d(TAG, "NAVIGATION selected");
            return true;
        }
        return false;
    }

    public boolean setLanguage(String nav) {
        editor.putString(LANGUAGE, nav);
        if (editor.commit()) {
            Log.d(TAG, "LANGUAGE selected");
            return true;
        }
        return false;
    }

    public void setNewprice(String key) {
        editor.putString(NEWPRICE, key);
        editor.commit();
    }

    public void setIsmargin(String key) {
        editor.putString(ISMARGIN, key);
        editor.commit();
    }


    public String getBaseUrl() {
        return pref.getString(BASE_URL, null);
    }

    public String getBaseUrlGuiddoo() {
        return pref.getString(BASE_URL_GUIDDOO, null);
    }


    ///////////////////////////////////////get All Data /////////////////////////////////////////////////////////


    public String getOldTimeStamp() {
        return pref.getString(OLDTIMESTAMP, "");
    }

    public String getNewTimeStamp() {
        return pref.getString(NEWTIMESTAMP, "");
    }

    public String getUserName() {
        return pref.getString(USER_NAME, "User Name");
    }

    public String getHashkey() {
        return pref.getString(HASHKEY, "");
    }

    public String getUserFirstName() {
        return pref.getString(USER_FNAME, "");
    }

    public String getUserLastName() {
        return pref.getString(USER_LNAME, "");
    }

    public String getContactPerson() {
        return pref.getString(CONTACT_PERSON, "");
    }

    public String getCountryCode() {
        return pref.getString(COUNTRY_CODE, "2");
    }

    public String getCityCode() {
        return pref.getString(CITY_CODE, "6");
    }

    public String getUserMobileNo() {
        return pref.getString(USER_MOBILE_NO, "");
    }

    public String getUserPhoto() {
        return pref.getString(USER_PHOTO, "");
    }

    // Filter   Sorting

    public String getSortingPosition() {
        return pref.getString(SORTINGFILTER, "0");
    }

    public String getActSortingPosition() {
        return pref.getString(SORTINGACTFILTER, "0");
    }


    public String getActCatFilter() {
        return pref.getString(CATFILTER, "()");
    }

    public String getActDiningFilter() {
        return pref.getString(DININGFILTER, "()");
    }

    public String getCheckInDate() {
        return pref.getString(CHECK_IN, "");
    }

    public String getCheckOutDate() {
        return pref.getString(CHECK_OUT, "");
    }


    public String getAdultMainCount() {
        return pref.getString(ADULT_MAIN_COUNT, "1");
    }
    public String getAdultCount() {
        return pref.getString(ADULT_COUNT, "1");
    }

    public String getChildMainCount() {
        return pref.getString(CHILD_MAIN_COUNT, "0");
    }

    public String getChildCount() {
        return pref.getString(CHILD_COUNT, "0");
    }

    public String getChildAge() {
        return pref.getString(CHILD_AGE, "");
    }


    ///-------------------Filter GRID ex TV,CAR, CURRENCY

    public String getFilterMinAmt() {
        return pref.getString(FILTER_MIN, "0");
    }
    public String getFilterMinAmtAct() {
        return pref.getString(FILTER_MIN_ACT, "0");
    }

    public String getFilterMaxAmt() {
        return pref.getString(FILTER_MAX, "500");
    }

    public String getFilterMaxAmtAct() {
        return pref.getString(FILTER_MAX_ACT, "500");
    }

    public boolean getFilterHotels() {
        return pref.getBoolean(FILTER_HOTEL, false);
    }

    public boolean getFilterActive() {
        return pref.getBoolean(FILTERACTIVE, false);
    }

    public String getTransferFilter() {
        return pref.getString(TRANSFER, "");
    }

    public String getAminitiesFilter() {
        return pref.getString(AMINITIESFILTER, "()");
    }

    public String getTvFilter() {
        return pref.getString(TVFILTER, "");
    }

    public String getBathtubFilter() {
        return pref.getString(BATHTUBFILTER, "");
    }


    public String getGymFilter() {
        return pref.getString(GYMFILTER, "");
    }

    public String getCountryName() {
        return pref.getString(COUNTRY_NAME, "");
    }

    public String getCityName() {
        return pref.getString(CITY_NAME, "");
    }

    public String getCarFilter() {
        return pref.getString(CARFILTER, "");
    }

    public String getCurrencyFilter() {
        return pref.getString(CURRENCYFILTER, "");

    }

    public String getFreeShuttleFilter() {
        return pref.getString(FREESHUTTLEFILTER, "");

    }

    public String getSwimmingFilter() {
        return pref.getString(SWIMMINGFILTER, "");

    }

    public String getWifiFilter() {
        return pref.getString(WIFIFILTER, "");

    }

    public String getWellnessSpaFilter() {
        return pref.getString(WELLNESSSPAFILTER, "");

    }

    public String getDiscoNightClubFilter() {
        return pref.getString(DISCONIGHTCLUBFILTER, "");

    }

    public String getRoomServiceFilter() {
        return pref.getString(ROOMSERVICEFILTER, "");

    }
    ///---------- Filter Star---------


    public String getFilterMinStar() {
        return pref.getString(FILTER_MIN_STAR, "0");
    }

    public String getFilterMinStarAct() {
        return pref.getString(FILTER_MIN_STAR_ACT, "0");
    }

    public String getFilterMaxStar() {
        return pref.getString(FILTER_MAX_STAR, "5");
    }

    public String getFilterMaxStarAct() {
        return pref.getString(FILTER_MAX_STAR_ACT, "5");
    }

    public String getOneFilter() {
        return pref.getString(STARONEFILTER, "");
    }

    public String getTwoFilter() {
        return pref.getString(STARTWOFILTER, "");
    }

    public String getThreeFilter() {
        return pref.getString(STARTHREEFILTER, "");

    }

    public String getFourFilter() {
        return pref.getString(STARFOURFILTER, "");
    }

    public String getFiveFilter() {
        return pref.getString(STARFIVEFILTER, "");
    }

    public String getEmailId() {
        return pref.getString(EMAIL_ID, "Email Address");
    }

    public String getUserId() {
        return pref.getString(USER_ID, "0");
    }


    public String getCartCount() {
        return pref.getString(USER_CART_COUNT, "0");
    }

    public boolean getLogedIn() {
        return pref.getBoolean(LOGED_IN, false);
    }


    public String getNavigation() {
        return pref.getString(NAVIGATION, "");
    }

    public String getLanguage() {
        return pref.getString(LANGUAGE, "En");
    }
    public String getNewPrice() {
        return pref.getString(NEWPRICE, "0");
    }

    public String getIsmargin() {
        return pref.getString(ISMARGIN, "1");
    }


    /*--------------------------- Delete Record ---------------------------------------*/

    public boolean deleteToken() {
        editor.remove(HASHKEY);
        if (editor.commit()) {
            Log.d(TAG, "Authorized key is deleted");
            return true;
        }
        return false;
    }


}
