package com.guiddoocn.holidays.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterArticalNewsList;
import com.guiddoocn.holidays.models.ModelNewsArticalList;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentMediaKit extends Fragment {
    private boolean doubleBackToExitPressedOnce = false;
    @BindView(R.id.rv_news_Artical)
    RecyclerView rv_news_Artical;
    private Context mContext;
    AdapterArticalNewsList adapterNewsArticalList;
    ArrayList<ModelNewsArticalList> modelInclusionLists = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_media_kit, container, false);
        mContext = getActivity();
        ButterKnife.bind(this, view);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }



        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical1_title),getString(R.string.news_artical1_details),"https://www.chinatravelnews.com/article/128715",R.drawable.news_artical1));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical2_title),getString(R.string.news_artical2_details),"http://www.travelbizmonitor.com/OTA/destination-experience-aggregator-guiddoo-to-enter-china-market-40116",R.drawable.news_artical2));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical3_title),getString(R.string.news_artical3_details),"https://inc42.com/buzz/traveltech-startup-guiddoo-raises-funds-to-lure-in-chinas-tourists/",R.drawable.news_artical3));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical4_title),getString(R.string.news_artical4_details),"https://www.moneycontrol.com/news/business/companies/young-turks-introduces-guiddoo-to-enhance-traveling-1065873.html",R.drawable.news_artical4));

        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical5_title),getString(R.string.news_artical5_details),"https://www.zeebiz.com/technology/news-travelling-abroad-these-5-apps-can-make-your-holiday-experience-better-95518",R.drawable.news_artical5));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical6_title),getString(R.string.news_artical6_details),"https://economictimes.indiatimes.com/industry/services/travel/customised-packages-will-gain-momentum-vineet-budki-ceo-guiddoo-world/articleshow/68633706.cms",R.drawable.news_artical6));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical7_title),getString(R.string.news_artical7_details),"https://economictimes.indiatimes.com/small-biz/startups/how-travel-startup-guiddoo-helps-build-your-holiday-experience/articleshow/49699624.cms",R.drawable.news_artical7));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical8_title),getString(R.string.news_artical8_details),"https://www.arabianbusiness.com/entrepreneurs-of-week-vineet-budki-nidhi-varma-founders-of-guiddoo-637836.html",R.drawable.news_artical8));

        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical9_title),getString(R.string.news_artical9_details),"https://yourstory.com/2015/08/guidoo",R.drawable.news_artical9));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical10_title),getString(R.string.news_artical10_details),"https://www.livemint.com/Companies/fqyW9rDFptVZJue9r5b7lK/Virtual-tour-sites-apps-make-travel-an-easier-ride.html",R.drawable.news_artical10));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical11_title),getString(R.string.news_artical11_details),"https://www.phocuswire.com/guiddoo-raises-800k",R.drawable.news_artical11));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical12_title),getString(R.string.news_artical12_details),"https://www.webintravel.com/wit-start-pitch-guiddoo-tour-guides-activities-one-app/",R.drawable.news_artical12));
        modelInclusionLists.add(new ModelNewsArticalList(getString(R.string.news_artical13_title),getString(R.string.news_artical3_details),"https://www.techinasia.com/indian-startup-converts-smartphone-tour-guide",R.drawable.news_artical13));

        GridLayoutManager manager1 = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
        FragmentManager fragmentManager1 = getActivity().getSupportFragmentManager();
        adapterNewsArticalList = new AdapterArticalNewsList(mContext, modelInclusionLists, getActivity(), fragmentManager1);
        rv_news_Artical.setLayoutManager(manager1);
        rv_news_Artical.setAdapter(adapterNewsArticalList);
        rv_news_Artical.setNestedScrollingEnabled(false);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if(SessionManager.drawer.isDrawerOpen(GravityCompat.START)) {
                        SessionManager.drawer.closeDrawer(GravityCompat.START);
                    }else{
                        if (doubleBackToExitPressedOnce) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }

                        doubleBackToExitPressedOnce = true;
                        Toast.makeText(getActivity(), getText(R.string.back_exit), Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                doubleBackToExitPressedOnce = false;
                            }
                        }, 2000);
                    }

                    return true;
                }
                return false;
            }
        });
    }

}
