package com.guiddoocn.holidays.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityAllPackagesList;
import com.guiddoocn.holidays.adapter.AdapterCountry;
import com.guiddoocn.holidays.adapter.AdapterQueryList;
import com.guiddoocn.holidays.models.ModelAllHotelList;
import com.guiddoocn.holidays.models.ModelDestinationLists;
import com.guiddoocn.holidays.models.ModelQueryList;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentBookingSmart extends Fragment {
    private static final String TAG = "FragmentBookingSmart";
    private boolean doubleBackToExitPressedOnce = false;

    @BindView(R.id.sp_Destination)
    Spinner sp_Destination;
    @BindView(R.id.sp_no_Night)
    Spinner sp_no_Night;
    @BindView(R.id.sp_month_Travel)
    Spinner sp_month_Travel;
    ////
    @BindView(R.id.ll_adult)
    LinearLayout ll_adult;
    @BindView(R.id.tv_max_Adult)
    TextView tv_max_Adult;
    @BindView(R.id.tv_min_Adult)
    TextView tv_min_Adult;
    @BindView(R.id.tv_value_Adult)
    TextView tv_value_Adult;

    ////
    @BindView(R.id.ll_child)
    LinearLayout ll_child;
    @BindView(R.id.tv_max_Child)
    TextView tv_max_Child;
    @BindView(R.id.tv_min_Child)
    TextView tv_min_Child;
    @BindView(R.id.tv_value_Child)
    TextView tv_value_Child;

    @BindView(R.id.bt_search)
    Button bt_search;

    @BindView(R.id.rv_bookingSmart)
    RecyclerView rv_bookingSmart;

    List<Integer> noNight = new ArrayList<Integer>();
    List<String> monthTravel = new ArrayList<String>();
    private Context mContext;
    private CustomResponseDialog dialog;
    private View v;
    ArrayList<ModelDestinationLists> modelDestinationLists = new ArrayList<>();
    ArrayList<ModelAllHotelList> modelAllHotelLists = new ArrayList<>();
    ArrayList<ModelQueryList> modelQueryLists = new ArrayList<>();
    private SQLiteHandler db;
    private SessionManager sessionManager;
    private int monthID=1,number_of_adults=1,number_of_child=0,NoOfNights=1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_smart, container, false);
        ButterKnife.bind(this, view);
        mContext = getActivity();
        db = new SQLiteHandler(mContext);
        sessionManager = new SessionManager(mContext);
        dialog = new CustomResponseDialog(mContext);
        v = view.findViewById(R.id.view);
        modelDestinationLists = db.getCountryResponce("SM");

        getQueryList();

        AdapterCountry adapterstate = new AdapterCountry(getActivity(), R.layout.spiner_country_list, R.id.title, modelDestinationLists);
        sp_Destination.setAdapter(adapterstate);

        sp_Destination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                Log.e(TAG, "Selection Item :" + modelDestinationLists.get(pos).getCountryID());
                sessionManager.setCountryCode(modelDestinationLists.get(pos).getCountryID());
                sessionManager.setCountryName(modelDestinationLists.get(pos).getCountryName());
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.e(TAG, "Positon :" + adapterView.getSelectedItemPosition() + "" + adapterView.getSelectedItem());
            }
        });

        //   Night Data for spinner

        noNight.add(3);
        noNight.add(4);
        noNight.add(5);
        noNight.add(6);
        noNight.add(7);
        noNight.add(8);
        //   Night adapter for spinner
        ArrayAdapter<Integer> nighgtAdapter = new ArrayAdapter<Integer>(getActivity(), android.R.layout.simple_spinner_item, noNight);
        sp_no_Night.setAdapter(nighgtAdapter);

        sp_no_Night.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                NoOfNights = position + 3;
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        monthTravel.add(" January");
        monthTravel.add(" February");
        monthTravel.add(" March");
        monthTravel.add(" April");
        monthTravel.add(" May");
        monthTravel.add(" June");
        monthTravel.add(" July");
        monthTravel.add(" August");
        monthTravel.add(" September");
        monthTravel.add(" October");
        monthTravel.add(" November");
        monthTravel.add(" December");


        //   Night adapter for spinner
        ArrayAdapter<String> monthTravelAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, monthTravel);
        sp_month_Travel.setAdapter(monthTravelAdapter);

        sp_month_Travel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthID = position+1;
                Log.e(TAG, "month id --->" + String.valueOf(monthID));
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });



        //setting a value for Adult And Child
        try {

            tv_max_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Adult.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e(TAG, "data =" + max);
                    if (max <= 10) {
                        tv_value_Adult.setText("" + max);
                        number_of_adults=max;
                        Log.e(TAG, "number_of_adults --->" + max);

                    } else {
                        tv_value_Adult.setText("10");
                        number_of_adults=10;
                        Log.e(TAG, "number_of_adults --->" + max);
                        ll_adult.startAnimation(shakeError());
                    }

                }
            });
            tv_min_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Adult.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 0) {
                        tv_value_Adult.setText("" + min);
                        number_of_adults=min;
                        Log.e(TAG, "number_of_adults --->" + min);
                    } else {
                        tv_value_Adult.setText("0");
                        number_of_adults=min;
                        Log.e(TAG, "number_of_adults --->" + min);
                        ll_adult.startAnimation(shakeError());
                    }

                }
            });

            //setting a value for Adult And Child
            Log.e(TAG, "data =");
            tv_max_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Child.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e(TAG, "data =" + max);
                    if (max <= 10) {
                        tv_value_Child.setText("" + max);
                        number_of_child=max;
                        Log.e(TAG, "number_of_child --->" + max);

                    } else {
                        tv_value_Child.setText("10");
                        number_of_child=10;
                        Log.e(TAG, "number_of_child --->" + max);
                        ll_child.startAnimation(shakeError());
                    }

                }
            });
            tv_min_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Child.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 0) {
                        tv_value_Child.setText("" + min);
                        number_of_child=min;
                        Log.e(TAG, "number_of_child --->" + min);
                    } else {
                        tv_value_Child.setText("0");
                        number_of_child=min;
                        Log.e(TAG, "number_of_child --->" + min);
                        ll_child.startAnimation(shakeError());

                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("erro", "code=" + e.getMessage());
        }
        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent= new Intent(getActivity(), ActivityAllPackagesList.class);
                intent.putExtra("Total_Days",NoOfNights);
                startActivity(intent);*/
                get_Package_List();
            }
        });

        return view;
    }

    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }


    private void getQueryList() {
        dialog.showCustomDialog();
        RequestQueue queue = Volley.newRequestQueue(mContext);
        //products/hotels?country_id={COUNTRY_ID}&api_key={API_KEY}
        final String url = sessionManager.getBaseUrlGuiddoo() + "getsmartqueries/0D1067102F935B4CC31E082BD45014D469E35268";
        Log.e("QueryList url-->", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e("QueryList Response-->", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONObject objectData = jsonObject.getJSONObject("data");
                            JSONObject objectStatus = objectData.getJSONObject("status");
                            if(objectStatus.getString("status_code").equalsIgnoreCase("200")){
                                JSONArray jsonArray = objectData.getJSONArray("smart_queries");
                                for(int i=0;jsonArray.length()>i;i++){
                                    ModelQueryList queryList = new ModelQueryList();
                                    JSONObject objectlist = jsonArray.getJSONObject(i);
                                    queryList.setDestination(objectlist.getString("destination"));
                                    queryList.setNumber_of_pax(objectlist.getString("number_of_pax"));
                                    queryList.setTravel_agent(objectlist.getString("travel_agent"));
                                    queryList.setTravel_date(objectlist.getString("travel_date"));
                                    modelQueryLists.add(queryList);

                                }

                                if(modelQueryLists.size()>0){
                                    GridLayoutManager manager1 = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
                                    FragmentManager fragmentManager1 = getActivity().getSupportFragmentManager();
                                    AdapterQueryList adapterQuery = new AdapterQueryList( getActivity(),modelQueryLists,getActivity(), fragmentManager1);
                                    rv_bookingSmart.setLayoutManager(manager1);
                                    rv_bookingSmart.setAdapter(adapterQuery);
                                }else {
                                    Snackbar.make(v, "query not found.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                }
                            }else {
                                Snackbar.make(v, "query not found.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }


                        } catch (JSONException e) {
                            Snackbar.make(v, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );

        queue.add(getRequest);
    }


    private void get_Package_List() {
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "getListOfPackages";
        Log.e(TAG, "ListOfPackages URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("api_key", "0D1067102F935B4CC31E082BD45014D469E35268");
            object.put("country_id",sessionManager.getCountryCode());
            object.put("is_fam", 0);
            object.put("month", monthID);//pass month id 1 to 12
            object.put("number_of_adults", number_of_adults);
            object.put("number_of_child", number_of_child);
            object.put("number_of_nights", NoOfNights);
            object.put("package_type", 2);//for standard 1, for smart 2, for unique 3
            object.put("traveler_type", 0);

            Log.e("Parameter pass-->",object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "ListOfPackages Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            JSONObject jsonStatus = jsonObject.getJSONObject("status");
                            if (jsonStatus.getString("status_code").equalsIgnoreCase("200")) {
                                modelAllHotelLists = new ArrayList<>();
                                JSONArray jsonArray = jsonObject.getJSONArray("package");
                                for(int i = 0;jsonArray.length()>i;i++){
                                    JSONObject json2 = jsonArray.getJSONObject(i);
                                    ModelAllHotelList hotelList = new ModelAllHotelList();
                                    hotelList.setID(json2.getString("package_id"));
                                    hotelList.setTitle(json2.getString("package_name"));
                                    hotelList.setCityCountryName(json2.getString("cities"));
                                    hotelList.setLocation("");
                                    hotelList.setAmount(json2.getString("starting_from_price"));
                                    hotelList.setSuccess_rate(json2.getString("success_rate").replace("null",""));
                                    hotelList.setQueries_received(json2.getString("queries_received").replace("null",""));
                                    hotelList.setCurrency(json2.getString("currency"));
                                    hotelList.setImage(json2.getString("thumbnail_image"));
                                    hotelList.setRating(json2.getString("rating").replace("","0"));


                                    JSONArray jsonArray1 = json2.getJSONArray("pack_theme");

                                    ArrayList<String> stringArray = new ArrayList<String>();

                                    for(int j = 0, count = jsonArray1.length(); j< count; j++)
                                    {
                                        try {
                                            JSONObject jsonObjects = jsonArray1.getJSONObject(j);
                                            stringArray.add(jsonObjects.getString("theme"));
                                        }
                                        catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    String s = TextUtils.join(", ", stringArray);

                                    hotelList.setAddress(s);

                                    modelAllHotelLists.add(hotelList);
                                }

                                if(modelAllHotelLists.size()>0){
                                    Intent intent= new Intent(getActivity(), ActivityAllPackagesList.class);
                                    intent.putExtra("Total_Days",NoOfNights);
                                    intent.putExtra("mylist", modelAllHotelLists);
                                    startActivity(intent);
                                }else{
                                    Toast.makeText(mContext, "package not found", Toast.LENGTH_LONG).show();
                                }


                            }else{
                                Toast.makeText(mContext, "package not found", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }


  /*  @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    getFragmentManager().popBackStack();
                    return true;
                }
                return false;
            }
        });
    }*/

}
