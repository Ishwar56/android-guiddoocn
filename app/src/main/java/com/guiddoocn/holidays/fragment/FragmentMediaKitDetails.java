package com.guiddoocn.holidays.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentMediaKitDetails extends Fragment {
    private boolean doubleBackToExitPressedOnce = false;
    private Context mContext;

    @BindView(R.id.webView)
    WebView webView;

    private String url ;
    private CustomResponseDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_media_kit_details, container, false);
        mContext = getActivity();
        ButterKnife.bind(this, view);
        dialog = new CustomResponseDialog(mContext);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        url = getArguments().getString("URL");

        try{
            webView.getSettings().setJavaScriptEnabled(true);
            WebSettings settings = webView.getSettings();
            settings.setJavaScriptEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);
            //webView.getSettings().setDatabaseEnabled(true);
        }catch (Exception e){

        }

        dialog.showCustomDialog();




        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                dialog.hideCustomeDialog();
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }
        });
        webView.loadUrl(url);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    getFragmentManager().popBackStack();

                    return true;
                }
                return false;
            }
        });
    }


}
