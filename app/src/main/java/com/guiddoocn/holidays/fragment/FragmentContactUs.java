package com.guiddoocn.holidays.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelContactUs;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentContactUs extends Fragment {
    private boolean doubleBackToExitPressedOnce = false;

    @BindView(R.id.activity_dashboard_cv_map)
    CardView activity_dashboard_cv_map;
    private static final String TAG = "FragmentContactUs";
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvContact)
    TextView tvContact;
    @BindView(R.id.tvEmail)
    TextView tvEmail;

    @BindView(R.id.etName)
    FormEditText etName;

    @BindView(R.id.etEmail)
    FormEditText etEmail;

    @BindView(R.id.etSubject)
    FormEditText etSubject;

    @BindView(R.id.etMessage)
    FormEditText etMessage;

    @BindView(R.id.btSend)
    Button btSend;


    private GoogleMap mMap1;
    public Marker myMarker;
    Activity context;
    private Context mContext;
    private CustomResponseDialog dialog;
    private SessionManager sessionManager;
    ArrayList<ModelContactUs> locationList = new ArrayList<>();

    ModelContactUs modelContactUs;

    public FragmentContactUs() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, view);
        context = getActivity();
        mContext=getContext();
        dialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);

        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allValid = true;
                FormEditText[] fields = {etName,etEmail,etSubject,etMessage};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }
                if (allValid) {
                    ContactUs();

                }
            }
        });

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                mMap.clear(); //clear old markers
                Log.e(TAG, "msg1");
                mMap1 = mMap;

                CameraPosition googlePlex = CameraPosition.builder().target(new LatLng(25.8476405, 85.3669578)).zoom(1).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex));

                locationList.add(new ModelContactUs("China Operations @ Shanghai,China", "Yuyuan East Road, Number 28, Donghai Plaza", "+86 13004166624", "support@guiddoocn.com", "31.0892569", "120.6085271"));
                locationList.add(new ModelContactUs("India Operations @ Mumbai, India", "721-722, Ijmima Complex, Malad (W), Mumbai, India 400064", "+91 8880615555", "support@guiddoo.com", "19.1822397", "72.8346754"));
                locationList.add(new ModelContactUs("Dubai Operations @ Dubai", "Ground Floor, Building: 07- Dubai Outsource City, Dubai, UAE", "+971-526322444", "support@guiddoo.com", "25.1238639", "55.4232367"));
                locationList.add(new ModelContactUs("Headquarter @ Singapore", "101, Cecil Street, #1813, Tong Eng Building. Singapore 069533", "+65-91955244", "support@guiddoo.com", "1.2810588", "103.8472624"));

                for (int i = 0; i <= 3; i++) {
                    modelContactUs = locationList.get(i);

                    LatLng sydney1 = new LatLng(Double.valueOf(modelContactUs.getLatitude()), Double.valueOf(modelContactUs.getLongitude()));
                    MarkerOptions markerOptions = new MarkerOptions().position(sydney1);
                    markerOptions.title(modelContactUs.getTitle());

                    markerOptions.icon(bitmapDescriptorFromVector(mContext, R.drawable.map_pin));
                    mMap1.addMarker(markerOptions);

                   /* myMarker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(modelContactUs.getLatitude()), Double.parseDouble(modelContactUs.getLongitude())))
                            .title(modelContactUs.getTitle()));*/

                }

                mMap1.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        Log.e(TAG,"Click --index--" + marker.getPosition()+ "--Title--" + marker.getTitle());

                        for(int j=0;locationList.size()>j;j++)
                        {
                            if(marker.getTitle().equalsIgnoreCase(locationList.get(j).getTitle())){
                                tvTitle.setText(locationList.get(j).getTitle());
                                tvAddress.setText(locationList.get(j).getAddress());
                                tvContact.setText(locationList.get(j).getContact());
                                tvEmail.setText(locationList.get(j).getEmail());
                            }
                        }

                        return true;
                    }
                });



            }
        });


        return view;


    }

    public GoogleMap getmMap1() {
        return mMap1;
    }

    public void setmMap1(GoogleMap mMap1) {
        this.mMap1 = mMap1;
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {

        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);

        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if (SessionManager.drawer.isDrawerOpen(GravityCompat.START)) {
                        SessionManager.drawer.closeDrawer(GravityCompat.START);
                    } else {
                        if (doubleBackToExitPressedOnce) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }

                        doubleBackToExitPressedOnce = true;
                        Toast.makeText(getActivity(), getText(R.string.back_exit), Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                doubleBackToExitPressedOnce = false;
                            }
                        }, 2000);
                    }

                    return true;
                }
                return false;
            }
        });
    }

    private void ContactUs() {
        dialog=new CustomResponseDialog(mContext);
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "guiddoocn/contactus";
        Log.e(TAG, "login URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("MessageBody", etMessage.getText().toString());
            object.put("SenderEamilId",etEmail.getText().toString());
            object.put("SenderName",etName.getText().toString());
            object.put("Subject",etSubject.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "login Parameters---->" + object.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "login Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            //JSONObject jsonStatus = jsonObject.getJSONObject("status");
                            if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                                Toast.makeText(mContext, jsonObject.getString("error_message"), Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(mContext, jsonObject.getString("error_message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }
}
