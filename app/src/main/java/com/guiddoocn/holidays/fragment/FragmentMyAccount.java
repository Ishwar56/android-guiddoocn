package com.guiddoocn.holidays.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityLogin;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentMyAccount extends Fragment {
    @BindView(R.id.ll_My_account)
    LinearLayout ll_My_account;

    @BindView(R.id.tv_Logout)
    Button tv_Logout;

    @BindView(R.id.tv_com_Name)
    TextView tv_com_Name;

    @BindView(R.id.tv_user_Name)
    TextView tv_user_Name;

    @BindView(R.id.tv_user_Mobile)
    TextView tv_user_Mobile;

    @BindView(R.id.tv_user_Email)
    TextView tv_user_Email;

    private boolean doubleBackToExitPressedOnce = false;
    private Context mContext;
    private SessionManager sessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        mContext=getActivity();
        ButterKnife.bind(this,view);
        sessionManager = new SessionManager(mContext);


        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        tv_com_Name.setText(sessionManager.getUserName());
        tv_user_Name.setText(sessionManager.getContactPerson());
        tv_user_Mobile.setText(sessionManager.getUserMobileNo());
        tv_user_Email.setText(sessionManager.getEmailId());

        ll_My_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(R.id.fragment_container, new FragmentEditAccountDetails(), true);
            }
        });

        tv_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setLogedIn(false);
                Intent intent = new Intent(mContext, ActivityLogin.class);
                mContext.startActivity(intent);
                getActivity().finish();

            }
        });

        return view;


    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if(SessionManager.drawer.isDrawerOpen(GravityCompat.START)) {
                        SessionManager.drawer.closeDrawer(GravityCompat.START);
                    }else{
                        if (doubleBackToExitPressedOnce) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }

                        doubleBackToExitPressedOnce = true;
                        Toast.makeText(getActivity(), getText(R.string.back_exit), Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                doubleBackToExitPressedOnce = false;
                            }
                        }, 2000);
                    }

                    return true;
                }
                return false;
            }
        });
    }

}
