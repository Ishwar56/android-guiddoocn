package com.guiddoocn.holidays.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityHome;
import com.guiddoocn.holidays.activity.ActivityLogin;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentEditAccountDetails extends Fragment {
    private static final String TAG = "LoginActivities";
  /*  @BindView(R.id.toolbar)
    Toolbar toolbar;*/

   /* @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView tv_back;*/

    @BindView(R.id.tv_company_Name)
    FormEditText tv_company_Name;

    @BindView(R.id.tv_persone_Name)
    FormEditText tv_persone_Name;

    @BindView(R.id.tv_signUp_Email)
    FormEditText tv_signUp_Email;

    @BindView(R.id.tv_SignUp_Mob)
    FormEditText tv_SignUp_Mob;

    @BindView(R.id.tv_Upadte)
    Button tv_Upadte;

    @BindView(R.id.tv_pass_Change)
    Button tv_pass_Change;


    private Context mContext;
    private String responce,resultObj,message,Query_ID;
    private CustomResponseDialog dialog;
    private boolean doubleBackToExitPressedOnce = false;
    private SessionManager sessionManager;
    private GridLayoutManager manager;
    private FragmentManager fragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_details, container, false);
        mContext=getActivity();
        ButterKnife.bind(this,view);
        dialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        //((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        //tv_title.setText(getText(R.string.account_profile));

        ((ActivityHome)mContext).setToolberTitle((String) getText(R.string.account_profile));

        tv_company_Name.setText(sessionManager.getUserName());
        tv_persone_Name.setText(sessionManager.getContactPerson());
        tv_signUp_Email.setText(sessionManager.getEmailId());
        tv_SignUp_Mob.setText(sessionManager.getUserMobileNo());

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }



        tv_Upadte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean allValid = true;
                FormEditText[] fields = {tv_company_Name,tv_persone_Name,tv_signUp_Email,tv_SignUp_Mob};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }
                if (allValid) {
                    updateProfile();

                }

            }
        });

        tv_pass_Change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               UpdatePass();
            }
        });


        return view;
    }


    private void UpdatePass(){
        final Dialog dialogMsg = new Dialog(mContext);
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.dialog_update_pass);
        dialogMsg.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMsg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMsg.getWindow().setAttributes(lp);
        dialogMsg.show();

        TextView btn_cancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
        TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_done);
        final FormEditText tv_old_Pass = (FormEditText) dialogMsg.findViewById(R.id.tv_old_Pass);
        final FormEditText tv_new_Pass = (FormEditText) dialogMsg.findViewById(R.id.tv_new_Pass);
        final FormEditText tv_conf_Pass = (FormEditText) dialogMsg.findViewById(R.id.tv_conf_Pass);


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean allValids = true;
                FormEditText[] fieldss = {tv_old_Pass,tv_new_Pass,tv_conf_Pass};
                for (FormEditText field : fieldss) {
                    if (field.testValidity() && allValids) {
                        allValids = field.testValidity() && allValids;
                    } else {
                        field.requestFocus();
                        allValids = false;
                        break;
                    }
                }
                if (allValids) {
                    if(tv_new_Pass.getText().toString().equalsIgnoreCase(tv_conf_Pass.getText().toString())){
                        dialogMsg.cancel();
                        updatePassword(tv_old_Pass.getText().toString(),tv_conf_Pass.getText().toString());
                    }else{
                        Toast.makeText(mContext, "new password and confirm password must be same", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
            }
        });

    }


    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){

        }
    }



    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }


    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((ActivityHome)mContext).setToolberTitle((String) getText(R.string.nav_my_account));
                    getFragmentManager().popBackStack();
                    return true;
                }
                return false;
            }
        });
    }


    private void updateProfile() {
        dialog=new CustomResponseDialog(mContext);
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "guiddoocn/updateprofile";
        Log.e(TAG, "login URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("TravelAgencyName", tv_company_Name.getText().toString());
            object.put("UserName",tv_persone_Name.getText().toString());
            object.put("AgentId",sessionManager.getUserId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "login Parameters---->" + object.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "login Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            //JSONObject jsonStatus = jsonObject.getJSONObject("status");
                            if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                                sessionManager.setLogedIn(true);
                               /* sessionManager.setEmailId(jsonObject.getString("email"));
                                sessionManager.setUserMobile(jsonObject.getString("mobile"));
                                sessionManager.setUserID(jsonObject.getString("TravelAgent_Id"));*/
                                sessionManager.setUserName(tv_company_Name.getText().toString());
                                sessionManager.setContactPerson(tv_persone_Name.getText().toString());

                                Toast.makeText(mContext, jsonObject.getString("error_message"), Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(mContext, jsonObject.getString("error_message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }

    private void updatePassword(String oldPass,String newPass) {
        dialog=new CustomResponseDialog(mContext);
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "guiddoocn/changepassword";
        Log.e(TAG, "login URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("new_password", newPass);
            object.put("old_password",oldPass);
            object.put("UserId",sessionManager.getUserId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "login Parameters---->" + object.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "login Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            //JSONObject jsonStatus = jsonObject.getJSONObject("status");
                            if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                                sessionManager.setLogedIn(true);
                               /* sessionManager.setEmailId(jsonObject.getString("email"));
                                sessionManager.setUserMobile(jsonObject.getString("mobile"));
                                sessionManager.setUserID(jsonObject.getString("TravelAgent_Id"));*/
                                sessionManager.setUserName(tv_company_Name.getText().toString());
                                sessionManager.setContactPerson(tv_persone_Name.getText().toString());

                                Toast.makeText(mContext, jsonObject.getString("error_message"), Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(mContext, jsonObject.getString("error_message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }


}
