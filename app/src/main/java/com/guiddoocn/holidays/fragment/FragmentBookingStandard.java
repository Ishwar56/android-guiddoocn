package com.guiddoocn.holidays.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityAllPackagesList;
import com.guiddoocn.holidays.adapter.AdapterCommenList;
import com.guiddoocn.holidays.adapter.AdapterCountry;
import com.guiddoocn.holidays.adapter.AdapterStandardCommenList;
import com.guiddoocn.holidays.models.ModelAllHotelList;
import com.guiddoocn.holidays.models.ModelDestinationLists;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentBookingStandard extends Fragment {
    private static final String TAG = "FragmentBookingStandard";
    private boolean doubleBackToExitPressedOnce = false;
    @BindView(R.id.sp_Destination)
    Spinner sp_Destination;
    @BindView(R.id.sp_no_Night)
    Spinner sp_no_Night;
    @BindView(R.id.sp_month_Travel)
    Spinner sp_month_Travel;
    @BindView(R.id.bt_search)
    Button bt_search;

    private SQLiteHandler db;
    private SessionManager sessionManager;
    ArrayList<ModelDestinationLists> modelDestinationLists = new ArrayList<>();
    ArrayList<ModelAllHotelList> modelAllHotelLists = new ArrayList<>();
    List<String> noNight = new ArrayList<String>();
    List<String> monthTravel = new ArrayList<String>();
    Context mContext;
    private int number_of_adults=1,number_of_child=0;
    private CustomResponseDialog dialog;
    private String monthID="0",NoOfNights="0";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_standard, container, false);
        ButterKnife.bind(this, view);
         mContext=getActivity();
        db = new SQLiteHandler(mContext);
        sessionManager = new SessionManager(mContext);
        dialog = new CustomResponseDialog(mContext);
        ModelDestinationLists destinationLists = new ModelDestinationLists();
        destinationLists.setCountryID("0");
        destinationLists.setCountryName("All Destination");
        //modelDestinationLists.add(destinationLists);

        modelDestinationLists = db.getCountryResponce("S");
        modelDestinationLists.add(0,destinationLists);
        AdapterCountry adapterstate = new AdapterCountry(getActivity(), R.layout.spiner_country_list, R.id.title, modelDestinationLists);
        sp_Destination.setAdapter(adapterstate);
        sp_Destination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                Log.e(TAG, "Selection Item :" + modelDestinationLists.get(pos).getCountryID());
                sessionManager.setCountryCode(modelDestinationLists.get(pos).getCountryID());
                sessionManager.setCountryName(modelDestinationLists.get(pos).getCountryName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.e(TAG, "Positon :" + adapterView.getSelectedItemPosition() + "" + adapterView.getSelectedItem());
            }
        });

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }



        //   Night Data for spinner
        noNight.add("Any number of night");
        noNight.add("1-3 Nights");
        noNight.add("4-6 Nights");
        noNight.add("7+ Nights");

        //   Night adapter for spinner
        AdapterStandardCommenList nighgtAdapter = new AdapterStandardCommenList(getActivity(), R.layout.spiner_country_list, R.id.title, noNight);
        sp_no_Night.setAdapter(nighgtAdapter);

        sp_no_Night.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    NoOfNights = "0";
                }else{
                    //NoOfNights= noNight.get(position).toString().replaceAll("[^A-Za-z]","");;
                    if(position==0){
                        NoOfNights = "0";
                    }else if(position==1){
                        NoOfNights= "1-3";
                    }else if(position==2){
                        NoOfNights = "4-6";
                    }else if(position==3){
                        NoOfNights = "7+";
                    }

                }
                Log.e("Selected no of night", String.valueOf(NoOfNights));
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        monthTravel.add(" Any Month");
        monthTravel.add(" Jan-Mar");
        monthTravel.add(" Apr-Jun");
        monthTravel.add(" Jul-Sep");
        monthTravel.add(" Oct-Dec");


       /* monthTravel.add(" January");
        monthTravel.add(" February");
        monthTravel.add(" March");
        monthTravel.add(" April");
        monthTravel.add(" May");
        monthTravel.add(" June");
        monthTravel.add(" July");
        monthTravel.add(" August");
        monthTravel.add(" September");
        monthTravel.add(" October");
        monthTravel.add(" November");
        monthTravel.add(" December");*/


        //   Night adapter for spinner
        AdapterStandardCommenList monthTravelAdapter = new AdapterStandardCommenList(getActivity(), R.layout.spiner_country_list, R.id.title, monthTravel);
        sp_month_Travel.setAdapter(monthTravelAdapter);

        sp_month_Travel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position==0){
                    monthID = "0";
                }else if(position==1){
                    monthID= "1-3";
                }else if(position==2){
                    monthID = "4-6";
                }else if(position==3){
                    monthID = "7-9";
                }else if(position==4){
                    monthID = "10-12";
                }
                Log.e("Selected month id", String.valueOf(monthID));
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                get_Package_List();
            }
        });
        return view;

    }


    private void get_Package_List() {
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "getListOfPackages";
        Log.e(TAG, "ListOfPackages URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("api_key", "0D1067102F935B4CC31E082BD45014D469E35268");
            object.put("country_id",sessionManager.getCountryCode());
            object.put("is_fam", 0);
            object.put("month", monthID);//pass month id 1 to 12
            object.put("number_of_adults", number_of_adults);
            object.put("number_of_child", number_of_child);
            object.put("number_of_nights", NoOfNights);
            object.put("package_type", 1);//for standard 1, for smart 2, for unique 3
            object.put("traveler_type", 0);

            Log.e("Parameter pass-->",object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "ListOfPackages Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            JSONObject jsonStatus = jsonObject.getJSONObject("status");
                            if (jsonStatus.getString("status_code").equalsIgnoreCase("200")) {
                                modelAllHotelLists = new ArrayList<>();
                                JSONArray jsonArray = jsonObject.getJSONArray("package");
                                for(int i = 0;jsonArray.length()>i;i++){
                                    JSONObject json2 = jsonArray.getJSONObject(i);
                                    ModelAllHotelList hotelList = new ModelAllHotelList();
                                    hotelList.setID(json2.getString("package_id"));
                                    hotelList.setTitle(json2.getString("package_name"));
                                    hotelList.setCityCountryName(json2.getString("cities"));
                                    hotelList.setLocation("");
                                    hotelList.setAmount(json2.getString("starting_from_price"));
                                    hotelList.setSuccess_rate(json2.getString("success_rate").replace("null",""));
                                    hotelList.setQueries_received(json2.getString("queries_received").replace("null",""));
                                    hotelList.setCurrency(json2.getString("currency"));
                                    hotelList.setImage(json2.getString("thumbnail_image"));
                                    hotelList.setRating(json2.getString("rating").replace("","0"));


                                    JSONArray jsonArray1 = json2.getJSONArray("pack_theme");

                                    ArrayList<String> stringArray = new ArrayList<String>();

                                    for(int j = 0, count = jsonArray1.length(); j< count; j++)
                                    {
                                        try {
                                            JSONObject jsonObjects = jsonArray1.getJSONObject(j);
                                            stringArray.add(jsonObjects.getString("theme"));
                                        }
                                        catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    String s = TextUtils.join(", ", stringArray);

                                    hotelList.setAddress(s);

                                    modelAllHotelLists.add(hotelList);
                                }

                                if(modelAllHotelLists.size()>0){
                                    Intent intent= new Intent(getActivity(), ActivityAllPackagesList.class);
                                    intent.putExtra("Total_Days",NoOfNights);
                                    intent.putExtra("mylist", modelAllHotelLists);
                                    startActivity(intent);
                                }else{
                                    Toast.makeText(mContext, "package not found", Toast.LENGTH_LONG).show();
                                }


                            }else{
                                Toast.makeText(mContext, "package not found", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }

/*
    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    getFragmentManager().popBackStack();
                    return true;
                }
                return false;
            }
        });
    }*/

}
