package com.guiddoocn.holidays.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityAllListPromo;
import com.guiddoocn.holidays.activity.ActivityDestination;
import com.guiddoocn.holidays.adapter.AdapterPromoExperiencesMiniList;
import com.guiddoocn.holidays.adapter.AdapterTopDestinationMiniList;
import com.guiddoocn.holidays.models.ModelDestinationLists;
import com.guiddoocn.holidays.models.ModelPromoExperiencesList;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentHome extends Fragment {
    private static final String TAG = "FragmentHome";
    private boolean doubleBackToExitPressedOnce = false;

    @BindView(R.id.rv_top_Destination)
    RecyclerView rv_top_Destination;
    @BindView(R.id.tv_destination_view_all)
    TextView tv_destination_view_all;

    @BindView(R.id.rv_promotional)
    RecyclerView rv_promotional;

    @BindView(R.id.tv_promotional_view_all)
    TextView tv_promotional_view_all;


    private Context mContext;
    private SQLiteHandler db;
    private SessionManager sessionManager;
    private CustomResponseDialog dialog;
    private AdapterTopDestinationMiniList adapterTopDestinationList;

    private AdapterPromoExperiencesMiniList adapterPromoExperiencesMiniList;

    private ArrayList<ModelDestinationLists> topDestinationLists = new ArrayList<>();
    private ArrayList<ModelPromoExperiencesList> modelPromoExperiencesLists = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = getActivity();
        ButterKnife.bind(this, view);
        db = new SQLiteHandler(mContext);
        sessionManager = new SessionManager(mContext);
        dialog = new CustomResponseDialog(mContext);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }

        sessionManager.setFilteractive(false);
        try{
            modelPromoExperiencesLists = db.getPromoData();
            if(modelPromoExperiencesLists.size()>0){
                setPromoAdapter();
            }else{
                get_promotional_activities();
            }
        }catch (Exception e){
            get_promotional_activities();
        }

        try {

            topDestinationLists = db.getCityResponce();
            if(topDestinationLists.size()>0){
                GridLayoutManager manager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                adapterTopDestinationList = new AdapterTopDestinationMiniList(mContext, topDestinationLists, getActivity(), fragmentManager);
                rv_top_Destination.setLayoutManager(manager);
                rv_top_Destination.setAdapter(adapterTopDestinationList);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "" + e.getMessage());
        }

        tv_promotional_view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityAllListPromo.class);
                mContext.startActivity(intent);
            }
        });
        tv_destination_view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityDestination.class);
                mContext.startActivity(intent);
            }
        });

        return view;


    }


    private void get_promotional_activities() {
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrl() + "products/activities/special_offers?api_key=0D1067102F935B4CC31E082BD45014D469E35268";
        Log.e(TAG, "promotional_activities URL---->" + url);


        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.hideCustomeDialog();
                Log.e(TAG, "promotional_activities Response---->" + response);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonStatus = jsonObject.getJSONObject("status");
                    if(jsonStatus.getString("status_code").equalsIgnoreCase("200")){

                        JSONArray jsonArray = jsonObject.getJSONArray("listing_data");
                        for(int i=0;jsonArray.length()>i;i++){
                            JSONObject jsonList = jsonArray.getJSONObject(i);
                            ModelPromoExperiencesList promoList = new ModelPromoExperiencesList();
                            promoList.setTour_id(jsonList.getString("tour_id"));
                            promoList.setCity(jsonList.getString("city"));
                            promoList.setName(jsonList.getString("name"));
                            promoList.setCategory_id(jsonList.getString("category_id"));
                            promoList.setCategory_name(jsonList.getString("category_name"));
                            promoList.setRating(jsonList.getString("rating"));
                            promoList.setCurrency(jsonList.getString("currency"));
                            //promoList.setAmount(jsonList.getString("starting_from_price"));//getIsMarginPrice
                            promoList.setAmount(getIsMarginPrice(jsonList.getString("starting_from_price")));//getIsMarginPrice
                            promoList.setDiscount(jsonList.getString("discount"));
                            promoList.setDiscount_cap(jsonList.getString("discount_cap"));
                            promoList.setImage(jsonList.getString("featured_image"));

                            JSONObject jsonDuration = jsonList.getJSONObject("duration");
                            promoList.setDay(jsonDuration.getString("day"));
                            promoList.setHour(jsonDuration.getString("hour"));
                            promoList.setMin(jsonDuration.getString("min"));

                            modelPromoExperiencesLists.add(promoList);

                        }



                        if(modelPromoExperiencesLists.size()>0){
                            setPromoAdapter();
                        }

                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                //TODO your background code
                                if(modelPromoExperiencesLists.size()>0){
                                    for(int i= 0; modelPromoExperiencesLists.size()>i;i++){
                                        ModelPromoExperiencesList promo = modelPromoExperiencesLists.get(i);
                                        db.setPromoData(promo.getTour_id(),promo.getCity(),promo.getName(),promo.getCategory_id(),promo.getCategory_name(),promo.getDay(),promo.getHour(),promo.getMin(),promo.getRating(),promo.getImage(),promo.getCurrency(),promo.getAmount(),promo.getDiscount(),promo.getDiscount_cap());
                                    }

                                }

                            }
                        });




                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.hideCustomeDialog();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                }
            }
        });
        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }

    private void setPromoAdapter(){
        GridLayoutManager manager1 = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        FragmentManager fragmentManager1 = getActivity().getSupportFragmentManager();//new AppCompatActivity().getSupportFragmentManager();
        adapterPromoExperiencesMiniList = new AdapterPromoExperiencesMiniList(mContext, modelPromoExperiencesLists, getActivity(), fragmentManager1);
        rv_promotional.setLayoutManager(manager1);
        rv_promotional.setAdapter(adapterPromoExperiencesMiniList);
    }

    public String getIsMarginPrice(String Main_price) {
        try{
            String data = null;
            double discount = Double.parseDouble(sessionManager.getIsmargin());
            double Old_price = Double.parseDouble(Main_price);
            double New_price = Old_price - (Old_price * discount / 100);
            data = String.format("%.02f", New_price );
            return data;
        }catch (Exception e){
            return Main_price;
        }


    }


    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if(SessionManager.drawer.isDrawerOpen(GravityCompat.START)) {
                        SessionManager.drawer.closeDrawer(GravityCompat.START);
                    }else{
                        if (doubleBackToExitPressedOnce) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            getActivity().finish();
                        }

                        doubleBackToExitPressedOnce = true;
                        Toast.makeText(getActivity(), getText(R.string.back_exit), Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                doubleBackToExitPressedOnce = false;
                            }
                        }, 2000);
                    }

                    return true;
                }
                return false;
            }
        });
    }


}
