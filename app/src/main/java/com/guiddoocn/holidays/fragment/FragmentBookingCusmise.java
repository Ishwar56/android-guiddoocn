package com.guiddoocn.holidays.fragment;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityAllListHotels;
import com.guiddoocn.holidays.adapter.AdapterCommenChildAgeList;
import com.guiddoocn.holidays.adapter.AdapterCountry;
import com.guiddoocn.holidays.models.ModelDestinationLists;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentBookingCusmise extends Fragment {
    private static final String TAG = "FragmentBookingCusmise";
    private boolean doubleBackToExitPressedOnce = false;
    @BindView(R.id.sp_booking_Custmise_Destination)
    Spinner sp_booking_Custmise_Destination;
    @BindView(R.id.et_booking_Check_In)
    EditText et_booking_Check_In;
    @BindView(R.id.et_booking_Check_Out)
    EditText et_booking_Check_Out;
    @BindView(R.id.ll_booking_Check_In)
    LinearLayout ll_booking_Check_In;
    @BindView(R.id.ll_booking_Check_Out)
    LinearLayout ll_booking_Check_Out;
    @BindView(R.id.et_no_of_nights)
    EditText et_no_of_nights;
    ////
    @BindView(R.id.ll_adult)
    LinearLayout ll_adult;
    @BindView(R.id.tv_max_Adult)
    TextView tv_max_Adult;
    @BindView(R.id.tv_min_Adult)
    TextView tv_min_Adult;
    @BindView(R.id.tv_value_Adult)
    TextView tv_value_Adult;

    @BindView(R.id.tv_child_hint)
    TextView tv_child_hint;

    ////
    @BindView(R.id.ll_child)
    LinearLayout ll_child;
    @BindView(R.id.tv_max_Child)
    TextView tv_max_Child;
    @BindView(R.id.tv_min_Child)
    TextView tv_min_Child;
    @BindView(R.id.tv_value_Child)
    TextView tv_value_Child;
    @BindView(R.id.bt_search)
    Button bt_search;

    @BindView(R.id.ll_linear_Child)
    LinearLayout mEditTextContainer;

    @BindView(R.id.ll_child_count)
    LinearLayout ll_child_count;

    @BindView(R.id.ll_child_count1)
    LinearLayout ll_child_count1;
    @BindView(R.id.sp_child_age1)
    Spinner sp_child_age1;

    @BindView(R.id.ll_child_count2)
    LinearLayout ll_child_count2;
    @BindView(R.id.sp_child_age2)
    Spinner sp_child_age2;

    @BindView(R.id.ll_linear_Child2)
    LinearLayout mEditTextContainer2;
    private Calendar check_in_Calendar, check_out_Calendar;
    private DatePickerDialog.OnDateSetListener Fromdate_Check_In, Fromdate_Check_Out;
    ArrayList<ModelDestinationLists> modelDestinationLists = new ArrayList<>();
    Context mContext;
    private int TotalCount = 2, numberOfLines;
    private SQLiteHandler db;
    private SessionManager sessionManager;
    private CustomResponseDialog dialog;
    ArrayList<String> child_age=new ArrayList<>();
    private String Child_Age1="",Child_Age2="";
    private  List<Integer> noRoomsSpiner;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booking_custmise, container, false);
        ButterKnife.bind(this, view);
        mContext = getActivity();
        check_in_Calendar = Calendar.getInstance();
        check_out_Calendar = Calendar.getInstance();
        db = new SQLiteHandler(mContext);
        sessionManager = new SessionManager(mContext);
        dialog = new CustomResponseDialog(mContext);
        sessionManager.setChildAge("");

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }
        sessionManager.setFilteractive(false);
        noRoomsSpiner = new ArrayList<Integer>();
        for(int i=1;i<13;i++){
            noRoomsSpiner.add(i);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


            }
        }, 500);

        modelDestinationLists = db.getCountryResponce("C");

        if (modelDestinationLists.size() > 0) {
            AdapterCountry adapterstate = new AdapterCountry(getActivity(), R.layout.spiner_country_list, R.id.title, modelDestinationLists);
            sp_booking_Custmise_Destination.setAdapter(adapterstate);

            sp_booking_Custmise_Destination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                    Log.e(TAG, "Selection Item :" + modelDestinationLists.get(pos).getCountryID());
                    sessionManager.setCountryCode(modelDestinationLists.get(pos).getCountryID());
                    sessionManager.setCountryName(modelDestinationLists.get(pos).getCountryName());
                    sessionManager.setCityCode(modelDestinationLists.get(pos).getCountryID());
                    sessionManager.setCityName(modelDestinationLists.get(pos).getCountryID());
                }


                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    Log.e(TAG, "Positon :" + adapterView.getSelectedItemPosition() + "" + adapterView.getSelectedItem());
                }
            });
        }


        //setting a value for Adult And Child
        try {


            Log.e(TAG, "data =");
            tv_max_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Adult.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e(TAG, "data =" + max);
                    if(TotalCount<=13){
                            TotalCount=TotalCount+1;
                            tv_value_Adult.setText("" + max);
                            Log.e(TAG, "" + tv_value_Adult.getText().toString().trim());
                    } else {
                        ll_adult.startAnimation(shakeError());
                    }

                }
            });
            tv_min_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Adult.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 1) {
                        tv_value_Adult.setText("" + min);
                        TotalCount=TotalCount-1;
                    } else {
                        tv_value_Adult.setText("0");
                        ll_adult.startAnimation(shakeError());
                    }

                }
            });

            tv_max_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Child.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e("Maximum Values count", String.valueOf(max));
                    if(TotalCount<=13){
                        if(max<=2){
                            tv_value_Child.setText("" + max);
                            TotalCount=TotalCount+1;
                            try {
                                Log.e(TAG, "" + tv_value_Child.getText().toString().trim());
                                if (max <= 5) {
                                   // Add_Line();
                                    ll_child_count.setVisibility(View.VISIBLE);
                                    if (max == 1) {
                                        ll_child_count1.setVisibility(View.VISIBLE);
                                        ll_child_count2.setVisibility(View.GONE);
                                        AdapterCommenChildAgeList age1 = new AdapterCommenChildAgeList(getActivity(), R.layout.spiner_child_age_list, R.id.title, noRoomsSpiner);
                                        sp_child_age1.setAdapter(age1);
                                    } else if (max == 2) {
                                        ll_child_count1.setVisibility(View.VISIBLE);
                                        ll_child_count2.setVisibility(View.VISIBLE);
                                        AdapterCommenChildAgeList age2 = new AdapterCommenChildAgeList(getActivity(), R.layout.spiner_child_age_list, R.id.title, noRoomsSpiner);
                                        sp_child_age2.setAdapter(age2);
                                    }
                                } else {
                                    //Add_Line2();
                                }
                            }catch (Exception e){}
                        }else {
                            ll_child.startAnimation(shakeError());
                        }

                    }else {
                        ll_child.startAnimation(shakeError());
                    }



                }
            });
            tv_min_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Child.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 0) {
                        TotalCount=TotalCount-1;
                        tv_value_Child.setText("" + min);
                        Log.e("Minimum Values count", String.valueOf(min));
                        try {
                            /*if (min <= 4) {
                                mEditTextContainer.removeViewAt(mEditTextContainer.getChildCount() - 1);
                                child_age.remove(mEditTextContainer.getChildCount() - 1);
                            } else {
                                mEditTextContainer2.removeViewAt(mEditTextContainer2.getChildCount() - 1);
                                child_age.remove(mEditTextContainer2.getChildCount() - 1);
                            }*/
                            if (min == 1) {
                               ll_child_count1.setVisibility(View.VISIBLE);
                                ll_child_count2.setVisibility(View.GONE);
                                Child_Age2="";

                            } else if (min == 2) {
                                ll_child_count1.setVisibility(View.VISIBLE);
                                ll_child_count2.setVisibility(View.VISIBLE);

                            }
                        }catch (Exception e){}

                        if(min==0){
                            tv_child_hint.setVisibility(View.GONE);
                            child_age=new ArrayList<>();
                            sessionManager.setChildAge("");
                            ll_child_count.setVisibility(View.GONE);
                            ll_child_count1.setVisibility(View.GONE);
                            ll_child_count2.setVisibility(View.GONE);
                            Child_Age1="";
                            Child_Age2="";
                        }
                    } else {
                        tv_value_Child.setText("0");
                        ll_child.startAnimation(shakeError());

                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("erro", "code=" + e.getMessage());
        }


        // Display Date Picker  ===>> user clike 2 time


        et_booking_Check_In.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate_Check_In, check_in_Calendar
                        .get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH),
                        check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                mDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                mDate.show();

            }
        });
        Fromdate_Check_In = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                check_in_Calendar.set(Calendar.YEAR, year);
                check_in_Calendar.set(Calendar.MONTH, monthOfYear);
                check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy";
                final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                et_booking_Check_In.setText(sdf.format(check_in_Calendar.getTime()));
                sessionManager.setCheckInDate(sdf.format(check_in_Calendar.getTime()));
                getDateDifferent();


            }

        };

        et_booking_Check_Out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate_Check_Out, check_out_Calendar
                            .get(Calendar.YEAR), check_out_Calendar.get(Calendar.MONTH),
                            check_out_Calendar.get(Calendar.DAY_OF_MONTH));

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date d = null;
                    try {
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(sdf.parse(et_booking_Check_In.getText().toString().trim()));
                        calendar.add(Calendar.DATE, 1);
                        String destDate = sdf.format(calendar.getTime());
                        d = sdf.parse(destDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    mDate.getDatePicker().setMinDate(d.getTime());
                    mDate.show();
                } catch (Exception e) {

                }

            }
        });
        Fromdate_Check_Out = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                check_out_Calendar.set(Calendar.YEAR, year);
                check_out_Calendar.set(Calendar.MONTH, monthOfYear);
                check_out_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy";
                final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                et_booking_Check_Out.setText(sdf.format(check_out_Calendar.getTime()));
                sessionManager.setCheckOutDate(sdf.format(check_out_Calendar.getTime()));
                getDateDifferent();
            }

        };

        try {
            Log.e("CLike", "clcke0");
            bt_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*try{
                        Log.e("Child age", TextUtils.join(",", child_age)) ;
                        sessionManager.setChildAge(TextUtils.join(",", child_age));
                    }catch (Exception e){
                        sessionManager.setChildAge("");
                    }*/
                     if(!Child_Age1.equalsIgnoreCase("") && Child_Age2.equalsIgnoreCase("")){
                         sessionManager.setChildAge(Child_Age1+" yrs");
                     }else if(!Child_Age1.equalsIgnoreCase("") && !Child_Age2.equalsIgnoreCase("")){
                         sessionManager.setChildAge(Child_Age1+" yrs, "+Child_Age2+" yrs");
                     }else if(Child_Age1.equalsIgnoreCase("") && Child_Age2.equalsIgnoreCase("")){
                         sessionManager.setChildAge("");
                     }

                    Log.e("Child age", sessionManager.getChildAge()) ;

                    if (et_booking_Check_In.getText().toString().equalsIgnoreCase("")) {
                        Snackbar.make(et_booking_Check_In, "Please select check in date!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    } else if (et_booking_Check_Out.getText().toString().equalsIgnoreCase("")) {
                        Snackbar.make(et_booking_Check_Out, "Please select check out date!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    } else {

                        String date = et_booking_Check_In.getText().toString();
                        String dateafter = et_booking_Check_Out.getText().toString();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        Date convertedDate = new Date();
                        Date convertedDate2 = new Date();
                        try {
                            convertedDate = dateFormat.parse(date);
                            convertedDate2 = dateFormat.parse(dateafter);
                            if (convertedDate2.after(convertedDate)) {

                                db.deleteActivityData();



                                db.deleteItineraryHotelsData();
                                db.deleteItineraryActivityData();
                                db.deleteAllRoomPriceData();
                                db.deleteAllHotelRoomPricingData();
                                db.deleteHotelsData();   // Delete data base values hotels
                                db.deleteAmenityData();  // Delete data base values Amenity
                                db.deleteRoomsData(); // Delete data base values Rooms
                                db.deleteAllRoomItineraryata();// Delete data base values Rooms Itinerary

                                db.updateAllColumn("True","False");
                                sessionManager.setAdultMainCount(tv_value_Adult.getText().toString().trim());
                                sessionManager.setChildMainCount(tv_value_Child.getText().toString().trim());
                                Intent intent = new Intent(getActivity(), ActivityAllListHotels.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(mContext, "Check-Out date older than Check-In date", Toast.LENGTH_LONG).show();
                            }
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    }
                }

            });
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e("error", "msg" + ex.getMessage());
        }





        AdapterCommenChildAgeList age1 = new AdapterCommenChildAgeList(getActivity(), R.layout.spiner_child_age_list, R.id.title, noRoomsSpiner);
        sp_child_age1.setAdapter(age1);

        sp_child_age1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Child_Age1=noRoomsSpiner.get(position).toString();
                Log.e("Child age1---> ",Child_Age1);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        AdapterCommenChildAgeList age2 = new AdapterCommenChildAgeList(getActivity(), R.layout.spiner_child_age_list, R.id.title, noRoomsSpiner);
        sp_child_age2.setAdapter(age2);

        sp_child_age2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Child_Age2=noRoomsSpiner.get(position).toString();
                Log.e("Child age2--> ",Child_Age2);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });




        return view;

    }

    public void Add_Line() {
        try {
            tv_child_hint.setVisibility(View.VISIBLE);
            Spinner et = new Spinner(mContext);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            p.setMargins(5, 5, 30, 5);
            et.setLayoutParams(p);
            et.setBackgroundResource(R.drawable.custom_bg_navy_dark_box);
            et.setPadding(20, 10, 20, 10);
            et.setGravity(Gravity.CENTER);
            mEditTextContainer.addView(et);


            final List<Integer> noRoomsSpiner = new ArrayList<Integer>();
            for(int i=1;i<13;i++){
                noRoomsSpiner.add(i);
            }

            ArrayAdapter<Integer> nighgtAdapter = new ArrayAdapter<Integer>(mContext, android.R.layout.simple_spinner_item, noRoomsSpiner);
            et.setAdapter(nighgtAdapter);

            et.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if(position==0){
                            child_age.add(noRoomsSpiner.get(position).toString());
                        }else {
                            child_age.remove(mEditTextContainer.getChildCount()-1);
                            child_age.add(noRoomsSpiner.get(position).toString());
                        }
                    }catch (Exception e){}


                }
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

        } catch (Exception e) {
        }

    }

    private void getDateDifferent(){
        int dateDifference = 0;
        String check_in_Date = et_booking_Check_In.getText().toString();
        String check_out_Date = et_booking_Check_Out.getText().toString();
        try{
            dateDifference = (int) getDateDiff(new SimpleDateFormat("dd-MM-yyyy"), check_in_Date, check_out_Date);
            et_no_of_nights.setText(String.valueOf(dateDifference));
        }catch (Exception e ){
            et_no_of_nights.setText("Number of Nights");
        }
    }
    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void Add_Line2() {
        try {
            Spinner et = new Spinner(mContext);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            p.setMargins(5, 5, 30, 5);
            p.gravity = Gravity.TOP|Gravity.CENTER;
            et.setLayoutParams(p);
            et.setBackgroundResource(R.drawable.custom_bg_navy_dark_box);
            et.setPadding(20, 10, 20, 10);
            et.setGravity(Gravity.CENTER);
            mEditTextContainer2.addView(et);

            final List<Integer> noRoomsSpiner = new ArrayList<Integer>();
            for(int i=0;i<13;i++){
                noRoomsSpiner.add(i);
            }

            ArrayAdapter<Integer> nighgtAdapter = new ArrayAdapter<Integer>(mContext, android.R.layout.simple_spinner_item, noRoomsSpiner);
            et.setAdapter(nighgtAdapter);

            et.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if(position==0){
                            child_age.add(noRoomsSpiner.get(position).toString());
                        }else {
                            child_age.remove(mEditTextContainer2.getChildCount()-1);
                            child_age.add(noRoomsSpiner.get(position).toString());
                        }
                    }catch (Exception e){}

                }
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } catch (Exception e) {
        }

    }

    //ll_adult.startAnimation(shakeError());
    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }


}
