package com.guiddoocn.holidays.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityAllPackagesList;
import com.guiddoocn.holidays.models.ModelAllHotelList;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentBookingUnique extends Fragment {
    private static final String TAG = "FragmentBookingUnique";
    private boolean doubleBackToExitPressedOnce = false;
    @BindView(R.id.sp_type_Travelers)
    Spinner sp_type_Travelers;
    @BindView(R.id.sp_month_Travel)
    Spinner sp_month_Travel;
    @BindView(R.id.sp_Interests)
    Spinner sp_Interests;
    ////
    @BindView(R.id.ll_adult)
    LinearLayout ll_adult;
    @BindView(R.id.tv_max_Adult)
    TextView tv_max_Adult;
    @BindView(R.id.tv_min_Adult)
    TextView tv_min_Adult;
    @BindView(R.id.tv_value_Adult)
    TextView tv_value_Adult;
    ////
    @BindView(R.id.ll_child)
    LinearLayout ll_child;
    @BindView(R.id.tv_max_Child)
    TextView tv_max_Child;
    @BindView(R.id.tv_min_Child)
    TextView tv_min_Child;
    @BindView(R.id.tv_value_Child)
    TextView tv_value_Child;

    @BindView(R.id.bt_search)
    Button bt_search;
    ////
    List<String> type_Travelers = new ArrayList<String>();
    List<String> monthTravel = new ArrayList<String>();
    List<String> interests = new ArrayList<String>();
    ArrayList<ModelAllHotelList> modelAllHotelLists = new ArrayList<>();
    Context mContext;
    private CustomResponseDialog dialog;
    private SessionManager sessionManager;
    private SQLiteHandler db;
    private int monthID=1,number_of_adults=1,number_of_child=0,NoOfNights=3,TravelID=1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_unique, container, false);
        mContext = getActivity();
        dialog = new CustomResponseDialog(mContext);
        db = new SQLiteHandler(mContext);
        sessionManager = new SessionManager(mContext);
        ButterKnife.bind(this, view);

        //type_Travelers data
        type_Travelers.add("Honeymoon");
        type_Travelers.add("Beach");
        type_Travelers.add("Family");
        type_Travelers.add("Adventure");
        type_Travelers.add("Friends");
        type_Travelers.add("Solo");
        type_Travelers.add("Couple");
        type_Travelers.add("Culture");
        type_Travelers.add("Nightlife");
        type_Travelers.add("Romantic");
        type_Travelers.add("Wildlife");
        type_Travelers.add("Luxury");
        type_Travelers.add("Culinary");
        type_Travelers.add("Shopping");

        //    TravelersAdapter for spinner
        ArrayAdapter<String> type_TravelersAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, type_Travelers);
        sp_type_Travelers.setAdapter(type_TravelersAdapter);

        sp_type_Travelers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TravelID = position+1;
                Log.e(TAG, "Travel id --->" + String.valueOf(TravelID));
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //monthT  data
        monthTravel.add(" January");
        monthTravel.add(" February");
        monthTravel.add(" March");
        monthTravel.add(" April");
        monthTravel.add(" May");
        monthTravel.add(" June");
        monthTravel.add(" July");
        monthTravel.add(" August");
        monthTravel.add(" September");
        monthTravel.add(" October");
        monthTravel.add(" November");
        monthTravel.add(" December");


        //   monthTravelAdapter adapter for spinner
        ArrayAdapter<String> monthTravelAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, monthTravel);
        sp_month_Travel.setAdapter(monthTravelAdapter);

        sp_month_Travel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthID = position+1;
                Log.e(TAG, "month id --->" + String.valueOf(monthID));
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        //interests data
        interests.add("Select all");
        interests.add("Culture");
        interests.add("Nightlife");
        interests.add("Romantic");
        interests.add("Wildlife");
        interests.add("Beach");
        interests.add("Adventure");
        interests.add("Luxury");
        interests.add("Culinary");
        interests.add("Shopping");

        //   interestsAdapter adapter for spinner
        ArrayAdapter<String> interestsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, interests);
        sp_Interests.setAdapter(interestsAdapter);


        //setting a value for Adult And Child
        try {


            tv_max_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Adult.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e(TAG, "data =" + max);
                    if (max <= 10) {
                        tv_value_Adult.setText("" + max);
                        number_of_adults=max;
                        Log.e(TAG, "number_of_adults --->" + max);

                    } else {
                        tv_value_Adult.setText("10");
                        number_of_adults=max;
                        Log.e(TAG, "number_of_adults --->" + max);
                        ll_adult.startAnimation(shakeError());
                    }

                }
            });
            tv_min_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Adult.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 0) {
                        tv_value_Adult.setText("" + min);
                        number_of_adults=min;
                        Log.e(TAG, "number_of_adults --->" + min);
                    } else {
                        tv_value_Adult.setText("0");
                        number_of_adults=min;
                        Log.e(TAG, "number_of_adults --->" + min);
                        ll_adult.startAnimation(shakeError());
                    }

                }
            });

            //setting a value for Adult And Child
            Log.e(TAG, "data =");
            tv_max_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Child.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e(TAG, "data =" + max);
                    if (max <= 10) {
                        tv_value_Child.setText("" + max);
                        number_of_child=max;
                        Log.e(TAG, "number_of_child --->" + max);

                    } else {
                        tv_value_Child.setText("10");
                        number_of_child=max;
                        Log.e(TAG, "number_of_child --->" + max);
                        ll_child.startAnimation(shakeError());
                    }

                }
            });
            tv_min_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Child.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 0) {
                        tv_value_Child.setText("" + min);
                        number_of_child=min;
                        Log.e(TAG, "number_of_child --->" + min);
                    } else {
                        tv_value_Child.setText("0");
                        number_of_child=min;
                        Log.e(TAG, "number_of_child --->" + min);
                        ll_child.startAnimation(shakeError());

                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("erro", "code=" + e.getMessage());
        }


        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*Intent intent= new Intent(getActivity(), ActivityAllPackagesList.class);
                intent.putExtra("Total_Days",NoOfNights);
                startActivity(intent);*/

                get_Package_List();
            }
        });

        return view;
    }


    private void get_Package_List() {
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "getListOfPackages";
        Log.e(TAG, "ListOfPackages URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("api_key", "0D1067102F935B4CC31E082BD45014D469E35268");
            object.put("country_id",0/*sessionManager.getCountryCode()*/);
            object.put("is_fam", 0);
            object.put("month", monthID);//pass month id 1 to 12
            object.put("number_of_adults", number_of_adults);
            object.put("number_of_child", number_of_child);
            object.put("number_of_nights", NoOfNights);
            object.put("package_type", 3);//for standard 1, for smart 2, for unique 3
            object.put("traveler_type", TravelID);

            Log.e("Parameter pass-->",object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "ListOfPackages Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            JSONObject jsonStatus = jsonObject.getJSONObject("status");
                            if (jsonStatus.getString("status_code").equalsIgnoreCase("200")) {
                                modelAllHotelLists = new ArrayList<>();
                                JSONArray jsonArray = jsonObject.getJSONArray("package");
                                for(int i = 0;jsonArray.length()>i;i++){
                                    JSONObject json2 = jsonArray.getJSONObject(i);
                                    ModelAllHotelList hotelList = new ModelAllHotelList();
                                    hotelList.setID(json2.getString("package_id"));
                                    hotelList.setTitle(json2.getString("package_name"));
                                    hotelList.setCityCountryName(json2.getString("cities"));
                                    hotelList.setLocation("");
                                    hotelList.setAmount(json2.getString("starting_from_price"));
                                    hotelList.setSuccess_rate(json2.getString("success_rate").replace("null",""));
                                    hotelList.setQueries_received(json2.getString("queries_received").replace("null",""));
                                    hotelList.setCurrency(json2.getString("currency"));
                                    hotelList.setImage(json2.getString("thumbnail_image"));
                                    hotelList.setRating(json2.getString("rating").replace("","0"));


                                    JSONArray jsonArray1 = json2.getJSONArray("pack_theme");

                                    ArrayList<String> stringArray = new ArrayList<String>();

                                    for(int j = 0, count = jsonArray1.length(); j< count; j++)
                                    {
                                        try {
                                            JSONObject jsonObjects = jsonArray1.getJSONObject(j);
                                            stringArray.add(jsonObjects.getString("theme"));
                                        }
                                        catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    String s = TextUtils.join(", ", stringArray);

                                    hotelList.setAddress(s);

                                    modelAllHotelLists.add(hotelList);
                                }

                                if(modelAllHotelLists.size()>0){
                                    Intent intent= new Intent(getActivity(), ActivityAllPackagesList.class);
                                    intent.putExtra("Total_Days",NoOfNights);
                                    intent.putExtra("mylist", modelAllHotelLists);
                                    startActivity(intent);
                                }else{
                                    Toast.makeText(mContext, "package not found", Toast.LENGTH_LONG).show();
                                }


                            }else{
                                Toast.makeText(mContext, "package not found", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }




    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }


/*
    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    getFragmentManager().popBackStack();
                    return true;
                }
                return false;
            }
        });
    }*/

}
