package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelStandardBookingPayment;
import com.guiddoocn.holidays.utils.AppConstants;
import com.guiddoocn.holidays.utils.AvenuesParams;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.ServiceUtility;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;



public class ActivityPaymentStatus extends Activity{

    private static final String TAG = "ActivityPackageBooking";

    @BindView(R.id.cv_done)
    CardView cv_done;

    @BindView(R.id.tv_try_again)
    TextView tv_try_again;

    @BindView(R.id.failed_linear_parent)
    LinearLayout failedLinearLayout;

    @BindView(R.id.success_linear_parent)
    LinearLayout successLinearLayout;

    String statusTrans = "";
    Intent mainIntent;
    int Status,Paymaent_way;
    boolean usePromoCode = false;
    SessionManager sessionManager;
    private CustomResponseDialog dialog;
    private boolean flag=true,APICallFlag=false,flagConfirm=true;
    private Context mContext;
    private int CountHit=0;
    private SQLiteHandler db;
    private View v;
    private JSONArray jsonArray = new JSONArray();
    ArrayList<ModelStandardBookingPayment> bookingPaymentArrayList =new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        ButterKnife.bind(this);
        mContext = this;
        dialog=new CustomResponseDialog(this);
        mainIntent = getIntent();
        v = findViewById(R.id.rl);
        sessionManager = new SessionManager(this);
        db = new SQLiteHandler(mContext);
        statusTrans = mainIntent.getStringExtra("transStatus");
        bookingPaymentArrayList = (ArrayList<ModelStandardBookingPayment>) getIntent().getSerializableExtra("BookingDetailModels");

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }

        if (statusTrans.equals("failure"))
        {
            Status = 2;
            failedLinearLayout.setVisibility(View.VISIBLE);
            successLinearLayout.setVisibility(View.GONE);
            create_PackageBooking();
        } else if (statusTrans.equals("success")) {
            Status = 1;
            failedLinearLayout.setVisibility(View.GONE);
            successLinearLayout.setVisibility(View.VISIBLE);
            create_PackageBooking();
        } else {
            Status = 2;
            failedLinearLayout.setVisibility(View.VISIBLE);
            successLinearLayout.setVisibility(View.GONE);
            create_PackageBooking();
        }

        TextView orderTv = (TextView) findViewById(R.id.success_parent_orderid_text);
        orderTv.setText(mainIntent.getStringExtra("orderId"));


        cv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityPaymentStatus.this, ActivityHome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });


        tv_try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityPaymentStatus.this, ActivityRazorPayPayment.class);
                i.putExtra(AvenuesParams.BILLING_EMAIL,mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
                i.putExtra(AvenuesParams.BILLING_TEL,mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
                i.putExtra("BookingDetailModels", (ArrayList<ModelStandardBookingPayment>) bookingPaymentArrayList);
                startActivity(i);
                finish();
            }
        });
    }




    private void create_PackageBooking() {

        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "BookPackages";
        Log.e(TAG, "ListOfPackages URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("amount_payable", bookingPaymentArrayList.get(0).getAmount_payable());
            object.put("booker_email", mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            object.put("booker_name", sessionManager.getContactPerson());
            object.put("booking_reference_no",mainIntent.getStringExtra("orderId") );//pass month id 1 to 12
            object.put("invoice_no", mainIntent.getStringExtra("orderId"));
            object.put("confirmation_no", mainIntent.getStringExtra("orderId"));
            object.put("currency", bookingPaymentArrayList.get(0).getCurrency());
            object.put("no_of_adult", bookingPaymentArrayList.get(0).getNo_of_adult());
            object.put("no_of_child", bookingPaymentArrayList.get(0).getNo_of_child());
            object.put("no_of_infant", "0");
            object.put("child_age", "");
            object.put("infant_age", "");
            object.put("cancellation_amount", "0");
            object.put("package_id", bookingPaymentArrayList.get(0).getPackage_id());
            object.put("package_type", bookingPaymentArrayList.get(0).getPackage_type());
            object.put("partner_id", sessionManager.getUserId());
            object.put("status", Status);
            object.put("total_amt", bookingPaymentArrayList.get(0).getTotal_amount());
            object.put("transaction_id", mainIntent.getStringExtra("transaction_id"));
            object.put("travel_date", bookingPaymentArrayList.get(0).getTravel_date());
            object.put("voucher_link", "");

            Log.e("Parameter pass-->", object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "ListOfPackages Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            String jsonStatus= jsonObject.getString("status_code");
                            Log.e(TAG, "Setaus---->" + response.toString());
                            if (jsonStatus.equalsIgnoreCase("200")) {

                                showSnackBar(ActivityPaymentStatus.this,jsonObject.getString("error_message"));

                            } else {
                                showSnackBar(ActivityPaymentStatus.this,jsonObject.getString("error_message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20 * 3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);


    }


    private void showSnackBar(Activity activityPackageBooking, String message) {
        View rootView = activityPackageBooking.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 1500).show();
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ActivityPaymentStatus.this, ActivityHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        finish();

    }


    @Override
    protected void onPause() {
        super.onPause();
    }


}
