package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterHotelsList;
import com.guiddoocn.holidays.adapter.AdapterInclusionList;
import com.guiddoocn.holidays.adapter.AdapterMonthList;
import com.guiddoocn.holidays.models.ModelHotelList;
import com.guiddoocn.holidays.models.ModelInclusionList;
import com.guiddoocn.holidays.models.ModelMonthList;
import com.guiddoocn.holidays.utils.FontsOverride;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityHotelDetails extends AppCompatActivity {


    Context mContext;
    Activity activity;
    //hotelList
    @BindView(R.id.rv_hotelList)
    RecyclerView rv_hotelList;
    AdapterHotelsList adapterHotelsList;
    ArrayList<ModelHotelList> modelHotelLists = new ArrayList<>();
    //InclusionList

    @BindView(R.id.rv_InclusionList)
    RecyclerView rv_InclusionList;
    AdapterInclusionList adapterInclusionList;
    ArrayList<ModelInclusionList> modelInclusionLists = new ArrayList<>();

    //MonthList
    @BindView(R.id.rv_MonthList)
    RecyclerView rv_MonthList;
    AdapterMonthList adapterMonthList;
    ArrayList<ModelMonthList> monthList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_details);
        ButterKnife.bind(this);
        mContext = this;
        activity = this;

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }



        // modelDestinationLists.add(new ModelDestinationList(R.drawable.flag_uae,"1", "AbuDubai","UAE","1"));

        /* Hotel list */
        modelHotelLists.add(new ModelHotelList(R.drawable.swimming, "CityMax Bar Dubai"));
        modelHotelLists.add(new ModelHotelList(R.drawable.swimming, "CityMax Bar Dubai"));
        GridLayoutManager manager = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
        FragmentManager fragmentManager = getSupportFragmentManager();
        adapterHotelsList = new AdapterHotelsList(mContext, modelHotelLists, activity, fragmentManager);
        rv_hotelList.setLayoutManager(manager);
        rv_hotelList.setAdapter(adapterHotelsList);
        adapterHotelsList.notifyDataSetChanged();

        /* Inclusion List*/

        modelInclusionLists.add(new ModelInclusionList("","Creek Dhow Cruise With Dinner Buffet", "Dubai", "Baniyas Road - Dubai-United Arab Emirates", "Duration -4 hrs"));
        modelInclusionLists.add(new ModelInclusionList("","Half Day Dubai City Tour - Sharing", "Dubai", "Baniyas Road - Dubai-United Arab Emirates", "Duration -4 hrs"));
        //modelInclusionLists.add(new ModelInclusionList("Creek Dhow Cruise With Dinner Buffet","Dubai","Baniyas Road - Dubai-United Arab Emirates","Duration -4 hrs"));

        GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
        FragmentManager fragmentManager1 = getSupportFragmentManager();
        adapterInclusionList = new AdapterInclusionList(mContext, modelInclusionLists, activity, fragmentManager1);
        rv_InclusionList.setLayoutManager(manager1);
        rv_InclusionList.setAdapter(adapterInclusionList);

        /* MOnth List*/
        monthList.clear();
        monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " January"  ));
        monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " February" ));
        monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " March"    ));
        monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " April"    ));
       // monthList.add(new ModelHotelList(R.drawable.ic_launcher_background, " May"      ));
        //monthList.add(new ModelHotelList(R.drawable.ic_launcher_background, " June"     ));
       // monthList.add(new ModelHotelList(R.drawable.ic_launcher_background, " July"     ));
       // monthList.add(new ModelHotelList(R.drawable.ic_launcher_background, " August"   ));
       // monthList.add(new ModelHotelList(R.drawable.ic_launcher_background, " September"));
        //monthList.add(new ModelHotelList(R.drawable.ic_launcher_background, " October"  ));
        monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " November" ));
        monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " December" ));
        GridLayoutManager manager2 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
        FragmentManager fragmentManager2 = getSupportFragmentManager();
        adapterMonthList = new AdapterMonthList(mContext, monthList, activity,fragmentManager2);
        rv_MonthList.setLayoutManager(manager2);
        rv_MonthList.setAdapter(adapterMonthList);


    }
}
