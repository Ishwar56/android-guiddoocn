package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelMyBooking;
import com.guiddoocn.holidays.utils.AppConstants;
import com.guiddoocn.holidays.utils.AvenuesParams;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.ServiceUtility;
import com.guiddoocn.holidays.utils.SessionManager;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityMainRazorPayPayment extends AppCompatActivity implements PaymentResultListener {

    @BindView(R.id.iv_back)
    ImageView tv_back;

    @BindView(R.id.tv_title)
    TextView tv_title;

    Intent mainIntent;
    Integer randomNum ;

    private ArrayList<ModelMyBooking> arrayModelMyBookings;

    private static final String TAG = "logPaymentActivity";
    private Activity activity = this;
    private int Total_cost=0;
    private Context mContext;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razorpay_payment);
        ButterKnife.bind(this);
        mainIntent = getIntent();
        mContext = this;
        sessionManager = new SessionManager(mContext);
        Checkout.preload(getApplicationContext());
        randomNum = ServiceUtility.randInt(0, 999999);

        tv_title.setText("Transaction Information");
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }

        arrayModelMyBookings = (ArrayList<ModelMyBooking>) getIntent().getSerializableExtra("CartListDetailModels");

        Double cost = Double.parseDouble(mainIntent.getStringExtra(AppConstants.amount));
        Log.e("Total Cost int", String.valueOf(cost));
        Total_cost = (int) (cost*100);
        Log.e("Pass Cost int", String.valueOf(Total_cost));
        if (sessionManager.isNetworkAvailable()) {
            startPayment();
        }else {
            Toast.makeText(ActivityMainRazorPayPayment.this,"Internet Connection Not Available", Toast.LENGTH_LONG).show();
            Intent i = new Intent(ActivityMainRazorPayPayment.this,StatusActivity.class);
            i.putExtra(AvenuesParams.BILLING_NAME,mainIntent.getStringExtra(AvenuesParams.BILLING_NAME));
            i.putExtra(AvenuesParams.BILLING_ADDRESS,mainIntent.getStringExtra(AvenuesParams.BILLING_ADDRESS));
            i.putExtra(AvenuesParams.BILLING_TEL,mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
            i.putExtra(AvenuesParams.DELIVERY_TEL,mainIntent.getStringExtra(AvenuesParams.DELIVERY_TEL));
            i.putExtra(AvenuesParams.BILLING_ZIP,mainIntent.getStringExtra(AvenuesParams.BILLING_ZIP));
            i.putExtra(AvenuesParams.DELIVERY_ZIP,mainIntent.getStringExtra(AvenuesParams.DELIVERY_ZIP));
            i.putExtra(AvenuesParams.BILLING_CITY,mainIntent.getStringExtra(AvenuesParams.BILLING_CITY));
            i.putExtra(AvenuesParams.DELIVERY_CITY,mainIntent.getStringExtra(AvenuesParams.DELIVERY_CITY));
            i.putExtra(AvenuesParams.DELIVERY_STATE,mainIntent.getStringExtra(AvenuesParams.DELIVERY_STATE));
            i.putExtra(AvenuesParams.BILLING_STATE,mainIntent.getStringExtra(AvenuesParams.BILLING_STATE));
            i.putExtra(AvenuesParams.DELIVERY_COUNTRY,mainIntent.getStringExtra(AvenuesParams.DELIVERY_COUNTRY));
            i.putExtra(AvenuesParams.BILLING_COUNTRY,mainIntent.getStringExtra(AvenuesParams.BILLING_COUNTRY));
            i.putExtra(AvenuesParams.BILLING_EMAIL,mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            i.putExtra(AvenuesParams.DELIVERY_NAME,mainIntent.getStringExtra(AvenuesParams.DELIVERY_NAME));
            i.putExtra(AvenuesParams.DELIVERY_ADDRESS,mainIntent.getStringExtra(AvenuesParams.DELIVERY_ADDRESS));
            i.putExtra("orderId",randomNum+"");
            i.putExtra("payment_way",mainIntent.getStringExtra("payment_way"));
            i.putExtra(AppConstants.curruncy,mainIntent.getStringExtra(AppConstants.curruncy));
            i.putExtra(AppConstants.amount,mainIntent.getStringExtra(AppConstants.amount));
            i.putExtra("CartListDetailModels", (ArrayList<ModelMyBooking>) arrayModelMyBookings);

            i.putExtra("transStatus","Failed");
            i.putExtra(AppConstants.payment_Type,mainIntent.getStringExtra(AppConstants.payment_Type));
            startActivity(i);
            finish();
        }

    }



    private void startPayment() {
        final Checkout co = new Checkout();
        try {
            JSONObject options = new JSONObject();
            options.put("name", arrayModelMyBookings.get(0).getActivity_name());
            options.put("description", "");
            options.put("image", arrayModelMyBookings.get(0).getActivity_image()); //https://s3.amazonaws.com/rzp-mobile/images/rzp.png
            options.put("currency", mainIntent.getStringExtra(AppConstants.curruncy));
            options.put("amount", Total_cost);
            JSONObject preFill = new JSONObject();
            preFill.put("email", mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            preFill.put("contact", mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
            options.put("prefill", preFill);

            co.open(this, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_LONG)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        Log.e("razorpayPaymentID--->",razorpayPaymentID);
        Log.e("razorpay response--->","Success");
        try {

            //Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_LONG).show();

            Intent i = new Intent(ActivityMainRazorPayPayment.this,StatusActivity.class);
            i.putExtra(AvenuesParams.BILLING_NAME,mainIntent.getStringExtra(AvenuesParams.BILLING_NAME));
            i.putExtra(AvenuesParams.BILLING_ADDRESS,mainIntent.getStringExtra(AvenuesParams.BILLING_ADDRESS));
            i.putExtra(AvenuesParams.BILLING_TEL,mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
            i.putExtra(AvenuesParams.DELIVERY_TEL,mainIntent.getStringExtra(AvenuesParams.DELIVERY_TEL));
            i.putExtra(AvenuesParams.BILLING_ZIP,mainIntent.getStringExtra(AvenuesParams.BILLING_ZIP));
            i.putExtra(AvenuesParams.DELIVERY_ZIP,mainIntent.getStringExtra(AvenuesParams.DELIVERY_ZIP));
            i.putExtra(AvenuesParams.BILLING_CITY,mainIntent.getStringExtra(AvenuesParams.BILLING_CITY));
            i.putExtra(AvenuesParams.DELIVERY_CITY,mainIntent.getStringExtra(AvenuesParams.DELIVERY_CITY));
            i.putExtra(AvenuesParams.DELIVERY_STATE,mainIntent.getStringExtra(AvenuesParams.DELIVERY_STATE));
            i.putExtra(AvenuesParams.BILLING_STATE,mainIntent.getStringExtra(AvenuesParams.BILLING_STATE));
            i.putExtra(AvenuesParams.DELIVERY_COUNTRY,mainIntent.getStringExtra(AvenuesParams.DELIVERY_COUNTRY));
            i.putExtra(AvenuesParams.BILLING_COUNTRY,mainIntent.getStringExtra(AvenuesParams.BILLING_COUNTRY));
            i.putExtra(AvenuesParams.BILLING_EMAIL,mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            i.putExtra(AvenuesParams.DELIVERY_NAME,mainIntent.getStringExtra(AvenuesParams.DELIVERY_NAME));
            i.putExtra(AvenuesParams.DELIVERY_ADDRESS,mainIntent.getStringExtra(AvenuesParams.DELIVERY_ADDRESS));
            i.putExtra("orderId",randomNum+"");
            i.putExtra("payment_way",mainIntent.getStringExtra("payment_way"));
            i.putExtra(AppConstants.amount,mainIntent.getStringExtra(AppConstants.amount));
            i.putExtra(AppConstants.curruncy,mainIntent.getStringExtra(AppConstants.curruncy));
            i.putExtra("CartListDetailModels", (ArrayList<ModelMyBooking>) arrayModelMyBookings);

            i.putExtra("transStatus","success");
            i.putExtra(AppConstants.payment_Type,mainIntent.getStringExtra(AppConstants.payment_Type));
            startActivity(i);
            finish();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        Log.e("razorpay response--->",response);
        try {
            //Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_LONG).show();
            Intent i = new Intent(ActivityMainRazorPayPayment.this,StatusActivity.class);
            i.putExtra(AvenuesParams.BILLING_NAME,mainIntent.getStringExtra(AvenuesParams.BILLING_NAME));
            i.putExtra(AvenuesParams.BILLING_ADDRESS,mainIntent.getStringExtra(AvenuesParams.BILLING_ADDRESS));
            i.putExtra(AvenuesParams.BILLING_TEL,mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
            i.putExtra(AvenuesParams.DELIVERY_TEL,mainIntent.getStringExtra(AvenuesParams.DELIVERY_TEL));
            i.putExtra(AvenuesParams.BILLING_ZIP,mainIntent.getStringExtra(AvenuesParams.BILLING_ZIP));
            i.putExtra(AvenuesParams.DELIVERY_ZIP,mainIntent.getStringExtra(AvenuesParams.DELIVERY_ZIP));
            i.putExtra(AvenuesParams.BILLING_CITY,mainIntent.getStringExtra(AvenuesParams.BILLING_CITY));
            i.putExtra(AvenuesParams.DELIVERY_CITY,mainIntent.getStringExtra(AvenuesParams.DELIVERY_CITY));
            i.putExtra(AvenuesParams.DELIVERY_STATE,mainIntent.getStringExtra(AvenuesParams.DELIVERY_STATE));
            i.putExtra(AvenuesParams.BILLING_STATE,mainIntent.getStringExtra(AvenuesParams.BILLING_STATE));
            i.putExtra(AvenuesParams.DELIVERY_COUNTRY,mainIntent.getStringExtra(AvenuesParams.DELIVERY_COUNTRY));
            i.putExtra(AvenuesParams.BILLING_COUNTRY,mainIntent.getStringExtra(AvenuesParams.BILLING_COUNTRY));
            i.putExtra(AvenuesParams.BILLING_EMAIL,mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            i.putExtra(AvenuesParams.DELIVERY_NAME,mainIntent.getStringExtra(AvenuesParams.DELIVERY_NAME));
            i.putExtra(AvenuesParams.DELIVERY_ADDRESS,mainIntent.getStringExtra(AvenuesParams.DELIVERY_ADDRESS));
            i.putExtra("orderId",randomNum+"");
            i.putExtra("payment_way",mainIntent.getStringExtra("payment_way"));
            i.putExtra(AppConstants.amount,mainIntent.getStringExtra(AppConstants.amount));
            i.putExtra(AppConstants.curruncy,mainIntent.getStringExtra(AppConstants.curruncy));
            i.putExtra("CartListDetailModels", (ArrayList<ModelMyBooking>) arrayModelMyBookings);
            i.putExtra("transStatus","failure");
            i.putExtra(AppConstants.payment_Type,mainIntent.getStringExtra(AppConstants.payment_Type));
            startActivity(i);
            finish();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
