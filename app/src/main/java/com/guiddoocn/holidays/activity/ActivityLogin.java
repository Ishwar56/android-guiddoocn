package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterHotelRoomList;
import com.guiddoocn.holidays.models.ModelActivitiesList;
import com.guiddoocn.holidays.models.ModelHotelRoomsList;
import com.guiddoocn.holidays.models.ModelSubCategoryPreferences;
import com.guiddoocn.holidays.models.ModelSubDiningPreferences;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityLogin extends AppCompatActivity {
    private static final String TAG = "LoginActivities";

    @BindView(R.id.ll_login)
    LinearLayout ll_login;

    @BindView(R.id.ll_SigUP)
    LinearLayout ll_SigUP;

    @BindView(R.id.ll_login_Tab)
    LinearLayout ll_login_Tab;

    @BindView(R.id.ll_signup_Tab)
    LinearLayout ll_signup_Tab;

    @BindView(R.id.vv_login)
    View vv_login;

    @BindView(R.id.vv_sign_up)
    View vv_sign_up;

    @BindView(R.id.tv_Email)
    FormEditText tv_Email;

    @BindView(R.id.tv_Pass)
    FormEditText tv_Pass;

    @BindView(R.id.tv_company_Name)
    FormEditText tv_company_Name;

    @BindView(R.id.tv_persone_Name)
    FormEditText tv_persone_Name;

    @BindView(R.id.tv_signUp_Email)
    FormEditText tv_signUp_Email;

    @BindView(R.id.tv_SignUp_Pass)
    FormEditText tv_SignUp_Pass;

    @BindView(R.id.tv_SignUp_Mob)
    FormEditText tv_SignUp_Mob;

    @BindView(R.id.tv_sign_up)
    TextView tv_sign_up;

    @BindView(R.id.tv_forgot_pass)
    TextView tv_forgot_pass;

    @BindView(R.id.tv_login_with_OTP)
    TextView tv_login_with_OTP;

    @BindView(R.id.tv_Login)
    Button tv_Login;

    @BindView(R.id.tv_SignUp)
    Button tv_SignUp;

    private Context mContext;
    private boolean flag = true;
    private SessionManager sessionManager;
    private CustomResponseDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mContext=this;
        sessionManager = new SessionManager(mContext);
        dialog=new CustomResponseDialog(mContext);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        ll_login_Tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_login.setVisibility(View.VISIBLE);
                ll_SigUP.setVisibility(View.GONE);
                vv_login.setBackgroundResource(R.color.colorButton);
                vv_sign_up.setBackgroundResource(R.color.colorWhite);
            }
        });

        ll_signup_Tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_login.setVisibility(View.GONE);
                ll_SigUP.setVisibility(View.VISIBLE);
                vv_login.setBackgroundResource(R.color.colorWhite);
                vv_sign_up.setBackgroundResource(R.color.colorButton);
            }
        });

        tv_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_login.setVisibility(View.GONE);
                ll_SigUP.setVisibility(View.VISIBLE);
                vv_login.setBackgroundResource(R.color.colorWhite);
                vv_sign_up.setBackgroundResource(R.color.colorButton);
            }
        });

        tv_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPass();
            }
        });

        tv_login_with_OTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityLogin.this,ActivityOTPVerified.class);
                startActivity(intent);
            }
        });


        tv_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(ActivityLogin.this,ActivityHome.class);
                startActivity(intent);
                finish();*/

                boolean allValid = true;
                FormEditText[] fields = {tv_Email,tv_Pass};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }
                if (allValid) {
                    login();
                }
            }
        });


        tv_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean allValid = true;
                FormEditText[] fields = {tv_company_Name,tv_persone_Name,tv_signUp_Email,tv_SignUp_Mob,tv_SignUp_Pass};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }
                if (allValid) {
                    registration();
                }

            }
        });




    }




    private void ForgotPass(){
        final Dialog dialogMsg = new Dialog(mContext);
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.dialog_forgot_pass);
        dialogMsg.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMsg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMsg.getWindow().setAttributes(lp);
        dialogMsg.show();

        TextView btn_cancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
        TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_done);
        final FormEditText fet_email = (FormEditText) dialogMsg.findViewById(R.id.fet_email);


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean allValids = true;
                FormEditText[] fieldss = {fet_email};
                for (FormEditText field : fieldss) {
                    if (field.testValidity() && allValids) {
                        allValids = field.testValidity() && allValids;
                    } else {
                        field.requestFocus();
                        allValids = false;
                        break;
                    }
                }
                if (allValids) {
                    forgot(fet_email.getText().toString());
                    dialogMsg.cancel();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
            }
        });

    }

    private void registration() {
        dialog=new CustomResponseDialog(mContext);
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "guiddoocn/userregistration";
        Log.e(TAG, "registration URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("TravelAgencyName", tv_company_Name.getText().toString());
            object.put("AgentName", tv_persone_Name.getText().toString());
            object.put("EmailAddress",tv_signUp_Email.getText().toString());
            object.put("MobileNo",tv_SignUp_Mob.getText().toString());
            object.put("Password",tv_SignUp_Pass.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "registration Parameters---->" + object.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "registration Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                                ll_login.setVisibility(View.VISIBLE);
                                ll_SigUP.setVisibility(View.GONE);
                                vv_login.setBackgroundResource(R.color.colorButton);
                                vv_sign_up.setBackgroundResource(R.color.colorWhite);
                                //Toast.makeText(mContext, jsonObject.getString("error_message"), Toast.LENGTH_LONG).show();
                                showSnackBar(ActivityLogin.this,jsonObject.getString("error_message"));
                            }else{
                                //Toast.makeText(mContext, jsonObject.getString("error_message"), Toast.LENGTH_LONG).show();
                                showSnackBar(ActivityLogin.this,jsonObject.getString("error_message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }

    private void login() {
        dialog=new CustomResponseDialog(mContext);
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "guiddoocn/logincredentials";
        Log.e(TAG, "login URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("login", tv_Email.getText().toString());
            object.put("password",tv_Pass.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "login Parameters---->" + object.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "login Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            JSONObject jsonStatus = jsonObject.getJSONObject("status");
                            if (jsonStatus.getString("status_code").equalsIgnoreCase("200")) {
                                sessionManager.setLogedIn(true);
                                sessionManager.setEmailId(jsonObject.getString("email"));
                                sessionManager.setUserMobile(jsonObject.getString("mobile"));
                                sessionManager.setUserID(jsonObject.getString("TravelAgent_Id"));
                                sessionManager.setUserName(jsonObject.getString("Company_name"));
                                sessionManager.setContactPerson(jsonObject.getString("Agent_name"));
                                Intent intent = new Intent(ActivityLogin.this,ActivityHome.class);
                                startActivity(intent);
                                finish();
                                Toast.makeText(mContext, jsonStatus.getString("error_message"), Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(mContext, jsonStatus.getString("error_message"), Toast.LENGTH_LONG).show();
                               // showSnackBar(ActivityLogin.this,jsonStatus.getString("error_message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }

    private void forgot(String Email) {
        dialog=new CustomResponseDialog(mContext);
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "guiddoocn/forgotpassword?emailid="+Email;
        Log.e(TAG, "login URL---->" + url);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "forgot Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            //JSONObject jsonData = jsonObject.getJSONObject("status");
                            JSONObject jsonStatus = jsonObject.getJSONObject("status");
                            if (jsonStatus.getString("status_code").equalsIgnoreCase("200")) {
                                showSnackBar(ActivityLogin.this,jsonStatus.getString("error_message"));
                            }else{
                                showSnackBar(ActivityLogin.this,jsonStatus.getString("error_message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }

    public void showSnackBar(Activity activity, String message){
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message,2000 ).show();
    }

}
