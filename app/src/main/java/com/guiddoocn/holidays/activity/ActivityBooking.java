package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.fragment.FragmentBookingCusmise;
import com.guiddoocn.holidays.fragment.FragmentBookingSmart;
import com.guiddoocn.holidays.fragment.FragmentBookingStandard;
import com.guiddoocn.holidays.fragment.FragmentBookingUnique;
import com.guiddoocn.holidays.fragment.FragmentHome;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.joaocruz04.lib.SOAPManager;
import pt.joaocruz04.lib.misc.JSoapCallback;
import pt.joaocruz04.lib.misc.JsoapError;


/*
  Created by Ishwar on 20/12/17.
 */
public class ActivityBooking extends AppCompatActivity {


    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.tv_title)
    TextView tv_title;


    @BindView(R.id.fragment_container2)
    LinearLayout fragment_container2;

    @BindView(R.id.bottom_navigation)
    AHBottomNavigation bottomNavigation;


    private LinearLayout layoutFabSave;

    private Context mContext;
    private String Status,msg,remark="",responce,resultObj;
    private boolean Backflag = false,FlagAPICall=true;
    private boolean FlagProfile = false, FlagAllData = false;
    private int Count = 0;
    private CustomResponseDialog customResponseDialog;
    private String ResponceData,deviceId;
    boolean doubleBackToExitPressedOnce = false,fabExpanded = false;;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{

            setContentView(R.layout.activity_booking);
            mContext = this;
            ButterKnife.bind(this);
            customResponseDialog = new CustomResponseDialog(mContext);
            sessionManager = new SessionManager(mContext);
            iv_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   onBackPressed();
                }
            });

            try {
                FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
            } catch (NullPointerException e) {

            }


            Show();

        }catch (Exception e){

        }

    }

    private void Show(){
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.tab1_customize, R.color.colorBlack);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.tab2_standard, R.color.colorBlack);
        //AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.tab3_smart, R.color.colorBlack);
       // AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.tab4_uniq, R.color.colorBlack);
        //AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.tab5_fem, R.color.colorBlack);

// Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
       // bottomNavigation.addItem(item3);
       // bottomNavigation.addItem(item4);
        bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.colorWhite));

// Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(true);
        bottomNavigation.restoreBottomNavigation(true);

// Change colors
        bottomNavigation.setAccentColor(getResources().getColor(R.color.colorButton));
        bottomNavigation.setInactiveColor(getResources().getColor(R.color.colorBlack1));

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        //bottomNavigation.setCurrentItem(2);

        bottomNavigation.setTranslucentNavigationEnabled(true);

        changeFragment(R.id.fragment_container2, new FragmentBookingCusmise(),false);
        tv_title.setText(R.string.tab_1);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (position == 0) {
                    bottomNavigation.setBehaviorTranslationEnabled(true);
                    bottomNavigation.restoreBottomNavigation(false);
                    tv_title.setText(R.string.tab_1);
                    changeFragment(R.id.fragment_container2, new FragmentBookingCusmise(),false);
                } else if (position == 1) {
                    bottomNavigation.setBehaviorTranslationEnabled(true);
                    bottomNavigation.restoreBottomNavigation(false);
                    changeFragment(R.id.fragment_container2, new FragmentBookingStandard(),false);
                    tv_title.setText(R.string.tab_2);
                }else if (position == 2) {
                    bottomNavigation.setBehaviorTranslationEnabled(false);
                    bottomNavigation.restoreBottomNavigation(false);
                    changeFragment(R.id.fragment_container2, new FragmentBookingSmart(),false);
                    tv_title.setText(R.string.tab_3);
                } else if (position == 3) {
                    bottomNavigation.setBehaviorTranslationEnabled(true);
                    changeFragment(R.id.fragment_container2, new FragmentBookingUnique(),false);
                    tv_title.setText(R.string.tab_4);
                }

                return true;
            }
        });


        bottomNavigation.restoreBottomNavigation(false);


    }


    @Override
    protected void onPause() {
        super.onPause();
        bottomNavigation.restoreBottomNavigation(false);
    }

    @Override
    protected void onResume() {

        super.onResume();

        if(sessionManager.isNetworkAvailable()){
           // CallSubscriptionStatusAPI();
        }else {
            showSnackBar(this,"Please check internet connection.");
        }



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ActivityBooking.this, ActivityHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void showSnackBar(Activity activity, String message){
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message,2000 ).show();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){

        }
    }

}
