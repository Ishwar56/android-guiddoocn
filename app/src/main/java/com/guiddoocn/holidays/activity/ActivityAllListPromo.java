package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterPromoExperiencesMaxList;
import com.guiddoocn.holidays.models.ModelPromoExperiencesList;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAllListPromo extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;


    @BindView(R.id.rv_allList)
    RecyclerView rv_allList;


    private BottomSheetDialog dialog;
    private Context mContext;
    private SessionManager sessionManager;
    private SQLiteHandler db;


    Activity activity;
    ArrayList<ModelPromoExperiencesList> modelPromoExperiencesLists = new ArrayList<>();


    AdapterPromoExperiencesMaxList adapterPromoExperiencesMaxList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_list);

        ButterKnife.bind(this);
        activity = this;
        mContext = this;
        dialog = new BottomSheetDialog(mContext);
        sessionManager = new SessionManager(mContext);
        db=new SQLiteHandler(mContext);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        //tv_title.setText(getText(R.string.all_events));
        tv_title.setText(getText(R.string.all_promotional_experiences));//All Promotional Experiences

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        try{
            modelPromoExperiencesLists = db.getPromoData();

            if(modelPromoExperiencesLists.size()>0){
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapterPromoExperiencesMaxList = new AdapterPromoExperiencesMaxList(activity, modelPromoExperiencesLists, activity, fragmentManager1);
                rv_allList.setLayoutManager(manager1);
                rv_allList.setAdapter(adapterPromoExperiencesMaxList);
            }
        }catch (Exception e){
            GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
            FragmentManager fragmentManager1 = getSupportFragmentManager();
            adapterPromoExperiencesMaxList = new AdapterPromoExperiencesMaxList(activity, modelPromoExperiencesLists, activity, fragmentManager1);
            rv_allList.setLayoutManager(manager1);
            rv_allList.setAdapter(adapterPromoExperiencesMaxList);
            e.printStackTrace();
        }

    }

}

