package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreabaccega.widget.FormEditText;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterDestinationMaxList;
import com.guiddoocn.holidays.adapter.AdapterPromoExperiencesMaxList;
import com.guiddoocn.holidays.models.ModelDestinationList;
import com.guiddoocn.holidays.models.ModelDestinationLists;
import com.guiddoocn.holidays.models.ModelPromoExperiencesList;
import com.guiddoocn.holidays.utils.FontsOverride;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityDestination extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;


    @BindView(R.id.rv_Destination)
    RecyclerView rv_Destination;

    private SQLiteHandler db;
    ModelDestinationList modelDestinationList;
    Context mContext;
    Activity activity;
    ArrayList<ModelDestinationLists> modelDestinationLists = new ArrayList<>();
    AdapterDestinationMaxList adapterDestinationMaxList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);
        ButterKnife.bind(this);
        mContext=this;
        activity=this;
        db = new SQLiteHandler(mContext);
        tv_title.setText(getText(R.string.destination));


        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        modelDestinationLists =db.getCityResponce();


        GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
        FragmentManager fragmentManager1 = getSupportFragmentManager();
        adapterDestinationMaxList = new AdapterDestinationMaxList(activity, modelDestinationLists, activity, fragmentManager1);
        rv_Destination.setLayoutManager(manager1);
        rv_Destination.setAdapter(adapterDestinationMaxList);

    }
}
