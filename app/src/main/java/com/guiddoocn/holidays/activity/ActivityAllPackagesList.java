package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterAllListHotels;
import com.guiddoocn.holidays.adapter.AdapterAllListPackages;
import com.guiddoocn.holidays.models.ModelAllHotelList;
import com.guiddoocn.holidays.models.ModelHotelList;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAllPackagesList extends AppCompatActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;


    @BindView(R.id.rv_allListPackages)
    RecyclerView rv_allListPackages;


    private Context mContext;
    private SessionManager sessionManager;
    private CustomResponseDialog dialog;
    private View v;



    Activity activity;
    ArrayList<ModelAllHotelList> modelAllHotelLists = new ArrayList<>();


    AdapterAllListPackages adapterAllListHotels;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_list_packages);
        ButterKnife.bind(this);
        v = findViewById(R.id.view);
        activity = this;
        mContext = this;

        sessionManager = new SessionManager(mContext);
        dialog = new CustomResponseDialog(mContext);
        tv_title.setText(getText(R.string.all_packages));

        modelAllHotelLists = (ArrayList<ModelAllHotelList>) getIntent().getSerializableExtra("mylist");

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
        FragmentManager fragmentManager1 = getSupportFragmentManager();
        AdapterAllListPackages adapterListHotels = new AdapterAllListPackages(activity, modelAllHotelLists, activity, fragmentManager1,getIntent().getIntExtra("Total_Days",2));
        rv_allListPackages.setLayoutManager(manager1);
        rv_allListPackages.setAdapter(adapterListHotels);


    }

    private void getHotelLIST() {
        dialog.showCustomDialog();
        RequestQueue queue = Volley.newRequestQueue(this);
        //products/hotels?country_id={COUNTRY_ID}&api_key={API_KEY}
        final String url = sessionManager.getBaseUrl() + "products/hotels?country_id="+sessionManager.getCountryCode()+"&api_key=0D1067102F935B4CC31E082BD45014D469E35268";
        Log.e("Hotel url-->", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e("Hotel Response-->", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONObject objectData = jsonObject.getJSONObject("data");
                            JSONArray jsonArray = objectData.getJSONArray("listing_data");
                            for(int i=0;jsonArray.length()>i;i++){
                                ModelAllHotelList hotelList = new ModelAllHotelList();
                                JSONObject objectlist = jsonArray.getJSONObject(i);
                                hotelList.setTitle(objectlist.getString("hotel_name"));
                                hotelList.setCityCountryName(objectlist.getString("city"));
                                hotelList.setLocation(objectlist.getString("location"));
                                hotelList.setAmount(objectlist.getString("starting_from_price"));
                                hotelList.setAddress(objectlist.getString("address"));
                                hotelList.setCurrency(objectlist.getString("currency"));
                                hotelList.setImage(objectlist.getString("featured_image"));
                                hotelList.setRating(objectlist.getString("rating"));
                                modelAllHotelLists.add(hotelList);

                            }

                            /*if(modelAllHotelLists.size()>0){
                                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                                FragmentManager fragmentManager1 = getSupportFragmentManager();
                                adapterAllListHotels = new AdapterAllListHotels(activity, modelAllHotelLists, activity, fragmentManager1);
                                rv_allListHotel.setLayoutManager(manager1);
                                rv_allListHotel.setAdapter(adapterAllListHotels);
                            }else {
                                Snackbar.make(v, "Hotel not found.", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }*/

                        } catch (JSONException e) {
                            Snackbar.make(v, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );

        queue.add(getRequest);
    }
}
