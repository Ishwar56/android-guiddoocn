package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelActivitiesList;
import com.guiddoocn.holidays.models.ModelMyBooking;
import com.guiddoocn.holidays.models.ModelSubCategoryPreferences;
import com.guiddoocn.holidays.models.ModelSubDiningPreferences;
import com.guiddoocn.holidays.utils.AppConstants;
import com.guiddoocn.holidays.utils.AvenuesParams;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;



public class StatusActivity extends Activity  {


    @BindView(R.id.cv_done)
    CardView cv_done;

    @BindView(R.id.tv_try_again)
    TextView tv_try_again;

    @BindView(R.id.failed_linear_parent)
    LinearLayout failedLinearLayout;

    @BindView(R.id.success_linear_parent)
    LinearLayout successLinearLayout;

    String statusTrans = "";
    Intent mainIntent;
    int Status,Paymaent_way;
    boolean usePromoCode = false;
    SessionManager sessionManager;
    private CustomResponseDialog dialog;
    private boolean flag=true,APICallFlag=false,flagConfirm=true;
    private Context mContext;
    private int CountHit=0;
    private SQLiteHandler db;
    private ArrayList<ModelMyBooking> arrayModelMyBookings;
    private View v;
    private JSONArray jsonArray = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        ButterKnife.bind(this);
        mContext = this;
        dialog=new CustomResponseDialog(this);
        mainIntent = getIntent();
        v = findViewById(R.id.rl);
        sessionManager = new SessionManager(this);
        db = new SQLiteHandler(mContext);
        statusTrans = mainIntent.getStringExtra("transStatus");

        if (statusTrans.equals("failure"))
        {
            Status = 2;
            failedLinearLayout.setVisibility(View.VISIBLE);
            successLinearLayout.setVisibility(View.GONE);
        } else if (statusTrans.equals("success")) {
            Status = 1;
            failedLinearLayout.setVisibility(View.GONE);
            successLinearLayout.setVisibility(View.VISIBLE);
        } else {
            Status = 2;
            failedLinearLayout.setVisibility(View.VISIBLE);
            successLinearLayout.setVisibility(View.GONE);
        }

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        TextView orderTv = (TextView) findViewById(R.id.success_parent_orderid_text);
        orderTv.setText(mainIntent.getStringExtra("orderId"));
        arrayModelMyBookings = (ArrayList<ModelMyBooking>) getIntent().getSerializableExtra("CartListDetailModels");
        Paymaent_way = Integer.parseInt(getIntent().getStringExtra("payment_way"));
        if(Paymaent_way==0 || Paymaent_way==1 || Paymaent_way==3){
            if(arrayModelMyBookings.size()>0 && arrayModelMyBookings !=null){
                for(int i = 0; arrayModelMyBookings.size()>i; i++){
                    try {
                        JSONObject jsonObject1 = new JSONObject();
                        jsonObject1.put("activity_date", arrayModelMyBookings.get(i).getActivity_date());
                        jsonObject1.put("activity_id", new BigDecimal(arrayModelMyBookings.get(i).getActivity_id()));
                        jsonObject1.put("cart_id", new BigDecimal(arrayModelMyBookings.get(i).getCart_id()));
                        jsonObject1.put("activity_time", arrayModelMyBookings.get(i).getActivity_time());
                        jsonObject1.put("amount", new BigDecimal(arrayModelMyBookings.get(i).getTotal_amount()));
                        jsonObject1.put("booking_note", "");
                        jsonObject1.put("childAgeStr", "");
                        jsonObject1.put("currency", arrayModelMyBookings.get(i).getActual_currency());

                        JSONArray jsonArrayChild_age = new JSONArray();

                        jsonObject1.put("child_age",jsonArrayChild_age);
                        jsonObject1.put("infantAgeStr","");

                        JSONArray jsonArrayinfant_age = new JSONArray();
                        jsonObject1.put("infant_age", jsonArrayinfant_age);

                        jsonObject1.put("no_of_adult", new BigDecimal(arrayModelMyBookings.get(i).getNo_of_adult()));
                        jsonObject1.put("no_of_child", new BigDecimal(arrayModelMyBookings.get(i).getNo_of_child()));
                        jsonObject1.put("no_of_infant", new BigDecimal(0));
                        jsonObject1.put("no_of_units", new BigDecimal(arrayModelMyBookings.get(i).getNo_of_units()));
                        jsonObject1.put("status", new BigDecimal(Status));
                        jsonObject1.put("use_promotion", usePromoCode);
                        jsonArray.put(jsonObject1);

                    }catch (JSONException e) {

                    }
                }
            }
        }


        cv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StatusActivity.this, ActivityHome.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

            }
        });

        if (statusTrans.equals("failure"))
        {
            Status = 2;
            getRequest();
            failedLinearLayout.setVisibility(View.VISIBLE);
            successLinearLayout.setVisibility(View.GONE);
        } else if (statusTrans.equals("success")) {
            Status = 1;
            getRequest();
            failedLinearLayout.setVisibility(View.GONE);
            successLinearLayout.setVisibility(View.VISIBLE);
        } else {
            Status = 2;
            getRequest();
            failedLinearLayout.setVisibility(View.VISIBLE);
            successLinearLayout.setVisibility(View.GONE);
        }

        tv_try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StatusActivity.this, ActivityMainRazorPayPayment.class);
                i.putExtra(AvenuesParams.BILLING_NAME, mainIntent.getStringExtra(AvenuesParams.BILLING_NAME));
                i.putExtra(AvenuesParams.BILLING_ADDRESS, mainIntent.getStringExtra(AvenuesParams.BILLING_ADDRESS));
                i.putExtra(AvenuesParams.BILLING_TEL, mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
                i.putExtra(AvenuesParams.DELIVERY_TEL, mainIntent.getStringExtra(AvenuesParams.DELIVERY_TEL));
                i.putExtra(AvenuesParams.BILLING_ZIP, mainIntent.getStringExtra(AvenuesParams.BILLING_ZIP));
                i.putExtra(AvenuesParams.DELIVERY_ZIP, mainIntent.getStringExtra(AvenuesParams.DELIVERY_ZIP));
                i.putExtra(AvenuesParams.BILLING_CITY, mainIntent.getStringExtra(AvenuesParams.BILLING_CITY));
                i.putExtra(AvenuesParams.DELIVERY_CITY, mainIntent.getStringExtra(AvenuesParams.DELIVERY_CITY));
                i.putExtra(AvenuesParams.DELIVERY_STATE, mainIntent.getStringExtra(AvenuesParams.DELIVERY_STATE));
                i.putExtra(AvenuesParams.BILLING_STATE, mainIntent.getStringExtra(AvenuesParams.BILLING_STATE));
                i.putExtra(AvenuesParams.DELIVERY_COUNTRY, mainIntent.getStringExtra(AvenuesParams.DELIVERY_COUNTRY));
                i.putExtra(AvenuesParams.BILLING_COUNTRY, mainIntent.getStringExtra(AvenuesParams.BILLING_COUNTRY));
                i.putExtra(AvenuesParams.BILLING_EMAIL, mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
                i.putExtra(AvenuesParams.DELIVERY_NAME, mainIntent.getStringExtra(AvenuesParams.DELIVERY_NAME));
                i.putExtra(AvenuesParams.DELIVERY_ADDRESS, mainIntent.getStringExtra(AvenuesParams.DELIVERY_ADDRESS));
                i.putExtra(AppConstants.amount, mainIntent.getStringExtra(AppConstants.amount));
                i.putExtra("CartListDetailModels", (ArrayList<ModelMyBooking>) arrayModelMyBookings);
                i.putExtra("curruncy", mainIntent.getStringExtra(AppConstants.curruncy));
                i.putExtra("payment_way",mainIntent.getStringExtra("payment_way"));
                i.putExtra(AppConstants.payment_Type,mainIntent.getStringExtra(AppConstants.payment_Type));
                startActivity(i);
                finish();
            }
        });
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(StatusActivity.this, ActivityHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();


    }


    private void getRequest() {
        dialog.showCustomDialog();
        //http://prodapi.wayrtoo.com/Wayrtoo_Service.svc/products/booking/create
        String url = sessionManager.getBaseUrl() + "products/booking/create";
        Log.e("URL", "Booking Create---->" + url);

        APICallFlag=true;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("booking_reference_no",mainIntent.getStringExtra("orderId"));
            jsonObject.put("buyer_address",mainIntent.getStringExtra(AvenuesParams.BILLING_ADDRESS));
            jsonObject.put("buyer_email",mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            jsonObject.put("buyer_name",mainIntent.getStringExtra(AvenuesParams.BILLING_NAME));
            jsonObject.put("buyer_phone",mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
            if(mainIntent.getStringExtra(AppConstants.payment_Type).equalsIgnoreCase("0")){
                jsonObject.put("crypto_currency",0);
                jsonObject.put("crypto_value",0);
                jsonObject.put("payment_mode",1);
            }else if(mainIntent.getStringExtra(AppConstants.payment_Type).equalsIgnoreCase("1")){
                jsonObject.put("crypto_currency",0);
                jsonObject.put("crypto_value", 0);
                jsonObject.put("payment_mode",2);
            }else if(mainIntent.getStringExtra(AppConstants.payment_Type).equalsIgnoreCase("2")){
                jsonObject.put("crypto_currency",0);
                jsonObject.put("crypto_value", 0);
                jsonObject.put("payment_mode",1);
            }
            jsonObject.put("currency",mainIntent.getStringExtra(AppConstants.curruncy));
            jsonObject.put("extra_charges",0);
            jsonObject.put("payment_way",new BigDecimal(Paymaent_way));
            jsonObject.put("products_booked", jsonArray);
            jsonObject.put("promotion_discount", new BigDecimal(0));
            jsonObject.put("status", new BigDecimal(Status));
            jsonObject.put("total_amount", new BigDecimal(mainIntent.getStringExtra(AppConstants.amount)));
            jsonObject.put("transaction_amount", new BigDecimal(mainIntent.getStringExtra(AppConstants.amount)));
            jsonObject.put("user_id", new BigDecimal(sessionManager.getUserId()));
            jsonObject.put("voucher_link", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e("Succecss response ", "Status---->  " + response.toString());
                        String Status_message;
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONObject jsonStatus = jsonObject.getJSONObject("status");

                            Status_message = jsonStatus.getString("Status_message");
                            if (jsonStatus.getString("status_code").equalsIgnoreCase("200")) {
                                dialog.hideCustomeDialog();
                                Snackbar.make(v, Status_message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }else if (jsonStatus.getString("status_code").equalsIgnoreCase("500")) {
                                Log.e("Goes inside Exception","--------");
                                OnError();
                            }else{
                                Snackbar.make(v, Status_message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //OnError();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }



    @Override
    protected void onPause() {
        super.onPause();
    }



    public void OnError(){
        final Dialog dialogMsg = new Dialog(StatusActivity.this);
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.faluire_dialog);
        dialogMsg.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMsg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMsg.getWindow().setAttributes(lp);
        dialogMsg.show();

        TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_text);

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
                APICallFlag=false;
                CountHit++;
                if(CountHit<2)
                    getRequest();
            }
        });
    }

}
