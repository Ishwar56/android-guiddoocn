package com.guiddoocn.holidays.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterCancellationPolicy;
import com.guiddoocn.holidays.adapter.AdapterOperationalTiming;
import com.guiddoocn.holidays.models.ModelActivitiesCancellatioPolicy;
import com.guiddoocn.holidays.models.ModelActivitiesDetails;
import com.guiddoocn.holidays.models.ModelActivitiesOperationTiming;
import com.guiddoocn.holidays.models.ModelPriceMain;
import com.guiddoocn.holidays.models.ModelImagesList;
import com.guiddoocn.holidays.models.ModelSubCategoryPreferences;
import com.guiddoocn.holidays.models.ModelSubDiningPreferences;
import com.guiddoocn.holidays.models.ModelTiming;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.GPSTracker;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;



public class ActivityCustomeEventDetails extends AppCompatActivity implements LocationListener, OnMapReadyCallback {

    @BindView(R.id.tv_toolbar_title)
    TextView tv_toolbar_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.tv_city)
    TextView tv_city;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tv_price)
    TextView tv_price;

    @BindView(R.id.tv_duration)
    TextView tv_duration;

    @BindView(R.id.tv_short_description)
    TextView tv_short_description;

    @BindView(R.id.rb_rating)
    RatingBar rb_rating;

    @BindView(R.id.tv_available_for_child)
    TextView tv_available_for_child;

    @BindView(R.id.tv_children_age_range)
    TextView tv_children_age_range;

    @BindView(R.id.tv_long_description)
    TextView tv_long_description;

    @BindView(R.id.redirect_direction)
    TextView redirect_direction;

    @BindView(R.id.read_more)
    ImageView read_more;

    @BindView(R.id.slider)
    SliderLayout sliderLayout;


    @BindView(R.id.tv_available_today)
    TextView tv_available_today;

    @BindView(R.id.tv_available_today_duration)
    TextView tv_available_today_duration;

    @BindView(R.id.tv_available_today_read_more)
    ImageView tv_available_today_read_more;

    @BindView(R.id.tv_cancellation_policy)
    ImageView tv_cancellation_policy;

    @BindView(R.id.tv_inclusion)
    TextView tv_inclusion;

    @BindView(R.id.tv_inclusion_read_more)
    ImageView tv_inclusion_read_more;

    @BindView(R.id.tv_highlights)
    TextView tv_highlights;

    @BindView(R.id.tv_highlights_read_more)
    ImageView tv_highlights_read_more;

    @BindView(R.id.collapsing_tool)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.abl_main)
    AppBarLayout abl_main;

    @BindView(R.id.ll_family)
    LinearLayout ll_family;
    @BindView(R.id.ll_adventure)
    LinearLayout ll_adventure;
    @BindView(R.id.ll_kids)
    LinearLayout ll_kids;
    @BindView(R.id.ll_romantic)
    LinearLayout ll_romantic;
    @BindView(R.id.ll_culturearts)
    LinearLayout ll_culturearts;
    @BindView(R.id.ll_luxury)
    LinearLayout ll_luxury;
    @BindView(R.id.ll_most_visited)
    LinearLayout ll_most_visited;
    @BindView(R.id.ll_jainfood)
    LinearLayout ll_jainfood;
    @BindView(R.id.ll_veg)
    LinearLayout ll_veg;
    @BindView(R.id.ll_no_dinings)
    LinearLayout ll_no_dinings;
    @BindView(R.id.ll_western)
    LinearLayout ll_western;
    @BindView(R.id.ll_halal)
    LinearLayout ll_halal;
    @BindView(R.id.ll_nonveg)
    LinearLayout ll_nonveg;
    @BindView(R.id.ll_excursiononly)
    LinearLayout ll_excursiononly;
    @BindView(R.id.ll_withtransfer)
    LinearLayout ll_withtransfer;


    private String Event_ID,Category_ID,Event_Name;
    private JSONObject jsonStatus;
    private CustomResponseDialog dialog;
    private Context mContext;
    private SessionManager sessionManager;
    private GPSTracker gpsTracker;
    private Calendar myCalendar;
    private int mHour, mMinute;
    private CustomResponseDialog customResponseDialog;
    private ArrayList<ModelActivitiesDetails> activitiesDetailstModels;
    private ArrayList<ModelActivitiesOperationTiming> activitiesOperationTiming;
    private ArrayList<ModelActivitiesCancellatioPolicy> cancellatioPolicyModels;
    private ArrayList<ModelSubDiningPreferences> modelSubDiningPreferences;
    private ArrayList<ModelSubCategoryPreferences> modelSubCategoryPreferences;
    private ArrayList<ModelTiming> modelTimings;
    private ArrayList<ModelPriceMain> pricingDetailsModels;
    private ArrayList<ModelImagesList> modelImagesLists;
    private ArrayList<String> arrayListHighLight;
    private ArrayList<String> arrayListInclusion;
    boolean isTextViewClicked = false, FlagDate = false, FlagTime = false, isInclusionClicked = false, isHighLightClicked = false, FlagWishlist = false;
    private double latitude, longitude, Adult_Total_Val = 0.0, Child_Total_Val = 0.0, Final_Cost = 0.0;
    private int adult_no = 1, child_no = 0, Position;
    private MarkerOptions markerOptions;
    private IconGenerator iconFactory;
    private SupportMapFragment mMapView;
    private boolean FlagResponce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        mContext = this;
        ButterKnife.bind(this);
        sessionManager = new SessionManager(mContext);
        customResponseDialog = new CustomResponseDialog(mContext);
        gpsTracker = new GPSTracker(mContext, this);
        dialog = new CustomResponseDialog(mContext);
        myCalendar = Calendar.getInstance();
        Bundle b = getIntent().getExtras();
        mMapView = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        if (b != null) {
            Event_ID = getIntent().getStringExtra("EventID");

        } else {
            Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
        }
        DateFormat formatter1;
        formatter1 = new SimpleDateFormat("dd/MM/yyyy");


        if (gpsTracker.isNetworkAvailable()) {
            getActivityDetails();
        } else {
            Snackbar.make(findViewById(R.id.coordinate_container), "Please check your internet connection.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }



        abl_main.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0 || verticalOffset <= toolbar.getHeight() && !toolbar.getTitle().equals(Event_Name)) {
                    tv_toolbar_title.setVisibility(View.INVISIBLE);
                    iv_back.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                } else {
                    tv_toolbar_title.setVisibility(View.VISIBLE);
                    tv_toolbar_title.setText(Event_Name);
                    iv_back.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite), PorterDuff.Mode.SRC_IN);
                }
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        read_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTextViewClicked) {
                    tv_long_description.setMaxLines(2);
                    isTextViewClicked = false;
                } else {
                    tv_long_description.setMaxLines(Integer.MAX_VALUE);
                    isTextViewClicked = true;
                }
            }
        });

        tv_inclusion_read_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInclusionClicked) {
                    tv_inclusion.setMaxLines(2);
                    isInclusionClicked = false;
                } else {
                    tv_inclusion.setMaxLines(Integer.MAX_VALUE);
                    isInclusionClicked = true;
                }
            }
        });

        tv_highlights_read_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isHighLightClicked) {
                    tv_highlights.setMaxLines(2);
                    isHighLightClicked = false;
                } else {
                    tv_highlights.setMaxLines(Integer.MAX_VALUE);
                    isHighLightClicked = true;
                }
            }
        });



       /* redirect_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityViewSingleMapDirection.class);
                intent.putExtra("ListDetailModels", (ArrayList<ModelActivitiesDetails>) activitiesDetailstModels);
                intent.putExtra("Category_ID", "");
                startActivity(intent);
                Bungee.split(mContext);
            }
        });*/

        tv_available_today_read_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (activitiesOperationTiming.size() > 0) {
                        final Dialog dialogMsg = new Dialog(mContext);
                        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogMsg.setContentView(R.layout.event_detail_operation_timing_dialog);
                        dialogMsg.setCancelable(true);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialogMsg.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.gravity = Gravity.CENTER;
                        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialogMsg.getWindow().setAttributes(lp);
                        dialogMsg.show();

                        RecyclerView rv_list = (RecyclerView) dialogMsg.findViewById(R.id.rv_list);
                        ImageView cardViewCancel = (ImageView) dialogMsg.findViewById(R.id.imageView_close);
                        rv_list.setNestedScrollingEnabled(false);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                        rv_list.setLayoutManager(mLayoutManager);
                        AdapterOperationalTiming timingAdapter = new AdapterOperationalTiming(getApplicationContext(), activitiesOperationTiming);
                        rv_list.setAdapter(timingAdapter);

                        cardViewCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogMsg.cancel();
                            }
                        });

                    } else {
                        Snackbar.make(findViewById(R.id.coordinate_container), "Operational timing record not found. Please try again later.", Snackbar.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Snackbar.make(findViewById(R.id.coordinate_container), "Operational timing record not found. Please reload page.", Snackbar.LENGTH_LONG).show();
                }

            }
        });

        tv_cancellation_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (cancellatioPolicyModels.size() > 0) {

                        final Dialog dialogMsg = new Dialog(mContext);
                        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogMsg.setContentView(R.layout.dialog_event_detail_cancellation_policy);
                        dialogMsg.setCancelable(true);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialogMsg.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.gravity = Gravity.CENTER;
                        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialogMsg.getWindow().setAttributes(lp);
                        dialogMsg.show();

                        RecyclerView rv_list = (RecyclerView) dialogMsg.findViewById(R.id.rv_list);
                        ImageView cardViewCancel = (ImageView) dialogMsg.findViewById(R.id.imageView_close);
                        rv_list.setNestedScrollingEnabled(false);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                        rv_list.setLayoutManager(mLayoutManager);
                        AdapterCancellationPolicy cancelAdapter = new AdapterCancellationPolicy(getApplicationContext(), cancellatioPolicyModels);
                        rv_list.setAdapter(cancelAdapter);

                        cardViewCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogMsg.cancel();
                            }
                        });

                    } else {
                        Snackbar.make(findViewById(R.id.coordinate_container), "Cancellation policy record not found. Please try again later.", Snackbar.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Snackbar.make(findViewById(R.id.coordinate_container), "Cancellation policy record not found. Please reload page.", Snackbar.LENGTH_LONG).show();
                }



            }
        });


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            //mMapView.onCreate();
            mMapView.onResume();
            googleMap.getUiSettings().setScrollGesturesEnabled(true);
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
            final LatLng currentPos = new LatLng(Double.valueOf(activitiesDetailstModels.get(0).getLatitude()), Double.valueOf(activitiesDetailstModels.get(0).getLongitude()));

            googleMap.clear();
            iconFactory = new IconGenerator(mContext);
            LatLng sydney = new LatLng(latitude, longitude);
            String add = getAddress(latitude, longitude);
            iconFactory.setStyle(IconGenerator.STYLE_GREEN);
            markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(add))).position(sydney).anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());
            googleMap.addMarker(markerOptions);

            LatLng sydney1 = new LatLng(Double.valueOf(activitiesDetailstModels.get(0).getLatitude()), Double.valueOf(activitiesDetailstModels.get(0).getLongitude()));
            markerOptions = new MarkerOptions().position(sydney1);
            markerOptions.title(activitiesDetailstModels.get(0).getName());



            if (Category_ID.equalsIgnoreCase("1")) {
                markerOptions.icon(bitmapDescriptorFromVector(mContext, R.drawable.ticket_click));
            } else if (Category_ID.equalsIgnoreCase("2")) {
                markerOptions.icon(bitmapDescriptorFromVector(mContext, R.drawable.activities_click));
            } else if (Category_ID.equalsIgnoreCase("3")) {
                markerOptions.icon(bitmapDescriptorFromVector(mContext, R.drawable.dining_click));
            } else if (Category_ID.equalsIgnoreCase("4")) {
                markerOptions.icon(bitmapDescriptorFromVector(mContext, R.drawable.entertainment_click));
            } else if (Category_ID.equalsIgnoreCase("5")) {
                markerOptions.icon(bitmapDescriptorFromVector(mContext, R.drawable.super_saver));
            }
            googleMap.addMarker(markerOptions);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentPos).zoom(10).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        } catch (Exception e) {
        }

    }

    private String getAddress(double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                // result.append(address.getAddressLine(1)).append("\n");
                result.append(address.getLocality() + " " + address.getCountryName());
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    private void getActivityDetails() {
        dialog.showCustomDialog();
        //https://prodapi.wayrtoo.com/Wayrtoo_Service.svc/
        //http://prodapi.wayrtoo.com/Wayrtoo_Service.svc/products/activities/details?tour_id={TOURID}&api_key={API_KEY}
        String url = sessionManager.getBaseUrl() +"products/activities/details?tour_id="+ Event_ID + "&api_key=0D1067102F935B4CC31E082BD45014D469E35268";
        Log.e("URL", "list_activities URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("category_id", "0");
            object.put("city_id", 0);
            object.put("country_id",sessionManager.getCountryCode());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e("Responce", "list_activities Response---->" + response.toString());

                        try {
                            new CallData().execute(response.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }



    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        FlagDate = false;
    }

    private String getTime(int hr, int min) {
        Time tme = new Time(hr, min, 0);//seconds by default set to zero
        SimpleDateFormat formatShort = new SimpleDateFormat("hh:mm aa", Locale.US);
        return formatShort.format(tme);
    }

    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }


    private class CallData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String s = params[0];

            Log.e("Activity Detail Resp-->", s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonData = jsonObject.getJSONObject("data");
                jsonStatus = jsonData.getJSONObject("status");

                if (jsonStatus.getString("Success").equalsIgnoreCase("true")) {

                    if (jsonStatus.getString("status_code").equalsIgnoreCase("200")) {

                        activitiesDetailstModels = new ArrayList<ModelActivitiesDetails>();

                        ModelActivitiesDetails listModel = new ModelActivitiesDetails();
                        JSONObject jsonActivity = jsonData.getJSONObject("activityDetail");
                        listModel.setAddress(jsonActivity.getString("address"));
                        listModel.setCategory(jsonActivity.getString("category"));
                        Category_ID=jsonActivity.getString("category_id");
                        listModel.setCity(jsonActivity.getString("city"));
                        listModel.setCurrency(jsonActivity.getString("currency"));

                        JSONObject jsonDuration = jsonActivity.getJSONObject("duration");
                        listModel.setDay(jsonDuration.getString("day"));
                        listModel.setHour(jsonDuration.getString("hour"));
                        listModel.setMin(jsonDuration.getString("min"));

                        JSONObject jsonChildAge = jsonActivity.getJSONObject("child_age");
                        listModel.setChild_max_age(jsonChildAge.getString("max_age"));
                        listModel.setChild_min_age(jsonChildAge.getString("min_age"));


                        listModel.setFeatured_image(jsonActivity.getString("featured_image"));
                        listModel.setName(jsonActivity.getString("name"));
                        listModel.setRating(jsonActivity.getString("rating"));
                        listModel.setShort_description(jsonActivity.getString("short_description"));
                        listModel.setStarting_from_price(getIsMarginPrice(jsonActivity.getString("starting_from_price")));
                        listModel.setDescription(jsonActivity.getString("description"));
                        listModel.setAvailable_for_child(jsonActivity.getString("available_for_child"));
                        listModel.setLatitude(jsonActivity.getString("latitude"));
                        listModel.setLongitude(jsonActivity.getString("longitude"));

                        JSONArray jsonArrayHighlight = jsonActivity.getJSONArray("highlights");
                        arrayListHighLight = new ArrayList<String>();
                        for (int l = 0; l < jsonArrayHighlight.length(); l++) {
                            arrayListHighLight.add(jsonArrayHighlight.get(l).toString());
                        }
                        listModel.setHighlights(arrayListHighLight);

                        JSONArray jsonArrayInclision = jsonActivity.getJSONArray("inclusion");
                        arrayListInclusion = new ArrayList<String>();
                        for (int l = 0; l < jsonArrayInclision.length(); l++) {
                            arrayListInclusion.add(jsonArrayInclision.get(l).toString());
                        }
                        listModel.setInclusion(arrayListInclusion);


                        JSONArray cancellation_policy = jsonActivity.getJSONArray("cancellation_policy");
                        cancellatioPolicyModels = new ArrayList<ModelActivitiesCancellatioPolicy>();
                        for (int k = 0; k < cancellation_policy.length(); k++) {
                            JSONObject activityJSON = cancellation_policy.getJSONObject(k);
                            ModelActivitiesCancellatioPolicy cancellatioPolicyModel = new ModelActivitiesCancellatioPolicy();
                            cancellatioPolicyModel.setCharge(activityJSON.getString("charge"));
                            cancellatioPolicyModel.setChargeable_by_percentage(activityJSON.getString("chargeable_by_percentage"));
                            cancellatioPolicyModel.setFrom_hr(activityJSON.getString("from_hr"));
                            cancellatioPolicyModel.setTo_hr(activityJSON.getString("to_hr"));

                            cancellatioPolicyModels.add(cancellatioPolicyModel);
                        }

                        listModel.setCancellatioPolicyModels(cancellatioPolicyModels);


                        JSONArray images = jsonActivity.getJSONArray("images");
                        modelImagesLists = new ArrayList<ModelImagesList>();
                        for (int k = 0; k < images.length(); k++) {
                            JSONObject activityJSON = images.getJSONObject(k);
                            ModelImagesList imagesModel = new ModelImagesList();
                            imagesModel.setDisplay_Position(activityJSON.getString("Display_Position"));
                            imagesModel.setImagePath(activityJSON.getString("ImagePath"));
                            modelImagesLists.add(imagesModel);
                        }

                        listModel.setModelImagesLists(modelImagesLists);


                        JSONArray activityListDay = jsonActivity.getJSONArray("operational_timings");
                        activitiesOperationTiming = new ArrayList<ModelActivitiesOperationTiming>();
                        modelTimings = new ArrayList<ModelTiming>();
                        for (int k = 0; k < activityListDay.length(); k++) {
                            JSONObject activityJSON = activityListDay.getJSONObject(k);
                            ModelActivitiesOperationTiming operationTimingModel = new ModelActivitiesOperationTiming();
                            operationTimingModel.setDay(activityJSON.getString("Day"));
                            JSONArray TourTimings = activityJSON.getJSONArray("TourTimings");
                            for (int j = 0; j < TourTimings.length(); j++) {
                                JSONObject activitytime = TourTimings.getJSONObject(j);
                                operationTimingModel.setFrom_Time(activitytime.getString("From_Time"));
                                operationTimingModel.setTo_Time(activitytime.getString("To_Time"));
                            }
                            JSONArray bookingtime = activityJSON.getJSONArray("booking_timings");

                            for (int j = 0; j < bookingtime.length(); j++) {
                                ModelTiming modelTiming = new ModelTiming();
                                modelTiming.setTiming(bookingtime.get(j).toString());
                                modelTimings.add(modelTiming);
                            }
                            operationTimingModel.setBooking_timings(modelTimings);
                            activitiesOperationTiming.add(operationTimingModel);
                        }

                        JSONArray Arraydinning_preference = jsonActivity.getJSONArray("dinning_preference");
                        modelSubDiningPreferences = new ArrayList<>();
                        for (int dp = 0;Arraydinning_preference.length()>dp;dp++){
                            ModelSubDiningPreferences diningpreferencesModel = new ModelSubDiningPreferences();
                            JSONObject jsonDinigPref = Arraydinning_preference.getJSONObject(dp);
                            diningpreferencesModel.setSubdiningid(jsonDinigPref.getString("id"));
                            diningpreferencesModel.setSubdiningName(jsonDinigPref.getString("name"));
                            modelSubDiningPreferences.add(diningpreferencesModel);
                        }
                        listModel.setModelSubDiningPreferences(modelSubDiningPreferences);

                        JSONArray Arraysubcategory_name = jsonActivity.getJSONArray("subcategory_name");
                        modelSubCategoryPreferences = new ArrayList<>();
                        for (int dp = 0;Arraysubcategory_name.length()>dp;dp++){
                            ModelSubCategoryPreferences categorypreferencesModel = new ModelSubCategoryPreferences();
                            JSONObject jsonDinigPref = Arraysubcategory_name.getJSONObject(dp);
                            categorypreferencesModel.setSubcategoryid(jsonDinigPref.getString("id"));
                            categorypreferencesModel.setSubcategoryName(jsonDinigPref.getString("name"));
                            modelSubCategoryPreferences.add(categorypreferencesModel);
                        }
                        listModel.setModelSubCategoryPreferences(modelSubCategoryPreferences);

                        JSONObject json_release_period = jsonActivity.getJSONObject("release_period");
                        listModel.setReleaseDay(json_release_period.getString("day"));
                        listModel.setTransfer_included(jsonActivity.getString("transfer_included"));

                        listModel.setOperationTimingModels(activitiesOperationTiming);

                        activitiesDetailstModels.add(listModel);

                    } else {
                        Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong.", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }

                } else {
                    Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            } catch (JSONException e) {
                Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                e.printStackTrace();
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                if (jsonStatus.getString("Success").equalsIgnoreCase("true")) {
                    mMapView.getMapAsync(ActivityCustomeEventDetails.this);
                    ShowCategoriedAndDining();
                    tv_city.setText(activitiesDetailstModels.get(0).getCity());
                    tv_title.setText(activitiesDetailstModels.get(0).getName());
                    rb_rating.setRating(Float.parseFloat(activitiesDetailstModels.get(0).getRating()));
                    //tv_rating.setText(activitiesDetailstModels.get(0).getRating() + " Ratings");
                    tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + activitiesDetailstModels.get(0).getStarting_from_price());
                    if (activitiesDetailstModels.get(0).getDay().equalsIgnoreCase("00")) {
                        tv_duration.setText("Duration - " + activitiesDetailstModels.get(0).getHour() + " hr " + activitiesDetailstModels.get(0).getMin() + " min");
                    } else {
                        tv_duration.setText("Duration - " + activitiesDetailstModels.get(0).getDay() + " day " + activitiesDetailstModels.get(0).getHour() + " hr " + activitiesDetailstModels.get(0).getMin() + " min");
                    }
                    tv_short_description.setText(activitiesDetailstModels.get(0).getShort_description());
                    if (activitiesDetailstModels.get(0).getAvailable_for_child().equalsIgnoreCase("true")) {
                        tv_available_for_child.setTextColor(Color.parseColor("#347038"));
                        tv_available_for_child.setText("Yes");
                    } else {
                        tv_available_for_child.setTextColor(Color.parseColor("#DB6556"));
                        tv_available_for_child.setText("No");
                    }
                    tv_children_age_range.setText(activitiesDetailstModels.get(0).getChild_min_age() + " to " + activitiesDetailstModels.get(0).getChild_max_age());
                    tv_long_description.setText(Html.fromHtml(activitiesDetailstModels.get(0).getDescription()));

                    if (arrayListInclusion.size() > 0 && arrayListInclusion != null) {
                        String toPrint = "";
                        for (int i = 0; i < arrayListInclusion.size(); i++) {
                            toPrint += i + 1 + ") " + arrayListInclusion.get(i) + "\n";
                        }
                        tv_inclusion.setText(toPrint);
                    }

                    if (arrayListHighLight.size() > 0 && arrayListHighLight != null) {
                        StringBuffer Highlite = new StringBuffer();
                        for (int i = 0; i < arrayListHighLight.size(); i++) {
                            Highlite.append(i + 1 + ") " + arrayListHighLight.get(i).toString() + ".").append('\n');
                        }
                        tv_highlights.setText(Highlite.toString());
                    }

                    if (activitiesOperationTiming.size() > 0 && activitiesOperationTiming != null) {
                        tv_available_today.setText("Start Booking at " + activitiesOperationTiming.get(0).getBooking_timings().get(0).getTiming());
                    }

                    if(activitiesDetailstModels.get(0).getTransfer_included().equalsIgnoreCase("true")){
                        ll_withtransfer.setVisibility(View.VISIBLE);
                        ll_excursiononly.setVisibility(View.GONE);
                    }else{
                        ll_withtransfer.setVisibility(View.GONE);
                        ll_excursiononly.setVisibility(View.VISIBLE);
                    }

                    if (activitiesDetailstModels.get(0).getDay().equalsIgnoreCase("00")) {
                        tv_available_today_duration.setText("Duration- " + activitiesDetailstModels.get(0).getHour() + " hr " + activitiesDetailstModels.get(0).getMin() + " min");
                    } else {
                        tv_available_today_duration.setText("Duration- " + activitiesDetailstModels.get(0).getDay() + " day " + activitiesDetailstModels.get(0).getHour() + " hr " + activitiesDetailstModels.get(0).getMin() + " min");
                    }


                    if (modelImagesLists.size() > 0 && modelImagesLists != null) {
                        for(int i = 0; i< modelImagesLists.size(); i++){
                            TextSliderView textSliderView = new TextSliderView(mContext);
                            textSliderView
                                    .description("")
                                    .image(modelImagesLists.get(i).getImagePath())
                                    .empty(R.drawable.place_holder)
                                    .error(R.drawable.place_holder)
                                    .setScaleType(BaseSliderView.ScaleType.Fit);
                            textSliderView.bundle(new Bundle());
                            textSliderView.getBundle()
                                    .putString("extra", "");
                            sliderLayout.addSlider(textSliderView);
                        }

                        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                        sliderLayout.setCustomAnimation(new DescriptionAnimation());
                        sliderLayout.setDuration(3000);
                    }

                } else {
                    Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
        }

    }
    public String getIsMarginPrice(String Main_price) {
        try{
            String data = null;
            double discount = Double.parseDouble(sessionManager.getIsmargin());
            double Old_price = Double.parseDouble(Main_price);
            double New_price = Old_price - (Old_price * discount / 100);
            data = String.format("%.02f", New_price );
            return data;
        }catch (Exception e){
            return Main_price;
        }


    }

    private void ShowCategoriedAndDining(){

        if(activitiesDetailstModels.get(0).getModelSubCategoryPreferences().size()>0 && !activitiesDetailstModels.get(0).getModelSubCategoryPreferences().isEmpty()){

            for (int i =0 ;activitiesDetailstModels.get(0).getModelSubCategoryPreferences().size()>i;i++){
                if(activitiesDetailstModels.get(0).getModelSubCategoryPreferences().get(i).getSubcategoryName().equalsIgnoreCase("Family")){
                    ll_family.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubCategoryPreferences().get(i).getSubcategoryName().equalsIgnoreCase("Adventure")){
                    ll_adventure.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubCategoryPreferences().get(i).getSubcategoryName().equalsIgnoreCase("Kids")){
                    ll_kids.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubCategoryPreferences().get(i).getSubcategoryName().equalsIgnoreCase("Romantic")){
                    ll_romantic.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubCategoryPreferences().get(i).getSubcategoryName().equalsIgnoreCase("Culture & Arts")){
                    ll_culturearts.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubCategoryPreferences().get(i).getSubcategoryName().equalsIgnoreCase("Luxury")){
                    ll_luxury.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubCategoryPreferences().get(i).getSubcategoryName().equalsIgnoreCase("Most Visited")){
                    ll_most_visited.setVisibility(View.VISIBLE);
                }
            }

        }

        if(activitiesDetailstModels.get(0).getModelSubDiningPreferences().size()>0 && !activitiesDetailstModels.get(0).getModelSubDiningPreferences().isEmpty()){
            for (int i =0 ;activitiesDetailstModels.get(0).getModelSubDiningPreferences().size()>i;i++){
                if(activitiesDetailstModels.get(0).getModelSubDiningPreferences().get(i).getSubdiningName().equalsIgnoreCase("Jain")){
                    ll_jainfood.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubDiningPreferences().get(i).getSubdiningName().equalsIgnoreCase("Vegetarian")){
                    ll_veg.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubDiningPreferences().get(i).getSubdiningName().equalsIgnoreCase("Non-vegetarian")){
                    ll_nonveg.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubDiningPreferences().get(i).getSubdiningName().equalsIgnoreCase("Halal")){
                    ll_halal.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubDiningPreferences().get(i).getSubdiningName().equalsIgnoreCase("Western")){
                    ll_western.setVisibility(View.VISIBLE);
                }else if(activitiesDetailstModels.get(0).getModelSubDiningPreferences().get(i).getSubdiningName().equalsIgnoreCase("Without Dining")){
                    ll_no_dinings.setVisibility(View.VISIBLE);
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       finish();
    }

}
