package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterCardDetails;
import com.guiddoocn.holidays.adapter.AdapterHotelsItineraryAllList;
import com.guiddoocn.holidays.adapter.AdapterHotelsItineraryList;
import com.guiddoocn.holidays.adapter.AdapterHotelsList;
import com.guiddoocn.holidays.adapter.AdapterItineraryActivitiesAllList;
import com.guiddoocn.holidays.adapter.AdapterItineraryActivitiesList;
import com.guiddoocn.holidays.models.ModelCardDetailsList;
import com.guiddoocn.holidays.models.ModelItineraryActivitiesList;
import com.guiddoocn.holidays.models.ModelItineraryHotelList;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityItineraryViewAll extends AppCompatActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.tv_Destination)
    TextView tv_Destination;
    @BindView(R.id.tv_Check_in)
    TextView tv_Check_in;
    @BindView(R.id.tv_Check_out)
    TextView tv_Check_out;
    @BindView(R.id.tv_total_pax)
    TextView tv_total_pax;

    @BindView(R.id.ll_add_new_hotel_List)
    LinearLayout ll_add_new_hotel_List;

    @BindView(R.id.ll_add_new_activity_List)
    LinearLayout ll_add_new_activity_List;

    AdapterHotelsList adapterHotelsList;
    ArrayList<ModelItineraryHotelList> modelHotelLists = new ArrayList<>();
    ArrayList<ModelItineraryActivitiesList> modelActivityLists = new ArrayList<>();
    List<ModelCardDetailsList> cardTypeList = new ArrayList<ModelCardDetailsList>();
    @BindView(R.id.sp_cardType)
    Spinner sp_cardType;
    Context mContext;
    Activity activity;

    @BindView(R.id.ll_visa)
    LinearLayout ll_visa;

    @BindView(R.id.tv_hotel_cost_per_Adult)
    TextView tv_hotel_cost_per_Adult;
    @BindView(R.id.tv_hotel_cost_per_Child)
    TextView tv_hotel_cost_per_Child;
    @BindView(R.id.tv_hotel_cost)
    TextView tv_hotel_cost;

    @BindView(R.id.tv_excursion_cost_per_Adult)
    TextView tv_excursion_cost_per_Adult;
    @BindView(R.id.tv_excursion_cost_per_Child)
    TextView tv_excursion_cost_per_Child;
    @BindView(R.id.tv_excursion_cost)
    TextView tv_excursion_cost;

    @BindView(R.id.tv_visa_cost_per_Adult)
    TextView tv_visa_cost_per_Adult;
    @BindView(R.id.tv_visa_cost_per_Child)
    TextView tv_visa_cost_per_Child;
    @BindView(R.id.tv_Visa_cost)
    TextView tv_Visa_cost;

    @BindView(R.id.tv_total_cost_per_Adult)
    TextView tv_total_cost_per_Adult;
    @BindView(R.id.tv_total_cost_per_Child)
    TextView tv_total_cost_per_Child;
    @BindView(R.id.tv_Total_Cost)
    TextView tv_Total_Cost;

    @BindView(R.id.ll_goToExcursions)
    LinearLayout ll_goToExcursions;
    @BindView(R.id.ll_generateQuote)
    LinearLayout ll_generateQuote;

    @BindView(R.id.ll_option)
    LinearLayout ll_option;
    @BindView(R.id.tv_option_Hotels)
    TextView tv_option_Hotels;

    @BindView(R.id.ll_option1)
    LinearLayout ll_option1;
    @BindView(R.id.tv_option1)
    TextView tv_option1;

    @BindView(R.id.ll_option2)
    LinearLayout ll_option2;
    @BindView(R.id.tv_option2)
    TextView tv_option2;

    @BindView(R.id.ll_option3)
    LinearLayout ll_option3;
    @BindView(R.id.tv_option3)
    TextView tv_option3;

    private SQLiteHandler db;
    private SessionManager sessionManager;


    private float total_hotel_cost = 0,total_hotel_cost_option1 = 0,total_hotel_cost_option2 = 0,total_hotel_cost_option3 = 0, total_activity_cost = 0,total_visa_cost=0, final_cost = 0,activity_Adult_cost=0,total_activity_Adult_cost=0,activity_Child_cost=0,total_activity_Child_cost=0,Transport_Charges_Adult=0, Total_transport_cost_adult =0, Total_transport_cost_child =0,Total_Voucher_cost=0;
    private String is_visa_included="false",visa_type,selected_Option="1",mainHint="",selected_Option1="",selected_Option2="",selected_Option3="";
    private boolean flag_Option=false,flag_Option1=false,flag_Option2=false,flag_Option3=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_itinerary_view_all);
        ButterKnife.bind(this);
        mContext = this;
        activity = this;
        db = new SQLiteHandler(mContext);
        sessionManager = new SessionManager(mContext);


        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


      //  tv_title.setText(getText(R.string.itinerarys));
        tv_title.setText("My Itinerary");
        tv_Destination.setText(sessionManager.getCountryName());
        tv_Check_in.setText(sessionManager.getCheckInDate());
        tv_Check_out.setText(sessionManager.getCheckOutDate());
        if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
            tv_total_pax.setText(sessionManager.getAdultMainCount() +" Adults");
        }else{
            tv_total_pax.setText(sessionManager.getAdultMainCount() +" Adults & "+sessionManager.getChildMainCount() +" Child ("+sessionManager.getChildAge()+")");
        }


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_add_new_hotel_List.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityItineraryViewAll.this, ActivityAllListHotels.class);
                startActivity(intent);
                finish();
            }
        });

        ll_add_new_activity_List.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityItineraryViewAll.this, ActivityAllListCustomeActivities.class);
                startActivity(intent);
                finish();
            }
        });


        ll_generateQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // GenerateQuoteDetailsActivity
                if(total_hotel_cost>0){
                    if(total_activity_cost>0){
                        Intent intent = new Intent(mContext, GenerateQuoteDetailsActivity.class);
                        intent.putExtra("Final_Cost",String.format("%.02f", final_cost-total_hotel_cost)) ;
                        intent.putExtra("Total_activity_cost",String.valueOf(String.format("%.02f", total_activity_cost)));
                        intent.putExtra("is_visa_included",is_visa_included);
                        intent.putExtra("visa_cost",String.format("%.02f", total_visa_cost));
                        intent.putExtra("visa_type",visa_type);
                        intent.putExtra("hotel_cost_adult",tv_hotel_cost_per_Adult.getText().toString());
                        intent.putExtra("hotel_cost_child",tv_hotel_cost_per_Child.getText().toString());
                        intent.putExtra("inclusion_cost_adult",tv_excursion_cost_per_Adult.getText().toString());
                        intent.putExtra("inclusion_cost_child",tv_excursion_cost_per_Child.getText().toString());
                        intent.putExtra("total_cost_adult",tv_total_cost_per_Adult.getText().toString());
                        intent.putExtra("total_cost_child",tv_total_cost_per_Child.getText().toString());
                        startActivity(intent);
                    }else{
                        Toast.makeText(activity, "Please add your Activity", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(activity, "Please add your Hotels", Toast.LENGTH_SHORT).show();
                }

                /*if(final_cost>0){
                    Intent intent = new Intent(mContext, GenerateQuoteDetailsActivity.class);
                    intent.putExtra("Final_Cost",String.format("%.02f", final_cost-total_hotel_cost)) ;
                    intent.putExtra("Total_activity_cost",String.valueOf(String.format("%.02f", total_activity_cost)));
                    intent.putExtra("is_visa_included",is_visa_included);
                    intent.putExtra("visa_cost",String.format("%.02f", visa_cost));
                    intent.putExtra("visa_type",visa_type);
                    startActivity(intent);
                }else {
                    Toast.makeText(activity, "Please create your Itinerary", Toast.LENGTH_SHORT).show();
                }*/

            }
        });


        ll_goToExcursions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // GenerateQuoteDetailsActivity
                Intent intent = new Intent(mContext, ActivityAllListCustomeActivities.class);
                startActivity(intent);
                finish();
            }
        });

        ll_option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_option1.setBackgroundResource(R.drawable.custom_bg_red_option);
                tv_option1.setTextColor(Color.parseColor("#FFFFFF"));
                ll_option2.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option2.setTextColor(Color.parseColor("#F66961"));
                ll_option3.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option3.setTextColor(Color.parseColor("#F66961"));
                selected_Option="1";
                Select_Option();
            }
        });

        ll_option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_option1.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option1.setTextColor(Color.parseColor("#F66961"));
                ll_option2.setBackgroundResource(R.drawable.custom_bg_red_option);
                tv_option2.setTextColor(Color.parseColor("#FFFFFF"));
                ll_option3.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option3.setTextColor(Color.parseColor("#F66961"));
                selected_Option="2";
                Select_Option();
            }
        });

        ll_option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_option1.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option1.setTextColor(Color.parseColor("#F66961"));
                ll_option2.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option2.setTextColor(Color.parseColor("#F66961"));
                ll_option3.setBackgroundResource(R.drawable.custom_bg_red_option);
                tv_option3.setTextColor(Color.parseColor("#FFFFFF"));
                selected_Option="3";
                Select_Option();
            }
        });


        modelHotelLists=new ArrayList<>();
        modelHotelLists = db.getItineraryHotelsData(sessionManager.getCountryCode());
        String optionHotels="";
        for (int i = 0; modelHotelLists.size() > i; i++) {
            total_hotel_cost = total_hotel_cost + Float.valueOf(modelHotelLists.get(i).getFinal_cost());

            if(i==0){
                String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle();
                optionHotels =optionHotels + Temp;
                tv_option_Hotels.setText("** Option 1 includes "+optionHotels);
                optionHotels=optionHotels+",";
            }else{
                if(modelHotelLists.size()-1 == i){
                    String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle();
                    optionHotels =optionHotels + Temp;
                    tv_option_Hotels.setText("** Option 1 includes "+optionHotels);
                }else{
                    String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle()+",";
                    optionHotels =optionHotels + Temp;
                    tv_option_Hotels.setText("** Option 1 includes "+optionHotels);
                }
            }

        }

        /* Add Activity */

        modelActivityLists = new ArrayList<>();
        modelActivityLists = db.getItineraryActivityData(sessionManager.getCountryCode());

        for (int i = 0; modelActivityLists.size() > i; i++) {
            total_activity_cost = total_activity_cost + Float.valueOf(modelActivityLists.get(i).getFinal_cost());

            if(!modelActivityLists.get(i).getTransfer_Charges().equalsIgnoreCase("0")){
                Double temp = Double.valueOf(modelActivityLists.get(i).getTransfer_Charges()) / Double.valueOf(modelActivityLists.get(i).getAdult());
                Double Main = temp +  Double.valueOf(modelActivityLists.get(i).getAdult_Amt());
                String t = String.valueOf(Main);
                Total_transport_cost_adult = Total_transport_cost_adult + Float.valueOf(t);
                if(Float.valueOf(modelActivityLists.get(i).getChild())>0){
                    Total_transport_cost_child = Total_transport_cost_child + Float.valueOf(modelActivityLists.get(i).getChild_Amt());
                }else{
                    Total_transport_cost_child = 0;
                }
            }

            if(!modelActivityLists.get(i).getVouchers_Charges().equalsIgnoreCase("0")){
                if(!modelActivityLists.get(i).getIsPack().equalsIgnoreCase("false")){
                    Float temp = Float.valueOf(modelActivityLists.get(i).getAdult()) + Float.valueOf(modelActivityLists.get(i).getChild());
                    Total_Voucher_cost=Total_Voucher_cost + (Float.valueOf(modelActivityLists.get(i).getVouchers_Charges())/temp);
                }else{
                    Total_Voucher_cost=Total_Voucher_cost + Float.valueOf(modelActivityLists.get(i).getVouchers_Charges());
                }
            }
            if(modelActivityLists.get(i).getTransfer_Charges().equalsIgnoreCase("0") && modelActivityLists.get(i).getVouchers_Charges().equalsIgnoreCase("0")){
                if(!modelActivityLists.get(i).getIsPack().equalsIgnoreCase("false")){
                    Float temp = Float.valueOf(modelActivityLists.get(i).getAdult()) + Float.valueOf(modelActivityLists.get(i).getChild());
                    activity_Adult_cost=activity_Adult_cost + (Float.valueOf(modelActivityLists.get(i).getFinal_cost())/temp);
                    if(Float.valueOf(modelActivityLists.get(i).getChild())>0){
                        activity_Child_cost=activity_Child_cost + (Float.valueOf(modelActivityLists.get(i).getFinal_cost())/temp);
                    }

                }else{
                    activity_Adult_cost=activity_Adult_cost + Float.valueOf(modelActivityLists.get(i).getAdult_Amt());
                    if(Float.valueOf(modelActivityLists.get(i).getChild())>0){
                        activity_Child_cost=activity_Child_cost + Float.valueOf(modelActivityLists.get(i).getChild_Amt());
                    }
                }
            }
        }

        try {

            List<String> items = new ArrayList<>();
            items = Arrays.asList(sessionManager.getChildAge().replaceAll("[a-zA-Z]", "").replaceAll(" ", "").split("\\s*,\\s*"));
            tv_hotel_cost.setText(String.valueOf(String.format("%.02f", total_hotel_cost)));
            double Hotel_Cost_Adult = total_hotel_cost ;/** 80 / 100;*/
            double Hotel_Cost_Child = total_hotel_cost ;/** 20 / 100;*/
            if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                tv_hotel_cost_per_Child.setText(String.format("%.02f", 0.00));
            }else{
                double Hotel_Cost_Adults = total_hotel_cost * 80 / 100;
                double Hotel_Cost_Childs = total_hotel_cost * 20 / 100;
                boolean flagTemp=false;
                int count=0;
                for(int j = 0;items.size()>j;j++){
                    if(Integer.parseInt(items.get(j))>2){
                        count=count+1;
                        flagTemp=true;
                    }
                }
                if(flagTemp){
                    tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adults / Float.parseFloat(sessionManager.getAdultMainCount())));
                    tv_hotel_cost_per_Child.setText(String.format("%.02f", Hotel_Cost_Childs / count /*Float.parseFloat(sessionManager.getChildMainCount())*/));
                }else {
                    tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                    tv_hotel_cost_per_Child.setText(String.format("%.02f", 0.00));
                }
            }
            final_cost = total_hotel_cost + total_activity_cost + total_visa_cost;
            tv_Total_Cost.setText("$"+String.format("%.02f", final_cost));
        }catch (Exception e){

        }

        try {

            tv_excursion_cost.setText(String.valueOf(String.format("%.02f", total_activity_cost)));

            // Transport_Charges_Adult = Total_Transport_Charges / Float.parseFloat(sessionManager.getAdultMainCount());

            total_activity_Adult_cost = Total_Voucher_cost + Total_transport_cost_adult + activity_Adult_cost;
            total_activity_Child_cost =  Total_Voucher_cost + Total_transport_cost_child + activity_Child_cost;

            tv_excursion_cost_per_Adult.setText(String.format("%.02f", total_activity_Adult_cost));
            tv_excursion_cost_per_Child.setText(String.format("%.02f", total_activity_Child_cost));

            final_cost = total_hotel_cost + total_activity_cost + total_visa_cost;
            tv_Total_Cost.setText("$"+String.format("%.02f", final_cost));
        }catch (Exception e){

        }

        try{
            tv_total_cost_per_Adult.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Adult.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Adult.getText().toString())));
            tv_total_cost_per_Child.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Child.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Child.getText().toString())));
        }catch (Exception  e){

        }

        setHotelList();

        if(total_hotel_cost>0){
            tv_option_Hotels.setVisibility(View.VISIBLE);
            Select_Option();
        }else{
            tv_option_Hotels.setVisibility(View.GONE);
        }

        /* Add Card Type */

        cardTypeList.add(new ModelCardDetailsList("0", "No Visa", "$", "0"));
        cardTypeList.add(new ModelCardDetailsList("1", "Visa Only", "$", "78"));
        cardTypeList.add(new ModelCardDetailsList("2", "Visa + OTB", "$", "83"));
        cardTypeList.add(new ModelCardDetailsList("3", "Express Visa", "$", "128"));


        FragmentManager fragmentManager1 = getSupportFragmentManager();


        AdapterCardDetails adapterCardDetails = new AdapterCardDetails(mContext, cardTypeList);


        if(sessionManager.getCountryCode().equalsIgnoreCase("2")){
            sp_cardType.setVisibility(View.VISIBLE);
            ll_visa.setVisibility(View.VISIBLE);
        }else{
            sp_cardType.setVisibility(View.GONE);
            ll_visa.setVisibility(View.GONE);
        }

        sp_cardType.setAdapter(adapterCardDetails);
        sp_cardType.getSelectedItem();

        for(int i=0;cardTypeList.size()>i;i++){
            if(cardTypeList.get(i).getCardType().equalsIgnoreCase(getIntent().getStringExtra("visa_type"))){
                sp_cardType.setSelection(i);
            }
        }
        try{
            sp_cardType.setEnabled(false);
            sp_cardType.setFocusable(false);
            sp_cardType.setClickable(false);
        }catch (Exception e){

        }

        sp_cardType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                if(pos==0){
                    is_visa_included="false";
                    ll_visa.setVisibility(View.GONE);
                }else{
                    is_visa_included="true";
                    ll_visa.setVisibility(View.VISIBLE);
                }
               // visa_cost= Float.parseFloat(cardTypeList.get(pos).getAmount());
                visa_type=cardTypeList.get(pos).getCardType();
                float visa_adult_Amount = Float.parseFloat(cardTypeList.get(pos).getAmount()) * Float.parseFloat(sessionManager.getAdultMainCount());
                float visa_adult_Child = Float.parseFloat(cardTypeList.get(pos).getAmount()) * Float.parseFloat(sessionManager.getChildMainCount());

                final_cost = 0;
                tv_visa_cost_per_Adult.setText(String.format("%.02f", Float.parseFloat(cardTypeList.get(pos).getAmount())));
                if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                    tv_visa_cost_per_Child.setText(String.format("%.02f", 0.00));
                }else{
                    tv_visa_cost_per_Child.setText(String.format("%.02f", Float.parseFloat(cardTypeList.get(pos).getAmount())));
                }


                total_visa_cost = visa_adult_Amount + visa_adult_Child;
                tv_Visa_cost.setText(String.format("%.02f", total_visa_cost));
                try {
                    final_cost = total_hotel_cost + total_activity_cost + total_visa_cost;
                    tv_Total_Cost.setText("$"+String.format("%.02f", final_cost));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                try{
                    tv_total_cost_per_Adult.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Adult.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Adult.getText().toString()) + Float.parseFloat(tv_visa_cost_per_Adult.getText().toString())));
                    tv_total_cost_per_Child.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Child.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Child.getText().toString()) + Float.parseFloat(tv_visa_cost_per_Child.getText().toString())));
                }catch (Exception  e){

                }
                Select_Option();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    public void Select_Option(){
        modelHotelLists=new ArrayList<>();
        modelHotelLists = db.getItineraryHotelsData(sessionManager.getCountryCode());

        selected_Option1="";selected_Option2="";selected_Option3="";
        total_hotel_cost_option1 = 0;total_hotel_cost_option2 = 0;total_hotel_cost_option3 = 0;
        flag_Option1=false; flag_Option2=false ;flag_Option3=false;

        for (int i = 0; modelHotelLists.size() > i; i++) {

            if(modelHotelLists.get(i).getOption().equalsIgnoreCase("1")){
                flag_Option1=true;
            }else if(modelHotelLists.get(i).getOption().equalsIgnoreCase("2")){
                flag_Option2=true;
            }else if(modelHotelLists.get(i).getOption().equalsIgnoreCase("3")){
                flag_Option3=true;
            }else if(modelHotelLists.get(i).getOption().equalsIgnoreCase("0")){
                flag_Option1=true;
            }
        }
        flag_Option=true;

        if(selected_Option.equalsIgnoreCase("1")){
            mainHint="** Option 1 includes ";
        }else if(selected_Option.equalsIgnoreCase("2")){
            mainHint="** Option 2 includes ";
        }else if(selected_Option.equalsIgnoreCase("3")){
            mainHint="** Option 3 includes ";
        }


            for (int i = 0; modelHotelLists.size() > i; i++) {

                if(modelHotelLists.get(i).getOption().equalsIgnoreCase("1") || modelHotelLists.get(i).getOption().equalsIgnoreCase("0")){

                    if(flag_Option1){
                        total_hotel_cost_option1 = total_hotel_cost_option1 + Float.valueOf(modelHotelLists.get(i).getFinal_cost());

                        if(i==0){
                            String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle();
                            selected_Option1 =selected_Option1 + Temp;
                            tv_option_Hotels.setText(mainHint+selected_Option1);
                            selected_Option1=selected_Option1+",";

                        }else{
                            if(modelHotelLists.size()-1 == i){
                                String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle();
                                selected_Option1 =selected_Option1 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option1);
                            }else{
                                String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle()+",";
                                selected_Option1 =selected_Option1 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option1);
                            }
                        }
                    }

                }

                if(modelHotelLists.get(i).getOption().equalsIgnoreCase("2") || modelHotelLists.get(i).getOption().equalsIgnoreCase("0")){

                    if(flag_Option2){
                        total_hotel_cost_option2 = total_hotel_cost_option2 + Float.valueOf(modelHotelLists.get(i).getFinal_cost());

                        ll_option.setVisibility(View.VISIBLE);
                        ll_option1.setVisibility(View.VISIBLE);
                        ll_option2.setVisibility(View.VISIBLE);

                        if(i==0){
                            String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle();
                            selected_Option2 =selected_Option2 + Temp;
                            tv_option_Hotels.setText(mainHint+selected_Option2);
                            selected_Option2=selected_Option2+",";
                        }else{
                            if(modelHotelLists.size()-1 == i){
                                String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle();
                                selected_Option2 =selected_Option2 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option2);
                            }else{
                                String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle()+",";
                                selected_Option2 =selected_Option2 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option2);
                            }
                        }
                    }

                }
                if(modelHotelLists.get(i).getOption().equalsIgnoreCase("3") || modelHotelLists.get(i).getOption().equalsIgnoreCase("0")){

                    if(flag_Option3){
                        total_hotel_cost_option3 = total_hotel_cost_option3 + Float.valueOf(modelHotelLists.get(i).getFinal_cost());

                        ll_option.setVisibility(View.VISIBLE);
                        ll_option3.setVisibility(View.VISIBLE);


                        if(i==0){
                            String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle();
                            selected_Option3 =selected_Option3 + Temp;
                            tv_option_Hotels.setText(mainHint+selected_Option3);
                            selected_Option3=selected_Option3+",";
                        }else{
                            if(modelHotelLists.size()-1 == i){
                                String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle();
                                selected_Option3 =selected_Option3 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option3);
                            }else{
                                String Temp = modelHotelLists.get(i).getHotelsLists().get(0).getTitle()+",";
                                selected_Option3 =selected_Option3 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option3);
                            }
                        }
                    }
                }
            }

            if(selected_Option.equalsIgnoreCase("1")){
                if (selected_Option1.endsWith(",")) {
                    selected_Option1 = selected_Option1.substring(0, selected_Option1.length() - 1);
                }
                tv_option_Hotels.setText(mainHint+selected_Option1);
                try {

                    List<String> items = new ArrayList<>();
                    items = Arrays.asList(sessionManager.getChildAge().replaceAll("[a-zA-Z]", "").replaceAll(" ", "").split("\\s*,\\s*"));
                    tv_hotel_cost.setText(String.valueOf(String.format("%.02f", total_hotel_cost_option1)));
                    double Hotel_Cost_Adult = total_hotel_cost_option1 ;/** 80 / 100;*/
                    double Hotel_Cost_Child = total_hotel_cost_option1 ;/** 20 / 100;*/
                    if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                        tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                        tv_hotel_cost_per_Child.setText(String.format("%.02f", 0.00));
                    }else{
                        double Hotel_Cost_Adults = total_hotel_cost_option1 * 80 / 100;
                        double Hotel_Cost_Childs = total_hotel_cost_option1 * 20 / 100;
                        boolean flagTemp=false;
                        int count=0;
                        for(int j = 0;items.size()>j;j++){
                            if(Integer.parseInt(items.get(j))>2){
                                count=count+1;
                                flagTemp=true;
                            }
                        }
                        if(flagTemp){
                            tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adults / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child.setText(String.format("%.02f", Hotel_Cost_Childs / count /*Float.parseFloat(sessionManager.getChildMainCount())*/));
                        }else {
                            tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child.setText(String.format("%.02f", 0.00));
                        }
                    }
                    final_cost = total_hotel_cost_option1 + total_activity_cost + total_visa_cost;
                    tv_Total_Cost.setText("$"+String.format("%.02f", final_cost));

                    try{
                        tv_total_cost_per_Adult.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Adult.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Adult.getText().toString())));
                        tv_total_cost_per_Child.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Child.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Child.getText().toString())));
                    }catch (Exception  e){

                    }
                }catch (Exception e){

                }
            }else if(selected_Option.equalsIgnoreCase("2")){

                if (selected_Option2.endsWith(",")) {
                    selected_Option2 = selected_Option2.substring(0, selected_Option2.length() - 1);
                }
                tv_option_Hotels.setText(mainHint+selected_Option2);
                try {

                    List<String> items = new ArrayList<>();
                    items = Arrays.asList(sessionManager.getChildAge().replaceAll("[a-zA-Z]", "").replaceAll(" ", "").split("\\s*,\\s*"));
                    tv_hotel_cost.setText(String.valueOf(String.format("%.02f", total_hotel_cost_option2)));
                    double Hotel_Cost_Adult = total_hotel_cost_option2 ;/** 80 / 100;*/
                    double Hotel_Cost_Child = total_hotel_cost_option2 ;/** 20 / 100;*/
                    if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                        tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                        tv_hotel_cost_per_Child.setText(String.format("%.02f", 0.00));
                    }else{
                        double Hotel_Cost_Adults = total_hotel_cost_option2 * 80 / 100;
                        double Hotel_Cost_Childs = total_hotel_cost_option2 * 20 / 100;
                        boolean flagTemp=false;
                        int count=0;
                        for(int j = 0;items.size()>j;j++){
                            if(Integer.parseInt(items.get(j))>2){
                                count=count+1;
                                flagTemp=true;
                            }
                        }
                        if(flagTemp){
                            tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adults / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child.setText(String.format("%.02f", Hotel_Cost_Childs / count /*Float.parseFloat(sessionManager.getChildMainCount())*/));
                        }else {
                            tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child.setText(String.format("%.02f", 0.00));
                        }
                    }
                    final_cost = total_hotel_cost_option2 + total_activity_cost + total_visa_cost;
                    tv_Total_Cost.setText("$"+String.format("%.02f", final_cost));

                    try{
                        tv_total_cost_per_Adult.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Adult.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Adult.getText().toString())));
                        tv_total_cost_per_Child.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Child.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Child.getText().toString())));
                    }catch (Exception  e){

                    }
                }catch (Exception e){

                }

            }else if(selected_Option.equalsIgnoreCase("3")){
                if (selected_Option3.endsWith(",")) {
                    selected_Option3 = selected_Option3.substring(0, selected_Option3.length() - 1);
                }
                tv_option_Hotels.setText(mainHint+selected_Option3);
                try {

                    List<String> items = new ArrayList<>();
                    items = Arrays.asList(sessionManager.getChildAge().replaceAll("[a-zA-Z]", "").replaceAll(" ", "").split("\\s*,\\s*"));
                    tv_hotel_cost.setText(String.valueOf(String.format("%.02f", total_hotel_cost_option3)));
                    double Hotel_Cost_Adult = total_hotel_cost_option3 ;/** 80 / 100;*/
                    double Hotel_Cost_Child = total_hotel_cost_option3 ;/** 20 / 100;*/
                    if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                        tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                        tv_hotel_cost_per_Child.setText(String.format("%.02f", 0.00));
                    }else{
                        double Hotel_Cost_Adults = total_hotel_cost_option3 * 80 / 100;
                        double Hotel_Cost_Childs = total_hotel_cost_option3 * 20 / 100;
                        boolean flagTemp=false;
                        int count=0;
                        for(int j = 0;items.size()>j;j++){
                            if(Integer.parseInt(items.get(j))>2){
                                count=count+1;
                                flagTemp=true;
                            }
                        }
                        if(flagTemp){
                            tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adults / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child.setText(String.format("%.02f", Hotel_Cost_Childs / count /*Float.parseFloat(sessionManager.getChildMainCount())*/));
                        }else {
                            tv_hotel_cost_per_Adult.setText(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child.setText(String.format("%.02f", 0.00));
                        }
                    }
                    final_cost = total_hotel_cost_option3 + total_activity_cost + total_visa_cost;
                    tv_Total_Cost.setText("$"+String.format("%.02f", final_cost));

                    try{
                        tv_total_cost_per_Adult.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Adult.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Adult.getText().toString())));
                        tv_total_cost_per_Child.setText(String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Child.getText().toString()) + Float.parseFloat(tv_excursion_cost_per_Child.getText().toString())));
                    }catch (Exception  e){

                    }
                }catch (Exception e){

                }
            }

            if(selected_Option2.equalsIgnoreCase("") && selected_Option3.equalsIgnoreCase("")){
                ll_option.setVisibility(View.GONE);
            }

            if(selected_Option2.equalsIgnoreCase("")){
                ll_option2.setVisibility(View.GONE);
            }else{
                ll_option2.setVisibility(View.VISIBLE);
            }
            if(selected_Option3.equalsIgnoreCase("")){
                ll_option3.setVisibility(View.GONE);
            }else{
                ll_option3.setVisibility(View.VISIBLE);
            }
    }


    public void deleteHotel(String hotel_id,String country_code,ModelItineraryHotelList model){
        try{
            db.DeleteHotelItinery(hotel_id,country_code);

            Log.e("Total Room Id",model.getHotelsTotalRooms());
            List<String> items = Arrays.asList(model.getHotelsTotalRooms().split("\\s*,\\s*"));
            for(int j=0;items.size()>j;j++){
                try{
                    Log.e("Room number",items.get(j).toString());
                    db.DeleteRoomItineraryData(items.get(j).toString(),hotel_id,country_code);
                }catch (Exception e){}

            }

            Toast.makeText(mContext, "Hotel deleted successfully", Toast.LENGTH_LONG).show();
            finish();
            overridePendingTransition( 0, 0);
            startActivity(getIntent());
            overridePendingTransition( 0, 0);
        }catch (Exception e){}

    }

    public void deleteActicity(String activity_id,String country_code){
        try{
            db.updateOneColumn(activity_id,country_code,"False");
            db.DeleteHotelItineryActivity(activity_id,country_code);
            Toast.makeText(mContext, "Activity deleted successfully", Toast.LENGTH_LONG).show();
            finish();
            overridePendingTransition( 0, 0);
            startActivity(getIntent());
            overridePendingTransition( 0, 0);
        }catch (Exception e){}

    }

    private void setHotelList() {

        final LinearLayout LLlist = (LinearLayout) findViewById(R.id.ll_Hotel_List);
        final LinearLayout lList = new LinearLayout(mContext);
        lList.setPadding(10, 10, 10, 10);
        RecyclerView rv_flight_list = new RecyclerView(mContext);
        LinearLayout.LayoutParams lppList = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        //setiig a weidth and Height
        lList.addView(rv_flight_list, lppList);
        LLlist.addView(lList);


        try {

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            rv_flight_list.setLayoutManager(mLayoutManager);
            rv_flight_list.setItemAnimator(new DefaultItemAnimator());
            rv_flight_list.setHasFixedSize(true);
            rv_flight_list.setNestedScrollingEnabled(false);
            //rv_flight_list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

            GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
            FragmentManager fragmentManager1 = getSupportFragmentManager();
            AdapterHotelsItineraryAllList adapterListHotels = new AdapterHotelsItineraryAllList(activity, modelHotelLists, activity, fragmentManager1);
            rv_flight_list.setLayoutManager(manager1);
            rv_flight_list.setAdapter(adapterListHotels);
            adapterListHotels.notifyDataSetChanged();


        } catch (Exception e) {
            Log.e("----inside--->", "Recycler adapter");
        }

        setActivityList();
    }

    private void setActivityList() {

        final LinearLayout LLlist = (LinearLayout) findViewById(R.id.ll_Activity_List);
        final LinearLayout lList = new LinearLayout(mContext);
        lList.setPadding(10, 10, 10, 10);
        RecyclerView rv_flight_list2 = new RecyclerView(mContext);
        LinearLayout.LayoutParams lppList = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        //setiig a weidth and Height
        lList.addView(rv_flight_list2, lppList);
        LLlist.addView(lList);


        try {

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            rv_flight_list2.setLayoutManager(mLayoutManager);
            rv_flight_list2.setItemAnimator(new DefaultItemAnimator());
            rv_flight_list2.setHasFixedSize(true);
            rv_flight_list2.setNestedScrollingEnabled(false);
            //rv_flight_list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));


            GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
            FragmentManager fragmentManager1 = getSupportFragmentManager();
            AdapterItineraryActivitiesAllList adapterListActivitty = new AdapterItineraryActivitiesAllList(activity, modelActivityLists, activity, fragmentManager1);
            rv_flight_list2.setLayoutManager(manager1);
            rv_flight_list2.setAdapter(adapterListActivitty);
            adapterListActivitty.notifyDataSetChanged();


        } catch (Exception e) {
            Log.e("----inside--->", "Recycler adapter");
        }

    }
}
