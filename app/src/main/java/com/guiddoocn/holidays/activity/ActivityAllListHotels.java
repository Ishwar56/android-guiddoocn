package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterAllListHotels;
import com.guiddoocn.holidays.adapter.AdapterHotelRoomList;
import com.guiddoocn.holidays.models.ModelAllHotelList;
import com.guiddoocn.holidays.models.ModelHotelAmnetiesList;
import com.guiddoocn.holidays.models.ModelHotelRoomsList;
import com.guiddoocn.holidays.models.ModelHotelRoomsPricingsList;
import com.guiddoocn.holidays.models.ModelRoomPriceList;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAllListHotels extends AppCompatActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.iv_filter)
    ImageView iv_filter;

    @BindView(R.id.rv_allListHotel)
    RecyclerView rv_allListHotel;

    @BindView(R.id.ll_ltinerary)
    LinearLayout ll_ltinerary;

    @BindView(R.id.ll_skip_excursion)
    LinearLayout ll_skip_excursion;

    @BindView(R.id.ll_record_not_found)
    LinearLayout ll_record_not_found;

    SearchView mSearchView;
    MenuItem mSearch;

    private Context mContext;
    private SessionManager sessionManager;
    private CustomResponseDialog dialog;
    private View v;
    private SQLiteHandler db;
    private AdapterAllListHotels adapterAllListHotels;
    private RecyclerView rv_Hotel_Rooms_List;
    private AdapterHotelRoomList adapterAllRoomList;
    ArrayList<ModelHotelRoomsList> hotelRoomsList = new ArrayList<>();


    Activity activity;
    ArrayList<ModelAllHotelList> modelAllHotelLists = new ArrayList<>();
    private ArrayList<ModelHotelAmnetiesList> hotelAmnetiesLists = new ArrayList<>();
    private ArrayList<ModelHotelRoomsList> hotelRoomsLists = new ArrayList<>();
    private ArrayList<ModelHotelRoomsPricingsList> hotelRoomsPricingLists = new ArrayList<ModelHotelRoomsPricingsList>();
    private int  number_of_adults = 1, number_of_child = 0, NoOfRooms = 1,TotalNoOfRooms;
    private double roomAmt = 0.0,FinalroomAmt=0.0;
    private Calendar check_in_Calendar, check_out_Calendar;
    private DatePickerDialog.OnDateSetListener Fromdate_Check_In, Fromdate_Check_Out;
    private String  searchText,Hotels_Added_Total_Rooms="0";
    private boolean Flag_price=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_list_hotels);
        ButterKnife.bind(this);
        v = findViewById(R.id.view);
        activity = this;
        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        check_in_Calendar = Calendar.getInstance();
        check_out_Calendar = Calendar.getInstance();
        sessionManager = new SessionManager(mContext);
        db = new SQLiteHandler(mContext);
        dialog = new CustomResponseDialog(mContext);
        tv_title.setText(getText(R.string.all_hotesl));

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAllListHotels.this, ActivityFilter.class);
                startActivity(intent);
            }
        });
        ll_ltinerary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAllListHotels.this, ActivityItinerary.class);
                startActivity(intent);
                finish();
            }
        });

        ll_skip_excursion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAllListHotels.this, ActivityAllListCustomeActivities.class);
                startActivity(intent);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menus) {
        try {
            getMenuInflater().inflate(R.menu.search_menu, menus);
            mSearch = menus.findItem(R.id.action_search);
            mSearchView = (SearchView) mSearch.getActionView();
            mSearchView.setPadding(0, 8, 0, 8);
            mSearchView.setMaxWidth(Integer.MAX_VALUE);
            View searchplate = (View) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
            searchplate.setBackgroundResource(R.drawable.custombg_white_border);
        }catch (Exception e){

        }

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchText = s;
                adapterAllListHotels.getFilter().filter(searchText);
                return true;
            }
        });

        return true;
    }

    private void getHotelLIST() {
        dialog.showCustomDialog();
        RequestQueue queue = Volley.newRequestQueue(this);
        //products/hotels?country_id={COUNTRY_ID}&api_key={API_KEY}
        final String url = sessionManager.getBaseUrl() + "products/hotels?country_id=" + sessionManager.getCountryCode() + "&api_key=0D1067102F935B4CC31E082BD45014D469E35268";
        Log.e("Hotel url-->", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e("Hotel Response-->", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONObject objectData = jsonObject.getJSONObject("data");
                            JSONArray jsonArray = objectData.getJSONArray("listing_data");
                            for (int i = 0; jsonArray.length() > i; i++) {
                                ModelAllHotelList hotelList = new ModelAllHotelList();
                                JSONObject objectlist = jsonArray.getJSONObject(i);

                                hotelList.setID(objectlist.getString("hotel_id"));
                                hotelList.setTitle(objectlist.getString("hotel_name"));
                                hotelList.setCityCountryName(objectlist.getString("city"));
                                hotelList.setCity_ID(objectlist.getString("city_id"));
                                hotelList.setLocation(objectlist.getString("location"));
                               // hotelList.setAmount(objectlist.getString("starting_from_price"));
                                hotelList.setAmount(getIsMarginPrice(objectlist.getString("starting_from_price")));
                                hotelList.setAddress(objectlist.getString("address"));
                                hotelList.setCurrency(objectlist.getString("currency"));
                                hotelList.setImage(objectlist.getString("featured_image"));
                                hotelList.setRating(objectlist.getString("rating"));
                                hotelList.setDistance_from_centre(objectlist.getString("distance_from_centre"));

                                JSONArray jsonAnimities = objectlist.getJSONArray("amneties");
                                hotelAmnetiesLists = new ArrayList<>();
                                for (int j = 0; jsonAnimities.length() > j; j++) {
                                    ModelHotelAmnetiesList amnetiesList = new ModelHotelAmnetiesList();
                                    JSONObject objectamneties = jsonAnimities.getJSONObject(j);
                                    amnetiesList.setID(objectamneties.getString("id"));
                                    amnetiesList.setName(objectamneties.getString("amnety"));
                                    amnetiesList.setHotel_ID(objectlist.getString("hotel_id"));
                                    hotelAmnetiesLists.add(amnetiesList);
                                }

                                hotelList.setHotelAmnetiesLists(hotelAmnetiesLists);

                                JSONArray jsonrooms = objectlist.getJSONArray("rooms");
                                hotelRoomsLists = new ArrayList<>();
                                for (int j = 0; jsonrooms.length() > j; j++) {
                                    ModelHotelRoomsList roomsList = new ModelHotelRoomsList();
                                    JSONObject objectamneties = jsonrooms.getJSONObject(j);
                                    roomsList.setRoom_id(objectamneties.getString("room_id"));
                                    roomsList.setRoom_type(objectamneties.getString("room_type"));
                                    roomsList.setRemarks(objectamneties.getString("remarks"));
                                    //roomsList.setNet_rate(objectamneties.getString("net_rate"));
                                    roomsList.setSelect("False");
                                    roomsList.setExtra_bed_cost(getIsMarginPrice(objectamneties.getString("extra_bed_cost")));
                                    roomsList.setCurrency(objectamneties.getString("currency"));
                                    roomsList.setBooking_type(objectamneties.getString("booking_type"));
                                    roomsList.setBatch_size(objectamneties.getString("batch_size"));
                                    roomsList.setAdditional_tax(objectamneties.getString("additional_tax"));
                                    roomsList.setAdditional_surcharge(objectamneties.getString("additional_surcharge"));
                                    roomsList.setBlock_dates(objectamneties.getString("block_dates"));
                                    roomsList.setHotel_id(objectlist.getString("hotel_id"));

                                    JSONArray jsonroom_pricing = objectamneties.getJSONArray("room_pricing");
                                    hotelRoomsPricingLists = new ArrayList<>();
                                    for (int k = 0; jsonroom_pricing.length() > k; k++) {
                                        ModelHotelRoomsPricingsList pricingList = new ModelHotelRoomsPricingsList();
                                        JSONObject objectRoomsPricing = jsonroom_pricing.getJSONObject(k);
                                        pricingList.setMeal_type(objectRoomsPricing.getString("meal_type"));
                                        pricingList.setMeal_type_id(objectRoomsPricing.getString("meal_type_id"));
                                        //pricingList.setPrice(objectRoomsPricing.getString("price"));
                                        pricingList.setPrice(getIsMarginPrice(objectRoomsPricing.getString("price")));
                                        pricingList.setSharing_type(objectRoomsPricing.getString("sharing_type"));
                                        pricingList.setSharing_type_id(objectRoomsPricing.getString("sharing_type_id"));
                                        pricingList.setValidity_from(objectRoomsPricing.getString("validity_from"));
                                        pricingList.setValidity_to(objectRoomsPricing.getString("validity_to"));

                                        pricingList.setHotel_id(objectlist.getString("hotel_id"));
                                        pricingList.setRoom_id(objectamneties.getString("room_id"));
                                        pricingList.setCountry_id(sessionManager.getCountryCode());

                                        hotelRoomsPricingLists.add(pricingList);
                                    }
                                    roomsList.setRoomsPricingsLists(hotelRoomsPricingLists);

                                    hotelRoomsLists.add(roomsList);
                                }
                                hotelList.setHotelRoomsLists(hotelRoomsLists);
                                modelAllHotelLists.add(hotelList);
                            }
                            if (modelAllHotelLists.size() > 0) {
                                setHotelAdapter();
                                iv_filter.setVisibility(View.INVISIBLE);
                                new LoadData().execute(); // Load data base values hotels
                            } else {
                                Snackbar.make(v, "Hotel not found.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }

                        } catch (JSONException e) {
                            Snackbar.make(v, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );

        queue.add(getRequest);
    }

    public String getIsMarginPrice(String Main_price) {
        try{
            String data = null;
            double discount = Double.parseDouble(sessionManager.getIsmargin());
            double Old_price = Double.parseDouble(Main_price);
            double New_price = Old_price - (Old_price * discount / 100);
            data = String.format("%.02f", New_price );
            return data;
        }catch (Exception e){
            return Main_price;
        }


    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                for (int i = 0; modelAllHotelLists.size() > i; i++) {    // Add hotel data
                    db.setHotelsData(modelAllHotelLists.get(i).getID(), modelAllHotelLists.get(i).getTitle(), modelAllHotelLists.get(i).getLocation(), modelAllHotelLists.get(i).getRating(), Double.parseDouble(modelAllHotelLists.get(i).getAmount()), modelAllHotelLists.get(i).getImage(), modelAllHotelLists.get(i).getCurrency(), modelAllHotelLists.get(i).getCity_ID(), modelAllHotelLists.get(i).getCityCountryName(), modelAllHotelLists.get(i).getDistance_from_centre(), modelAllHotelLists.get(i).getAddress(),sessionManager.getCountryCode());
                    if (modelAllHotelLists.get(i).getHotelAmnetiesLists().size() > 0) {    // Add hotel amnesties data
                        for (int j = 0; modelAllHotelLists.get(i).getHotelAmnetiesLists().size() > j; j++) {
                            db.setAmenityData(modelAllHotelLists.get(i).getHotelAmnetiesLists().get(j).getID(), modelAllHotelLists.get(i).getHotelAmnetiesLists().get(j).getName(), modelAllHotelLists.get(i).getID());
                        }
                    }
                    if (modelAllHotelLists.get(i).getHotelRoomsLists().size() > 0) {    // Add hotel rooms data
                        for (int k = 0; modelAllHotelLists.get(i).getHotelRoomsLists().size() > k; k++) {
                            db.setRoomsData(modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getRoom_id(), modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getRoom_type(), modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getRemarks(),"0","",modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getExtra_bed_cost(),modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getCurrency(),modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getBooking_type(),modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getBatch_size(),modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getBlock_dates(),modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getAdditional_tax(),modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getAdditional_surcharge(),modelAllHotelLists.get(i).getID(),sessionManager.getCountryCode());
                            if(modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getRoomsPricingsLists().size()>0){
                                for (int l = 0; modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getRoomsPricingsLists().size() > l; l++) {
                                    ModelHotelRoomsPricingsList model = modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getRoomsPricingsLists().get(l);
                                    db.setHotelRoomPricingData(model.getMeal_type(),model.getMeal_type_id(),model.getPrice(),model.getSharing_type(),model.getSharing_type_id(),model.getValidity_from(),model.getValidity_to(),modelAllHotelLists.get(i).getHotelRoomsLists().get(k).getRoom_id(),modelAllHotelLists.get(i).getID(),sessionManager.getCountryCode());
                                }
                            }

                        }
                    }
                }


            } catch (SecurityException | NullPointerException e) {
                try {


                } catch (SecurityException | NullPointerException e1) {

                }
            }catch (Exception ex) {

                ex.printStackTrace();
                Log.e("DATA", "Errro" + ex.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e("@@@@@@@@@@@@@@", "Inside onPostExecute--->");
            iv_filter.setVisibility(View.VISIBLE);
        }
    }

    public void reset() {

        GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
        FragmentManager fragmentManager1 = getSupportFragmentManager();
       // adapterAllRoomList = new AdapterHotelRoomList(activity, hotelRoomsList, activity, fragmentManager1,noR);
        rv_Hotel_Rooms_List.setLayoutManager(manager1);
        rv_Hotel_Rooms_List.setAdapter(adapterAllRoomList);

    }

    public void HotelRoomsPrice(final String share_Type_ID,String meal_Type_ID) {


    }

    public void SetHotelRoomsPrice(String room_id,String hotel_id,int noOfRooms,double roomAmt,String sharing_Type,String meals_Type,String select_share_Type,String select_meal_Type,String room_name,String extra_bed_included) {
        Log.e("Selected Amt--->", String.valueOf(roomAmt)+" ---->Totals Rooms "+String.valueOf(noOfRooms)+" ---->meals_Type "+meals_Type+" ---->share_Type "+sharing_Type+" ---->select_meal_Type "+select_meal_Type+" ---->select_share_Type "+select_share_Type);
        if(roomAmt==0){
            Flag_price=false;
            Toast.makeText(mContext, "Amount is 0, please Change share type and meal type", Toast.LENGTH_LONG).show();
        }else{
            Flag_price=true;
            if(noOfRooms==0){
                db.DeleteRoomPriceData(room_id,hotel_id,sessionManager.getCountryCode());
            }else{
                db.setRoomPriceData(room_id,hotel_id,sessionManager.getCountryCode(),noOfRooms,roomAmt,sharing_Type,meals_Type,select_share_Type,select_meal_Type,room_name,extra_bed_included,"false","0");
            }
            /*if(db.CheckIsAvailableRoom(room_id,hotel_id,sessionManager.getCountryCode(),sharing_Type,meals_Type)){
                if(noOfRooms==0){
                    db.DeleteRoomPriceData(room_id,hotel_id,sessionManager.getCountryCode());
                    //db.DeleteRoomItineraryData(room_id,hotel_id,sessionManager.getCountryCode());
                }else{
                    db.updateRoomPriceData(noOfRooms,roomAmt,room_id,hotel_id,sessionManager.getCountryCode(),sharing_Type,meals_Type);
                }
            }else{
                db.setRoomPriceData(room_id,hotel_id,sessionManager.getCountryCode(),noOfRooms,roomAmt,sharing_Type,meals_Type);
            }*/
        }


    }

    public void HotelRoomsDialog(final int pos) {

        final Dialog dialogMsg = new Dialog(mContext);
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.dialog_hotel_rooms);
        dialogMsg.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMsg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.FILL_PARENT;
        lp.height = WindowManager.LayoutParams.FILL_PARENT;
        lp.gravity = Gravity.CENTER;
        dialogMsg.addContentView(new View(this), (new WindowManager.LayoutParams(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT)));
        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMsg.getWindow().setAttributes(lp);
        dialogMsg.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogMsg.show();

        TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
        TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_done);
        TextView tv_hotel_Name = (TextView) dialogMsg.findViewById(R.id.tv_hotel_Name);
        Spinner sp_no_of_rooms = (Spinner) dialogMsg.findViewById(R.id.sp_no_of_rooms);
        final EditText et_booking_Check_In = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In);
        final EditText et_booking_Check_Out = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_Out);
       // rv_Hotel_Rooms_List = (RecyclerView) dialogMsg.findViewById(R.id.rv_Hotel_Rooms_List);
       // LinearLayout ll_booking_Check_In = (LinearLayout)dialogMsg.findViewById(R.id.ll_booking_Check_In);
       // LinearLayout ll_booking_Check_Out = (LinearLayout)dialogMsg.findViewById(R.id.ll_booking_Check_Out);
        final LinearLayout ll_dynamic = (LinearLayout)dialogMsg.findViewById(R.id.ll_dynamic);
        Hotels_Added_Total_Rooms="0";
        Flag_price=false;
        final List<Integer> noRooms = new ArrayList<Integer>();
        final List<Integer> noRoomsSpiner = new ArrayList<Integer>();


        noRoomsSpiner.add(0);
        noRoomsSpiner.add(1);
        noRoomsSpiner.add(2);
        noRoomsSpiner.add(3);
        noRoomsSpiner.add(4);
        noRoomsSpiner.add(5);
        noRoomsSpiner.add(6);
        check_in_Calendar = Calendar.getInstance();
        check_out_Calendar = Calendar.getInstance();

        et_booking_Check_In.setText(sessionManager.getCheckInDate());
        et_booking_Check_Out.setText(sessionManager.getCheckOutDate());
       /* try{
            String dt = sessionManager.getCheckInDate();  // Start date
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(dt));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DATE, 1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
            String output = sdf1.format(c.getTime());
            et_booking_Check_Out.setText(output);

        }catch (Exception e){

        }*/
        //et_booking_Check_Out.setText(sessionManager.getCheckOutDate());

        hotelRoomsList = new ArrayList<>();
        hotelRoomsList = modelAllHotelLists.get(pos).getHotelRoomsLists();

        try{
            NoOfRooms=1;
            roomAmt = 0.0;
            number_of_adults = Integer.parseInt(sessionManager.getAdultCount());
            number_of_child = Integer.parseInt(sessionManager.getChildCount());
            db.deleteAllRoomPriceData();
        }catch (Exception e){

        }
       // hotelRoomsList = modelAllHotelLists.get(pos).getHotelRoomsLists();
        if (hotelRoomsList.size() > 0) {
            tv_hotel_Name.setText(modelAllHotelLists.get(pos).getTitle());

            noRooms.add(1);
            noRooms.add(2);
            noRooms.add(3);
            noRooms.add(4);
            noRooms.add(5);
            noRooms.add(6);
            //   Night adapter for spinner
            ArrayAdapter<Integer> nighgtAdapter = new ArrayAdapter<Integer>(mContext, android.R.layout.simple_spinner_item, noRooms);
            sp_no_of_rooms.setAdapter(nighgtAdapter);

            sp_no_of_rooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ll_dynamic.removeAllViews();
                    db.deleteAllRoomPriceData();
                    db.updateAllColumnHotelRooms("True","False");
                    NoOfRooms = position + 1;
                    //ll_dynamic.removeViewAt(ll_dynamic.getChildCount()-position);
                    for(int i=1;NoOfRooms>=i; i++){

                        TextView tv_title = new TextView(mContext);
                        LinearLayout.LayoutParams p0 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p0.setMargins(15, 05, 0, 0);
                        tv_title.setLayoutParams(p0);
                        tv_title.setText("Room "+i);
                        tv_title.setTextColor(getResources().getColor(R.color.colorBlack));
                        ll_dynamic.addView(tv_title);

                        RecyclerView rv = new RecyclerView(mContext);
                        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.setMargins(0, 0, 0, 05);
                        rv.setLayoutParams(p);
                        rv.setPadding(0, 0, 0, 0);
                        ll_dynamic.addView(rv);

                        View vv_line = new View(mContext);
                        LinearLayout.LayoutParams p2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                        p2.setMargins(0, 05, 0, 05);
                        vv_line.setLayoutParams(p2);
                        vv_line.setBackgroundColor(Color.parseColor("#F66961"));
                        ll_dynamic.addView(vv_line);


                        if(NoOfRooms>=2){
                            vv_line.setVisibility(View.VISIBLE);
                        }else{
                            vv_line.setVisibility(View.GONE);
                        }

                        if(NoOfRooms==i){
                            vv_line.setVisibility(View.GONE);
                        }

                        GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                        FragmentManager fragmentManager1 = getSupportFragmentManager();
                        adapterAllRoomList = new AdapterHotelRoomList(activity, hotelRoomsList, activity, fragmentManager1,noRoomsSpiner);
                        rv.setLayoutManager(manager1);
                        rv.setAdapter(adapterAllRoomList);
                    }

                }
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            et_booking_Check_In.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate_Check_In, check_in_Calendar
                            .get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH),
                            check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date startDate = null;
                    Date endDate = null;
                    try {
                        Calendar startCalendar = Calendar.getInstance();
                        startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                        String sDate = sdf.format(startCalendar.getTime());
                        startDate = sdf.parse(sDate);
                        mDate.getDatePicker().setMinDate(startDate.getTime());

                        Calendar endCalendar = Calendar.getInstance();
                        endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                        String eDate = sdf.format(endCalendar.getTime());
                        endDate = sdf.parse(eDate);
                        mDate.getDatePicker().setMaxDate(endDate.getTime());

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    mDate.show();

                }
            });
            Fromdate_Check_In = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    check_in_Calendar.set(Calendar.YEAR, year);
                    check_in_Calendar.set(Calendar.MONTH, monthOfYear);
                    check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "dd-MM-yyyy";
                    final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                    et_booking_Check_In.setText(sdf.format(check_in_Calendar.getTime()));


                }

            };

            et_booking_Check_Out.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try{
                        DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate_Check_Out, check_out_Calendar
                                .get(Calendar.YEAR), check_out_Calendar.get(Calendar.MONTH),
                                check_out_Calendar.get(Calendar.DAY_OF_MONTH));
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date startDate = null;
                        Date endDate = null;
                        try {
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(sdf.parse(et_booking_Check_In.getText().toString().trim()));
                            calendar.add(Calendar.DATE, 1);
                            String destDate = sdf.format(calendar.getTime());
                            startDate = sdf.parse(destDate);
                            mDate.getDatePicker().setMinDate(startDate.getTime());
                            mDate.show();

                            Calendar endCalendar = Calendar.getInstance();
                            endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                            String eDate = sdf.format(endCalendar.getTime());
                            endDate = sdf.parse(eDate);
                            mDate.getDatePicker().setMaxDate(endDate.getTime());

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        mDate.show();
                    }catch (Exception e){

                    }




                }
            });
            Fromdate_Check_Out = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    check_out_Calendar.set(Calendar.YEAR, year);
                    check_out_Calendar.set(Calendar.MONTH, monthOfYear);
                    check_out_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "dd-MM-yyyy";
                    final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                    et_booking_Check_Out.setText(sdf.format(check_out_Calendar.getTime()));
                }

            };

        } else {
            dialogMsg.cancel();
            Snackbar.make(v, "No Rooms Available.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    ArrayList<ModelRoomPriceList> roomPriceLists = new ArrayList<>();
                    roomPriceLists = db.getRoomPriceData(sessionManager.getCountryCode());
                    if(roomPriceLists.size()>0){
                        TotalNoOfRooms=0; FinalroomAmt=0.0;
                        List<String> list = new ArrayList<>();
                        for (int i=0;i<roomPriceLists.size();i++){
                            TotalNoOfRooms = TotalNoOfRooms + roomPriceLists.get(i).getNoOFnight();
                            //TotalroomAmt = TotalroomAmt + roomPriceLists.get(i).getHotel_Amt();
                            Log.e("Selected Amt--->", String.valueOf(roomPriceLists.get(i).getHotel_Amt())+" ---->Totals Rooms "+String.valueOf(roomPriceLists.get(i).getNoOFnight()));
                            Double temp = Double.valueOf(roomPriceLists.get(i).getNoOFnight() * roomPriceLists.get(i).getHotel_Amt());
                            FinalroomAmt = FinalroomAmt + temp;

                            /*if(roomPriceLists.get(i).getNoOFnight()==1){
                                if(db.CheckIsAvailableRoomItinerary(roomPriceLists.get(i).getRoom_id(),roomPriceLists.get(i).getHotel_id(),sessionManager.getCountryCode(),roomPriceLists.get(i).getSharing_type_id(),roomPriceLists.get(i).getMeal_type_id())){
                                    db.updateItineraryData(roomPriceLists.get(i).getNoOFnight(),roomPriceLists.get(i).getHotel_Amt(),roomPriceLists.get(i).getRoom_id(),roomPriceLists.get(i).getHotel_id(),sessionManager.getCountryCode(),roomPriceLists.get(i).getSharing_type_id(),roomPriceLists.get(i).getMeal_type_id());
                                }else{
                                    db.setRoomItineraryData(roomPriceLists.get(i).getRoom_id(),roomPriceLists.get(i).getHotel_id(),sessionManager.getCountryCode(),roomPriceLists.get(i).getNoOFnight(),roomPriceLists.get(i).getHotel_Amt(),roomPriceLists.get(i).getSharing_type_id(),roomPriceLists.get(i).getMeal_type_id());
                                }
                            }else{

                            }*/

                          for(int j=1;roomPriceLists.get(i).getNoOFnight()>=j;j++){
                              try{
                                  db.setRoomItineraryData(roomPriceLists.get(i).getRoom_id(),roomPriceLists.get(i).getHotel_id(),sessionManager.getCountryCode(),roomPriceLists.get(i).getNoOFnight(),roomPriceLists.get(i).getHotel_Amt(),roomPriceLists.get(i).getSharing_type_id(),roomPriceLists.get(i).getMeal_type_id(),roomPriceLists.get(i).getRoom_name()+","+roomPriceLists.get(i).getSharing_type()+","+roomPriceLists.get(i).getMeal_type(),roomPriceLists.get(i).getExtra_bed_included(),roomPriceLists.get(i).getIs_additional_hotel(),roomPriceLists.get(i).getSupplier());
                              }catch (Exception e){

                              }
                              if(modelAllHotelLists.get(pos).getID().equalsIgnoreCase(roomPriceLists.get(i).getHotel_id())){
                                  list.add(roomPriceLists.get(i).getRoom_id());
                              }
                          }

                        }
                        Hotels_Added_Total_Rooms = TextUtils.join(",", list);
                        Log.e("Total Rooms--->", String.valueOf(TotalNoOfRooms)+" , selected rooms---->"+NoOfRooms+" , FinalroomAmt---->"+FinalroomAmt+" , Total Rooms Added---->"+Hotels_Added_Total_Rooms);
                    }

                    if(FinalroomAmt>0){

                        if(Flag_price){

                        int dateDifference = 0;
                        double final_Cost = 0.0,temp=0.0;
                        String check_in_Date = et_booking_Check_In.getText().toString();
                        String check_out_Date = et_booking_Check_Out.getText().toString();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        Date convertedDate = new Date();
                        Date convertedDate2 = new Date();
                        try{
                            dateDifference = (int) getDateDiff(new SimpleDateFormat("dd-MM-yyyy"), check_in_Date, check_out_Date);
                            Log.e("Date different--->", String.valueOf(dateDifference +" no of night "+TotalNoOfRooms +" Amt "+FinalroomAmt));

                           /* if(number_of_adults>0 && number_of_child==0){
                                temp = FinalroomAmt * number_of_adults;
                                final_Cost = temp * 1;
                                //final_Cost = temp * NoOfRooms;
                                final_Cost = final_Cost * dateDifference;
                            }else if(number_of_adults>0 && number_of_child>0){
                                double count = number_of_adults + number_of_child;
                                temp = FinalroomAmt * count;
                                final_Cost = temp * 1;
                                //final_Cost = temp * NoOfRooms;
                                final_Cost = final_Cost * dateDifference;
                            }*/

                            final_Cost = FinalroomAmt * dateDifference;
                            Log.e("Final cost--->", String.valueOf(String.format("%.2f", final_Cost)));

                        }catch (Exception e){

                        }
                        try {
                            convertedDate = dateFormat.parse(check_in_Date);
                            convertedDate2 = dateFormat.parse(check_out_Date);
                            if (convertedDate2.after(convertedDate)) {
                                //Log.e("Total Rooms--->", String.valueOf(TotalNoOfRooms)+" selected Rooms------------>"+NoOfRooms);

                                if(db.CheckIsAvailable(modelAllHotelLists.get(pos).getID(),sessionManager.getCountryCode())){
                                    //Update
                                    db.updateItineraryHotelsData(Integer.parseInt(modelAllHotelLists.get(pos).getID()),check_in_Date,check_out_Date,String.valueOf(TotalNoOfRooms),sessionManager.getAdultMainCount(),sessionManager.getChildMainCount(),String.valueOf(dateDifference), String.valueOf(FinalroomAmt),String.valueOf(String.format("%.2f", final_Cost)),sessionManager.getCountryCode(),Hotels_Added_Total_Rooms);
                                }else{
                                    //Insert
                                    db.setItineraryHotelsData(modelAllHotelLists.get(pos).getID(),check_in_Date,check_out_Date,String.valueOf(TotalNoOfRooms),sessionManager.getAdultMainCount(),sessionManager.getChildMainCount(),String.valueOf(dateDifference),String.valueOf(FinalroomAmt),String.valueOf(String.format("%.2f", final_Cost)),sessionManager.getCountryCode(),Hotels_Added_Total_Rooms,"0","1");
                                }
                                Toast.makeText(mContext, "Hotel added in your itinerary", Toast.LENGTH_LONG).show();
                                dialogMsg.cancel();

                                /*if(TotalNoOfRooms==NoOfRooms){

                                }else{
                                    Toast.makeText(mContext, "Hotels number of rooms must be equal of package rooms", Toast.LENGTH_LONG).show();
                                }*/

                            } else {
                                Toast.makeText(mContext, "Check-Out date older than Check-In date", Toast.LENGTH_LONG).show();
                            }
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        }else{
                            Toast.makeText(mContext, "Amount is 0, please Change share type and meal type", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(mContext, "Please select rooms", Toast.LENGTH_LONG).show();
                    }







            }
        });

        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
            }
        });

    }

    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        modelAllHotelLists = new ArrayList<>();
        if(sessionManager.getFilterHotels()){
            modelAllHotelLists = db.getHotelFilterData(sessionManager.getCountryCode(),sessionManager.getSortingPosition(),sessionManager.getFilterMinAmt()+".00",sessionManager.getFilterMaxAmt()+".00",sessionManager.getFilterMinStar(),sessionManager.getFilterMaxStar(),sessionManager.getAminitiesFilter());
                setHotelAdapter();
        }else{
            modelAllHotelLists = db.getHotelAllData(sessionManager.getCountryCode());
            if(modelAllHotelLists.size()>0){
                setHotelAdapter();
            }else{
                getHotelLIST();
            }
        }


    }

    public void setHotelAdapter(){

        if (modelAllHotelLists.size() > 0) {
            GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
            FragmentManager fragmentManager1 = getSupportFragmentManager();
            adapterAllListHotels = new AdapterAllListHotels(activity, modelAllHotelLists, activity, fragmentManager1);
            rv_allListHotel.setLayoutManager(manager1);
            rv_allListHotel.setAdapter(adapterAllListHotels);
            iv_filter.setVisibility(View.VISIBLE);
            tv_title.setText(getText(R.string.all_hotesl)+"("+modelAllHotelLists.size()+")");
            ll_record_not_found.setVisibility(View.GONE);
            rv_allListHotel.setVisibility(View.VISIBLE);

        } else {
            Snackbar.make(v, "Hotel not found.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
            FragmentManager fragmentManager1 = getSupportFragmentManager();
            adapterAllListHotels = new AdapterAllListHotels(activity, modelAllHotelLists, activity, fragmentManager1);
            rv_allListHotel.setLayoutManager(manager1);
            rv_allListHotel.setAdapter(adapterAllListHotels);
            iv_filter.setVisibility(View.VISIBLE);
            tv_title.setText(getText(R.string.all_hotesl));
            ll_record_not_found.setVisibility(View.VISIBLE);
            rv_allListHotel.setVisibility(View.GONE);
        }


    }
}
