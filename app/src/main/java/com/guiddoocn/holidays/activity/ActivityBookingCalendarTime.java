package com.guiddoocn.holidays.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterBookingTiming;
import com.guiddoocn.holidays.models.ModelActivitiesCancellatioPolicy;
import com.guiddoocn.holidays.models.ModelActivitiesDetails;
import com.guiddoocn.holidays.models.ModelActivitiesOperationTiming;
import com.guiddoocn.holidays.models.ModelPriceMain;
import com.guiddoocn.holidays.models.ModelPricesDetails;
import com.guiddoocn.holidays.models.ModelPricingList;
import com.guiddoocn.holidays.models.ModelTiming;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityBookingCalendarTime extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView tv_back;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.datePicker)
    CalendarView datePicker;

    @BindView(R.id.tv_selected_date)
    TextView tv_selected_date;

    @BindView(R.id.tv_selected_time)
    TextView tv_selected_time;

    @BindView(R.id.rv_list)
    RecyclerView rv_ticket_list;

    @BindView(R.id.btn_Pay_Now)
    CardView btn_Pay_Now;

    boolean isBookingDate = false, FlagDate = false, FlagTime = false,FlagDateTimeCheck = true,AvailableDayFlag=false;
    private String Event_ID, Discount, Discount_Cap, Today_Day = "";
    private Context mContext;
    private SessionManager sessionManager;

    private CustomResponseDialog customResponseDialog;
    private ArrayList<ModelActivitiesDetails> activitiesDetailstModels;
    private ArrayList<ModelPriceMain> pricingDetailsModels;
    private ArrayList<ModelPricesDetails> pricesDetails;
    private ArrayList<ModelActivitiesOperationTiming> activitiesOperationTiming;
    private ArrayList<ModelActivitiesCancellatioPolicy> cancellatioPolicyModels;
    private ArrayList<ModelTiming> modelTimings;
    private AdapterBookingTiming activitiesListAdapter;
    private Calendar myCalendar;
    private SQLiteHandler db;
    private String time = "", Selected_Date, Selected_Time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_calander_time);
        mContext = this;
        db = new SQLiteHandler(mContext);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(mContext);
        customResponseDialog = new CustomResponseDialog(mContext);
        myCalendar = Calendar.getInstance();

        tv_title.setVisibility(View.VISIBLE);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }

        pricingDetailsModels = new ArrayList<ModelPriceMain>();

        try{

            Bundle b = getIntent().getExtras();
            if (b != null) {
                Event_ID = getIntent().getStringExtra("EventID");
                Discount = getIntent().getStringExtra("Discount");
                Discount_Cap = getIntent().getStringExtra("Discount_cap");
                tv_title.setText(getIntent().getStringExtra("EventName"));
                cancellatioPolicyModels = (ArrayList<ModelActivitiesCancellatioPolicy>) getIntent().getSerializableExtra("CancellationPolicyModels");
                activitiesDetailstModels = (ArrayList<ModelActivitiesDetails>) getIntent().getSerializableExtra("ListDetailModels");

                datePicker.setBackgroundColor(Color.parseColor("#FFFFFF"));

                myCalendar.add(Calendar.DATE, Integer.parseInt(activitiesDetailstModels.get(0).getReleaseDay()));  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                String output = sdf1.format(myCalendar.getTime());

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                try {
                    Date date = format.parse(output);
                    datePicker.setMinDate(date.getTime());
                    datePicker.setDate(date.getTime());
                    Log.e("Updated Date---->", String.valueOf(date.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                    Date date = new Date();
                    datePicker.setMinDate(date.getTime());
                }

                GetCurrentDate();


                if (sessionManager.isNetworkAvailable()) {
                    getPriceDetails();
                } else {
                    Snackbar.make(findViewById(R.id.coordinate_container), "Please check your internet connection.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            } else {
                Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
            }

            tv_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   onBackPressed();
                }
            });


            btn_Pay_Now.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (FlagDate) {

                        if (FlagTime) {
                            // GetAllBookDetails();
                            Intent intent = new Intent(mContext, ActivityBookingPricing.class);
                            intent.putExtra("BookingFlag", getIntent().getStringExtra("BookingFlag"));
                            intent.putExtra("EventID", Event_ID);
                            intent.putExtra("EventName", getIntent().getStringExtra("EventName"));
                            intent.putExtra("Selected_Date", Selected_Date);
                            intent.putExtra("Selected_Time", Selected_Time);//
                            intent.putExtra("pricesDetails", (ArrayList<ModelPricesDetails>) pricesDetails);
                            intent.putExtra("ListDetailModels", (ArrayList<ModelActivitiesDetails>) activitiesDetailstModels);
                            intent.putExtra("ModelPriceMain", (ArrayList<ModelPriceMain>) pricingDetailsModels);
                            intent.putExtra("CancellationPolicyModels", (ArrayList<ModelActivitiesCancellatioPolicy>) cancellatioPolicyModels);
                            mContext.startActivity(intent);

                            if (getIntent().getStringExtra("BookingFlag").equalsIgnoreCase("Cart")) {
                                finish();
                            }
                        } else {
                            tv_selected_time.startAnimation(shakeError());
                            Snackbar.make(findViewById(R.id.coordinate_container), "Please Select Time", Snackbar.LENGTH_LONG).show();
                            tv_selected_time.setText("Please Select Time");
                            tv_selected_time.setTextColor(Color.parseColor("#DB6556"));
                            FlagTime = false;
                        }
                    } else {
                        tv_selected_date.startAnimation(shakeError());
                        Snackbar.make(findViewById(R.id.coordinate_container), "Please Select Date", Snackbar.LENGTH_LONG).show();
                        tv_selected_date.setText("Please Select Date");
                        tv_selected_date.setTextColor(Color.parseColor("#DB6556"));
                        FlagDate = false;
                    }
                }
            });

            datePicker.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, month);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                    if (pricingDetailsModels.size() > 0) {
                        try {

                            String myFormat = "dd/MM/yyyy";
                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                            int dayOfWeek = myCalendar.get(Calendar.DAY_OF_WEEK);
                            String day = new DateFormatSymbols().getShortWeekdays()[dayOfWeek];

                            if (!activitiesOperationTiming.get(0).getDay().equalsIgnoreCase("daily")) {
                                if (!db.getCalendarValue(day.toLowerCase()).equalsIgnoreCase("")) {
                                    Log.e("Selected Date", day.toLowerCase() + " --->" + db.getCalendarValue(day.toLowerCase()));
                                    JSONArray bookingtime = null;
                                    modelTimings = new ArrayList<>();
                                    modelTimings.clear();
                                    try {
                                        bookingtime = new JSONArray(db.getCalendarValue(day.toLowerCase()));
                                        for (int j = 0; j < bookingtime.length(); j++) {
                                            ModelTiming modelTiming = new ModelTiming();
                                            try {
                                                modelTiming.setTiming(bookingtime.get(j).toString());
                                                Log.e("Booking Time", bookingtime.get(j).toString());

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            modelTimings.add(modelTiming);
                                        }
                                        tv_selected_date.setText(day + ", " + sdf.format(myCalendar.getTime()));
                                        Selected_Date = tv_selected_date.getText().toString();
                                        tv_selected_date.setTextColor(Color.parseColor("#347038"));
                                        FlagDate = true;
                                        FlagTime = false;
                                        tv_selected_time.setText("Available time slot");
                                        tv_selected_time.setTextColor(Color.parseColor("#000000"));
                                        rv_ticket_list.setVisibility(View.VISIBLE);
                                        ShowBookingDate();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }else{
                                    modelTimings = new ArrayList<>();
                                    modelTimings.clear();
                                    ShowBookingDate();
                                    Snackbar.make(findViewById(R.id.coordinate_container), "Activity is not available on this date.Please select a different date.", Snackbar.LENGTH_LONG).show();
                                    tv_selected_date.setText("Check Operational Day");
                                    tv_selected_date.setTextColor(Color.parseColor("#DB6556"));
                                    FlagDate = false;
                                    tv_selected_time.setText("Activity is not available on this date.");
                                    tv_selected_time.setTextColor(Color.parseColor("#DB6556"));
                                    FlagTime = false;
                                    rv_ticket_list.setVisibility(View.GONE);
                                }
                            }else if(activitiesOperationTiming.get(0).getDay().equalsIgnoreCase("daily")){
                                tv_selected_date.setText(day + ", " + sdf.format(myCalendar.getTime()) /*+" "+activitiesOperationTiming.get(0).getBooking_timings()*/);
                                Selected_Date = tv_selected_date.getText().toString();
                                tv_selected_date.setTextColor(Color.parseColor("#347038"));
                                FlagDate = true;
                                FlagTime = false;
                                tv_selected_time.setText("Available time slot");
                                tv_selected_time.setTextColor(Color.parseColor("#000000"));
                                rv_ticket_list.setVisibility(View.VISIBLE);
                                ShowBookingDate();
                            }

                            ArrayList<ModelActivitiesOperationTiming> arrayList = activitiesDetailstModels.get(0).getOperationTimingModels();
                            boolean findDate = true;



                        } catch (Exception e) {
                            Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    } else {
                        Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong.pricing details not found.", Snackbar.LENGTH_LONG).show();
                    }
                }
            });

        }catch (Exception e){
            Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        FlagTime = false;
        tv_selected_time.setText("Available time slot");
        tv_selected_time.setTextColor(Color.parseColor("#000000"));

    }

    private void getPriceDetails() {
        customResponseDialog.showCustomDialog();

        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = sessionManager.getBaseUrl() + "products/activities/pricing?tour_id=" + Event_ID + "&api_key=0D1067102F935B4CC31E082BD45014D469E35268";

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        customResponseDialog.hideCustomeDialog();
                        Log.d("Price Response", response.toString());
                        try{
                            customResponseDialog.hideCustomeDialog();
                            db.deleteCalendarData();
                            new CallPriceData().execute(response.toString());

                        }catch (Exception e){
                            customResponseDialog.hideCustomeDialog();
                            Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
                    }
                }
        );

        queue.add(getRequest);
    }

    private class CallPriceData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String s = strings[0];

            Log.e("Price Detail Resp-->", s);
            try {
                pricingDetailsModels = new ArrayList<ModelPriceMain>();
                activitiesOperationTiming = new ArrayList<ModelActivitiesOperationTiming>();
                modelTimings = new ArrayList<ModelTiming>();

                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonData = jsonObject.getJSONObject("data");
                JSONObject jsonStatus = jsonData.getJSONObject("status");

                if (jsonStatus.getString("Success").equalsIgnoreCase("true")) {

                    try{

                        ModelPriceMain pricingModel = new ModelPriceMain();
                        pricingModel.setPricing_per_person(jsonData.getString("pricing_per_person"));
                        JSONArray activityListDay = jsonData.getJSONArray("operational_timings");

                        for (int k = 0; k < activityListDay.length(); k++) {
                            JSONObject activityJSON = activityListDay.getJSONObject(k);
                            ModelActivitiesOperationTiming operationTimingModel = new ModelActivitiesOperationTiming();
                            operationTimingModel.setDay(activityJSON.getString("Day"));
                            JSONArray TourTimings = activityJSON.getJSONArray("TourTimings");
                            for (int j = 0; j < TourTimings.length(); j++) {
                                JSONObject activitytime = TourTimings.getJSONObject(j);
                                operationTimingModel.setFrom_Time(activitytime.getString("From_Time"));
                                operationTimingModel.setTo_Time(activitytime.getString("To_Time"));
                            }
                            JSONArray bookingtime = activityJSON.getJSONArray("booking_timings");
                            if (Today_Day.equalsIgnoreCase(activityJSON.getString("Day"))) {
                                AvailableDayFlag=true;
                                FlagDateTimeCheck=false;
                                for (int j = 0; j < bookingtime.length(); j++) {
                                    ModelTiming modelTiming = new ModelTiming();
                                    modelTiming.setTiming(bookingtime.get(j).toString());
                                    modelTimings.add(modelTiming);
                                }
                            } else if (activityJSON.getString("Day").equalsIgnoreCase("daily")) {
                                AvailableDayFlag=true;
                                FlagDateTimeCheck=false;
                                for (int j = 0; j < bookingtime.length(); j++) {
                                    ModelTiming modelTiming = new ModelTiming();
                                    modelTiming.setTiming(bookingtime.get(j).toString());
                                    modelTimings.add(modelTiming);
                                }
                            }else if(FlagDateTimeCheck){
                                AvailableDayFlag=false;
                            }

                            db.setCalendarData(activityJSON.getString("Day"), String.valueOf(bookingtime));

                            operationTimingModel.setBooking_timings(modelTimings);
                            activitiesOperationTiming.add(operationTimingModel);
                        }

                        pricingModel.setOperationTimingModels(activitiesOperationTiming);

                        ArrayList<ModelPricingList> pricingLists = new ArrayList<>();
                        JSONArray jsonActivity = jsonData.getJSONArray("pricing");
                        for (int i = 0; i < jsonActivity.length(); i++) {
                            JSONObject activityJSON = jsonActivity.getJSONObject(i);
                            ModelPricingList pricingList = new ModelPricingList();
                            pricingList.setCurrency(activityJSON.getString("currency"));
                            pricingList.setFrom_date(activityJSON.getString("from_date"));
                            pricingList.setTo_date(activityJSON.getString("to_date"));

                            ArrayList<ModelPricesDetails> pricesDetailsArrayList = new ArrayList<>();
                            if (jsonData.getString("pricing_per_person").equalsIgnoreCase("true")) {
                                Log.e("group_pricing Fix", "---------------->" + sessionManager.getNewPrice());
                                JSONArray jsonpricing = activityJSON.getJSONArray("group_pricing");

                                for (int j = 0; j < jsonpricing.length(); j++) {
                                    JSONObject jsonObjectPrice = jsonpricing.getJSONObject(j);
                                    ModelPricesDetails pricesDetails = new ModelPricesDetails();
                                    if (sessionManager.getNewPrice().equalsIgnoreCase("0")) {
                                        pricesDetails.setAdult_price(getIsMarginPrice(jsonObjectPrice.getString("adult_price")));
                                        pricesDetails.setChild_price(getIsMarginPrice(jsonObjectPrice.getString("child_price")));
                                    } else {
                                        pricesDetails.setAdult_price(getIsMarginPrice(jsonObjectPrice.getString("adult_price")));
                                        pricesDetails.setChild_price(getIsMarginPrice(jsonObjectPrice.getString("child_price")));
                                    }
                                    pricesDetails.setFrom_pax_count(jsonObjectPrice.getString("from_pax_count"));
                                    pricesDetails.setInfant_price(getIsMarginPrice(jsonObjectPrice.getString("infant_price")));
                                    pricesDetails.setInventory(jsonObjectPrice.getString("inventory"));
                                    pricesDetails.setTo_pax_count(jsonObjectPrice.getString("to_pax_count"));
                                    pricesDetails.setCurrency(activityJSON.getString("currency"));
                                    pricesDetails.setMaximum_pax(jsonData.getString("maximum_pax"));
                                    pricesDetailsArrayList.add(pricesDetails);
                                }

                            } else {
                                Log.e("Unit Price Fix", "---------------->");

                                ModelPricesDetails pricesDetails = new ModelPricesDetails();
                                if (sessionManager.getNewPrice().equalsIgnoreCase("0")) {
                                    pricesDetails.setAdult_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                    pricesDetails.setChild_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                } else {
                                    pricesDetails.setAdult_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                    pricesDetails.setChild_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                }
                                pricesDetails.setInfant_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                pricesDetails.setInventory(activityJSON.getString("inventory"));
                                pricesDetails.setFrom_pax_count("1");
                                pricesDetails.setTo_pax_count(activityJSON.getString("inventory"));
                                pricesDetails.setCurrency(activityJSON.getString("currency"));
                                pricesDetails.setMaximum_pax(jsonData.getString("maximum_pax"));
                                pricesDetailsArrayList.add(pricesDetails);
                            }
                            pricingList.setPricingDetails(pricesDetailsArrayList);
                            pricingLists.add(pricingList);
                        }
                        pricingModel.setPricesArrayList(pricingLists);
                        pricingDetailsModels.add(pricingModel);

                    }catch (RuntimeException e){
                        Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong.", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }



                } else {
                    Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            } catch (JSONException e) {
                Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                e.printStackTrace();
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String s) {

            if(AvailableDayFlag){
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            tv_selected_time.setText("Available time slot");
                            tv_selected_time.setTextColor(Color.parseColor("#000000"));
                            FlagTime = true;
                        }
                    });
                }else{

                    tv_selected_time.setText("Available time slot");
                    tv_selected_time.setTextColor(Color.parseColor("#000000"));
                    FlagTime = true;
                }
            }else{
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv_selected_time.setText("Activity is not available on this date.");
                            tv_selected_time.setTextColor(Color.parseColor("#DB6556"));
                            FlagTime = false;
                        }
                    });
                }else{
                    tv_selected_time.setText("Activity is not available on this date.");
                    tv_selected_time.setTextColor(Color.parseColor("#DB6556"));
                    FlagTime = false;
                }
            }
            ShowBookingDate();
        }
    }


    public void ShowBookingDate() {
        Log.e("Booking Size-->", String.valueOf(modelTimings.size()));
        try{

            FlagTime = false;
            String myFormat = "dd/MM/yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            for(int i=0;pricingDetailsModels.get(0).getPricesArrayList().size()>i;i++){

                ModelPricingList pricingList = pricingDetailsModels.get(0).getPricesArrayList().get(i);


                String oeStartDateStr = pricingList.getFrom_date();
                String oeEndDateStr = pricingList.getTo_date();

                Log.e("From Date",oeStartDateStr);
                Log.e("To Date",oeEndDateStr);

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                Date startDate = null;
                Date endDate = null;

                startDate = dateFormat.parse(oeStartDateStr);
                endDate = dateFormat.parse(oeEndDateStr);
                Date d = myCalendar.getTime();
                String currDt = sdf.format(d);


                Log.e("Selected Date",currDt);


                if ((d.after(startDate) && (d.before(endDate))) || (currDt.equals(sdf.format(startDate)) || currDt.equals(sdf.format(endDate)))) {
                    FlagDate = true;
                    Log.e("Found From Date",startDate.toString());
                    Log.e("Found To Date",endDate.toString());
                    Log.e("Found Selected Date",currDt);
                    pricesDetails = new ArrayList<>();
                    pricesDetails = pricingList.getPricingDetails();
                    Log.e("Final Found List",pricesDetails.toString());

                    break;
                } else{
                    FlagDate = false;
                }


            }

            if(FlagDate){

            }else{
                tv_selected_date.startAnimation(shakeError());
                Snackbar.make(findViewById(R.id.coordinate_container), "Selected date not available in operational date range", Snackbar.LENGTH_LONG).show();
                tv_selected_date.setText("Check Operational Date Range ");
                tv_selected_date.setTextColor(Color.parseColor("#DB6556"));
                FlagDate = false;
            }


        }catch (ParseException e){

        }








        rv_ticket_list.setHasFixedSize(true);
        activitiesListAdapter = new AdapterBookingTiming(mContext, modelTimings, new AdapterBookingTiming.OnItemClickListener() {

            @Override
            public void onItemClick(ModelTiming item, LinearLayout layout) {
                FlagTime = true;
                Selected_Time = item.getTiming();
                tv_selected_time.setText("Available time slot");
                tv_selected_time.setTextColor(Color.parseColor("#000000"));
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_ticket_list.setLayoutManager(mLayoutManager);
        rv_ticket_list.setAdapter(activitiesListAdapter);

    }

    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }

    public void GetCurrentDate() {

        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        Log.e("Increase Day",activitiesDetailstModels.get(0).getReleaseDay());
        // myCalendar.add(Calendar.DATE, Integer.parseInt(activitiesDetailstModels.get(0).getReleaseDay()));
        int dayOfWeek = myCalendar.get(Calendar.DAY_OF_WEEK);
        String day = new DateFormatSymbols().getShortWeekdays()[dayOfWeek];
        Today_Day = day.toLowerCase();
        Log.e("Today_Day-->", Today_Day);
        tv_selected_date.setText(day + ", " + sdf.format(myCalendar.getTime()));
        Selected_Date = tv_selected_date.getText().toString();
        tv_selected_date.setTextColor(Color.parseColor("#347038"));
        FlagDate = true;
    }

    public String getDiscountPrice(String Main_price) {
        String data = null;
        double discount = Double.parseDouble(Discount);
        double discount_cap = Double.parseDouble(Discount_Cap);
        double Old_price = Double.parseDouble(Main_price);
        double New_price = Old_price - (Old_price / 100 * discount);
        double Difference = Old_price - New_price;

        if (discount_cap > 0) {

            if (discount_cap < Difference) {
                Double Discount_Cap_Price = Old_price - discount_cap;
                data = String.valueOf(Discount_Cap_Price);
            } else {
                data = String.valueOf(New_price);
            }
        } else {
            data = String.valueOf(New_price);
        }

        return data;
    }

    public String getIsMarginPrice(String Main_price) {
        try{
            String data = null;
            double discount = Double.parseDouble(sessionManager.getIsmargin());
            double Old_price = Double.parseDouble(Main_price);
            double New_price = Old_price - (Old_price * discount / 100);
            data = String.format("%.02f", New_price );
            return data;
        }catch (Exception e){
            return Main_price;
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
