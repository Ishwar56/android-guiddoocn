package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterAllListCustomeActivities;
import com.guiddoocn.holidays.adapter.AdapterSpinerList;
import com.guiddoocn.holidays.models.ModelActivitiesList;
import com.guiddoocn.holidays.models.ModelActivitiesOperationTiming;
import com.guiddoocn.holidays.models.ModelPricesDetails;
import com.guiddoocn.holidays.models.ModelPriceMain;
import com.guiddoocn.holidays.models.ModelPricingList;
import com.guiddoocn.holidays.models.ModelSubCategoryPreferences;
import com.guiddoocn.holidays.models.ModelSubDiningPreferences;
import com.guiddoocn.holidays.models.ModelTiming;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAllListCustomeActivities extends AppCompatActivity {
    private static final String TAG = "AllListActivities";

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.iv_filter)
    ImageView iv_filter;

    @BindView(R.id.rv_top_selling_list)
    RecyclerView rv_top_selling_list;

    @BindView(R.id.rv_allList)
    RecyclerView rv_allList;

    @BindView(R.id.rv_transfer_list)
    RecyclerView rv_transfer_list;

    @BindView(R.id.rv_ticket_list)
    RecyclerView rv_ticket_list;

    @BindView(R.id.rv_dining_list)
    RecyclerView rv_dining_list;

    @BindView(R.id.rv_vouchers_list)
    RecyclerView rv_vouchers_list;

    @BindView(R.id.rv_private_list)
    RecyclerView rv_private_list;

    @BindView(R.id.ll_ltinerary)
    LinearLayout ll_ltinerary;

    @BindView(R.id.ll_skip_excursion)
    LinearLayout ll_skip_excursion;//

    @BindView(R.id.ll_record_not_found)
    LinearLayout ll_record_not_found;

    SearchView mSearchView;
    MenuItem mSearch;

    @BindView(R.id.ll_tab1_top_celling)
    LinearLayout ll_tab1_top_celling;
    @BindView(R.id.tv_tab1_top_celling)
    TextView tv_tab1_top_celling;
    @BindView(R.id.vv_tab1_top_celling)
    View vv_tab1_top_celling;

    @BindView(R.id.ll_tab2_all_activities)
    LinearLayout ll_tab2_all_activities;
    @BindView(R.id.tv_tab2_all_activities)
    TextView tv_tab2_all_activities;
    @BindView(R.id.vv_tab2_all_activities)
    View vv_tab2_all_activities;

    @BindView(R.id.ll_tab3_transfers)
    LinearLayout ll_tab3_transfers;
    @BindView(R.id.tv_tab3_transfers)
    TextView tv_tab3_transfers;
    @BindView(R.id.vv_tab3_transfers)
    View vv_tab3_transfers;

    @BindView(R.id.ll_tab4_tickets)
    LinearLayout ll_tab4_tickets;
    @BindView(R.id.tv_tab4_tickets)
    TextView tv_tab4_tickets;
    @BindView(R.id.vv_tab4_tickets)
    View vv_tab4_tickets;

    @BindView(R.id.ll_tab5_dining)
    LinearLayout ll_tab5_dining;
    @BindView(R.id.tv_tab5_dining)
    TextView tv_tab5_dining;
    @BindView(R.id.vv_tab5_dining)
    View vv_tab5_dining;

    @BindView(R.id.ll_tab6_vouchers)
    LinearLayout ll_tab6_vouchers;
    @BindView(R.id.tv_tab6_vouchers)
    TextView tv_tab6_vouchers;
    @BindView(R.id.vv_tab6_vouchers)
    View vv_tab6_vouchers;

    @BindView(R.id.ll_tab7_private)
    LinearLayout ll_tab7_private;
    @BindView(R.id.tv_tab7_private)
    TextView tv_tab7_private;
    @BindView(R.id.vv_tab7_private)
    View vv_tab7_private;


    private CustomResponseDialog dialog;
    private Context mContext;
    private SessionManager sessionManager;
    private SQLiteHandler db;
    private ArrayList<ModelActivitiesList> array_top_selling = new ArrayList<>();
    private ArrayList<ModelActivitiesList> arrayListActivities = new ArrayList<>();
    private ArrayList<ModelActivitiesList> array_transfer = new ArrayList<>();
    private ArrayList<ModelActivitiesList> array_tickets = new ArrayList<>();
    private ArrayList<ModelActivitiesList> array_dining = new ArrayList<>();
    private ArrayList<ModelActivitiesList> array_vouchers = new ArrayList<>();
    private ArrayList<ModelActivitiesList> array_private = new ArrayList<>();

    private ArrayList<ModelActivitiesList> arrayListDialogActivities = new ArrayList<>();
    private ArrayList<ModelSubDiningPreferences> modelSubDiningPreferences;
    private ArrayList<ModelSubCategoryPreferences> modelSubCategoryPreferences;
    private ArrayList<ModelPriceMain> pricingDetailsModels;
    private ArrayList<ModelPricingList> pricingArrayLists;
    private ArrayList<ModelPricesDetails> pricesDetailsArrayList;

    private ArrayList<ModelActivitiesOperationTiming> activitiesOperationTiming;
    private ArrayList<ModelTiming> modelTimings;
    private AdapterAllListCustomeActivities adapter_top_selling;
    private AdapterAllListCustomeActivities adapterAllListCustomeActivities;
    private AdapterAllListCustomeActivities adapter_transfer;
    private AdapterAllListCustomeActivities adapter_tickets;
    private AdapterAllListCustomeActivities adapter_dining;
    private AdapterAllListCustomeActivities adapter_vouchers;
    private AdapterAllListCustomeActivities adapter_private;
    private Calendar check_in_Calendar;
    private DatePickerDialog.OnDateSetListener Fromdate_Check_In;
    private int dialog_pos;
    private String Event_ID,activityName,Discount,booking_Time="",booking_Days="", Discount_Cap, Today_Day = "",searchText;;
    boolean FlagTopSelling =false, FlagAllActivities=true, FlagTransfer=false,FlagTickets=false,FlagDining=false,FlagVouchers=false,FlagPrivate=false, isBookingDate = false, FlagDate = false, FlagTime = false,FlagDateTimeCheck = true,AvailableDayFlag=false;
    private double Starting_price;
    ArrayList<String> noRooms = new ArrayList<String>();
    ArrayList<String> noDays = new ArrayList<String>();
    Spinner sp_time,sp_Date;
    private int  number_of_Days = 0, number_of_child = 0, NoOfRooms = 1;
    private double adult_price = 0.0,child_price=0.0,total_adult_price = 0.0,total_child_price=0.0,activity_Amt = 0.0;
    private double latitude, longitude, Adult_Total_Val = 0.0, Child_Total_Val = 0.0, Infant_Total_Val = 0.0, Final_Cost = 0.0;
    private int total_count = 1, unit_count = 0, unit_minus = 0, FinalCount = 0,adult_no = 1, child_no = 0,child_no_Count = 0, infant_no = 0,No_of_Unit_Count;

    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_list);

        ButterKnife.bind(this);
        activity = this;
        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        db = new SQLiteHandler(mContext);
        check_in_Calendar = Calendar.getInstance();
        GetCurrentDate();

        int dateDifference = 0;
        String check_in_Date = sessionManager.getCheckInDate();
        String check_out_Date = sessionManager.getCheckOutDate();
        try{
            dateDifference = (int) getDateDiff(new SimpleDateFormat("dd-MM-yyyy"), check_in_Date, check_out_Date);
            NoOfRooms = dateDifference;
            Log.e("Date different--->", String.valueOf(dateDifference));
        }catch (Exception e){
            NoOfRooms=1;

        }

        pricingArrayLists = new ArrayList<>();
        pricesDetailsArrayList = new ArrayList<>();



        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }

        ll_tab1_top_celling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlagTopSelling=true;
                FlagAllActivities=false;
                FlagTransfer=false;
                FlagTickets=false;
                FlagDining=false;
                FlagVouchers=false;
                FlagPrivate=false;
                UpdateTabUI();
            }
        });

        ll_tab2_all_activities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlagTopSelling=false;
                FlagAllActivities=true;
                FlagTransfer=false;
                FlagTickets=false;
                FlagDining=false;
                FlagVouchers=false;
                FlagPrivate=false;
                UpdateTabUI();
            }
        });

        ll_tab3_transfers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlagTopSelling=false;
                FlagAllActivities=false;
                FlagTransfer=true;
                FlagTickets=false;
                FlagDining=false;
                FlagVouchers=false;
                FlagPrivate=false;
                UpdateTabUI();
            }
        });

        ll_tab4_tickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlagTopSelling=false;
                FlagAllActivities=false;
                FlagTransfer=false;
                FlagTickets=true;
                FlagDining=false;
                FlagVouchers=false;
                FlagPrivate=false;
                UpdateTabUI();
            }
        });

        ll_tab5_dining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlagTopSelling=false;
                FlagAllActivities=false;
                FlagTransfer=false;
                FlagTickets=false;
                FlagDining=true;
                FlagVouchers=false;
                FlagPrivate=false;
                UpdateTabUI();
            }
        });

        ll_tab6_vouchers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlagTopSelling=false;
                FlagAllActivities=false;
                FlagTransfer=false;
                FlagTickets=false;
                FlagDining=false;
                FlagVouchers=true;
                FlagPrivate=false;
                UpdateTabUI();
            }
        });

        ll_tab7_private.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlagTopSelling=false;
                FlagAllActivities=false;
                FlagTransfer=false;
                FlagTickets=false;
                FlagDining=false;
                FlagVouchers=false;
                FlagPrivate=true;
                UpdateTabUI();
            }
        });


        tv_title.setText(getText(R.string.all_events));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext,ActivityFilterActivities.class));

            }
        });

        ll_ltinerary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ActivityAllListCustomeActivities.this,ActivityItinerary.class);
                startActivity(intent);
                finish();
            }
        });

        ll_skip_excursion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityAllListCustomeActivities.this, ActivityAllListHotels.class);
                startActivity(intent);
                finish();
            }
        });

        try {

            List<String> items = new ArrayList<>();
            items = Arrays.asList(sessionManager.getChildAge().replaceAll("[a-zA-Z]", "").replaceAll(" ", "").split("\\s*,\\s*"));

            if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                child_no = 0;
            }else{

                boolean flagTemp=false;
                int count=0;
                for(int j = 0;items.size()>j;j++){
                    if(Integer.parseInt(items.get(j))>2){
                        count=count+1;
                        flagTemp=true;
                    }
                }
                if(flagTemp){
                    child_no = count;
                }else {
                    child_no = 0;
                }
            }

        }catch (Exception e){

        }

    }

    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void UpdateTabUI(){
        mSearchView.clearFocus();

        if(FlagTopSelling){
            tv_tab1_top_celling.setTextColor(Color.parseColor("#000000"));
            vv_tab1_top_celling.setBackgroundColor(Color.parseColor("#F66961"));
            tv_tab2_all_activities.setTextColor(Color.parseColor("#727272"));
            vv_tab2_all_activities.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab3_transfers.setTextColor(Color.parseColor("#727272"));
            vv_tab3_transfers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab4_tickets.setTextColor(Color.parseColor("#727272"));
            vv_tab4_tickets.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab5_dining.setTextColor(Color.parseColor("#727272"));
            vv_tab5_dining.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab6_vouchers.setTextColor(Color.parseColor("#727272"));
            vv_tab6_vouchers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab7_private.setTextColor(Color.parseColor("#727272"));
            vv_tab7_private.setBackgroundColor(Color.parseColor("#FFFFFF"));
            rv_top_selling_list.setVisibility(View.VISIBLE);
            rv_allList.setVisibility(View.GONE);
            rv_transfer_list.setVisibility(View.GONE);
            rv_ticket_list.setVisibility(View.GONE);
            rv_dining_list.setVisibility(View.GONE);
            rv_vouchers_list.setVisibility(View.GONE);
            rv_private_list.setVisibility(View.GONE);
        }else if(FlagAllActivities){
            tv_tab1_top_celling.setTextColor(Color.parseColor("#727272"));
            vv_tab1_top_celling.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab2_all_activities.setTextColor(Color.parseColor("#000000"));
            vv_tab2_all_activities.setBackgroundColor(Color.parseColor("#F66961"));
            tv_tab3_transfers.setTextColor(Color.parseColor("#727272"));
            vv_tab3_transfers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab4_tickets.setTextColor(Color.parseColor("#727272"));
            vv_tab4_tickets.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab5_dining.setTextColor(Color.parseColor("#727272"));
            vv_tab5_dining.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab6_vouchers.setTextColor(Color.parseColor("#727272"));
            vv_tab6_vouchers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab7_private.setTextColor(Color.parseColor("#727272"));
            vv_tab7_private.setBackgroundColor(Color.parseColor("#FFFFFF"));
            rv_top_selling_list.setVisibility(View.GONE);
            rv_allList.setVisibility(View.VISIBLE);
            rv_transfer_list.setVisibility(View.GONE);
            rv_ticket_list.setVisibility(View.GONE);
            rv_dining_list.setVisibility(View.GONE);
            rv_vouchers_list.setVisibility(View.GONE);
            rv_private_list.setVisibility(View.GONE);
        }else if(FlagTransfer){
            tv_tab1_top_celling.setTextColor(Color.parseColor("#727272"));
            vv_tab1_top_celling.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab2_all_activities.setTextColor(Color.parseColor("#727272"));
            vv_tab2_all_activities.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab3_transfers.setTextColor(Color.parseColor("#000000"));
            vv_tab3_transfers.setBackgroundColor(Color.parseColor("#F66961"));
            tv_tab4_tickets.setTextColor(Color.parseColor("#727272"));
            vv_tab4_tickets.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab5_dining.setTextColor(Color.parseColor("#727272"));
            vv_tab5_dining.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab6_vouchers.setTextColor(Color.parseColor("#727272"));
            vv_tab6_vouchers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab7_private.setTextColor(Color.parseColor("#727272"));
            vv_tab7_private.setBackgroundColor(Color.parseColor("#FFFFFF"));
            rv_top_selling_list.setVisibility(View.GONE);
            rv_allList.setVisibility(View.GONE);
            rv_transfer_list.setVisibility(View.VISIBLE);
            rv_ticket_list.setVisibility(View.GONE);
            rv_dining_list.setVisibility(View.GONE);
            rv_vouchers_list.setVisibility(View.GONE);
            rv_private_list.setVisibility(View.GONE);
        }else if(FlagTickets){
            tv_tab1_top_celling.setTextColor(Color.parseColor("#727272"));
            vv_tab1_top_celling.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab2_all_activities.setTextColor(Color.parseColor("#727272"));
            vv_tab2_all_activities.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab3_transfers.setTextColor(Color.parseColor("#727272"));
            vv_tab3_transfers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab4_tickets.setTextColor(Color.parseColor("#000000"));
            vv_tab4_tickets.setBackgroundColor(Color.parseColor("#F66961"));
            tv_tab5_dining.setTextColor(Color.parseColor("#727272"));
            vv_tab5_dining.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab6_vouchers.setTextColor(Color.parseColor("#727272"));
            vv_tab6_vouchers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab7_private.setTextColor(Color.parseColor("#727272"));
            vv_tab7_private.setBackgroundColor(Color.parseColor("#FFFFFF"));
            rv_top_selling_list.setVisibility(View.GONE);
            rv_allList.setVisibility(View.GONE);
            rv_transfer_list.setVisibility(View.GONE);
            rv_ticket_list.setVisibility(View.VISIBLE);
            rv_dining_list.setVisibility(View.GONE);
            rv_vouchers_list.setVisibility(View.GONE);
            rv_private_list.setVisibility(View.GONE);
        }else if(FlagDining){
            tv_tab1_top_celling.setTextColor(Color.parseColor("#727272"));
            vv_tab1_top_celling.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab2_all_activities.setTextColor(Color.parseColor("#727272"));
            vv_tab2_all_activities.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab3_transfers.setTextColor(Color.parseColor("#727272"));
            vv_tab3_transfers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab4_tickets.setTextColor(Color.parseColor("#727272"));
            vv_tab4_tickets.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab5_dining.setTextColor(Color.parseColor("#000000"));
            vv_tab5_dining.setBackgroundColor(Color.parseColor("#F66961"));
            tv_tab6_vouchers.setTextColor(Color.parseColor("#727272"));
            vv_tab6_vouchers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab7_private.setTextColor(Color.parseColor("#727272"));
            vv_tab7_private.setBackgroundColor(Color.parseColor("#FFFFFF"));
            rv_top_selling_list.setVisibility(View.GONE);
            rv_allList.setVisibility(View.GONE);
            rv_transfer_list.setVisibility(View.GONE);
            rv_ticket_list.setVisibility(View.GONE);
            rv_dining_list.setVisibility(View.VISIBLE);
            rv_vouchers_list.setVisibility(View.GONE);
            rv_private_list.setVisibility(View.GONE);
        }else if(FlagVouchers){
            tv_tab1_top_celling.setTextColor(Color.parseColor("#727272"));
            vv_tab1_top_celling.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab2_all_activities.setTextColor(Color.parseColor("#727272"));
            vv_tab2_all_activities.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab3_transfers.setTextColor(Color.parseColor("#727272"));
            vv_tab3_transfers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab4_tickets.setTextColor(Color.parseColor("#727272"));
            vv_tab4_tickets.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab5_dining.setTextColor(Color.parseColor("#727272"));
            vv_tab5_dining.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab6_vouchers.setTextColor(Color.parseColor("#000000"));
            vv_tab6_vouchers.setBackgroundColor(Color.parseColor("#F66961"));
            tv_tab7_private.setTextColor(Color.parseColor("#727272"));
            vv_tab7_private.setBackgroundColor(Color.parseColor("#FFFFFF"));
            rv_top_selling_list.setVisibility(View.GONE);
            rv_allList.setVisibility(View.GONE);
            rv_transfer_list.setVisibility(View.GONE);
            rv_ticket_list.setVisibility(View.GONE);
            rv_dining_list.setVisibility(View.GONE);
            rv_vouchers_list.setVisibility(View.VISIBLE);
            rv_private_list.setVisibility(View.GONE);
        }else if(FlagPrivate){
            tv_tab1_top_celling.setTextColor(Color.parseColor("#727272"));
            vv_tab1_top_celling.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab2_all_activities.setTextColor(Color.parseColor("#727272"));
            vv_tab2_all_activities.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab3_transfers.setTextColor(Color.parseColor("#727272"));
            vv_tab3_transfers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab4_tickets.setTextColor(Color.parseColor("#727272"));
            vv_tab4_tickets.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab5_dining.setTextColor(Color.parseColor("#727272"));
            vv_tab5_dining.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab6_vouchers.setTextColor(Color.parseColor("#727272"));
            vv_tab6_vouchers.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_tab7_private.setTextColor(Color.parseColor("#000000"));
            vv_tab7_private.setBackgroundColor(Color.parseColor("#F66961"));
            rv_top_selling_list.setVisibility(View.GONE);
            rv_allList.setVisibility(View.GONE);
            rv_transfer_list.setVisibility(View.GONE);
            rv_ticket_list.setVisibility(View.GONE);
            rv_dining_list.setVisibility(View.GONE);
            rv_vouchers_list.setVisibility(View.GONE);
            rv_private_list.setVisibility(View.VISIBLE);
        }
        CheckRecord();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menus) {
        try {
            getMenuInflater().inflate(R.menu.search_menu, menus);
            mSearch = menus.findItem(R.id.action_search);
            mSearchView = (SearchView) mSearch.getActionView();
            mSearchView.setPadding(0, 8, 0, 8);
            mSearchView.setMaxWidth(Integer.MAX_VALUE);
            View searchplate = (View) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
            searchplate.setBackgroundResource(R.drawable.custombg_white_border);
        }catch (Exception e){

        }

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(FlagTopSelling){
                    searchText = s;
                    adapter_top_selling.getFilter().filter(searchText);
                }else if(FlagAllActivities){
                    searchText = s;
                    adapterAllListCustomeActivities.getFilter().filter(searchText);
                }else if(FlagTransfer){
                    searchText = s;
                    adapter_transfer.getFilter().filter(searchText);
                }else if(FlagTickets){
                    searchText = s;
                    adapter_tickets.getFilter().filter(searchText);
                }else if(FlagDining){
                    searchText = s;
                    adapter_dining.getFilter().filter(searchText);
                }else if(FlagVouchers){
                    searchText = s;
                    adapter_vouchers.getFilter().filter(searchText);
                }else if(FlagPrivate){
                    searchText = s;
                    adapter_private.getFilter().filter(searchText);
                }

                return true;
            }
        });

        return true;
    }


    private void get_list_activities() {
        String search_date="";
        try{
            String strCurrentDate = sessionManager.getCheckInDate();
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date newDate = format.parse(strCurrentDate);

            format = new SimpleDateFormat("yyyy-MM-dd");
            search_date = format.format(newDate);

        }catch (Exception e){

        }
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrl() + "products/activities/listing";
        Log.e(TAG, "list_activities URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("category_id", "0");
            object.put("city_id", 0);
            object.put("country_id",sessionManager.getCountryCode());
            object.put("search_date",search_date);

            Log.e(TAG, "parameters URL---->" + object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "list_activities Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            arrayListActivities = new ArrayList<ModelActivitiesList>();
                            array_top_selling = new ArrayList<ModelActivitiesList>();
                            array_transfer =  new ArrayList<ModelActivitiesList>();
                            array_tickets = new ArrayList<>();
                            array_dining = new ArrayList<>();
                            array_vouchers = new ArrayList<>();
                            array_private = new ArrayList<>();
                            JSONObject jsonStatus = jsonObject.getJSONObject("status");
                            if (jsonStatus.getString("status_code").equalsIgnoreCase("200")) {

                                JSONArray jsonArrayActivity = jsonObject.getJSONArray("listing_data");
                                for (int i = 0; jsonArrayActivity.length() > i; i++) {

                                    ModelActivitiesList listModel = new ModelActivitiesList();
                                    JSONObject jsonActivity = jsonArrayActivity.getJSONObject(i);
                                    listModel.setTour_id(jsonActivity.getString("tour_id"));
                                    listModel.setAddress(jsonActivity.getString("address"));
                                    listModel.setCategory_id(jsonActivity.getString("category_id"));
                                    listModel.setCategory(jsonActivity.getString("category_name"));
                                    listModel.setCity(jsonActivity.getString("city"));
                                    listModel.setCity_id(jsonActivity.getString("city_id"));
                                    listModel.setCurrency(jsonActivity.getString("currency"));
                                    listModel.setDiscount(jsonActivity.getString("discount"));
                                    listModel.setMaximum_pax(jsonActivity.getString("maximum_pax"));
                                    listModel.setIs_pricing_per_pax(jsonActivity.getString("is_pricing_per_pax"));
                                    listModel.setAdded_in_itinerary("False");
                                    listModel.setIs_special_offer(jsonActivity.getString("is_special_offer"));//
                                    if(jsonActivity.getString("discount_cap").equalsIgnoreCase("") || jsonActivity.getString("discount_cap").isEmpty()){
                                        listModel.setDiscount_cap("0");
                                    }else{
                                        listModel.setDiscount_cap(jsonActivity.getString("discount_cap"));
                                    }
                                    listModel.setFeatured_image(jsonActivity.getString("featured_image"));
                                    listModel.setFrom_time(jsonActivity.getString("from_time"));
                                    listModel.setTo_time(jsonActivity.getString("to_time"));

                                    JSONObject jsonDuration = jsonActivity.getJSONObject("duration");
                                    listModel.setDay(jsonDuration.getString("day"));
                                    listModel.setHour(jsonDuration.getString("hour"));
                                    listModel.setMin(jsonDuration.getString("min"));

                                    listModel.setName(jsonActivity.getString("name"));
                                    listModel.setRating(jsonActivity.getString("rating"));
                                    listModel.setShort_description(jsonActivity.getString("short_description"));
                                    //listModel.setStarting_from_price(jsonActivity.getString("starting_from_price"));
                                    listModel.setStarting_from_price(getIsMarginPrice(jsonActivity.getString("starting_from_price")));
                                    listModel.setLatitude(jsonActivity.getString("latitude"));
                                    listModel.setLongitude(jsonActivity.getString("longitude"));


                                    JSONArray Arraydinning_preference = jsonActivity.getJSONArray("dinning_preference");
                                    modelSubDiningPreferences = new ArrayList<>();
                                    for (int dp = 0;Arraydinning_preference.length()>dp;dp++){
                                        ModelSubDiningPreferences diningpreferencesModel = new ModelSubDiningPreferences();
                                        JSONObject jsonDinigPref = Arraydinning_preference.getJSONObject(dp);
                                        diningpreferencesModel.setSubdiningid(jsonDinigPref.getString("id"));
                                        diningpreferencesModel.setSubdiningName(jsonDinigPref.getString("name"));
                                        diningpreferencesModel.setCount(jsonDinigPref.getString("count"));
                                        diningpreferencesModel.setTour_id(jsonActivity.getString("tour_id"));
                                        modelSubDiningPreferences.add(diningpreferencesModel);
                                    }
                                    listModel.setModelSubDiningPreferences(modelSubDiningPreferences);

                                    JSONArray Arraysubcategory_name = jsonActivity.getJSONArray("subcategory_name");
                                    modelSubCategoryPreferences = new ArrayList<>();
                                    for (int dp = 0;Arraysubcategory_name.length()>dp;dp++){
                                        ModelSubCategoryPreferences categorypreferencesModel = new ModelSubCategoryPreferences();
                                        JSONObject jsonDinigPref = Arraysubcategory_name.getJSONObject(dp);
                                        categorypreferencesModel.setSubcategoryid(jsonDinigPref.getString("id"));
                                        categorypreferencesModel.setSubcategoryName(jsonDinigPref.getString("name"));
                                        categorypreferencesModel.setCount(jsonDinigPref.getString("count"));
                                        categorypreferencesModel.setTour_id(jsonActivity.getString("tour_id"));
                                        modelSubCategoryPreferences.add(categorypreferencesModel);
                                    }
                                    listModel.setModelSubCategoryPreferences(modelSubCategoryPreferences);

                                    if(jsonActivity.getString("transfer_included").equalsIgnoreCase("true")){
                                        listModel.setTransfer_included("WithTransfers");
                                    }else{
                                        listModel.setTransfer_included("Excursion");
                                    }

                                    ll_tab2_all_activities.setVisibility(View.VISIBLE);
                                    if(jsonActivity.getString("category_id").equalsIgnoreCase("1")){ //tickets
                                        array_tickets.add(listModel);
                                    }else if(jsonActivity.getString("category_id").equalsIgnoreCase("2")){ //tours
                                        //array_transfer.add(listModel);
                                    }else if(jsonActivity.getString("category_id").equalsIgnoreCase("3")){  //Dining
                                        array_dining.add(listModel);
                                    }else if(jsonActivity.getString("category_id").equalsIgnoreCase("4")){ //Leisure
                                        //array_transfer.add(listModel);
                                    }else if(jsonActivity.getString("category_id").equalsIgnoreCase("5")){ //Super Saver
                                       // array_top_selling.add(listModel);
                                    }else if(jsonActivity.getString("category_id").equalsIgnoreCase("6")){ //transfer
                                        array_transfer.add(listModel);
                                    }else if(jsonActivity.getString("category_id").equalsIgnoreCase("8")){ //top selling
                                        array_top_selling.add(listModel);
                                    }else if(jsonActivity.getString("category_id").equalsIgnoreCase("9")){ //Vouchers
                                        array_vouchers.add(listModel);
                                    }else if(jsonActivity.getString("category_id").equalsIgnoreCase("10")){ //Private tours
                                        array_private.add(listModel);
                                    }

                                    arrayListActivities.add(listModel);
                                }

                                if (arrayListActivities.size() > 0) {
                                    setHotelAdapter();
                                    iv_filter.setVisibility(View.INVISIBLE);
                                    new LoadData().execute(); // Load data base values hotels
                                } else {
                                    Toast.makeText(mContext, "record not found", Toast.LENGTH_LONG).show();
                                }



                                }else{
                                Toast.makeText(activity, jsonStatus.getString("Status_message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
       new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.hideCustomeDialog();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }

    public String getIsMarginPrice(String Main_price) {
        try{
            String data = null;
            double discount = Double.parseDouble(sessionManager.getIsmargin());
            double Old_price = Double.parseDouble(Main_price);
            double New_price = Old_price - (Old_price * discount / 100);
            data = String.format("%.02f", New_price );
            return data;
        }catch (Exception e){
            return Main_price;
        }


    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                for (int i = 0; arrayListActivities.size() > i; i++) {    // Add hotel data
                    db.setActivityData(arrayListActivities.get(i).getTour_id(),arrayListActivities.get(i).getName(),arrayListActivities.get(i).getCategory_id(),arrayListActivities.get(i).getCategory(),arrayListActivities.get(i).getDiscount(),arrayListActivities.get(i).getDiscount_cap(),arrayListActivities.get(i).getDay(),arrayListActivities.get(i).getHour(),arrayListActivities.get(i).getMin(),arrayListActivities.get(i).getRating(), Double.parseDouble(arrayListActivities.get(i).getStarting_from_price()),arrayListActivities.get(i).getFeatured_image(),arrayListActivities.get(i).getCurrency(),arrayListActivities.get(i).getCity_id(),arrayListActivities.get(i).getCity(),arrayListActivities.get(i).getFrom_time(),arrayListActivities.get(i).getTo_time(),arrayListActivities.get(i).getAddress(),arrayListActivities.get(i).getIs_pricing_per_pax(),arrayListActivities.get(i).getIs_special_offer(),arrayListActivities.get(i).getLatitude(),arrayListActivities.get(i).getLongitude(),arrayListActivities.get(i).getMaximum_pax(),arrayListActivities.get(i).getTransfer_included(),sessionManager.getCountryCode(),arrayListActivities.get(i).getAdded_in_itinerary());
                    if (arrayListActivities.get(i).getModelSubDiningPreferences().size() > 0) {    // Add hotel amnesties data
                        for (int j = 0; arrayListActivities.get(i).getModelSubDiningPreferences().size() > j; j++) {
                            db.setSubDinningData(arrayListActivities.get(i).getModelSubDiningPreferences().get(j).getSubdiningid(), arrayListActivities.get(i).getModelSubDiningPreferences().get(j).getSubdiningName(), arrayListActivities.get(i).getModelSubDiningPreferences().get(j).getCount(),arrayListActivities.get(i).getTour_id());
                        }
                    }

                    if (arrayListActivities.get(i).getModelSubCategoryPreferences().size() > 0) {    // Add hotel amnesties data
                        for (int j = 0; arrayListActivities.get(i).getModelSubCategoryPreferences().size() > j; j++) {
                            db.setSubCategoryData(arrayListActivities.get(i).getModelSubCategoryPreferences().get(j).getSubcategoryid(), arrayListActivities.get(i).getModelSubCategoryPreferences().get(j).getSubcategoryName(), arrayListActivities.get(i).getModelSubCategoryPreferences().get(j).getCount(),arrayListActivities.get(i).getTour_id());
                        }
                    }
                }


            } catch (SecurityException | NullPointerException e) {
                try {


                } catch (SecurityException | NullPointerException e1) {

                }
            }catch (Exception ex) {

                ex.printStackTrace();
                Log.e("DATA", "Errro" + ex.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e("@@@@@@@@@@@@@@", "Data inserting Done--->");
            iv_filter.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        array_top_selling = new ArrayList<>();
        array_transfer = new ArrayList<>();
        arrayListActivities = new ArrayList<>();
        array_tickets = new ArrayList<>();
        array_dining = new ArrayList<>();
        array_vouchers = new ArrayList<>();
        array_private=new ArrayList<>();

        if(sessionManager.getFilterActive()){
            if(FlagTopSelling){
                array_top_selling = db.getActivityCustFilterData(sessionManager.getCountryCode(),sessionManager.getActSortingPosition(),sessionManager.getFilterMinAmtAct()+".00",sessionManager.getFilterMaxAmtAct()+".00",sessionManager.getFilterMinStarAct(),sessionManager.getFilterMaxStarAct(),sessionManager.getActDiningFilter(),sessionManager.getActCatFilter(),sessionManager.getTransferFilter(),8);
                arrayListActivities = db.getActivityAllData(sessionManager.getCountryCode());
                array_transfer = db.getActivityCatData(sessionManager.getCountryCode(),"6");
                array_tickets = db.getActivityCatData(sessionManager.getCountryCode(),"1");
                array_dining = db.getActivityCatData(sessionManager.getCountryCode(),"3");
                array_vouchers = db.getActivityCatData(sessionManager.getCountryCode(),"9");
            }else if(FlagAllActivities){
                array_top_selling = db.getActivityCatData(sessionManager.getCountryCode(),"8");
                arrayListActivities = db.getActivityCustFilterData(sessionManager.getCountryCode(),sessionManager.getActSortingPosition(),sessionManager.getFilterMinAmtAct()+".00",sessionManager.getFilterMaxAmtAct()+".00",sessionManager.getFilterMinStarAct(),sessionManager.getFilterMaxStarAct(),sessionManager.getActDiningFilter(),sessionManager.getActCatFilter(),sessionManager.getTransferFilter(),0);
                array_transfer = db.getActivityCatData(sessionManager.getCountryCode(),"6");
                array_tickets = db.getActivityCatData(sessionManager.getCountryCode(),"1");
                array_dining = db.getActivityCatData(sessionManager.getCountryCode(),"3");
                array_vouchers = db.getActivityCatData(sessionManager.getCountryCode(),"9");
            }else if(FlagTransfer){
                array_top_selling = db.getActivityCatData(sessionManager.getCountryCode(),"8");
                arrayListActivities = db.getActivityAllData(sessionManager.getCountryCode());
                array_transfer = db.getActivityCustFilterData(sessionManager.getCountryCode(),sessionManager.getActSortingPosition(),sessionManager.getFilterMinAmtAct()+".00",sessionManager.getFilterMaxAmtAct()+".00",sessionManager.getFilterMinStarAct(),sessionManager.getFilterMaxStarAct(),sessionManager.getActDiningFilter(),sessionManager.getActCatFilter(),sessionManager.getTransferFilter(),6);
                array_tickets = db.getActivityCatData(sessionManager.getCountryCode(),"1");
                array_dining = db.getActivityCatData(sessionManager.getCountryCode(),"3");
                array_vouchers = db.getActivityCatData(sessionManager.getCountryCode(),"9");
            }else if(FlagTickets){
                array_top_selling = db.getActivityCatData(sessionManager.getCountryCode(),"8");
                arrayListActivities = db.getActivityAllData(sessionManager.getCountryCode());
                array_transfer = db.getActivityCatData(sessionManager.getCountryCode(),"6");
                array_tickets = db.getActivityCustFilterData(sessionManager.getCountryCode(),sessionManager.getActSortingPosition(),sessionManager.getFilterMinAmtAct()+".00",sessionManager.getFilterMaxAmtAct()+".00",sessionManager.getFilterMinStarAct(),sessionManager.getFilterMaxStarAct(),sessionManager.getActDiningFilter(),sessionManager.getActCatFilter(),sessionManager.getTransferFilter(),1);
                array_dining = db.getActivityCatData(sessionManager.getCountryCode(),"3");
                array_vouchers = db.getActivityCatData(sessionManager.getCountryCode(),"9");
            }else if(FlagDining){
                array_top_selling = db.getActivityCatData(sessionManager.getCountryCode(),"8");
                arrayListActivities = db.getActivityAllData(sessionManager.getCountryCode());
                array_transfer = db.getActivityCatData(sessionManager.getCountryCode(),"6");
                array_tickets = db.getActivityCatData(sessionManager.getCountryCode(),"1");
                array_dining = db.getActivityCustFilterData(sessionManager.getCountryCode(),sessionManager.getActSortingPosition(),sessionManager.getFilterMinAmtAct()+".00",sessionManager.getFilterMaxAmtAct()+".00",sessionManager.getFilterMinStarAct(),sessionManager.getFilterMaxStarAct(),sessionManager.getActDiningFilter(),sessionManager.getActCatFilter(),sessionManager.getTransferFilter(),3);
                array_vouchers = db.getActivityCatData(sessionManager.getCountryCode(),"9");
            }else if(FlagVouchers){
                array_top_selling = db.getActivityCatData(sessionManager.getCountryCode(),"8");
                arrayListActivities = db.getActivityAllData(sessionManager.getCountryCode());
                array_transfer = db.getActivityCatData(sessionManager.getCountryCode(),"6");
                array_tickets = db.getActivityCatData(sessionManager.getCountryCode(),"1");
                array_dining = db.getActivityCatData(sessionManager.getCountryCode(),"3");
                array_vouchers = db.getActivityCustFilterData(sessionManager.getCountryCode(),sessionManager.getActSortingPosition(),sessionManager.getFilterMinAmtAct()+".00",sessionManager.getFilterMaxAmtAct()+".00",sessionManager.getFilterMinStarAct(),sessionManager.getFilterMaxStarAct(),sessionManager.getActDiningFilter(),sessionManager.getActCatFilter(),sessionManager.getTransferFilter(),9);
            }else if(FlagPrivate){
                array_top_selling = db.getActivityCatData(sessionManager.getCountryCode(),"8");
                arrayListActivities = db.getActivityAllData(sessionManager.getCountryCode());
                array_transfer = db.getActivityCatData(sessionManager.getCountryCode(),"6");
                array_tickets = db.getActivityCatData(sessionManager.getCountryCode(),"1");
                array_dining = db.getActivityCatData(sessionManager.getCountryCode(),"3");
                array_vouchers = db.getActivityCatData(sessionManager.getCountryCode(),"9");
                array_private = db.getActivityCustFilterData(sessionManager.getCountryCode(),sessionManager.getActSortingPosition(),sessionManager.getFilterMinAmtAct()+".00",sessionManager.getFilterMaxAmtAct()+".00",sessionManager.getFilterMinStarAct(),sessionManager.getFilterMaxStarAct(),sessionManager.getActDiningFilter(),sessionManager.getActCatFilter(),sessionManager.getTransferFilter(),10);

            }

            setHotelAdapter();
        }else{
        arrayListActivities = db.getActivityAllData(sessionManager.getCountryCode());
        array_top_selling = db.getActivityCatData(sessionManager.getCountryCode(),"8");
        array_transfer = db.getActivityCatData(sessionManager.getCountryCode(),"6");
        array_tickets = db.getActivityCatData(sessionManager.getCountryCode(),"1");
        array_dining = db.getActivityCatData(sessionManager.getCountryCode(),"3");
        array_vouchers = db.getActivityCatData(sessionManager.getCountryCode(),"9");
        array_private = db.getActivityCatData(sessionManager.getCountryCode(),"10");

        if(arrayListActivities.size()>0){
            setHotelAdapter();
        }else{
            get_list_activities();
        }
        }

    }

    public void setHotelAdapter(){

        try {
            if(array_top_selling.size()>0){
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_top_selling = new AdapterAllListCustomeActivities(activity, array_top_selling, activity, fragmentManager1);
                rv_top_selling_list.setLayoutManager(manager1);
                rv_top_selling_list.setAdapter(adapter_top_selling);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab1_top_celling.setText("Top Selling"+"("+array_top_selling.size()+")");
            }else{
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_top_selling = new AdapterAllListCustomeActivities(activity, array_top_selling, activity, fragmentManager1);
                rv_top_selling_list.setLayoutManager(manager1);
                rv_top_selling_list.setAdapter(adapter_top_selling);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab1_top_celling.setText("Top Selling(0)");
            }

            if(arrayListActivities.size()>0){
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapterAllListCustomeActivities = new AdapterAllListCustomeActivities(activity, arrayListActivities, activity, fragmentManager1);
                rv_allList.setLayoutManager(manager1);
                rv_allList.setAdapter(adapterAllListCustomeActivities);
                iv_filter.setVisibility(View.VISIBLE);
                tv_title.setText(getText(R.string.all_events)+"("+arrayListActivities.size()+")");
                tv_tab2_all_activities.setText("All Activities"+"("+arrayListActivities.size()+")");
            }else{
                Toast.makeText(mContext, "record not found", Toast.LENGTH_LONG).show();
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapterAllListCustomeActivities = new AdapterAllListCustomeActivities(activity, arrayListActivities, activity, fragmentManager1);
                rv_allList.setLayoutManager(manager1);
                rv_allList.setAdapter(adapterAllListCustomeActivities);
                iv_filter.setVisibility(View.VISIBLE);
                tv_title.setText(getText(R.string.all_events));
                tv_tab2_all_activities.setText("All Activities(0)");
            }

            if(array_transfer.size()>0){
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_transfer = new AdapterAllListCustomeActivities(activity, array_transfer, activity, fragmentManager1);
                rv_transfer_list.setLayoutManager(manager1);
                rv_transfer_list.setAdapter(adapter_transfer);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab3_transfers.setText(array_transfer.get(0).getCategory()+"("+array_transfer.size()+")");
            }else{
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_transfer = new AdapterAllListCustomeActivities(activity, array_transfer, activity, fragmentManager1);
                rv_transfer_list.setLayoutManager(manager1);
                rv_transfer_list.setAdapter(adapter_transfer);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab3_transfers.setText("Transfers(0)");
            }

            if(array_tickets.size()>0){
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_tickets = new AdapterAllListCustomeActivities(activity, array_tickets, activity, fragmentManager1);
                rv_ticket_list.setLayoutManager(manager1);
                rv_ticket_list.setAdapter(adapter_tickets);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab4_tickets.setText(array_tickets.get(0).getCategory()+"("+array_tickets.size()+")");
            }else{
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_tickets = new AdapterAllListCustomeActivities(activity, array_transfer, activity, fragmentManager1);
                rv_ticket_list.setLayoutManager(manager1);
                rv_ticket_list.setAdapter(adapter_tickets);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab4_tickets.setText("Tickets(0)");
            }

            if(array_dining.size()>0){
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_dining = new AdapterAllListCustomeActivities(activity, array_dining, activity, fragmentManager1);
                rv_dining_list.setLayoutManager(manager1);
                rv_dining_list.setAdapter(adapter_dining);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab5_dining.setText(array_dining.get(0).getCategory()+"("+array_dining.size()+")");
            }else{
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_dining = new AdapterAllListCustomeActivities(activity, array_dining, activity, fragmentManager1);
                rv_dining_list.setLayoutManager(manager1);
                rv_dining_list.setAdapter(adapter_dining);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab5_dining.setText("Dining(0)");
            }

            if(array_vouchers.size()>0){
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_vouchers = new AdapterAllListCustomeActivities(activity, array_vouchers, activity, fragmentManager1);
                rv_vouchers_list.setLayoutManager(manager1);
                rv_vouchers_list.setAdapter(adapter_vouchers);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab6_vouchers.setText(array_vouchers.get(0).getCategory()+"("+array_vouchers.size()+")");
            }else{
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_vouchers = new AdapterAllListCustomeActivities(activity, array_vouchers, activity, fragmentManager1);
                rv_vouchers_list.setLayoutManager(manager1);
                rv_vouchers_list.setAdapter(adapter_vouchers);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab6_vouchers.setText("Vouchers(0)");
            }

            if(array_private.size()>0){
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_private = new AdapterAllListCustomeActivities(activity, array_private, activity, fragmentManager1);
                rv_private_list.setLayoutManager(manager1);
                rv_private_list.setAdapter(adapter_private);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab7_private.setText(array_private.get(0).getCategory()+"("+array_private.size()+")");
            }else{
                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                adapter_private = new AdapterAllListCustomeActivities(activity, array_private, activity, fragmentManager1);
                rv_private_list.setLayoutManager(manager1);
                rv_private_list.setAdapter(adapter_private);
                iv_filter.setVisibility(View.VISIBLE);
                tv_tab7_private.setText("Private Tours(0)");
            }
            CheckRecord();

        }catch (Exception e){
            arrayListActivities = new ArrayList<>();
            Toast.makeText(mContext, "record not found", Toast.LENGTH_LONG).show();
            GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
            FragmentManager fragmentManager1 = getSupportFragmentManager();
            adapterAllListCustomeActivities = new AdapterAllListCustomeActivities(activity, arrayListActivities, activity, fragmentManager1);
            rv_allList.setLayoutManager(manager1);
            rv_allList.setAdapter(adapterAllListCustomeActivities);
            iv_filter.setVisibility(View.VISIBLE);
            tv_title.setText(getText(R.string.all_events));
        }


    }

    public void CheckRecord(){
        if(FlagTopSelling){
            if(array_top_selling.size()>0){
                rv_top_selling_list.setVisibility(View.VISIBLE);
                ll_record_not_found.setVisibility(View.GONE);
            }else{
                rv_top_selling_list.setVisibility(View.GONE);
                ll_record_not_found.setVisibility(View.VISIBLE);
            }
        }else if(FlagAllActivities){
            if(arrayListActivities.size()>0){
                rv_allList.setVisibility(View.VISIBLE);
                ll_record_not_found.setVisibility(View.GONE);
            }else{
                rv_allList.setVisibility(View.GONE);
                ll_record_not_found.setVisibility(View.VISIBLE);
            }
        }else if(FlagTransfer){
            if(array_transfer.size()>0){
                rv_transfer_list.setVisibility(View.VISIBLE);
                ll_record_not_found.setVisibility(View.GONE);
            }else{
                rv_transfer_list.setVisibility(View.GONE);
                ll_record_not_found.setVisibility(View.VISIBLE);
            }
        }else if(FlagTickets){
            if(array_tickets.size()>0){
                rv_ticket_list.setVisibility(View.VISIBLE);
                ll_record_not_found.setVisibility(View.GONE);
            }else{
                rv_ticket_list.setVisibility(View.GONE);
                ll_record_not_found.setVisibility(View.VISIBLE);
            }
        }else if(FlagDining){
            if(array_dining.size()>0){
                rv_dining_list.setVisibility(View.VISIBLE);
                ll_record_not_found.setVisibility(View.GONE);
            }else{
                rv_dining_list.setVisibility(View.GONE);
                ll_record_not_found.setVisibility(View.VISIBLE);
            }
        }else if(FlagVouchers){
            if(array_vouchers.size()>0){
                rv_vouchers_list.setVisibility(View.VISIBLE);
                ll_record_not_found.setVisibility(View.GONE);
            }else{
                rv_vouchers_list.setVisibility(View.GONE);
                ll_record_not_found.setVisibility(View.VISIBLE);
            }
        }else if(FlagPrivate){
            if(array_private.size()>0){
                rv_private_list.setVisibility(View.VISIBLE);
                ll_record_not_found.setVisibility(View.GONE);
            }else{
                rv_private_list.setVisibility(View.GONE);
                ll_record_not_found.setVisibility(View.VISIBLE);
            }
        }
    }

    public void ActivitiesHotelRemove(String activity_id) {
        db.updateOneColumn(activity_id,sessionManager.getCountryCode(),"False");
        db.DeleteHotelItineryActivity(activity_id,sessionManager.getCountryCode());
        Toast.makeText(mContext, "Activity Removed successfully", Toast.LENGTH_LONG).show();
        finish();
        overridePendingTransition( 0, 0);
        startActivity(getIntent());
        overridePendingTransition( 0, 0);
    }

    public void ActivitiesDialog(int pos,String id,double price,String Name,ArrayList<ModelActivitiesList> arrayListDialog) {
        arrayListDialogActivities = arrayListDialog;
        dialog_pos = pos;
        Event_ID=id;
        activity_Amt =price;
        activityName = Name;
        getDiscount();
        getPriceDetails();
    }

    private void ShowAllCommenDialog(){
        final Dialog dialogMsg = new Dialog(mContext);
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.dialog_activities);
        dialogMsg.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMsg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMsg.getWindow().setAttributes(lp);
        dialogMsg.show();

        TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
        TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_done);
        TextView tv_hotel_Name = (TextView) dialogMsg.findViewById(R.id.tv_hotel_Name);
        sp_time = (Spinner) dialogMsg.findViewById(R.id.sp_time);
        final EditText et_booking_Check_In = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In);

        final LinearLayout ll_adult = (LinearLayout) dialogMsg.findViewById(R.id.ll_adult);
        TextView tv_max_Adult= (TextView) dialogMsg.findViewById(R.id.tv_max_Adult);
        TextView tv_min_Adult= (TextView) dialogMsg.findViewById(R.id.tv_min_Adult);
        final TextView tv_value_Adult= (TextView) dialogMsg.findViewById(R.id.tv_value_Adult);
        final TextView tv_adult_price= (TextView) dialogMsg.findViewById(R.id.tv_adult_price);

        final LinearLayout ll_child= (LinearLayout) dialogMsg.findViewById(R.id.ll_child);
        TextView tv_max_Child= (TextView) dialogMsg.findViewById(R.id.tv_max_Child);
        TextView tv_min_Child= (TextView) dialogMsg.findViewById(R.id.tv_min_Child);

        final TextView tv_value_Child= (TextView) dialogMsg.findViewById(R.id.tv_value_Child);
        final TextView tv_child_price= (TextView) dialogMsg.findViewById(R.id.tv_child_price);
        final TextView tv_total_cost= (TextView) dialogMsg.findViewById(R.id.tv_total_cost);

        isBookingDate = false; FlagDate = false; FlagTime = false;FlagDateTimeCheck = true; AvailableDayFlag=false;
        check_in_Calendar = Calendar.getInstance();
        tv_hotel_Name.setText(activityName);
        booking_Days="";
        number_of_Days=0;
        FinalCount = 0;
        child_no_Count =child_no;

        //et_booking_Check_In.setText(sessionManager.getCheckInDate());

        //ShowBookingDate();

        sp_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                booking_Time = noRooms.get(position).toString();
                Log.e("Select Booking Time-->",booking_Time);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        child_no_Count =child_no;
        if(child_no>0){
            ll_child.setVisibility(View.VISIBLE);
            tv_value_Child.setText(String.valueOf(child_no));
        }else{
            ll_child.setVisibility(View.GONE);
            tv_value_Child.setText(String.valueOf(child_no));
        }

        tv_value_Adult.setText(sessionManager.getAdultMainCount());
        //tv_value_Child.setText(sessionManager.getChildMainCount());



        adult_no = Integer.parseInt(tv_value_Adult.getText().toString());
        //child_no = Integer.parseInt(tv_value_Child.getText().toString());

        sessionManager.setAdultCount(String.valueOf(adult_no));
        sessionManager.setChildCount(String.valueOf(child_no));

        tv_value_Adult.setText(String.valueOf(adult_no));

        pricingArrayLists =pricingDetailsModels.get(0).getPricesArrayList();
        pricesDetailsArrayList = pricingArrayLists.get(0).getPricingDetails();

        if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
            infant_no = 0;
            if (pricesDetailsArrayList.get(0).getAdult_price().equalsIgnoreCase("0")) {
                ll_adult.setVisibility(View.GONE);
            } else {
                tv_adult_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price() + " per head");
            }

            if (pricesDetailsArrayList.get(0).getChild_price().equalsIgnoreCase("0")) {
                ll_child.setVisibility(View.GONE);
            } else {
                tv_child_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getChild_price() + " per head");
            }

        } else {
            unit_count = Integer.parseInt(pricesDetailsArrayList.get(0).getMaximum_pax());

            if (pricesDetailsArrayList.get(0).getAdult_price().equalsIgnoreCase("0")) {
                ll_adult.setVisibility(View.GONE);
            } else {
                tv_adult_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price() + " maximum " + unit_count + " person");
            }

            if (pricesDetailsArrayList.get(0).getChild_price().equalsIgnoreCase("0")) {
                ll_child.setVisibility(View.GONE);
            } else {
                tv_child_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getChild_price() + " maximum " + unit_count + " person");
            }

            /*if (pricingDetailsModels.get(0).getInfant_price().equalsIgnoreCase("0")) {
                ll_infant.setVisibility(View.GONE);
            } else {
                tv_infant_price.setText(pricingDetailsModels.get(0).getCurrency() + " " + pricingDetailsModels.get(0).getInfant_price() + " per head");
                ll_infant.setVisibility(View.GONE);

            }*/
        }

        et_booking_Check_In.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate_Check_In, check_in_Calendar
                        .get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH),
                        check_in_Calendar.get(Calendar.DAY_OF_MONTH));

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }


                mDate.show();

            }
        });
        Fromdate_Check_In = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                check_in_Calendar.set(Calendar.YEAR, year);
                check_in_Calendar.set(Calendar.MONTH, monthOfYear);
                check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy";
                final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                try {

                    int dayOfWeek = check_in_Calendar.get(Calendar.DAY_OF_WEEK);
                    String day = new DateFormatSymbols().getShortWeekdays()[dayOfWeek];

                    if (!activitiesOperationTiming.get(0).getDay().equalsIgnoreCase("daily")) {
                        if (!db.getCalendarValue(day.toLowerCase()).equalsIgnoreCase("")) {
                            Log.e("Selected Date", day.toLowerCase() + " --->" + db.getCalendarValue(day.toLowerCase()));
                            JSONArray bookingtime = null;
                            modelTimings = new ArrayList<>();
                            modelTimings.clear();
                            try {
                                bookingtime = new JSONArray(db.getCalendarValue(day.toLowerCase()));
                                for (int j = 0; j < bookingtime.length(); j++) {
                                    ModelTiming modelTiming = new ModelTiming();
                                    try {
                                        modelTiming.setTiming(bookingtime.get(j).toString());
                                        Log.e("Booking Time", bookingtime.get(j).toString());

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    modelTimings.add(modelTiming);
                                }
                               // tv_selected_date.setText(day + ", " + sdf.format(myCalendar.getTime()));
                                //Selected_Date = tv_selected_date.getText().toString();
                                //tv_selected_date.setTextColor(Color.parseColor("#347038"));
                                FlagDate = true;
                                FlagTime = false;
                               // tv_selected_time.setText("Available time slot");
                               // tv_selected_time.setTextColor(Color.parseColor("#000000"));
                               // rv_ticket_list.setVisibility(View.VISIBLE);
                                ShowBookingDate();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            modelTimings = new ArrayList<>();
                            modelTimings.clear();
                           //ShowBookingDate();
                            showSnackBar(ActivityAllListCustomeActivities.this,"Activity is not available on this date.Please select a different date.");
                          //  tv_selected_date.setText("Check Operational Day");
                           // tv_selected_date.setTextColor(Color.parseColor("#DB6556"));
                            FlagDate = false;
                           // tv_selected_time.setText("Activity is not available on this date.");
                           // tv_selected_time.setTextColor(Color.parseColor("#DB6556"));
                            FlagTime = false;
                           // rv_ticket_list.setVisibility(View.GONE);
                        }
                    }else if(activitiesOperationTiming.get(0).getDay().equalsIgnoreCase("daily")){
                        //tv_selected_date.setText(day + ", " + sdf.format(myCalendar.getTime()) /*+" "+activitiesOperationTiming.get(0).getBooking_timings()*/);
                       // Selected_Date = tv_selected_date.getText().toString();
                       // tv_selected_date.setTextColor(Color.parseColor("#347038"));
                        FlagDate = true;
                        FlagTime = false;
                       // tv_selected_time.setText("Available time slot");
                       // tv_selected_time.setTextColor(Color.parseColor("#000000"));
                       // rv_ticket_list.setVisibility(View.VISIBLE);
                        ShowBookingDate();
                    }


                } catch (Exception e) {
                    showSnackBar(ActivityAllListCustomeActivities.this,"Something went wrong. Please try again.");
                    e.printStackTrace();
                }

                et_booking_Check_In.setText(sdf.format(check_in_Calendar.getTime()));


            }

        };


        try {

            adult_price = Double.parseDouble(pricesDetailsArrayList.get(0).getAdult_price());
            child_price = Double.parseDouble(pricesDetailsArrayList.get(0).getChild_price());

            //tv_adult_amt.setText(pricingDetailsModels.get(0).getCurrency()+" "+String.valueOf(adult_price));
            //tv_child_amt.setText(pricingDetailsModels.get(0).getCurrency()+" "+String.valueOf(child_price));

            total_adult_price = adult_price * adult_no;


            Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
            Adult_Total_Val = per_Adult_Price * adult_no;

            Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
            Child_Total_Val = per_Child_Price * child_no;




            if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                String final_result = String.format("%.2f", Final_Cost);
                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
            }else{
                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price());
                Final_Cost = adult_price;
            }

            tv_min_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                    Log.e("FinalCount--->", String.valueOf(FinalCount));
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        if (adult_no > 1/*Integer.valueOf(pricingDetailsModels.get(0).getFrom_pax_count())*/) {
                            adult_no--;
                            tv_value_Adult.setText(String.valueOf(adult_no));
                            Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                            Adult_Total_Val = per_Adult_Price * adult_no;
                            Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                            String final_result = String.format("%.2f", Final_Cost);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setAdultCount(String.valueOf(adult_no));
                        } else {
                            ll_adult.startAnimation(shakeError());
                        }
                    } else {
                        if (adult_no > 1) {
                            adult_no--;
                            tv_value_Adult.setText(String.valueOf(adult_no));
                            float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                            Adult_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getAdult_price()));
                            String final_result = String.format("%.2f", Adult_Total_Val);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setAdultCount(String.valueOf(adult_no));
                            Final_Cost = Double.parseDouble(final_result);
                        } else {
                            ll_adult.startAnimation(shakeError());
                        }
                    }

                }
            });


            tv_max_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                    Log.e("FinalCount--->", String.valueOf(FinalCount));
                    if(FinalCount<=13){
                        if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                            if (adult_no >= 1/*Integer.valueOf(pricingDetailsModels.get(0).getFrom_pax_count())*/ /*&& adult_no < Integer.valueOf(pricingDetailsModels.get(0).getTo_pax_count())*/) {
                                adult_no++;
                                tv_value_Adult.setText(String.valueOf(adult_no));
                                Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                                Adult_Total_Val = per_Adult_Price * adult_no;
                                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                String final_result = String.format("%.2f", Final_Cost);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setAdultCount(String.valueOf(adult_no));
                            } else {
                                ll_adult.startAnimation(shakeError());
                            }
                        } else {
                            if (adult_no >= 1/*Integer.valueOf(pricingDetailsModels.get(0).getFrom_pax_count()) && adult_no < Integer.valueOf(pricingDetailsModels.get(0).getTo_pax_count())*/) {
                                adult_no++;
                                tv_value_Adult.setText(String.valueOf(adult_no));
                                float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                                Adult_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getAdult_price()));
                                String final_result = String.format("%.2f", Adult_Total_Val);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setAdultCount(String.valueOf(adult_no));
                                Final_Cost = Double.parseDouble(final_result);
                            } else {
                                ll_adult.startAnimation(shakeError());
                            }
                        }
                    }else {
                        ll_adult.startAnimation(shakeError());
                    }


                }
            });

            tv_min_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                    Log.e("FinalCount--->", String.valueOf(FinalCount));
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        if (child_no > 0) {
                            child_no--;
                            tv_value_Child.setText(String.valueOf(child_no));
                            Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
                            Child_Total_Val = per_Child_Price * child_no;
                            Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                            String final_result = String.format("%.2f", Final_Cost);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setChildCount(String.valueOf(child_no));
                        } else {
                            ll_child.startAnimation(shakeError());
                        }
                    } else {
                        if (child_no > 0) {
                            child_no--;
                            tv_value_Child.setText(String.valueOf(child_no));
                            float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                            Child_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getChild_price()));
                            String final_result = String.format("%.2f", Child_Total_Val);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setChildCount(String.valueOf(child_no));
                            Final_Cost = Double.parseDouble(final_result);
                        } else {
                            ll_child.startAnimation(shakeError());
                        }

                    }



                }
            });

            tv_max_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                    Log.e("FinalCount--->", String.valueOf(FinalCount));
                    if(FinalCount<=13){
                        if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                            if (child_no_Count > child_no /*&& child_no < Integer.valueOf(pricingDetailsModels.get(0).getTo_pax_count())*/) {
                                child_no += 1;
                                tv_value_Child.setText(String.valueOf(child_no));
                                Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
                                Child_Total_Val = per_Child_Price * child_no;
                                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                String final_result = String.format("%.2f", Final_Cost);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setChildCount(String.valueOf(child_no));
                            } else {
                                ll_child.startAnimation(shakeError());
                            }
                        } else {
                            if (child_no_Count > child_no /*&& child_no < Integer.valueOf(pricingDetailsModels.get(0).getTo_pax_count())*/) {
                                child_no += 1;
                                tv_value_Child.setText(String.valueOf(child_no));
                                float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                                Child_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getChild_price()));
                                String final_result = String.format("%.2f", Child_Total_Val);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setChildCount(String.valueOf(child_no));
                                Final_Cost = Double.parseDouble(final_result);
                            } else {
                                ll_child.startAnimation(shakeError());
                            }
                        }
                    }else {
                        ll_child.startAnimation(shakeError());
                    }

                }
            });




        } catch (Exception e) {
            e.printStackTrace();
            Log.e("erro", "code=" + e.getMessage());
        }



        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity_Amt >0){

                    String check_in_Date = et_booking_Check_In.getText().toString();
                    if(check_in_Date==null){
                        check_in_Date="";
                    }

                    db.updateOneColumn(arrayListDialogActivities.get(dialog_pos).getTour_id(), sessionManager.getCountryCode(),"True");


                    String isPack="False";
                    db.updateOneColumn(arrayListDialogActivities.get(dialog_pos).getTour_id(), sessionManager.getCountryCode(),"True");
                    Double vouchers_cost=0.0;
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        Float Adult_Count=  Float.valueOf(sessionManager.getAdultCount()) + Float.valueOf(sessionManager.getChildCount());
                        vouchers_cost = Final_Cost * number_of_Days / Adult_Count;
                    } else {

                        FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                        if (FinalCount <= Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())) {
                            vouchers_cost = Final_Cost * number_of_Days ;
                            isPack="1 Units";
                        } else {
                            vouchers_cost = Final_Cost * number_of_Days ;
                            isPack="2 Units";
                        }
                    }

                    try {
                        if(db.CheckIsAvailableActivity(arrayListDialogActivities.get(dialog_pos).getTour_id(),sessionManager.getCountryCode())){
                            //Update
                                db.updateItineraryActivityData(Integer.parseInt(arrayListDialogActivities.get(dialog_pos).getTour_id()),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0", String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost)),sessionManager.getCountryCode(),booking_Days,number_of_Days,"0","0");
                        }else{
                            //Insert
                                db.setItineraryActivityData(arrayListDialogActivities.get(dialog_pos).getTour_id(),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0",String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost)),sessionManager.getCountryCode(),booking_Days,number_of_Days,pricesDetailsArrayList.get(0).getAdult_price(),pricesDetailsArrayList.get(0).getChild_price(),"0","0",isPack);
                        }

                        sessionManager.setCityCode(arrayListDialogActivities.get(dialog_pos).getCity_id());
                        sessionManager.setCityName(arrayListDialogActivities.get(dialog_pos).getCity());
                        dialogMsg.cancel();
                        Toast.makeText(mContext, "Activity added in your itinerary", Toast.LENGTH_LONG).show();
                        finish();
                        overridePendingTransition( 0, 0);
                        startActivity(getIntent());
                        overridePendingTransition( 0, 0);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }
        });

        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
            }
        });

    }

    private void ShowAllPrivateToursDialog(){
        final Dialog dialogMsg = new Dialog(mContext);
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.dialog_activities);
        dialogMsg.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMsg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMsg.getWindow().setAttributes(lp);
        dialogMsg.show();

        TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
        TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_done);
        TextView tv_hotel_Name = (TextView) dialogMsg.findViewById(R.id.tv_hotel_Name);
        sp_time = (Spinner) dialogMsg.findViewById(R.id.sp_time);
        final EditText et_booking_Check_In = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In);

        final LinearLayout ll_adult = (LinearLayout) dialogMsg.findViewById(R.id.ll_adult);
        TextView tv_max_Adult= (TextView) dialogMsg.findViewById(R.id.tv_max_Adult);
        TextView tv_min_Adult= (TextView) dialogMsg.findViewById(R.id.tv_min_Adult);
        final TextView tv_value_Adult= (TextView) dialogMsg.findViewById(R.id.tv_value_Adult);
        final TextView tv_adult_price= (TextView) dialogMsg.findViewById(R.id.tv_adult_price);
        final TextView tv_note0 = (TextView) dialogMsg.findViewById(R.id.tv_note0);
        final TextView tv_note = (TextView) dialogMsg.findViewById(R.id.tv_note);
        final TextView tv_note1 = (TextView) dialogMsg.findViewById(R.id.tv_note1);

        final LinearLayout ll_child= (LinearLayout) dialogMsg.findViewById(R.id.ll_child);
        TextView tv_max_Child= (TextView) dialogMsg.findViewById(R.id.tv_max_Child);
        TextView tv_min_Child= (TextView) dialogMsg.findViewById(R.id.tv_min_Child);
        final TextView tv_value_Child= (TextView) dialogMsg.findViewById(R.id.tv_value_Child);
        final TextView tv_child_price= (TextView) dialogMsg.findViewById(R.id.tv_child_price);
        final TextView tv_total_cost= (TextView) dialogMsg.findViewById(R.id.tv_total_cost);

        isBookingDate = false; FlagDate = false; FlagTime = false;FlagDateTimeCheck = true; AvailableDayFlag=false;
        check_in_Calendar = Calendar.getInstance();
        tv_hotel_Name.setText(activityName);
        booking_Days="";
        number_of_Days=0;
        FinalCount = 0;

        //et_booking_Check_In.setText(sessionManager.getCheckInDate());

        //ShowBookingDate();

        pricingArrayLists =pricingDetailsModels.get(0).getPricesArrayList();
        pricesDetailsArrayList = pricingArrayLists.get(0).getPricingDetails();

        tv_value_Adult.setText(sessionManager.getAdultMainCount());
        //tv_value_Child.setText(sessionManager.getChildMainCount());

        try{
            if(pricesDetailsArrayList.size()>0){
                tv_note0.setText("Transfer charge is will be added in private tours.");
                tv_note.setText("Transfer charges for less than "+pricesDetailsArrayList.get(0).getTo_pax_count()+" person - "+pricingArrayLists.get(0).getCurrency()+ " "+pricesDetailsArrayList.get(0).getInfant_price());
                tv_note1.setText("Transfer charges for more than "+pricesDetailsArrayList.get(1).getFrom_pax_count()+" person - "+pricingArrayLists.get(0).getCurrency()+ " "+pricesDetailsArrayList.get(1).getInfant_price());
                tv_note1.setVisibility(View.VISIBLE);
                tv_note0.setVisibility(View.VISIBLE);
                tv_note.setVisibility(View.VISIBLE);
            }else{
                tv_note0.setVisibility(View.GONE);
                tv_note.setVisibility(View.GONE);
                tv_note1.setVisibility(View.GONE);
            }
        }catch (Exception e){

        }


        sp_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                booking_Time = noRooms.get(position).toString();
                Log.e("Select Booking Time-->",booking_Time);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //adult_no = Integer.parseInt(pricingDetailsModels.get(0).getFrom_pax_count());

        child_no_Count =child_no;
        if(child_no>0){
            ll_child.setVisibility(View.VISIBLE);
            tv_value_Child.setText(String.valueOf(child_no));
        }else{
            ll_child.setVisibility(View.GONE);
            tv_value_Child.setText(String.valueOf(child_no));
        }

        adult_no = Integer.parseInt(tv_value_Adult.getText().toString());
       // child_no = Integer.parseInt(tv_value_Child.getText().toString());
        sessionManager.setAdultCount(String.valueOf(adult_no));
        sessionManager.setChildCount(String.valueOf(child_no));

        tv_value_Adult.setText(String.valueOf(adult_no));
        if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
            //child_no = 0;
            infant_no = 0;
            if (pricesDetailsArrayList.get(0).getAdult_price().equalsIgnoreCase("0")) {
                ll_adult.setVisibility(View.GONE);
            } else {
                tv_adult_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price() + " per head");
            }

            if (pricesDetailsArrayList.get(0).getChild_price().equalsIgnoreCase("0")) {
                ll_child.setVisibility(View.GONE);
            } else {
                tv_child_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getChild_price() + " per head");
            }

           /* if (pricingDetailsModels.get(0).getInfant_price().equalsIgnoreCase("0")) {
                ll_infant.setVisibility(View.GONE);
            } else {
                tv_infant_price.setText(pricingDetailsModels.get(0).getCurrency() + " " + pricingDetailsModels.get(0).getInfant_price() + " per head");
                ll_infant.setVisibility(View.GONE);

            }*/
        } else {
            unit_count = Integer.parseInt(pricesDetailsArrayList.get(0).getMaximum_pax());


            if (pricesDetailsArrayList.get(0).getAdult_price().equalsIgnoreCase("0")) {
                ll_adult.setVisibility(View.GONE);
            } else {
                tv_adult_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price() + " maximum " + unit_count + " person");
            }

            if (pricesDetailsArrayList.get(0).getChild_price().equalsIgnoreCase("0")) {
                ll_child.setVisibility(View.GONE);
            } else {
                tv_child_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getChild_price() + " maximum " + unit_count + " person");
            }

            /*if (pricingDetailsModels.get(0).getInfant_price().equalsIgnoreCase("0")) {
                ll_infant.setVisibility(View.GONE);
            } else {
                tv_infant_price.setText(pricingDetailsModels.get(0).getCurrency() + " " + pricingDetailsModels.get(0).getInfant_price() + " per head");
                ll_infant.setVisibility(View.GONE);

            }*/
        }

        et_booking_Check_In.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate_Check_In, check_in_Calendar
                        .get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH),
                        check_in_Calendar.get(Calendar.DAY_OF_MONTH));

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }


                mDate.show();

            }
        });
        Fromdate_Check_In = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                check_in_Calendar.set(Calendar.YEAR, year);
                check_in_Calendar.set(Calendar.MONTH, monthOfYear);
                check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy";
                final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                try {

                    int dayOfWeek = check_in_Calendar.get(Calendar.DAY_OF_WEEK);
                    String day = new DateFormatSymbols().getShortWeekdays()[dayOfWeek];

                    if (!activitiesOperationTiming.get(0).getDay().equalsIgnoreCase("daily")) {
                        if (!db.getCalendarValue(day.toLowerCase()).equalsIgnoreCase("")) {
                            Log.e("Selected Date", day.toLowerCase() + " --->" + db.getCalendarValue(day.toLowerCase()));
                            JSONArray bookingtime = null;
                            modelTimings = new ArrayList<>();
                            modelTimings.clear();
                            try {
                                bookingtime = new JSONArray(db.getCalendarValue(day.toLowerCase()));
                                for (int j = 0; j < bookingtime.length(); j++) {
                                    ModelTiming modelTiming = new ModelTiming();
                                    try {
                                        modelTiming.setTiming(bookingtime.get(j).toString());
                                        Log.e("Booking Time", bookingtime.get(j).toString());

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    modelTimings.add(modelTiming);
                                }
                                // tv_selected_date.setText(day + ", " + sdf.format(myCalendar.getTime()));
                                //Selected_Date = tv_selected_date.getText().toString();
                                //tv_selected_date.setTextColor(Color.parseColor("#347038"));
                                FlagDate = true;
                                FlagTime = false;
                                // tv_selected_time.setText("Available time slot");
                                // tv_selected_time.setTextColor(Color.parseColor("#000000"));
                                // rv_ticket_list.setVisibility(View.VISIBLE);
                                ShowBookingDate();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            modelTimings = new ArrayList<>();
                            modelTimings.clear();
                            //ShowBookingDate();
                            showSnackBar(ActivityAllListCustomeActivities.this,"Activity is not available on this date.Please select a different date.");
                            //  tv_selected_date.setText("Check Operational Day");
                            // tv_selected_date.setTextColor(Color.parseColor("#DB6556"));
                            FlagDate = false;
                            // tv_selected_time.setText("Activity is not available on this date.");
                            // tv_selected_time.setTextColor(Color.parseColor("#DB6556"));
                            FlagTime = false;
                            // rv_ticket_list.setVisibility(View.GONE);
                        }
                    }else if(activitiesOperationTiming.get(0).getDay().equalsIgnoreCase("daily")){
                        //tv_selected_date.setText(day + ", " + sdf.format(myCalendar.getTime()) /*+" "+activitiesOperationTiming.get(0).getBooking_timings()*/);
                        // Selected_Date = tv_selected_date.getText().toString();
                        // tv_selected_date.setTextColor(Color.parseColor("#347038"));
                        FlagDate = true;
                        FlagTime = false;
                        // tv_selected_time.setText("Available time slot");
                        // tv_selected_time.setTextColor(Color.parseColor("#000000"));
                        // rv_ticket_list.setVisibility(View.VISIBLE);
                        ShowBookingDate();
                    }


                } catch (Exception e) {
                    showSnackBar(ActivityAllListCustomeActivities.this,"Something went wrong. Please try again.");
                    e.printStackTrace();
                }

                et_booking_Check_In.setText(sdf.format(check_in_Calendar.getTime()));


            }

        };


        try {

            adult_price = Double.parseDouble(pricesDetailsArrayList.get(0).getAdult_price());
            child_price = Double.parseDouble(pricesDetailsArrayList.get(0).getChild_price());

            Log.e(TAG, "----> adult ="+adult_price+ " Child ="+child_price);

            //tv_adult_amt.setText(pricingDetailsModels.get(0).getCurrency()+" "+String.valueOf(adult_price));
            //tv_child_amt.setText(pricingDetailsModels.get(0).getCurrency()+" "+String.valueOf(child_price));

            total_adult_price = adult_price * adult_no;

            sessionManager.setAdultCount(String.valueOf(adult_no));

            Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
            Adult_Total_Val = per_Adult_Price * adult_no;

            Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
            Child_Total_Val = per_Child_Price * child_no;

            if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                String final_result = String.format("%.2f", Final_Cost);
                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
            }else{
                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price());
                Final_Cost = adult_price;
            }

            tv_min_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                    Log.e("FinalCount--->", String.valueOf(FinalCount));
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        if (adult_no > 1/*Integer.valueOf(pricingDetailsModels.get(0).getFrom_pax_count())*/) {
                            adult_no--;
                            tv_value_Adult.setText(String.valueOf(adult_no));
                            Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                            Adult_Total_Val = per_Adult_Price * adult_no;
                            Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                            String final_result = String.format("%.2f", Final_Cost);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setAdultCount(String.valueOf(adult_no));
                        } else {
                            ll_adult.startAnimation(shakeError());
                        }
                    } else {
                        if (adult_no > 1) {
                            adult_no--;
                            tv_value_Adult.setText(String.valueOf(adult_no));
                            float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                            Adult_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getAdult_price()));
                            String final_result = String.format("%.2f", Adult_Total_Val);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setAdultCount(String.valueOf(adult_no));
                            Final_Cost = Double.parseDouble(final_result);
                        } else {
                            ll_adult.startAnimation(shakeError());
                        }
                    }

                }
            });


            tv_max_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                    Log.e("FinalCount--->", String.valueOf(FinalCount));
                    if(FinalCount<=13){
                        if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                            if (adult_no >= 1/*Integer.valueOf(pricingDetailsModels.get(0).getFrom_pax_count())*/ /*&& adult_no < Integer.valueOf(pricingDetailsModels.get(0).getTo_pax_count())*/) {
                                adult_no++;
                                tv_value_Adult.setText(String.valueOf(adult_no));
                                Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                                Adult_Total_Val = per_Adult_Price * adult_no;
                                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                String final_result = String.format("%.2f", Final_Cost);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setAdultCount(String.valueOf(adult_no));
                            } else {
                                ll_adult.startAnimation(shakeError());
                            }
                        } else {
                            if (adult_no >= 1/*Integer.valueOf(pricingDetailsModels.get(0).getFrom_pax_count()) && adult_no < Integer.valueOf(pricingDetailsModels.get(0).getTo_pax_count())*/) {
                                adult_no++;
                                tv_value_Adult.setText(String.valueOf(adult_no));
                                float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                                Adult_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getAdult_price()));
                                String final_result = String.format("%.2f", Adult_Total_Val);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setAdultCount(String.valueOf(adult_no));
                                Final_Cost = Double.parseDouble(final_result);
                            } else {
                                ll_adult.startAnimation(shakeError());
                            }
                        }
                    }else {
                        ll_adult.startAnimation(shakeError());
                    }


                }
            });

            tv_min_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                    Log.e("FinalCount--->", String.valueOf(FinalCount));
                        if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                            if (child_no > 0) {
                                child_no--;
                                tv_value_Child.setText(String.valueOf(child_no));
                                Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
                                Child_Total_Val = per_Child_Price * child_no;
                                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                String final_result = String.format("%.2f", Final_Cost);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setChildCount(String.valueOf(child_no));
                            } else {
                                ll_child.startAnimation(shakeError());
                            }
                        } else {
                            if (child_no > 0) {
                                child_no--;
                                tv_value_Child.setText(String.valueOf(child_no));
                                float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                                Child_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getChild_price()));
                                String final_result = String.format("%.2f", Child_Total_Val);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setChildCount(String.valueOf(child_no));
                                Final_Cost = Double.parseDouble(final_result);
                            } else {
                                ll_child.startAnimation(shakeError());
                            }

                        }



                }
            });

            tv_max_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                    Log.e("FinalCount--->", String.valueOf(FinalCount));
                    if(FinalCount<=13){
                        if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                            if (child_no_Count > child_no /*&& child_no < Integer.valueOf(pricingDetailsModels.get(0).getTo_pax_count())*/) {
                                child_no++;
                                tv_value_Child.setText(String.valueOf(child_no));
                                Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
                                Child_Total_Val = per_Child_Price * child_no;
                                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                String final_result = String.format("%.2f", Final_Cost);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setChildCount(String.valueOf(child_no));
                            } else {
                                ll_child.startAnimation(shakeError());
                            }
                        } else {
                            if (child_no_Count > child_no /*&& child_no < Integer.valueOf(pricingDetailsModels.get(0).getTo_pax_count())*/) {
                                child_no += 1;
                                tv_value_Child.setText(String.valueOf(child_no));
                                float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                                Child_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getChild_price()));
                                String final_result = String.format("%.2f", Child_Total_Val);
                                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                                sessionManager.setChildCount(String.valueOf(child_no));
                                Final_Cost = Double.parseDouble(final_result);
                            } else {
                                ll_child.startAnimation(shakeError());
                            }

                        }
                    }else {
                        ll_child.startAnimation(shakeError());
                    }


                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("erro", "code=" + e.getMessage());
        }



        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity_Amt >0){

                    String check_in_Date = et_booking_Check_In.getText().toString();
                    if(check_in_Date==null){
                        check_in_Date="";
                    }

                    Final_Cost=0;

                    db.updateOneColumn(arrayListDialogActivities.get(dialog_pos).getTour_id(), sessionManager.getCountryCode(),"True");

                    try {
                        FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                        Log.e("Main Final Count--->", String.valueOf(FinalCount));
                        if(db.CheckIsAvailableActivity(arrayListDialogActivities.get(dialog_pos).getTour_id(),sessionManager.getCountryCode())){
                            //Update
                            if(FinalCount<=Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())){
                                /*Double temp = Double.valueOf(pricesDetailsArrayList.get(0).getInfant_price()) / Double.valueOf(sessionManager.getAdultCount());
                                Final_Cost = temp +  Float.valueOf(pricesDetailsArrayList.get(0).getAdult_price());*/
                                Double temp = Float.valueOf(pricesDetailsArrayList.get(0).getAdult_price())* Double.valueOf(sessionManager.getAdultCount());
                                Double tempC = Float.valueOf(pricesDetailsArrayList.get(0).getChild_price())* Double.valueOf(sessionManager.getChildCount());
                                Final_Cost = temp + tempC +  Double.valueOf(pricesDetailsArrayList.get(0).getInfant_price());
                                db.updateItineraryActivityData(Integer.parseInt(arrayListDialogActivities.get(dialog_pos).getTour_id()),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0", String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost/*Double.valueOf(pricingDetailsModels.get(0).getPricingDetails().get(0).getInfant_price())*/)),sessionManager.getCountryCode(),booking_Days,number_of_Days,pricesDetailsArrayList.get(0).getInfant_price(),"0");
                            }else{
                                try{
                                    //Double temp = Double.valueOf(pricesDetailsArrayList.get(1).getInfant_price()) / Double.valueOf(sessionManager.getAdultCount());
                                    //Final_Cost = temp +  Float.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                                    Double temp = Float.valueOf(pricesDetailsArrayList.get(0).getAdult_price())* Double.valueOf(sessionManager.getAdultCount());
                                    Double tempC = Float.valueOf(pricesDetailsArrayList.get(0).getChild_price())* Double.valueOf(sessionManager.getChildCount());
                                    Final_Cost = temp + tempC + Double.valueOf(pricesDetailsArrayList.get(1).getInfant_price());
                                    db.updateItineraryActivityData(Integer.parseInt(arrayListDialogActivities.get(dialog_pos).getTour_id()),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0", String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost/*+ Double.valueOf(pricingDetailsModels.get(0).getPricingDetails().get(1).getInfant_price())*/)),sessionManager.getCountryCode(),booking_Days,number_of_Days,pricesDetailsArrayList.get(1).getInfant_price(),"0");
                                }catch (Exception e){
                                    db.updateItineraryActivityData(Integer.parseInt(arrayListDialogActivities.get(dialog_pos).getTour_id()),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0", String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost/*+ Double.valueOf(pricingDetailsModels.get(0).getPricingDetails().get(0).getInfant_price())*/)),sessionManager.getCountryCode(),booking_Days,number_of_Days,pricesDetailsArrayList.get(0).getInfant_price(),"0");
                                }
                            }
                        }else{
                            //Insert
                            if(FinalCount<=Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())){
                                //Double temp = Double.valueOf(pricesDetailsArrayList.get(0).getInfant_price()) / Double.valueOf(sessionManager.getAdultCount());
                                Double temp = Float.valueOf(pricesDetailsArrayList.get(0).getAdult_price())* Double.valueOf(sessionManager.getAdultCount());
                                Double tempC = Float.valueOf(pricesDetailsArrayList.get(0).getChild_price())* Double.valueOf(sessionManager.getChildCount());
                                Final_Cost = temp + tempC + Double.valueOf(pricesDetailsArrayList.get(0).getInfant_price());
                                db.setItineraryActivityData(arrayListDialogActivities.get(dialog_pos).getTour_id(),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0",String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost/*+ Double.valueOf(pricingDetailsModels.get(0).getPricingDetails().get(0).getInfant_price())*/)),sessionManager.getCountryCode(),booking_Days,number_of_Days,pricesDetailsArrayList.get(0).getAdult_price(),pricesDetailsArrayList.get(0).getChild_price(),pricesDetailsArrayList.get(0).getInfant_price(),"0","false");
                            }else{
                                try {
                                    //Double temp = Double.valueOf(pricesDetailsArrayList.get(1).getInfant_price()) / Double.valueOf(sessionManager.getAdultCount());
                                    //Final_Cost = temp +  Float.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                                    Double temp = Float.valueOf(pricesDetailsArrayList.get(0).getAdult_price())* Double.valueOf(sessionManager.getAdultCount());
                                    Double tempC = Float.valueOf(pricesDetailsArrayList.get(0).getChild_price())* Double.valueOf(sessionManager.getChildCount());
                                    Final_Cost = temp + tempC + Double.valueOf(pricesDetailsArrayList.get(1).getInfant_price());
                                    db.setItineraryActivityData(arrayListDialogActivities.get(dialog_pos).getTour_id(),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0",String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost/*+ Double.valueOf(pricingDetailsModels.get(0).getPricingDetails().get(1).getInfant_price())*/)),sessionManager.getCountryCode(),booking_Days,number_of_Days,pricesDetailsArrayList.get(0).getAdult_price(),pricesDetailsArrayList.get(0).getChild_price(),pricesDetailsArrayList.get(1).getInfant_price(),"0","false");
                                }catch (Exception e){
                                    db.setItineraryActivityData(arrayListDialogActivities.get(dialog_pos).getTour_id(),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0",String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost/*+ Double.valueOf(pricingDetailsModels.get(0).getPricingDetails().get(0).getInfant_price())*/)),sessionManager.getCountryCode(),booking_Days,number_of_Days,pricesDetailsArrayList.get(0).getAdult_price(),pricesDetailsArrayList.get(0).getChild_price(),pricesDetailsArrayList.get(0).getInfant_price(),"0","false");

                                }
                            }
                        }

                        sessionManager.setCityCode(arrayListDialogActivities.get(dialog_pos).getCity_id());
                        sessionManager.setCityName(arrayListDialogActivities.get(dialog_pos).getCity());
                        dialogMsg.cancel();
                        Toast.makeText(mContext, "Activity added in your itinerary", Toast.LENGTH_LONG).show();
                        finish();
                        overridePendingTransition( 0, 0);
                        startActivity(getIntent());
                        overridePendingTransition( 0, 0);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }
        });

        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
            }
        });

    }

    private void ShowAllTransferAndVounchersDialog(){
        final Dialog dialogMsg = new Dialog(mContext);
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.dialog_transfer_and_vouchers_activities);
        dialogMsg.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMsg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMsg.getWindow().setAttributes(lp);
        dialogMsg.show();

        TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
        TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_done);
        TextView tv_hotel_Name = (TextView) dialogMsg.findViewById(R.id.tv_hotel_Name);
        sp_Date = (Spinner) dialogMsg.findViewById(R.id.sp_Date);

        final LinearLayout ll_booking_Check_In1 = (LinearLayout) dialogMsg.findViewById(R.id.ll_booking_Check_In1);
        final LinearLayout ll_booking_Check_In2 = (LinearLayout) dialogMsg.findViewById(R.id.ll_booking_Check_In2);
        final LinearLayout ll_booking_Check_In3 = (LinearLayout) dialogMsg.findViewById(R.id.ll_booking_Check_In3);
        final LinearLayout ll_booking_Check_In4 = (LinearLayout) dialogMsg.findViewById(R.id.ll_booking_Check_In4);
        final LinearLayout ll_booking_Check_In5 = (LinearLayout) dialogMsg.findViewById(R.id.ll_booking_Check_In5);
        final LinearLayout ll_booking_Check_In6 = (LinearLayout) dialogMsg.findViewById(R.id.ll_booking_Check_In6);
        final LinearLayout ll_booking_Check_In7 = (LinearLayout) dialogMsg.findViewById(R.id.ll_booking_Check_In7);

        final EditText et_booking_Check_In1 = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In1);
        final EditText et_booking_Check_In2 = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In2);
        final EditText et_booking_Check_In3 = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In3);
        final EditText et_booking_Check_In4 = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In4);
        final EditText et_booking_Check_In5 = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In5);
        final EditText et_booking_Check_In6 = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In6);
        final EditText et_booking_Check_In7 = (EditText) dialogMsg.findViewById(R.id.et_booking_Check_In7);

        final LinearLayout ll_adult = (LinearLayout) dialogMsg.findViewById(R.id.ll_adult);
        TextView tv_max_Adult= (TextView) dialogMsg.findViewById(R.id.tv_max_Adult);
        TextView tv_min_Adult= (TextView) dialogMsg.findViewById(R.id.tv_min_Adult);
        final TextView tv_value_Adult= (TextView) dialogMsg.findViewById(R.id.tv_value_Adult);
        final TextView tv_adult_price= (TextView) dialogMsg.findViewById(R.id.tv_adult_price);

        final LinearLayout ll_child= (LinearLayout) dialogMsg.findViewById(R.id.ll_child);
        TextView tv_max_Child= (TextView) dialogMsg.findViewById(R.id.tv_max_Child);
        TextView tv_min_Child= (TextView) dialogMsg.findViewById(R.id.tv_min_Child);
        final TextView tv_value_Child= (TextView) dialogMsg.findViewById(R.id.tv_value_Child);
        final TextView tv_child_price= (TextView) dialogMsg.findViewById(R.id.tv_child_price);
        final TextView tv_total_cost= (TextView) dialogMsg.findViewById(R.id.tv_total_cost);

        isBookingDate = false; FlagDate = false; FlagTime = false;FlagDateTimeCheck = true; AvailableDayFlag=false;
        check_in_Calendar = Calendar.getInstance();
        tv_hotel_Name.setText(activityName);
        number_of_Days=0;
        booking_Days="";

        tv_value_Adult.setText(sessionManager.getAdultMainCount());
        tv_value_Child.setText(sessionManager.getChildMainCount());

        noDays = new ArrayList<>();
        for(int i=1;NoOfRooms>=i;i++){
            noDays.add(i+" Day(s)");
        }

        pricingArrayLists =pricingDetailsModels.get(0).getPricesArrayList();
        pricesDetailsArrayList = pricingArrayLists.get(0).getPricingDetails();


        AdapterSpinerList adapterstate = new AdapterSpinerList(activity, R.layout.spiner_commen_list, R.id.spiner_list, noDays);
        sp_Date.setAdapter(adapterstate);

        sp_Date.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                booking_Days = noDays.get(position).toString();
                int pos=0;
                pos= position;
                pos=pos+1;
                number_of_Days = pos;
                Log.e("Select Booking Days-->",booking_Days+"  "+String.valueOf(pos));
                 if(number_of_Days==2){
                     ll_booking_Check_In2.setVisibility(View.VISIBLE);
                     ll_booking_Check_In3.setVisibility(View.GONE);
                     ll_booking_Check_In4.setVisibility(View.GONE);
                     ll_booking_Check_In5.setVisibility(View.GONE);
                     ll_booking_Check_In6.setVisibility(View.GONE);
                     ll_booking_Check_In7.setVisibility(View.GONE);
                }else if(number_of_Days==3){
                     ll_booking_Check_In2.setVisibility(View.VISIBLE);
                     ll_booking_Check_In3.setVisibility(View.VISIBLE);
                     ll_booking_Check_In4.setVisibility(View.GONE);
                     ll_booking_Check_In5.setVisibility(View.GONE);
                     ll_booking_Check_In6.setVisibility(View.GONE);
                     ll_booking_Check_In7.setVisibility(View.GONE);
                }else if(number_of_Days==4){
                     ll_booking_Check_In2.setVisibility(View.VISIBLE);
                     ll_booking_Check_In3.setVisibility(View.VISIBLE);
                     ll_booking_Check_In4.setVisibility(View.VISIBLE);
                     ll_booking_Check_In5.setVisibility(View.GONE);
                     ll_booking_Check_In6.setVisibility(View.GONE);
                     ll_booking_Check_In7.setVisibility(View.GONE);
                }else if(number_of_Days==5){
                     ll_booking_Check_In2.setVisibility(View.VISIBLE);
                     ll_booking_Check_In3.setVisibility(View.VISIBLE);
                     ll_booking_Check_In4.setVisibility(View.VISIBLE);
                     ll_booking_Check_In5.setVisibility(View.VISIBLE);
                     ll_booking_Check_In6.setVisibility(View.GONE);
                     ll_booking_Check_In7.setVisibility(View.GONE);
                }else if(number_of_Days==6){
                     ll_booking_Check_In2.setVisibility(View.VISIBLE);
                     ll_booking_Check_In3.setVisibility(View.VISIBLE);
                     ll_booking_Check_In4.setVisibility(View.VISIBLE);
                     ll_booking_Check_In5.setVisibility(View.VISIBLE);
                     ll_booking_Check_In6.setVisibility(View.VISIBLE);
                     ll_booking_Check_In7.setVisibility(View.GONE);
                 }else if(number_of_Days>=7){
                     ll_booking_Check_In2.setVisibility(View.VISIBLE);
                     ll_booking_Check_In3.setVisibility(View.VISIBLE);
                     ll_booking_Check_In4.setVisibility(View.VISIBLE);
                     ll_booking_Check_In5.setVisibility(View.VISIBLE);
                     ll_booking_Check_In6.setVisibility(View.VISIBLE);
                     ll_booking_Check_In7.setVisibility(View.VISIBLE);
                 }
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //adult_no = Integer.parseInt(pricingDetailsModels.get(0).getFrom_pax_count());

        child_no_Count =child_no;
        if(child_no>0){
            ll_child.setVisibility(View.VISIBLE);
            tv_value_Child.setText(String.valueOf(child_no));
        }else{
            ll_child.setVisibility(View.GONE);
            tv_value_Child.setText(String.valueOf(child_no));
        }

        adult_no = Integer.parseInt(tv_value_Adult.getText().toString());
        //child_no = Integer.parseInt(tv_value_Child.getText().toString());
        sessionManager.setAdultCount(String.valueOf(adult_no));
        sessionManager.setChildCount(String.valueOf(child_no));

        tv_value_Adult.setText(String.valueOf(adult_no));
        if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
            //child_no = 0;
            infant_no = 0;
            if (pricesDetailsArrayList.get(0).getAdult_price().equalsIgnoreCase("0")) {
                ll_adult.setVisibility(View.GONE);
            } else {
                tv_adult_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price() + " per head");
            }

            if (pricesDetailsArrayList.get(0).getChild_price().equalsIgnoreCase("0")) {
                ll_child.setVisibility(View.GONE);
            } else {
                tv_child_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getChild_price() + " per head");
            }

        } else {
            unit_count = Integer.parseInt(pricesDetailsArrayList.get(0).getMaximum_pax());


            if (pricesDetailsArrayList.get(0).getAdult_price().equalsIgnoreCase("0")) {
                ll_adult.setVisibility(View.GONE);
            } else {
                tv_adult_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price() + " maximum " + unit_count + " person");
            }

            if (pricesDetailsArrayList.get(0).getChild_price().equalsIgnoreCase("0")) {
                ll_child.setVisibility(View.GONE);
            } else {
                tv_child_price.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getChild_price() + " maximum " + unit_count + " person");
            }

        }

        et_booking_Check_In1.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        check_in_Calendar.set(Calendar.YEAR, year);
                        check_in_Calendar.set(Calendar.MONTH, month);
                        check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy";
                        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        et_booking_Check_In1.setText(sdf.format(check_in_Calendar.getTime()));
                    }
                }, check_in_Calendar.get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH), check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());



                } catch (ParseException e) {
                    e.printStackTrace();
                }


                mDate.show();

            }
        });


        et_booking_Check_In2.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        check_in_Calendar.set(Calendar.YEAR, year);
                        check_in_Calendar.set(Calendar.MONTH, month);
                        check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy";
                        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        et_booking_Check_In2.setText(sdf.format(check_in_Calendar.getTime()));
                    }
                }, check_in_Calendar.get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH), check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());



                } catch (ParseException e) {
                    e.printStackTrace();
                }


                mDate.show();

            }
        });

        et_booking_Check_In3.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        check_in_Calendar.set(Calendar.YEAR, year);
                        check_in_Calendar.set(Calendar.MONTH, month);
                        check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy";
                        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        et_booking_Check_In3.setText(sdf.format(check_in_Calendar.getTime()));
                    }
                }, check_in_Calendar.get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH), check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());



                } catch (ParseException e) {
                    e.printStackTrace();
                }


                mDate.show();

            }
        });

        et_booking_Check_In4.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        check_in_Calendar.set(Calendar.YEAR, year);
                        check_in_Calendar.set(Calendar.MONTH, month);
                        check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy";
                        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        et_booking_Check_In4.setText(sdf.format(check_in_Calendar.getTime()));
                    }
                }, check_in_Calendar.get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH), check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());



                } catch (ParseException e) {
                    e.printStackTrace();
                }


                mDate.show();

            }
        });

        et_booking_Check_In5.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        check_in_Calendar.set(Calendar.YEAR, year);
                        check_in_Calendar.set(Calendar.MONTH, month);
                        check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy";
                        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        et_booking_Check_In5.setText(sdf.format(check_in_Calendar.getTime()));
                    }
                }, check_in_Calendar.get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH), check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());



                } catch (ParseException e) {
                    e.printStackTrace();
                }


                mDate.show();

            }
        });

        et_booking_Check_In6.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        check_in_Calendar.set(Calendar.YEAR, year);
                        check_in_Calendar.set(Calendar.MONTH, month);
                        check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy";
                        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        et_booking_Check_In6.setText(sdf.format(check_in_Calendar.getTime()));
                    }
                }, check_in_Calendar.get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH), check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());



                } catch (ParseException e) {
                    e.printStackTrace();
                }


                mDate.show();

            }
        });

        et_booking_Check_In7.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                DatePickerDialog mDate = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        check_in_Calendar.set(Calendar.YEAR, year);
                        check_in_Calendar.set(Calendar.MONTH, month);
                        check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd-MM-yyyy";
                        final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        et_booking_Check_In7.setText(sdf.format(check_in_Calendar.getTime()));
                    }
                }, check_in_Calendar.get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH), check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());



                } catch (ParseException e) {
                    e.printStackTrace();
                }


                mDate.show();

            }
        });




        /*Fromdate_Check_In = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                check_in_Calendar.set(Calendar.YEAR, year);
                check_in_Calendar.set(Calendar.MONTH, monthOfYear);
                check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy";
                final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                et_booking_Check_In1.setText(sdf.format(check_in_Calendar.getTime()));
            }

        };*/


        try {

            adult_price = Double.parseDouble(pricesDetailsArrayList.get(0).getAdult_price());
            child_price = Double.parseDouble(pricesDetailsArrayList.get(0).getChild_price());

            Log.e(TAG, "----> adult ="+adult_price+ " Child ="+child_price);

            //tv_adult_amt.setText(pricingDetailsModels.get(0).getCurrency()+" "+String.valueOf(adult_price));
            //tv_child_amt.setText(pricingDetailsModels.get(0).getCurrency()+" "+String.valueOf(child_price));

            total_adult_price = adult_price * adult_no;

            sessionManager.setAdultCount(String.valueOf(adult_no));

            Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
            Adult_Total_Val = per_Adult_Price * adult_no;

            Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
            Child_Total_Val = per_Child_Price * child_no;

            if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                String final_result = String.format("%.2f", Final_Cost);
                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
            }else{
                tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price());
                Final_Cost = adult_price;
            }


            tv_min_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        if (adult_no > 1/*Integer.valueOf(pricingDetailsModels.get(0).getFrom_pax_count())*/) {
                            adult_no--;
                            tv_value_Adult.setText(String.valueOf(adult_no));
                            Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                            Adult_Total_Val = per_Adult_Price * adult_no;
                            Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                            String final_result = String.format("%.2f", Final_Cost);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setAdultCount(String.valueOf(adult_no));
                        } else {
                            ll_adult.startAnimation(shakeError());
                        }
                    } else {
                        if (adult_no > 1) {
                            adult_no--;
                            tv_value_Adult.setText(String.valueOf(adult_no));
                            float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                            Adult_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getAdult_price()));
                            String final_result = String.format("%.2f", Adult_Total_Val);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setAdultCount(String.valueOf(adult_no));
                            Final_Cost = Double.parseDouble(final_result);
                        } else {
                            ll_adult.startAnimation(shakeError());
                        }
                    }

                }
            });

            tv_max_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        if (adult_no >= 1/*Integer.valueOf(pricingDetailsModels.get(0).getFrom_pax_count())*/ && adult_no < Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())) {
                            adult_no++;
                            tv_value_Adult.setText(String.valueOf(adult_no));
                            Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                            Adult_Total_Val = per_Adult_Price * adult_no;
                            Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                            String final_result = String.format("%.2f", Final_Cost);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setAdultCount(String.valueOf(adult_no));
                        } else {
                            ll_adult.startAnimation(shakeError());
                        }
                    } else {
                        if (adult_no >= 1/*Integer.valueOf(pricingDetailsModels.get(0).getFrom_pax_count())*/ && adult_no < Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())) {
                            adult_no++;
                            tv_value_Adult.setText(String.valueOf(adult_no));
                            float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                            Adult_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getAdult_price()));
                            String final_result = String.format("%.2f", Adult_Total_Val);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setAdultCount(String.valueOf(adult_no));
                            Final_Cost = Double.parseDouble(final_result);
                        } else {
                            ll_adult.startAnimation(shakeError());
                        }
                    }

                }
            });

            tv_min_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        if (child_no > 0) {
                            child_no--;
                            tv_value_Child.setText(String.valueOf(child_no));
                            Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
                            Child_Total_Val = per_Child_Price * child_no;
                            Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                            String final_result = String.format("%.2f", Final_Cost);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setChildCount(String.valueOf(child_no));
                        } else {
                            ll_child.startAnimation(shakeError());
                        }
                    } else {
                        if (child_no > 0) {
                            child_no--;
                            tv_value_Child.setText(String.valueOf(child_no));
                            float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                            Child_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getChild_price()));
                            String final_result = String.format("%.2f", Child_Total_Val);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setChildCount(String.valueOf(child_no));
                            Final_Cost = Double.parseDouble(final_result);
                        } else {
                            ll_child.startAnimation(shakeError());
                        }

                    }

                }
            });

            tv_max_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        if (child_no_Count > child_no/* && child_no < Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())*/) {
                            child_no++;
                            tv_value_Child.setText(String.valueOf(child_no));
                            Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
                            Child_Total_Val = per_Child_Price * child_no;
                            Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                            String final_result = String.format("%.2f", Final_Cost);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setChildCount(String.valueOf(child_no));
                        } else {
                            ll_child.startAnimation(shakeError());
                        }
                    } else {
                        if (child_no_Count > child_no /*&& child_no < Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())*/) {
                            child_no += 1;
                            tv_value_Child.setText(String.valueOf(child_no));
                            float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                            Child_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getChild_price()));
                            String final_result = String.format("%.2f", Child_Total_Val);
                            tv_total_cost.setText(pricingArrayLists.get(0).getCurrency() + " " + final_result);
                            sessionManager.setChildCount(String.valueOf(child_no));
                            Final_Cost = Double.parseDouble(final_result);
                        } else {
                            ll_child.startAnimation(shakeError());
                        }

                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("erro", "code=" + e.getMessage());
        }



        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity_Amt >0){
                    String check_in_Date="";
                    if(number_of_Days==1){
                         check_in_Date = et_booking_Check_In1.getText().toString();
                        if(check_in_Date==null){
                            check_in_Date="";
                        }
                    }else if(number_of_Days==2){
                        check_in_Date = et_booking_Check_In1.getText().toString();
                        if(check_in_Date==null){
                            check_in_Date="";
                        }
                         String date2 = et_booking_Check_In2.getText().toString();
                        if(date2!=null){
                            check_in_Date = check_in_Date+","+date2;
                            String new_str = check_in_Date.replaceAll(","," "); //replace , with space
                            String temp = new_str.trim();             //remove trailing space
                            check_in_Date="";
                            check_in_Date = temp.replaceAll(" ",",");
                        }
                    }else if(number_of_Days==3){
                        String date1 = et_booking_Check_In1.getText().toString();
                        if(date1==null){
                            date1="";
                        }
                        String date2 = et_booking_Check_In2.getText().toString();
                        if(date2==null){
                           date2="";
                        }

                        String date3 = et_booking_Check_In3.getText().toString();
                        if(date3==null){
                           date3="";
                        }
                        check_in_Date = date1+","+date2+","+date3;
                        String new_str = check_in_Date.replaceAll(","," "); //replace , with space
                        String temp = new_str.trim();             //remove trailing space
                        check_in_Date="";
                        check_in_Date = temp.replaceAll(" ",",");

                    }else if(number_of_Days==4){
                        String date1 = et_booking_Check_In1.getText().toString();
                        if(date1==null){
                            date1="";
                        }
                        String date2 = et_booking_Check_In2.getText().toString();
                        if(date2==null){
                            date2="";
                        }

                        String date3 = et_booking_Check_In3.getText().toString();
                        if(date3==null){
                            date3="";
                        }

                        String date4 = et_booking_Check_In4.getText().toString();
                        if(date4==null){
                            date4="";
                        }
                        check_in_Date = date1+","+date2+","+date3+","+date4;
                        String new_str = check_in_Date.replaceAll(","," "); //replace , with space
                        String temp = new_str.trim();             //remove trailing space
                        check_in_Date="";
                        check_in_Date = temp.replaceAll(" ",",");

                    }else if(number_of_Days==5){
                        String date1 = et_booking_Check_In1.getText().toString();
                        if(date1==null){
                            date1="";
                        }
                        String date2 = et_booking_Check_In2.getText().toString();
                        if(date2==null){
                            date2="";
                        }

                        String date3 = et_booking_Check_In3.getText().toString();
                        if(date3==null){
                            date3="";
                        }

                        String date4 = et_booking_Check_In4.getText().toString();
                        if(date4==null){
                            date4="";
                        }

                        String date5 = et_booking_Check_In4.getText().toString();
                        if(date5==null){
                            date5="";
                        }
                        check_in_Date = date1+","+date2+","+date3+","+date4+","+date5;

                        String new_str = check_in_Date.replaceAll(","," "); //replace , with space
                        String temp = new_str.trim();             //remove trailing space
                        check_in_Date="";
                        check_in_Date = temp.replaceAll(" ",",");

                    }
                   Log.e("Slected All Dates",check_in_Date);
                    String isPack="False";
                    db.updateOneColumn(arrayListDialogActivities.get(dialog_pos).getTour_id(), sessionManager.getCountryCode(),"True");
                    Double vouchers_cost=0.0;
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        Float Adult_Count=  Float.valueOf(sessionManager.getAdultCount()) + Float.valueOf(sessionManager.getChildCount());
                        vouchers_cost = Final_Cost * number_of_Days / Adult_Count;
                    } else {

                        FinalCount = Integer.parseInt(tv_value_Adult.getText().toString()) + Integer.parseInt(tv_value_Child.getText().toString());
                        if (FinalCount <= Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())) {
                            vouchers_cost = Final_Cost * number_of_Days ;
                            isPack="1 Units";
                        } else {
                            vouchers_cost = Final_Cost * number_of_Days ;
                            isPack="2 Units";
                        }
                    }

                    try {
                        if(db.CheckIsAvailableActivity(arrayListDialogActivities.get(dialog_pos).getTour_id(),sessionManager.getCountryCode())){
                            //Update
                            db.updateItineraryActivityData(Integer.parseInt(arrayListDialogActivities.get(dialog_pos).getTour_id()),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0", String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost=Final_Cost * number_of_Days)),sessionManager.getCountryCode(),booking_Days,number_of_Days,"0",String.valueOf(String.format("%.2f", vouchers_cost)));
                        }else{
                            //Insert
                            db.setItineraryActivityData(arrayListDialogActivities.get(dialog_pos).getTour_id(),check_in_Date,booking_Time,String.valueOf(NoOfRooms),sessionManager.getAdultCount(),sessionManager.getChildCount(),"0",String.valueOf(activity_Amt),String.valueOf(String.format("%.2f", Final_Cost=Final_Cost * number_of_Days)),sessionManager.getCountryCode(),booking_Days,number_of_Days,pricesDetailsArrayList.get(0).getAdult_price(),pricesDetailsArrayList.get(0).getChild_price(),"0",String.valueOf(String.format("%.2f", vouchers_cost)),isPack);
                        }

                        sessionManager.setCityCode(arrayListDialogActivities.get(dialog_pos).getCity_id());
                        sessionManager.setCityName(arrayListDialogActivities.get(dialog_pos).getCity());
                        dialogMsg.cancel();
                        Toast.makeText(mContext, "Activity added in your itinerary", Toast.LENGTH_LONG).show();
                        finish();
                        overridePendingTransition( 0, 0);
                        startActivity(getIntent());
                        overridePendingTransition( 0, 0);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }
        });

        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
            }
        });

    }

    public void ShowBookingDate() {
        Log.e("Booking Size-->", String.valueOf(modelTimings.size()));

        if(modelTimings.size()>0){
            noRooms = new ArrayList<>();
            for(int i = 0; i < modelTimings.size() ; i++){
                noRooms.add(modelTimings.get(i).getTiming());
            }

            /*ArrayAdapter<String> nighgtAdapter = new ArrayAdapter<String>(mContext, R.layout.spiner_commen_list, noRooms);
            sp_time.setAdapter(nighgtAdapter);*/


            AdapterSpinerList adapterstate = new AdapterSpinerList(activity, R.layout.spiner_commen_list, R.id.spiner_list, noRooms);
            sp_time.setAdapter(adapterstate);
        }


    }


    private void getPriceDetails() {
        dialog.showCustomDialog();
        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = sessionManager.getBaseUrl() + "products/activities/pricing?tour_id=" + Event_ID + "&api_key=0D1067102F935B4CC31E082BD45014D469E35268";
        Log.e("Price URL",url);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        dialog.hideCustomeDialog();
                        Log.e("Price Response--->", response.toString());
                        try{
                            dialog.hideCustomeDialog();
                            db.deleteCalendarData();
                            new CallPriceData().execute(response.toString());

                        }catch (Exception e){
                            dialog.hideCustomeDialog();
                            showSnackBar(ActivityAllListCustomeActivities.this,"Something went wrong. Please try again.");
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();
                        showSnackBar(ActivityAllListCustomeActivities.this,"Something went wrong. Please try again.");
                       }
                }
        );

        queue.add(getRequest);
    }

    private class CallPriceData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String s = strings[0];

            Log.e("Price Detail Resp-->", s);
            try {
                pricingDetailsModels = new ArrayList<ModelPriceMain>();
                activitiesOperationTiming = new ArrayList<ModelActivitiesOperationTiming>();
                modelTimings = new ArrayList<ModelTiming>();

                JSONObject jsonObject = new JSONObject(s);
                JSONObject jsonData = jsonObject.getJSONObject("data");
                JSONObject jsonStatus = jsonData.getJSONObject("status");

                if (jsonStatus.getString("Success").equalsIgnoreCase("true")) {

                    try{

                        ModelPriceMain pricingModel = new ModelPriceMain();
                        pricingModel.setPricing_per_person(jsonData.getString("pricing_per_person"));
                        JSONArray activityListDay = jsonData.getJSONArray("operational_timings");

                        for (int k = 0; k < activityListDay.length(); k++) {
                            JSONObject activityJSON = activityListDay.getJSONObject(k);
                            ModelActivitiesOperationTiming operationTimingModel = new ModelActivitiesOperationTiming();
                            operationTimingModel.setDay(activityJSON.getString("Day"));
                            JSONArray TourTimings = activityJSON.getJSONArray("TourTimings");
                            for (int j = 0; j < TourTimings.length(); j++) {
                                JSONObject activitytime = TourTimings.getJSONObject(j);
                                operationTimingModel.setFrom_Time(activitytime.getString("From_Time"));
                                operationTimingModel.setTo_Time(activitytime.getString("To_Time"));
                            }
                            JSONArray bookingtime = activityJSON.getJSONArray("booking_timings");
                            if (Today_Day.equalsIgnoreCase(activityJSON.getString("Day"))) {
                                AvailableDayFlag=true;
                                FlagDateTimeCheck=false;
                                for (int j = 0; j < bookingtime.length(); j++) {
                                    ModelTiming modelTiming = new ModelTiming();
                                    modelTiming.setTiming(bookingtime.get(j).toString());
                                    modelTimings.add(modelTiming);
                                }
                            } else if (activityJSON.getString("Day").equalsIgnoreCase("daily")) {
                                AvailableDayFlag=true;
                                FlagDateTimeCheck=false;
                                for (int j = 0; j < bookingtime.length(); j++) {
                                    ModelTiming modelTiming = new ModelTiming();
                                    modelTiming.setTiming(bookingtime.get(j).toString());
                                    modelTimings.add(modelTiming);
                                }
                            }else if(FlagDateTimeCheck){
                                AvailableDayFlag=false;
                            }

                            db.setCalendarData(activityJSON.getString("Day"), String.valueOf(bookingtime));

                            operationTimingModel.setBooking_timings(modelTimings);
                            activitiesOperationTiming.add(operationTimingModel);
                        }

                        pricingModel.setOperationTimingModels(activitiesOperationTiming);

                        ArrayList<ModelPricingList> pricingLists = new ArrayList<>();
                        JSONArray jsonActivity = jsonData.getJSONArray("pricing");
                        for (int i = 0; i < jsonActivity.length(); i++) {
                            JSONObject activityJSON = jsonActivity.getJSONObject(i);
                            ModelPricingList pricingList = new ModelPricingList();
                            pricingList.setCurrency(activityJSON.getString("currency"));
                            pricingList.setFrom_date(activityJSON.getString("from_date"));
                            pricingList.setTo_date(activityJSON.getString("to_date"));

                            ArrayList<ModelPricesDetails> pricesDetailsArrayList = new ArrayList<>();
                            if (jsonData.getString("pricing_per_person").equalsIgnoreCase("true")) {
                                Log.e("group_pricing Fix", "---------------->" + sessionManager.getNewPrice());
                                JSONArray jsonpricing = activityJSON.getJSONArray("group_pricing");

                                for (int j = 0; j < jsonpricing.length(); j++) {
                                    JSONObject jsonObjectPrice = jsonpricing.getJSONObject(j);
                                    ModelPricesDetails pricesDetails = new ModelPricesDetails();
                                    if (sessionManager.getNewPrice().equalsIgnoreCase("0")) {
                                        pricesDetails.setAdult_price(getIsMarginPrice(jsonObjectPrice.getString("adult_price")));
                                        pricesDetails.setChild_price(getIsMarginPrice(jsonObjectPrice.getString("child_price")));
                                    } else {
                                        pricesDetails.setAdult_price(getIsMarginPrice(jsonObjectPrice.getString("adult_price")));
                                        pricesDetails.setChild_price(getIsMarginPrice(jsonObjectPrice.getString("child_price")));
                                    }
                                    pricesDetails.setFrom_pax_count(jsonObjectPrice.getString("from_pax_count"));
                                    pricesDetails.setInfant_price(getIsMarginPrice(jsonObjectPrice.getString("infant_price")));
                                    pricesDetails.setInventory(jsonObjectPrice.getString("inventory"));
                                    pricesDetails.setTo_pax_count(jsonObjectPrice.getString("to_pax_count"));
                                    pricesDetails.setCurrency(activityJSON.getString("currency"));
                                    pricesDetails.setMaximum_pax(jsonData.getString("maximum_pax"));
                                    pricesDetailsArrayList.add(pricesDetails);
                                }

                            } else {
                                Log.e("Unit Price Fix", "---------------->");

                                ModelPricesDetails pricesDetails = new ModelPricesDetails();
                                if (sessionManager.getNewPrice().equalsIgnoreCase("0")) {
                                    pricesDetails.setAdult_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                    pricesDetails.setChild_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                } else {
                                    pricesDetails.setAdult_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                    pricesDetails.setChild_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                }
                                pricesDetails.setInfant_price(getIsMarginPrice(activityJSON.getString("unit_price")));
                                pricesDetails.setInventory(activityJSON.getString("inventory"));
                                pricesDetails.setFrom_pax_count("1");
                                pricesDetails.setTo_pax_count(activityJSON.getString("inventory"));
                                pricesDetails.setCurrency(activityJSON.getString("currency"));
                                pricesDetails.setMaximum_pax(jsonData.getString("maximum_pax"));
                                pricesDetailsArrayList.add(pricesDetails);
                            }
                            pricingList.setPricingDetails(pricesDetailsArrayList);
                            pricingLists.add(pricingList);
                        }
                        pricingModel.setPricesArrayList(pricingLists);
                        pricingDetailsModels.add(pricingModel);

                    }catch (RuntimeException e){
                        Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong.", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }



                } else {
                    Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            } catch (JSONException e) {
                Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                e.printStackTrace();
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String s) {
            if(arrayListDialogActivities.get(0).getCategory_id().equalsIgnoreCase("6")|| arrayListDialogActivities.get(0).getCategory_id().equalsIgnoreCase("9")){//Vouchers
                ShowAllTransferAndVounchersDialog();
            }else if(arrayListDialogActivities.get(0).getCategory_id().equalsIgnoreCase("10")){
                ShowAllPrivateToursDialog();
            }else{
                ShowAllCommenDialog();
            }
            //ShowAllCommenDialog();

        }
    }


    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }

    public void GetCurrentDate() {

        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        int dayOfWeek = check_in_Calendar.get(Calendar.DAY_OF_WEEK);
        String day = new DateFormatSymbols().getShortWeekdays()[dayOfWeek];
        Today_Day = day.toLowerCase();
        Log.e("Today_Day-->", Today_Day);

        FlagDate = true;
    }

    public String getDiscountPrice(String Main_price) {
        Discount = arrayListDialogActivities.get(dialog_pos).getDiscount();
        Discount_Cap = arrayListDialogActivities.get(dialog_pos).getDiscount_cap();
        String data = null;
        double discount = Double.parseDouble(Discount);
        double discount_cap = Double.parseDouble(Discount_Cap);
        double Old_price = Double.parseDouble(Main_price);
        double New_price = Old_price - (Old_price / 100 * discount);
        double Difference = Old_price - New_price;

        if (discount_cap > 0) {

            if (discount_cap < Difference) {
                Double Discount_Cap_Price = Old_price - discount_cap;
                data = String.valueOf(Discount_Cap_Price);
            } else {
                data = String.valueOf(New_price);
            }
        } else {
            data = String.valueOf(New_price);
        }

        return data;
    }



    private void getDiscount() {

        Starting_price = Double.parseDouble(arrayListDialogActivities.get(dialog_pos).getStarting_from_price());

        if (arrayListDialogActivities.get(dialog_pos).getDiscount() == null || "".equalsIgnoreCase(arrayListDialogActivities.get(dialog_pos).getDiscount()) || "0".equalsIgnoreCase(arrayListDialogActivities.get(dialog_pos).getDiscount()) || arrayListActivities.get(dialog_pos).getDiscount().isEmpty()) {
           // rl_discount.setVisibility(View.GONE);
           // tv_old_price.setVisibility(View.GONE);
            sessionManager.setNewprice("0");


            try {
                int intpart = (int) Starting_price;
                float decpart = (float) (Starting_price - intpart);
                if (decpart == 0.0f) {
                    //tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + activitiesDetailstModels.get(0).getStarting_from_price());
                } else {
                   // tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f", Starting_price));
                }
            } catch (Exception e) {
               // tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + activitiesDetailstModels.get(0).getStarting_from_price());

            }

        } else {

            try{

                double discount =  Double.parseDouble(arrayListDialogActivities.get(dialog_pos).getDiscount());
                double discount_cap = Double.parseDouble(arrayListDialogActivities.get(dialog_pos).getDiscount_cap());
                double Old_price = Double.parseDouble(arrayListDialogActivities.get(dialog_pos).getStarting_from_price());
                double New_price = Old_price -(Old_price/100 * discount) ;

                double Difference = Old_price - New_price;
                Log.e("Old Price-->",String.valueOf(Old_price));
                Log.e("New Price-->",String.valueOf(New_price));
                Log.e("New Price vv-->",String.valueOf(Math.round(New_price)));
                Log.e("Difference vv-->",String.valueOf(String.format("%.2f",New_price)));
                Log.e("Difference-->",String.valueOf(Difference));
                Log.e("Difference vv-->",String.valueOf(String.format("%.2f",Difference)));
                Log.e("discount_cap-->",String.valueOf(discount_cap));
                Log.e("discount_cap-->","---------------------------------");

                if(discount_cap > 0 ){

                    if(discount_cap < Difference){
                        Double Discount_Cap_Price = Old_price - discount_cap;
                        //tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + Discount_Cap_Price);
                        //tv_discount.setText(Math.round(discount) + "% Off ");
                        //tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                        //tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        sessionManager.setNewprice(String.valueOf(Discount_Cap_Price));
                    }else{
                        //tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",New_price));
                        //tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                       // tv_discount.setText(Math.round(discount) + "% Off ");
                        //tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        sessionManager.setNewprice(String.valueOf(New_price));                    }
                }else{
                    //tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",New_price));
                    //tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                    //tv_discount.setText(Math.round(discount) + "% Off ");
                    //tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                    sessionManager.setNewprice(String.valueOf(New_price));
                }
            }catch(Exception e){

            }

        }
    }

    public void showSnackBar(Activity activity, String message){
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message,2000 ).show();
    }
}

