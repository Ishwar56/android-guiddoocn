package com.guiddoocn.holidays.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.BaseKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelAllHotelList;
import com.guiddoocn.holidays.models.ModelMyBooking;
import com.guiddoocn.holidays.models.ModelStandardBookingPayment;
import com.guiddoocn.holidays.utils.AvenuesParams;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.ServiceUtility;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityPackageBooking extends AppCompatActivity {

    private static final String TAG = "ActivityPackageBooking";
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.et_booking_Check_In)
    EditText et_booking_Check_In;
    Context mContext;
    private DatePickerDialog.OnDateSetListener Fromdate_Check_In;
    private Calendar check_in_Calendar, check_out_Calendar;
    private int Pacakge_Amount = 0, Package_ID = 0;
    @BindView(R.id.tv_Packages_Name)
    TextView tv_Packages_Name;

    @BindView(R.id.tv_dateSelection)
    TextView tv_dateSelection;//

    @BindView(R.id.ll_adult)
    LinearLayout ll_adult;
    @BindView(R.id.tv_max_Adult)
    TextView tv_max_Adult;
    @BindView(R.id.tv_min_Adult)
    TextView tv_min_Adult;
    @BindView(R.id.tv_value_Adult)
    TextView tv_value_Adult;

    @BindView(R.id.ll_child)
    LinearLayout ll_child;
    @BindView(R.id.tv_max_Child)
    TextView tv_max_Child;
    @BindView(R.id.tv_min_Child)
    TextView tv_min_Child;
    @BindView(R.id.tv_value_Child)
    TextView tv_value_Child;

    @BindView(R.id.tv_adultAmount)
    TextView tv_adultAmount;
    @BindView(R.id.tv_childAmount)
    TextView tv_childAmount;
    @BindView(R.id.tv_totalAmount)
    TextView tv_totalAmount;

    @BindView(R.id.bt_send)
    Button bt_send;

    @BindView(R.id.bt_Buy)
    Button bt_Buy;

    int AdultTotal = 0, ChildTotal = 0, FinalTotal = 0,randomNum;
    float Total_Final_Cost=0;
    String mPackage_Name, mCurrency;
    private CustomResponseDialog dialog;
    private SessionManager sessionManager;
    String currentDate="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_booking);
        ButterKnife.bind(this);
        mContext = this;
        check_in_Calendar = Calendar.getInstance();
        dialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);
        Bundle bundleExtra = getIntent().getExtras();
        if (bundleExtra != null) {
            try {
                try {
                    FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
                } catch (NullPointerException e) {

                }

                Pacakge_Amount = getIntent().getIntExtra("Amount", 1);
                Package_ID = getIntent().getIntExtra("Package_ID", 1);
                mPackage_Name = getIntent().getStringExtra("Package_Name");
                mCurrency = getIntent().getStringExtra("Currency");

            } catch (Exception e) {
                showSnackBar(ActivityPackageBooking.this, "Something went wrong. Please try again.");
            }

        } else {
            showSnackBar(ActivityPackageBooking.this, "Something went wrong. Please try again.");
        }


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {


            et_booking_Check_In.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate_Check_In, check_in_Calendar
                            .get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH),
                            check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                    mDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    mDate.show();

                }
            });
            Fromdate_Check_In = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    check_in_Calendar.set(Calendar.YEAR, year);
                    check_in_Calendar.set(Calendar.MONTH, monthOfYear);
                    check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "dd-MM-yyyy";
                    final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    et_booking_Check_In.setText(sdf.format(check_in_Calendar.getTime()));
                    String myFormat1 = "dd/MM/yyyy";
                    final SimpleDateFormat sdf1 = new SimpleDateFormat(myFormat1, Locale.US);
                    currentDate = sdf1.format(check_in_Calendar.getTime());
                    tv_dateSelection.setText(sdf1.format(check_in_Calendar.getTime()));
                }

            };


        } catch (Exception e) {
            e.printStackTrace();
        }

        tv_adultAmount.setText(Pacakge_Amount + " x " + 1 + " = " + Pacakge_Amount);
        tv_childAmount.setText(Pacakge_Amount + " x " + 0 + " = " + AdultTotal);

        //tv_totalAmount.setText(String.valueOf(String.format("%.02f", Pacakge_Amount)));
        tv_totalAmount.setText(String.valueOf(Pacakge_Amount));
        tv_title.setText(mPackage_Name);
        tv_Packages_Name.setText(mPackage_Name);

        try {
            bt_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        if (currentDate.equalsIgnoreCase("")) {
                            Toast.makeText(mContext, "Invalid Date Please Select Date", Toast.LENGTH_LONG).show();
                        } else {

                            Toast.makeText(mContext, "Work in progress", Toast.LENGTH_SHORT).show();
                           /* ArrayList<ModelStandardBookingPayment> bookingPaymentArrayList =new ArrayList<>();
                            ModelStandardBookingPayment bookingPayment =new ModelStandardBookingPayment();
                            bookingPayment.setPackage_id(String.valueOf(Package_ID));
                            bookingPayment.setPackage_Name(mPackage_Name);
                            bookingPayment.setPackage_type(String.valueOf(1));
                            bookingPayment.setCurrency(mCurrency);
                            bookingPayment.setAmount_payable(String.valueOf(FinalTotal));
                            bookingPayment.setTotal_amount(String.valueOf(FinalTotal));
                            bookingPayment.setTravel_date(currentDate);
                            bookingPayment.setNo_of_adult(tv_value_Adult.getText().toString());
                            bookingPayment.setNo_of_child(tv_value_Child.getText().toString());
                            bookingPaymentArrayList.add(bookingPayment);

                            Intent intent = new Intent(ActivityPackageBooking.this, ActivityRazorPayPayment.class);
                            intent.putExtra("amount",String.valueOf(FinalTotal));
                            intent.putExtra("Package_ID", Package_ID);
                            intent.putExtra("Package_Name", mPackage_Name);
                            intent.putExtra("curruncy", mCurrency);
                            intent.putExtra("Current_Date", currentDate);
                            intent.putExtra(AvenuesParams.BILLING_EMAIL, sessionManager.getEmailId());
                            intent.putExtra(AvenuesParams.BILLING_TEL, sessionManager.getUserMobileNo());
                            intent.putExtra("BookingDetailModels", (ArrayList<ModelStandardBookingPayment>) bookingPaymentArrayList);
                            startActivity(intent);*/
                        }
                    }catch (Exception e)
                    {
                        Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            });


            tv_max_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Adult.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e(TAG, "data =" + max);
                    if (max <= 10) {
                        tv_value_Adult.setText("" + max);
                        Log.e(TAG, "" + tv_value_Adult.getText().toString().trim());
                        Log.e(TAG, "" + tv_value_Child.getText().toString().trim());
                        AdultTotal = Pacakge_Amount * max;
                        tv_adultAmount.setText(Pacakge_Amount + " x " + max + " = " + AdultTotal);
                        FinalTotal = AdultTotal + ChildTotal;
                        tv_totalAmount.setText(String.valueOf(FinalTotal));
                    } else {
                        tv_value_Adult.setText("10");

                    }

                }
            });


            tv_min_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Adult.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 1) {
                        tv_value_Adult.setText("" + min);
                        AdultTotal = Pacakge_Amount * min;
                        tv_adultAmount.setText(Pacakge_Amount + " x " + min + " = " + AdultTotal);
                        FinalTotal = AdultTotal + ChildTotal;
                        tv_totalAmount.setText(String.valueOf(FinalTotal));
                    } else {
                        tv_value_Adult.setText("1");
                    }

                }
            });


            //setting a value for Adult And Child
            Log.e(TAG, "data =");
            tv_max_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Child.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e(TAG, "data =" + max);
                    if (max <= 10) {
                        tv_value_Child.setText("" + max);
                        Log.e(TAG, "" + tv_value_Child.getText().toString().trim());
                        ChildTotal = Pacakge_Amount * max;
                        tv_childAmount.setText(Pacakge_Amount + " x " + max + " = " + ChildTotal);
                        FinalTotal = AdultTotal + ChildTotal;
                        tv_totalAmount.setText(String.valueOf(FinalTotal));
                    } else {
                        tv_value_Child.setText("10");
                    }
                }
            });
            tv_min_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Child.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 0) {
                        tv_value_Child.setText("" + min);
                        ChildTotal = Pacakge_Amount * min;
                        tv_childAmount.setText(Pacakge_Amount + " x " + min + " = " + ChildTotal);
                        FinalTotal = AdultTotal + ChildTotal;
                        tv_totalAmount.setText(String.valueOf(FinalTotal));
                    } else {
                        tv_value_Child.setText("0");
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("erro", "code=" + e.getMessage());
        }

        try {
            bt_Buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        if (currentDate.equalsIgnoreCase("")) {
                            Toast.makeText(mContext, "Invalid Date Please Select Date", Toast.LENGTH_LONG).show();
                        } else {

                            ArrayList<ModelStandardBookingPayment> bookingPaymentArrayList =new ArrayList<>();
                            ModelStandardBookingPayment bookingPayment =new ModelStandardBookingPayment();
                            bookingPayment.setPackage_id(String.valueOf(Package_ID));
                            bookingPayment.setPackage_Name(mPackage_Name);
                            bookingPayment.setPackage_type(String.valueOf(1));
                            bookingPayment.setCurrency(mCurrency);
                            bookingPayment.setAmount_payable(String.valueOf(FinalTotal));
                            bookingPayment.setTotal_amount(String.valueOf(FinalTotal));
                            bookingPayment.setTravel_date(currentDate);
                            bookingPayment.setNo_of_adult(tv_value_Adult.getText().toString());
                            bookingPayment.setNo_of_child(tv_value_Child.getText().toString());
                            bookingPaymentArrayList.add(bookingPayment);

                            Intent intent = new Intent(ActivityPackageBooking.this, ActivityRazorPayPayment.class);
                            intent.putExtra("amount",String.valueOf(FinalTotal));
                            intent.putExtra("Package_ID", Package_ID);
                            intent.putExtra("Package_Name", mPackage_Name);
                            intent.putExtra("curruncy", mCurrency);
                            intent.putExtra("Current_Date", currentDate);
                            intent.putExtra(AvenuesParams.BILLING_EMAIL, sessionManager.getEmailId());
                            intent.putExtra(AvenuesParams.BILLING_TEL, sessionManager.getUserMobileNo());
                            intent.putExtra("BookingDetailModels", (ArrayList<ModelStandardBookingPayment>) bookingPaymentArrayList);
                            startActivity(intent);
                        }
                    }catch (Exception e)
                    {
                        Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            });


            tv_max_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Adult.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e(TAG, "data =" + max);
                    if (max <= 10) {
                        tv_value_Adult.setText("" + max);
                        Log.e(TAG, "" + tv_value_Adult.getText().toString().trim());
                        Log.e(TAG, "" + tv_value_Child.getText().toString().trim());
                        AdultTotal = Pacakge_Amount * max;
                        tv_adultAmount.setText(Pacakge_Amount + " x " + max + " = " + AdultTotal);
                        FinalTotal = AdultTotal + ChildTotal;
                        tv_totalAmount.setText(String.valueOf(FinalTotal));
                    } else {
                        tv_value_Adult.setText("10");

                    }

                }
            });


            tv_min_Adult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Adult.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 1) {
                        tv_value_Adult.setText("" + min);
                        AdultTotal = Pacakge_Amount * min;
                        tv_adultAmount.setText(Pacakge_Amount + " x " + min + " = " + AdultTotal);
                        FinalTotal = AdultTotal + ChildTotal;
                        tv_totalAmount.setText(String.valueOf(FinalTotal));
                    } else {
                        tv_value_Adult.setText("1");
                    }

                }
            });


            //setting a value for Adult And Child
            Log.e(TAG, "data =");
            tv_max_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String maxvalue = tv_value_Child.getText().toString().trim();
                    int max = Integer.parseInt(maxvalue);
                    max = max + 1;
                    Log.e(TAG, "data =" + max);
                    if (max <= 10) {
                        tv_value_Child.setText("" + max);
                        Log.e(TAG, "" + tv_value_Child.getText().toString().trim());
                        ChildTotal = Pacakge_Amount * max;
                        tv_childAmount.setText(Pacakge_Amount + " x " + max + " = " + ChildTotal);
                        FinalTotal = AdultTotal + ChildTotal;
                        tv_totalAmount.setText(String.valueOf(FinalTotal));
                    } else {
                        tv_value_Child.setText("10");
                    }
                }
            });
            tv_min_Child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String minvalue = tv_value_Child.getText().toString().trim();
                    int min = Integer.parseInt(minvalue);
                    min--;
                    if (min >= 0) {
                        tv_value_Child.setText("" + min);
                        ChildTotal = Pacakge_Amount * min;
                        tv_childAmount.setText(Pacakge_Amount + " x " + min + " = " + ChildTotal);
                        FinalTotal = AdultTotal + ChildTotal;
                        tv_totalAmount.setText(String.valueOf(FinalTotal));
                    } else {
                        tv_value_Child.setText("0");
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("erro", "code=" + e.getMessage());
        }

    }

    /*private void create_PackageBooking() {

        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "BookPackages";
        Log.e(TAG, "ListOfPackages URL---->" + url);

        JSONObject object = new JSONObject();
        try {
            object.put("amount_payable", bookingPaymentArrayList.get(0).getAmount_payable());
            object.put("booker_email", mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            object.put("booker_name", sessionManager.getContactPerson());
            object.put("booking_reference_no",mainIntent.getStringExtra("orderId") );//pass month id 1 to 12
            object.put("invoice_no", mainIntent.getStringExtra("orderId"));
            object.put("confirmation_no", mainIntent.getStringExtra("orderId"));
            object.put("currency", bookingPaymentArrayList.get(0).getCurrency());
            object.put("no_of_adult", bookingPaymentArrayList.get(0).getNo_of_adult());
            object.put("no_of_child", bookingPaymentArrayList.get(0).getNo_of_child());
            object.put("no_of_infant", "0");
            object.put("child_age", "");
            object.put("infant_age", "");
            object.put("cancellation_amount", "0");
            object.put("package_id", bookingPaymentArrayList.get(0).getPackage_id());
            object.put("package_type", bookingPaymentArrayList.get(0).getPackage_type());
            object.put("partner_id", sessionManager.getUserId());
            object.put("status", 2);
            object.put("total_amt", bookingPaymentArrayList.get(0).getTotal_amount());
            object.put("transaction_id", mainIntent.getStringExtra("transaction_id"));
            object.put("travel_date", bookingPaymentArrayList.get(0).getTravel_date());
            object.put("voucher_link", "");

            Log.e("Parameter pass-->", object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e(TAG, "ListOfPackages Response---->" + response.toString());

                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.toString()));
                            String jsonStatus= jsonObject.getString("status_code");
                            Log.e(TAG, "Setaus---->" + response.toString());
                            if (jsonStatus.equalsIgnoreCase("200")) {

                                showSnackBar(ActivityPaymentStatus.this,jsonObject.getString("error_message"));

                            } else {
                                showSnackBar(ActivityPaymentStatus.this,jsonObject.getString("error_message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20 * 3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);


    }*/


    private void showSnackBar(ActivityPackageBooking activityPackageBooking, String message) {
        View rootView = activityPackageBooking.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 1500).show();
    }
}
