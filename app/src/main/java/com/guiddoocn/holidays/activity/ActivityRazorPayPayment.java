package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelStandardBookingPayment;
import com.guiddoocn.holidays.utils.AppConstants;
import com.guiddoocn.holidays.utils.AvenuesParams;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.ServiceUtility;
import com.guiddoocn.holidays.utils.SessionManager;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityRazorPayPayment extends AppCompatActivity implements PaymentResultListener {

    @BindView(R.id.iv_back)
    ImageView tv_back;

    @BindView(R.id.tv_title)
    TextView tv_title;

    Intent mainIntent;
    Integer randomNum ;



    private static final String TAG = "logPaymentActivity";
    private Activity activity = this;
    private int Total_cost=0;
    private Context mContext;
    private SessionManager sessionManager;
    private String Amount,Package_ID,Package_Name,Currency,Current_Date;
    ArrayList<ModelStandardBookingPayment> bookingPaymentArrayList =new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razorpay_payment);
        ButterKnife.bind(this);
        mainIntent = getIntent();
        mContext = this;
        Checkout.preload(getApplicationContext());
        sessionManager = new SessionManager(mContext);
        randomNum = ServiceUtility.randInt(0, 999999);

        tv_title.setText("Transaction");
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }
        bookingPaymentArrayList = (ArrayList<ModelStandardBookingPayment>) getIntent().getSerializableExtra("BookingDetailModels");
        Amount=bookingPaymentArrayList.get(0).getTotal_amount();
        Package_ID=bookingPaymentArrayList.get(0).getPackage_id();
        Package_Name=bookingPaymentArrayList.get(0).getPackage_Name();
        Currency=bookingPaymentArrayList.get(0).getCurrency();



        Double cost = Double.parseDouble(Amount);
        Log.e("Total Cost int", String.valueOf(cost));
        Total_cost = (int) (cost*100);
        Log.e("Pass Cost int", String.valueOf(Total_cost));

        if (sessionManager.isNetworkAvailable()) {
            startPayment();
        } else {
            try {
                Toast.makeText(activity, "Please check internet connectivity", Toast.LENGTH_SHORT).show();
               /* Intent i = new Intent(ActivityRazorPayPayment.this, ActivityPaymentStatus.class);
                i.putExtra(AvenuesParams.BILLING_EMAIL,mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
                i.putExtra(AvenuesParams.BILLING_TEL,mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
                i.putExtra("orderId",randomNum+"");
                i.putExtra("transStatus","failure");
                i.putExtra("transaction_id",randomNum);
                i.putExtra("BookingDetailModels", (ArrayList<ModelStandardBookingPayment>) bookingPaymentArrayList);
                startActivity(i);*/
                finish();
            } catch (Exception e) {
                Log.e(TAG, "Exception in onPaymentError", e);
            }
        }

    }



    private void startPayment() {
        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
           // options.put("key", "GnWy0nj8txFfvXwyyAgKtimd");
            options.put("name", Package_Name);
            options.put("description", "");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png"); //https://s3.amazonaws.com/rzp-mobile/images/rzp.png
            options.put("currency", Currency);
            options.put("amount", Total_cost);
            JSONObject preFill = new JSONObject();
            preFill.put("email", mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            preFill.put("contact", mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
            options.put("prefill", preFill);
            co.open(this, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_LONG)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        Log.e("razorpayPaymentID--->",razorpayPaymentID);
        Log.e("razorpay response--->","Success");
        try {

            Intent i = new Intent(ActivityRazorPayPayment.this, ActivityPaymentStatus.class);
            i.putExtra(AvenuesParams.BILLING_EMAIL,mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            i.putExtra(AvenuesParams.BILLING_TEL,mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
            i.putExtra("orderId",randomNum+"");
            i.putExtra("BookingDetailModels", (ArrayList<ModelStandardBookingPayment>) bookingPaymentArrayList);
            i.putExtra("transStatus","success");
            i.putExtra("transaction_id",razorpayPaymentID);
            startActivity(i);
            finish();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        Log.e("razorpay response--->",response);
        try {
            Intent i = new Intent(ActivityRazorPayPayment.this, ActivityPaymentStatus.class);
            i.putExtra(AvenuesParams.BILLING_EMAIL,mainIntent.getStringExtra(AvenuesParams.BILLING_EMAIL));
            i.putExtra(AvenuesParams.BILLING_TEL,mainIntent.getStringExtra(AvenuesParams.BILLING_TEL));
            i.putExtra("orderId",randomNum+"");
            i.putExtra("transStatus","failure");
            i.putExtra("transaction_id",String.valueOf(randomNum));
            i.putExtra("BookingDetailModels", (ArrayList<ModelStandardBookingPayment>) bookingPaymentArrayList);
            startActivity(i);
            finish();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
