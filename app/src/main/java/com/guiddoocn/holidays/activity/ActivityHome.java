package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.fragment.FragmentAboutUs;
import com.guiddoocn.holidays.fragment.FragmentContactUs;
import com.guiddoocn.holidays.fragment.FragmentHome;
import com.guiddoocn.holidays.fragment.FragmentMediaKit;
import com.guiddoocn.holidays.fragment.FragmentMyAccount;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.LocaleHelper;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityHome extends AppCompatActivity  {

    @BindView(R.id.profile_photo)
    ImageView profile_photo;

    @BindView(R.id.tv_user_Name)
    TextView tv_user_Name;

    @BindView(R.id.tv_user_Mobile)
    TextView tv_user_Mobile;

    @BindView(R.id.tv_user_Email)
    TextView tv_user_Email;

    @BindView(R.id.fragment_container)
    LinearLayout fragment_container;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_main_toolbar_title)
    TextView tv_main_title;

    @BindView(R.id.ll_tool_Packages)
    LinearLayout ll_tool_Packages;


    @BindView(R.id.iv_menu)
    ImageView iv_menu;

    @BindView(R.id.iv_share)
    ImageView iv_share;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.ll_Home)
    LinearLayout ll_Home;
    @BindView(R.id.iv_Home)
    ImageView iv_Home;
    @BindView(R.id.tv_Home)
    TextView tv_Home;

    @BindView(R.id.ll_My_account)
    LinearLayout ll_My_account;
    @BindView(R.id.iv_My_account)
    ImageView iv_My_account;
    @BindView(R.id.tv_My_account)
    TextView tv_My_account;


    @BindView(R.id.ll_Packages)
    LinearLayout ll_Packages;
    @BindView(R.id.iv_Packages)
    ImageView iv_Packages;
    @BindView(R.id.tv_Packages)
    TextView tv_Packages;

    @BindView(R.id.ll_About_us)
    LinearLayout ll_About_us;
    @BindView(R.id.iv_About_us)
    ImageView iv_About_us;
    @BindView(R.id.tv_About_us)
    TextView tv_About_us;

    @BindView(R.id.ll_Media_Kit)
    LinearLayout ll_Media_Kit;
    @BindView(R.id.iv_Media_Kit)
    ImageView iv_Media_Kit;
    @BindView(R.id.tv_Media_Kit)
    TextView tv_Media_Kit;

    @BindView(R.id.ll_Destination)
    LinearLayout ll_Destination;
    @BindView(R.id.iv_Destination)
    ImageView iv_Destination;
    @BindView(R.id.tv_Destination)
    TextView tv_Destination;

    @BindView(R.id.ll_Contact_us)
    LinearLayout ll_Contact_us;
    @BindView(R.id.iv_Contact_us)
    ImageView iv_Contact_us;
    @BindView(R.id.tv_Contact_us)
    TextView tv_Contact_us;

    @BindView(R.id.ll_Logout)
    LinearLayout ll_Logout;
    @BindView(R.id.iv_Logout)
    ImageView iv_Logout;
    @BindView(R.id.tv_Logout)
    TextView tv_Logout;

    @BindView(R.id.ll_English)
    LinearLayout ll_English;
    @BindView(R.id.tv_english)
    TextView tv_english;

    @BindView(R.id.ll_Chinese)
    LinearLayout ll_Chinese;
    @BindView(R.id.tv_chinese)
    TextView tv_chinese;



    private CustomResponseDialog customResponseDialog;
    private Context mContext;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mContext = this;
        sessionManager = new SessionManager(mContext);
        customResponseDialog = new CustomResponseDialog(mContext);
        ButterKnife.bind(this);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        SessionManager.drawer = drawer;


        tv_user_Name.setText(sessionManager.getUserName());
        tv_user_Mobile.setText(sessionManager.getUserMobileNo());
        tv_user_Email.setText(sessionManager.getEmailId());
        /*Call by Open Navigation Drawer*/
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);

            }
        });

        ll_Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                sessionManager.setNavigation("");
                setNavigationView();
            }
        });

        ll_My_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                sessionManager.setNavigation("0");
                setNavigationView();
            }
        });

        ll_tool_Packages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                Intent intent =new Intent(ActivityHome.this,ActivityBooking.class);
                startActivity(intent);
            }
        });

        ll_Packages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                sessionManager.setNavigation("1");
                setNavigationView();
            }
        });

        ll_About_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                sessionManager.setNavigation("2");
                setNavigationView();
            }
        });

        ll_Media_Kit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                sessionManager.setNavigation("3");
                setNavigationView();
            }
        });

        ll_Destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                sessionManager.setNavigation("4");
                setNavigationView();
            }
        });

        ll_Contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                sessionManager.setNavigation("5");
                setNavigationView();
            }
        });

        ll_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                /*sessionManager.setNavigation("6");*/
                setNavigationView();
                CallLogOut();
            }
        });

        ll_English.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                sessionManager.setLanguage("En");
                setLanguageView();
            }
        });
        ll_Chinese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                sessionManager.setLanguage("Cn");
                setLanguageView();
            }
        });


    }

    public void setLanguageView(){
        if(sessionManager.getLanguage().equalsIgnoreCase("En")){
            tv_english.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
            tv_chinese.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            LocaleHelper.persist(mContext,"en");
            setLocale(LocaleHelper.getLanguage(mContext));
        }else{
            tv_english.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            tv_chinese.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
            LocaleHelper.persist(mContext,"zh");
            setLocale(LocaleHelper.getLanguage(mContext));
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void setNavigationView(){

        try{

            if(sessionManager.getNavigation().equalsIgnoreCase("0")){       //Select My_account
                tv_main_title.setText(getText(R.string.nav_my_account));
                ll_Home.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Home.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Home.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_My_account.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationSelect));
                tv_My_account.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
                DrawableCompat.setTint(iv_My_account.getDrawable(), ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));

                ll_Packages.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Packages.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Packages.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_About_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_About_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_About_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Media_Kit.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Media_Kit.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Media_Kit.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Destination.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Destination.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Destination.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Contact_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Contact_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Contact_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Logout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Logout.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Logout.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                changeFragment(R.id.fragment_container, new FragmentMyAccount(), true);

            }else if(sessionManager.getNavigation().equalsIgnoreCase("1")){       //Select Packages
                //tv_main_title.setText(getText(R.string.nav_packages));
               /* ll_Home.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Home.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Home.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_My_account.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_My_account.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_My_account.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Packages.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationSelect));
                tv_Packages.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
                DrawableCompat.setTint(iv_Packages.getDrawable(), ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));

                ll_About_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_About_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_About_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Media_Kit.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Media_Kit.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Media_Kit.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Destination.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Destination.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Destination.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Contact_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Contact_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Contact_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Logout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Logout.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Logout.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));*/
                Intent intent =new Intent(ActivityHome.this,ActivityBooking.class);
                startActivity(intent);


            }else  if(sessionManager.getNavigation().equalsIgnoreCase("2")){//Select About_us
                tv_main_title.setText(getText(R.string.nav_about_us));
                ll_Home.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Home.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Home.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_My_account.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_My_account.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_My_account.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Packages.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Packages.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Packages.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_About_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationSelect));
                tv_About_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
                DrawableCompat.setTint(iv_About_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));

                ll_Media_Kit.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Media_Kit.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Media_Kit.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Destination.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Destination.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Destination.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Contact_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Contact_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Contact_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Logout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Logout.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Logout.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                changeFragment(R.id.fragment_container, new FragmentAboutUs(), true);

            }else if(sessionManager.getNavigation().equalsIgnoreCase("3")){       //Select Media_Kit

                tv_main_title.setText(getText(R.string.nav_media_kit));
                ll_Home.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Home.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Home.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_My_account.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_My_account.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_My_account.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Packages.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Packages.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Packages.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_About_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_About_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_About_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Media_Kit.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationSelect));
                tv_Media_Kit.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
                DrawableCompat.setTint(iv_Media_Kit.getDrawable(), ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));

                ll_Destination.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Destination.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Destination.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Contact_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Contact_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Contact_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Logout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Logout.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Logout.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                changeFragment(R.id.fragment_container, new FragmentMediaKit(), true);

            }else if(sessionManager.getNavigation().equalsIgnoreCase("4")){       //Select Destination
               // Intent intent=new Intent(ActivityHome.this, ActivityAllListMainActivities.class);
                Intent intent=new Intent(ActivityHome.this, ActivityDestination.class);
                mContext.startActivity(intent);

               /* Intent intent=new Intent(ActivityHome.this, ActivityDestination.class);
                mContext.startActivity(intent);*/

                /*tv_main_title.setText(getText(R.string.nav_destination));
                ll_Home.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Home.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Home.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_My_account.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_My_account.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_My_account.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Packages.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Packages.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Packages.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_About_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_About_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_About_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Media_Kit.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Media_Kit.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Media_Kit.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Destination.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationSelect));
                tv_Destination.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
                DrawableCompat.setTint(iv_Destination.getDrawable(), ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));

                ll_Contact_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Contact_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Contact_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Logout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Logout.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Logout.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                changeFragment(R.id.fragment_container, new FragmentMediaKitDetails(), true);*/

            }else if(sessionManager.getNavigation().equalsIgnoreCase("5")){     //Select Contact_us

                tv_main_title.setText(getText(R.string.nav_contact_us));
                ll_Home.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Home.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Home.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_My_account.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_My_account.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_My_account.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Packages.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Packages.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Packages.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_About_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_About_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_About_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Media_Kit.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Media_Kit.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Media_Kit.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Destination.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Destination.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Destination.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Contact_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationSelect));
                tv_Contact_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
                DrawableCompat.setTint(iv_Contact_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));

                ll_Logout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Logout.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Logout.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                changeFragment(R.id.fragment_container, new FragmentContactUs(), true);
            }else if(sessionManager.getNavigation().equalsIgnoreCase("6")){     //Select LogOut

                tv_main_title.setText(getText(R.string.nav_contact_us));
                ll_Home.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Home.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Home.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_My_account.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_My_account.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_My_account.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Packages.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Packages.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Packages.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_About_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_About_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_About_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Media_Kit.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Media_Kit.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Media_Kit.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Destination.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Destination.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Destination.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Contact_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Contact_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Contact_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Logout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationSelect));
                tv_Logout.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
                DrawableCompat.setTint(iv_Logout.getDrawable(), ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));

            }else{          //Select byDefault Home Page

                tv_main_title.setText(getText(R.string.nav_home));
                ll_Home.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationSelect));
                tv_Home.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
                DrawableCompat.setTint(iv_Home.getDrawable(), ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));

                ll_My_account.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_My_account.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_My_account.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Packages.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Packages.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Packages.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_About_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_About_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_About_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Media_Kit.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Media_Kit.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Media_Kit.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Destination.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Destination.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Destination.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Contact_us.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Contact_us.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Contact_us.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                ll_Logout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorNavigationUnSelect));
                tv_Logout.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                DrawableCompat.setTint(iv_Logout.getDrawable(), ContextCompat.getColor(mContext, R.color.colorWhite));

                changeFragment(R.id.fragment_container, new FragmentHome(), false);
            }

        }catch (Exception e){

        }


    }

    public void closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }

    public void showDownload() {
        Intent i = new Intent();
        i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //getFragmentManager().popBackStack();
        sessionManager.setNavigation("");
        setNavigationView();
        if(sessionManager.getLanguage().equalsIgnoreCase("En")){
            tv_english.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
            tv_chinese.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
        }else{
            tv_english.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            tv_chinese.setTextColor(ContextCompat.getColor(mContext, R.color.colorNavigationYellowShade));
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

    }



    /*Call by update toolbar Name*/
    public void setToolberTitle(String title){
        tv_main_title.setText(title);
    }


    /*Call by change fragmnent */
    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {

        }
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        Intent refresh = new Intent(ActivityHome.this, ActivityHome.class);
        startActivity(refresh);
        finish();
    }

    private void CallLogOut(){

        final Dialog dialogMsg = new Dialog(mContext);
        dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMsg.setContentView(R.layout.dialog_login);
        dialogMsg.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogMsg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogMsg.getWindow().setAttributes(lp);
        dialogMsg.show();

        TextView cardViewCancel = (TextView) dialogMsg.findViewById(R.id.btn_cancel);
        TextView tv_title = (TextView) dialogMsg.findViewById(R.id.tv_title);
        TextView tv_msg = (TextView) dialogMsg.findViewById(R.id.tv_msg);
        TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_done);

        tv_title.setText(getString(R.string.logout));
        tv_msg.setText(R.string.logout_text);
        btn_text.setText(getString(R.string.logout));


        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
                sessionManager.setLogedIn(false);
                //sessionManager.setUserID("0");
                sessionManager.setUserMobile("");
                Intent intent = new Intent(ActivityHome.this, ActivityLogin.class);
                startActivity(intent);
                finish();

            }
        });

        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.cancel();
            }
        });
    }


}
