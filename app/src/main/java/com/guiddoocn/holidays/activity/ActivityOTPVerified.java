package com.guiddoocn.holidays.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.GPSTracker;
import com.guiddoocn.holidays.utils.SessionManager;
import com.hbb20.CountryCodePicker;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityOTPVerified extends AppCompatActivity implements View.OnClickListener  {

    @BindView(R.id.ll_send_OTP)
    LinearLayout ll_send_OTP;
    @BindView(R.id.ll_verify_OTP)
    LinearLayout ll_verify_OTP;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;

    @BindView(R.id.iv_back)
    ImageView tv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.fetMobile)
    FormEditText fetMobile;
    @BindView(R.id.btn_send_OTP)
    Button btn_send_OTP;

    @BindView(R.id.otp_mobile)
    TextView otp_mobile;

    @BindView(R.id.tv_edit_mobile)
    TextView tv_edit_mobile;

    @BindView(R.id.et_first)
    EditText et_first;

    @BindView(R.id.et_second)
    EditText et_second;

    @BindView(R.id.et_third)
    EditText et_third;

    @BindView(R.id.et_four)
    EditText et_four;

    @BindView(R.id.et_five)
    EditText et_five;

    @BindView(R.id.et_six)
    EditText et_six;

    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.tv_resend_OTP)
    TextView tv_resend_OTP;

    @BindView(R.id.tv_timer)
    TextView tv_timer;

    @BindView(R.id.tv_skip)
    TextView tv_skip;

    @BindView(R.id.iv_clear)
    ImageView iv_clear;

    @BindView(R.id.btn_verify_OTP)
    Button btn_verify_OTP;

    private SessionManager sessionManager;
    private boolean OTP=false,FlagPermissionFlow = true;
    private String OTPValue="",deviceId="";
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private Context mContext;
    private String[] AppPermissions = {Manifest.permission.RECEIVE_SMS,Manifest.permission.READ_SMS};
    private int Count=1;
    private GPSTracker gpsTracker;
    private CustomResponseDialog customResponseDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverified);
        mContext = this;
        ButterKnife.bind(this);
        sessionManager=new SessionManager(this);
        gpsTracker = new GPSTracker(mContext, ActivityOTPVerified.this);
        customResponseDialog =new CustomResponseDialog(mContext);
        setUI();
        tv_title.setText("Mobile Verification");
        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);


        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        /*if (checkAppPermissions(mContext, AppPermissions)) {

        } else if(FlagPermissionFlow){
            ActivityCompat.requestPermissions(this, AppPermissions, 102);
        }*/

        et_first.addTextChangedListener(new GenericTextWatcher(et_first));
        et_second.addTextChangedListener(new GenericTextWatcher(et_second));
        et_third.addTextChangedListener(new GenericTextWatcher(et_third));
        et_four.addTextChangedListener(new GenericTextWatcher(et_four));
        et_five.addTextChangedListener(new GenericTextWatcher(et_five));
        et_six.addTextChangedListener(new GenericTextWatcher(et_six));

        et_six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.setVisibility(View.GONE);
            }
        });

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        iv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_first.setText("");
                et_second.setText("");
                et_third.setText("");
                et_four.setText("");
                et_five.setText("");
                et_six.setText("");
                et_six.clearFocus();
                et_first.requestFocus();
            }
        });

    }



    boolean checkAppPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(FlagPermissionFlow){
            if (requestCode == 102 && FlagPermissionFlow && Count <=5) {
                if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                    FlagPermissionFlow=false;
                }else{
                    Count++;
                    ActivityCompat.requestPermissions(this, AppPermissions, 102);
                }
            }
        }
    }



    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch(view.getId())
            {

                case R.id.et_first:
                    if(text.length()==1)
                        et_first.clearFocus();
                    et_second.requestFocus();
                    et_first.setSelection(text.length());
                    et_first.setCursorVisible(false);
                    break;
                case R.id.et_second:
                    if(text.length()==1){
                        et_second.clearFocus();
                        et_third.requestFocus();
                        et_second.setSelection(text.length());
                        et_second.setCursorVisible(false);
                    }else if(text.length()==0){
                        et_second.clearFocus();
                        et_first.requestFocus();
                        et_first.setCursorVisible(true);

                    }

                    break;
                case R.id.et_third:
                    if(text.length()==1){
                        et_third.clearFocus();
                        et_four.requestFocus();
                        et_third.setSelection(text.length());
                        et_third.setCursorVisible(false);
                    }else if(text.length()==0){
                        et_third.clearFocus();
                        et_second.requestFocus();
                        et_second.setCursorVisible(true);
                    }
                    break;
                case R.id.et_four:
                    if(text.length()==1){
                        et_four.clearFocus();
                        et_five.requestFocus();
                        et_four.setSelection(text.length());
                        et_four.setCursorVisible(false);
                    }else if(text.length()==0){
                        et_four.clearFocus();
                        et_third.requestFocus();
                        et_third.setCursorVisible(true);
                    }
                    break;
                case R.id.et_five:
                    if(text.length()==1){
                        et_five.clearFocus();
                        et_six.requestFocus();
                        et_five.setSelection(text.length());
                        et_five.setCursorVisible(false);
                    }else if(text.length()==0){
                        et_five.clearFocus();
                        et_four.requestFocus();
                        et_four.setCursorVisible(true);
                    }
                    break;
                case R.id.et_six:
                    if(text.length()==1){
                        progress.setVisibility(View.GONE);
                        btn_verify_OTP.setVisibility(View.VISIBLE);
                        et_six.setSelection(text.length());
                        et_six.clearFocus();
                    }else if(text.length()==0){
                        et_six.clearFocus();
                        et_five.requestFocus();
                        et_five.setCursorVisible(true);
                    }
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }



    private void setUI() {

        btn_verify_OTP.setOnClickListener(this);
        btn_send_OTP.setOnClickListener(this);
        tv_edit_mobile.setOnClickListener(this);
        tv_resend_OTP.setOnClickListener(this);
    }

    private void setTimer(){
        try{
            new CountDownTimer(30* 1000+1000, 1000) {

                public void onTick(long millisUntilFinished) {
                    int seconds = (int) (millisUntilFinished / 1000);
                    int minutes = seconds / 60;
                    seconds = seconds % 60;
                    // tv_timer.setText("Time remaining: " + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                    tv_timer.setText("Time remaining: " + String.format("%02d", seconds)+" sec");
                }
                public void onFinish() {
                    tv_timer.setVisibility(View.GONE);
                    tv_resend_OTP.setVisibility(View.VISIBLE);
                }
            }.start();
        }catch (Exception e){
            tv_timer.setVisibility(View.GONE);
            tv_resend_OTP.setVisibility(View.VISIBLE);
        }
    }



    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {

            case R.id.btn_send_OTP:

                if(fetMobile.getText().length()>=8){
                    if (gpsTracker.isNetworkAvailable()) {
                        sendOTP();
                    } else {
                        showSnackBar(ActivityOTPVerified.this, "Please check your internet connection .");
                    }

                }else{
                    showSnackBar(ActivityOTPVerified.this, "Please enter valid mobile number");
                }
                break;

            case R.id.tv_edit_mobile:

                ll_verify_OTP.setVisibility(View.GONE);
                ll_send_OTP.setVisibility(View.GONE);
                et_first.setText("");
                et_second.setText("");
                et_third.setText("");
                et_four.setText("");
                et_five.setText("");
                et_six.setText("");
                et_six.clearFocus();
                et_first.requestFocus();
                btn_verify_OTP.setVisibility(View.GONE);

                break;

            case R.id.btn_verify_OTP:

                if(et_first.getText().length()==1 && et_second.getText().length()==1 && et_third.getText().length()==1 && et_four.getText().length()==1 && et_five.getText().length()==1 && et_six.getText().length()==1){
                    String EnterOTPValue = et_first.getText().toString()+et_second.getText().toString()+et_third.getText().toString()+et_four.getText().toString()+et_five.getText().toString()+et_six.getText().toString();
                    if(OTPValue.equalsIgnoreCase(EnterOTPValue.replace(" ",""))){
                        sessionManager.setLogedIn(true);
                        Intent intents = new Intent(ActivityOTPVerified.this,ActivityHome.class);
                        startActivity(intents);
                        finish();

                    }else{
                        showSnackBar(ActivityOTPVerified.this, "Invalid OTP");
                    }
                }else{
                    showSnackBar(ActivityOTPVerified.this, "Please enter OTP");
                }

                break;

            case R.id.tv_resend_OTP:

                et_first.setText("");
                et_second.setText("");
                et_third.setText("");
                et_four.setText("");
                et_five.setText("");
                et_six.setText("");
                et_six.clearFocus();
                et_first.requestFocus();
                btn_verify_OTP.setVisibility(View.GONE);

                if (gpsTracker.isNetworkAvailable()) {
                    sendOTP();
                } else {
                    showSnackBar(ActivityOTPVerified.this, "Please check your internet connection .");
                }

                break;
        }
    }



    public void sendOTP() {
        customResponseDialog.showCustomDialog();
        btn_verify_OTP.setVisibility(View.GONE);
        tv_timer.setVisibility(View.VISIBLE);
        tv_resend_OTP.setVisibility(View.GONE);

        final String url = sessionManager.getBaseUrlGuiddoo() + "guiddoocn/sendOTPLogin";
        Log.e("send OTP URL--->",url);

        JSONObject object = new JSONObject();
        try {
            object.put("ContactNo", fetMobile.getText().toString());
            object.put("CountryCode",ccp.getSelectedCountryCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("", "send OTP Parameters---->" + object.toString());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonString = response.toString();
                            Log.e("send OTP Responce--->",jsonString);
                            customResponseDialog.hideCustomeDialog();

                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(jsonString);
                                JSONObject object = jsonObject.getJSONObject("status");
                                if(object.getString("status_code").equals("200")){
                                    OTPValue = jsonObject.getString("OTP");
                                    ll_send_OTP.setVisibility(View.GONE);
                                    ll_verify_OTP.setVisibility(View.VISIBLE);
                                    progress.setVisibility(View.VISIBLE);
                                    otp_mobile.setText("Verify " +ccp.getSelectedCountryCode()+fetMobile.getText().toString());
                                    tv_timer.setVisibility(View.VISIBLE);
                                    tv_resend_OTP.setVisibility(View.GONE);
                                        sessionManager.setEmailId(jsonObject.getString("email"));
                                        sessionManager.setUserMobile(jsonObject.getString("mobile"));
                                        sessionManager.setUserID(jsonObject.getString("TravelAgent_Id"));
                                        sessionManager.setUserName(jsonObject.getString("Company_name"));
                                        sessionManager.setContactPerson(jsonObject.getString("Agent_name"));
                                        setTimer();
                                    showSnackBar(ActivityOTPVerified.this, object.getString("error_message"));
                                }else{
                                    showSnackBar(ActivityOTPVerified.this, object.getString("error_message"));
                                    progress.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }catch (Exception e){
                            showSnackBar(ActivityOTPVerified.this, "Something went wrong.");
                            progress.setVisibility(View.VISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customResponseDialog.hideCustomeDialog();
                        showSnackBar(ActivityOTPVerified.this, "Something went wrong.");
                        progress.setVisibility(View.VISIBLE);
                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(getRequest);

    }

    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 3000).show();
    }
}
