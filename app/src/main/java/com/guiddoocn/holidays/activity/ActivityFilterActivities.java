package com.guiddoocn.holidays.activity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelActivitiesList;
import com.guiddoocn.holidays.models.ModelMainCategoryFilter;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityFilterActivities extends AppCompatActivity {

    static final String FAMILY = "Family";
    static final String ADVENTURE = "Adventure";
    static final String KIDS = "Kids";
    static final String ROMANTIC = "Romantic";
    static final String CULTURE = "Culture & Arts";
    static final String LUXARY = "Luxury";
    static final String JAINFOOD = "Jain";
    static final String VEG = "Vegetarian";
    static final String NONVEG = "Non-vegetarian";
    static final String HALAL = "Halal";
    static final String WESTERN = "Western";
    static final String WITHOUTDINING = "Without Dining";
    static final String EXCURSION = "Excursion";
    static final String WITHTRANSFERS = "WithTransfers";


    static final String FAMILYID = "1";
    static final String ADVENTUREID = "2";
    static final String KIDSID = "3";
    static final String ROMANTICID = "4";
    static final String CULTUREID = "5";
    static final String LUXARYID = "6";

    static final String JAINFOODID = "1";
    static final String VEGID = "2";
    static final String NONVEGID = "3";
    static final String HALALID = "4";
    static final String WESTERNID = "5";
    static final String WITHOUTDININGID = "6";


    private static final String TAG = "ActivityFilter";

    @BindView(R.id.iv_back)
    ImageView tv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    //Populary
    @BindView(R.id.llPopularity)
    LinearLayout llPopularity;
    @BindView(R.id.ivPopularity)
    ImageView ivPopularity;
    @BindView(R.id.tvPopularity)
    TextView tvPopularity;
    // Price High to Low
    @BindView(R.id.llPriceLow)
    LinearLayout llPriceLow;
    @BindView(R.id.ivPriceLow)
    ImageView ivPriceLow;
    @BindView(R.id.tvPriceLow)
    TextView tvPriceLow;

    //Price Low to High
    @BindView(R.id.ll_priceHigh)
    LinearLayout ll_priceHigh;
    @BindView(R.id.iv_priceHigh)
    ImageView iv_priceHigh;
    @BindView(R.id.tv_PriceHigh)
    TextView tv_PriceHigh;
    @BindView(R.id.tv_Reset_all)
    TextView tv_Reset_all;
    @BindView(R.id.rangeSeekbar)
    CrystalRangeSeekbar rangeSeekbar;
    @BindView(R.id.textMin)
    TextView textMin;
    @BindView(R.id.textMax)
    TextView textMax;
    @BindView(R.id.tv_Apply_Filter)
    TextView tv_Apply_Filter;
    @BindView(R.id.ll_withtransfer)
    LinearLayout ll_withtransfer;
    @BindView(R.id.iv_withtransfer)
    ImageView iv_withtransfer;
    @BindView(R.id.ll_excursiononly)
    LinearLayout ll_excursiononly;
    @BindView(R.id.iv_excursiononly)
    ImageView iv_excursiononly;
    @BindView(R.id.ll_nonveg)
    LinearLayout ll_nonveg;
    @BindView(R.id.iv_nonveg)
    ImageView iv_nonveg;
    @BindView(R.id.ll_halal)
    LinearLayout ll_halal;
    @BindView(R.id.iv_halal)
    ImageView iv_halal;
    @BindView(R.id.ll_western)
    LinearLayout ll_western;
    @BindView(R.id.iv_western)
    ImageView iv_western;
    @BindView(R.id.ll_dining)
    LinearLayout ll_dining;
    @BindView(R.id.iv_dining)
    ImageView iv_dining;
    @BindView(R.id.ll_veg)
    LinearLayout ll_veg;
    @BindView(R.id.iv_veg)
    ImageView iv_veg;
    @BindView(R.id.ll_jainfood)
    LinearLayout ll_jainfood;
    @BindView(R.id.iv_jainfood)
    ImageView iv_jainfood;
    @BindView(R.id.ll_luxury)
    LinearLayout ll_luxury;
    @BindView(R.id.iv_luxury)
    ImageView iv_luxury;
    @BindView(R.id.ll_culturearts)
    LinearLayout ll_culturearts;
    @BindView(R.id.iv_culturearts)
    ImageView iv_culturearts;
    @BindView(R.id.ll_romantic)
    LinearLayout ll_romantic;
    @BindView(R.id.iv_romantic)
    ImageView iv_romantic;
    @BindView(R.id.ll_kids)
    LinearLayout ll_kids;
    @BindView(R.id.iv_kids)
    ImageView iv_kids;
    @BindView(R.id.ll_adventure)
    LinearLayout ll_adventure;
    @BindView(R.id.iv_adventure)
    ImageView iv_adventure;
    @BindView(R.id.ll_family)
    LinearLayout ll_family;
    @BindView(R.id.iv_family)
    ImageView iv_family;
    @BindView(R.id.tv_family)
    TextView tv_family;
    @BindView(R.id.tv_adventure)
    TextView tv_adventure;
    @BindView(R.id.tv_kids)
    TextView tv_kids;
    @BindView(R.id.tv_romantic)
    TextView tv_romantic;
    @BindView(R.id.tv_culture)
    TextView tv_culture;
    @BindView(R.id.tv_luxary)
    TextView tv_luxary;
    @BindView(R.id.tv_jain)
    TextView tv_jain;
    @BindView(R.id.tv_veg)
    TextView tv_veg;
    @BindView(R.id.tv_nonveg)
    TextView tv_nonveg;
    @BindView(R.id.tv_halal)
    TextView tv_halal;
    @BindView(R.id.tv_western)
    TextView tv_western;
    @BindView(R.id.tv_dining)
    TextView tv_dining;
    @BindView(R.id.tv_excursion)
    TextView tv_excursion;
    @BindView(R.id.tv_withTransfer)
    TextView tv_withTransfer;

    @BindView(R.id.rangeSeekbarRatings)
    CrystalRangeSeekbar rangeSeekbarRatings;
    @BindView(R.id.textMinRating)
    TextView textMinRating;
    @BindView(R.id.textMaxRating)
    TextView textMaxRating;

    private Context mContext;
    private SessionManager sessionManager;
    private SQLiteHandler db;
    private ArrayList<ModelActivitiesList> ticketListModels;
    private boolean flag_Popularity, flag_high_to_Low, flag_low_to_high;
    private boolean flagwithtransfer = false, flagexcursiononly = false, flagnonveg = false, flaghalal = false, flagwestern = false, flagdining = false, flagveg = false,
            flagjainfood = false, flagluxury = false, flagculturearts = false, flagromantic = false, flagkids = false, flagadventure = false, flagfamily = false;
    private ModelMainCategoryFilter modelMainCategoryFilter = new ModelMainCategoryFilter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_activities);
        mContext = this;
        ButterKnife.bind(this);
        sessionManager = new SessionManager(mContext);
        db = new SQLiteHandler(mContext);

        tv_title.setText(R.string.filter_title);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_Reset_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterUnselectAll();
                sessionManager.setFilteractive(false);
                sessionManager.setTransferFilter("");
            }
        });

        tv_Apply_Filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setFilteractive(true);
                ApplyFilter();
                finish();
            }
        });

        SetAmountSeekBar();
        SetStarSeekBar();


        CheckFilter();
    }

    private void CheckFilter() {

        if (sessionManager.getActSortingPosition().equalsIgnoreCase("1")) {
            ivPopularity.setColorFilter(getResources().getColor(R.color.colorButton));
            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Popularity = true;

        } else if (sessionManager.getActSortingPosition().equalsIgnoreCase("2")) {
            ivPriceLow.setColorFilter(getResources().getColor(R.color.colorButton));
            tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_high_to_Low = true;
        } else if (sessionManager.getActSortingPosition().equalsIgnoreCase("3")) {
            iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorButton));
            tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_low_to_high = true;
        }

        if (modelMainCategoryFilter.filters.contains(FAMILY)) {
            tv_family.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_family.setColorFilter(getResources().getColor(R.color.colorButton));
            flagfamily=true;
        }

        if (modelMainCategoryFilter.filters.contains(ADVENTURE)) {
            tv_adventure.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_adventure.setColorFilter(getResources().getColor(R.color.colorButton));
            flagadventure=true;
        }
        if (modelMainCategoryFilter.filters.contains(KIDS)) {
            tv_kids.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_kids.setColorFilter(getResources().getColor(R.color.colorButton));
            flagkids=true;
        }

        if (modelMainCategoryFilter.filters.contains(ROMANTIC)) {
            tv_romantic.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_romantic.setColorFilter(getResources().getColor(R.color.colorButton));
            flagromantic=true;
        }

        if (modelMainCategoryFilter.filters.contains(CULTURE)) {
            tv_culture.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_culturearts.setColorFilter(getResources().getColor(R.color.colorButton));
            flagculturearts=true;
        }

        if (modelMainCategoryFilter.filters.contains(LUXARY)) {
            tv_luxary.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_luxury.setColorFilter(getResources().getColor(R.color.colorButton));
            flagluxury=true;
        }

        if (modelMainCategoryFilter.filtersDining.contains(JAINFOOD)) {
            tv_jain.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_jainfood.setColorFilter(getResources().getColor(R.color.colorButton));
            flagjainfood=true;
        }

        if (modelMainCategoryFilter.filtersDining.contains(VEG)) {
            tv_veg.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_veg.setColorFilter(getResources().getColor(R.color.colorButton));
            flagveg=true;
        }

        if (modelMainCategoryFilter.filtersDining.contains(NONVEG)) {
            tv_nonveg.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_nonveg.setColorFilter(getResources().getColor(R.color.colorButton));
            flagnonveg=true;
        }

        if (modelMainCategoryFilter.filtersDining.contains(HALAL)) {
            tv_halal.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_halal.setColorFilter(getResources().getColor(R.color.colorButton));
            flaghalal=true;
        }

        if (modelMainCategoryFilter.filtersDining.contains(WESTERN)) {
            tv_western.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_western.setColorFilter(getResources().getColor(R.color.colorButton));
            flagwestern=true;
        }

        if (modelMainCategoryFilter.filtersDining.contains(WITHOUTDINING)) {
            tv_dining.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_dining.setColorFilter(getResources().getColor(R.color.colorButton));
            flagdining=true;
        }

        if (sessionManager.getTransferFilter().equalsIgnoreCase(EXCURSION)) {
            tv_excursion.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_excursiononly.setColorFilter(getResources().getColor(R.color.colorButton));
            flagexcursiononly=true;
            flagwithtransfer=false;
        } else if (sessionManager.getTransferFilter().equalsIgnoreCase(WITHTRANSFERS)) {
            tv_withTransfer.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            iv_withtransfer.setColorFilter(getResources().getColor(R.color.colorButton));
            flagwithtransfer=true;
            flagexcursiononly=false;
        }

    }

    private void FilterUnselectAll() {

        // ----------- Sorting --------------
        flag_Popularity = false;
        flag_high_to_Low = false;
        flag_low_to_high = false;
        sessionManager.setActSortingPosition("0");
        sessionManager.setActDiningFilter("()");
        sessionManager.setActCatFilter("()");

        ivPopularity.setColorFilter(getResources().getColor(R.color.colorBlack));
        tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        ivPriceLow.setColorFilter(getResources().getColor(R.color.colorBlack));
        tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorBlack));
        tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

        flagwithtransfer = false; flagexcursiononly = false; flagnonveg = false; flaghalal = false; flagwestern = false; flagdining = false; flagveg = false;
        flagjainfood = false; flagluxury = false; flagculturearts = false; flagromantic = false; flagkids = false; flagadventure = false; flagfamily = false;

        tv_family.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_family.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilter(FAMILY);
        modelMainCategoryFilter.removeFilterID(FAMILYID);

        tv_adventure.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_adventure.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilter(ADVENTURE);
        modelMainCategoryFilter.removeFilterID(ADVENTUREID);

        tv_kids.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_kids.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilter(KIDS);
        modelMainCategoryFilter.removeFilterID(KIDSID);

        tv_romantic.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_romantic.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilter(ROMANTIC);
        modelMainCategoryFilter.removeFilterID(ROMANTICID);

        tv_culture.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_culturearts.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilter(CULTURE);
        modelMainCategoryFilter.removeFilterID(CULTUREID);

        tv_luxary.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_luxury.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilter(LUXARY);
        modelMainCategoryFilter.removeFilterID(LUXARYID);

        tv_jain.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_jainfood.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilterDining(JAINFOOD);
        modelMainCategoryFilter.removeFilterDiningID(JAINFOODID);

        tv_veg.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_veg.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilterDining(VEG);
        modelMainCategoryFilter.removeFilterDiningID(VEGID);

        tv_nonveg.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_nonveg.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilterDining(NONVEG);
        modelMainCategoryFilter.removeFilterDiningID(NONVEGID);

        tv_halal.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_halal.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilterDining(HALAL);
        modelMainCategoryFilter.removeFilterDiningID(HALALID);

        tv_western.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_western.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilterDining(WESTERN);
        modelMainCategoryFilter.removeFilterDiningID(WESTERNID);

        tv_dining.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_dining.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilterDining(WITHOUTDINING);
        modelMainCategoryFilter.removeFilterDiningID(WITHOUTDININGID);

        tv_excursion.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_excursiononly.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilter(EXCURSION);

        tv_withTransfer.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        iv_withtransfer.setColorFilter(getResources().getColor(R.color.colorBlack));
        modelMainCategoryFilter.removeFilter(WITHTRANSFERS);

        try {
            try {
                int min = (int) Double.parseDouble(db.getMinValueAct(sessionManager.getCountryCode()));
                int max = (int) Double.parseDouble(db.getMaxValueAct(sessionManager.getCountryCode()));
                sessionManager.setFilterMinAmtAct(String.valueOf(min));
                sessionManager.setFilterMaxAmtAct(String.valueOf(max));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            try {
                int min = 0;
                int max =5;
                sessionManager.setMinStarFilterAct(String.valueOf(min));
                sessionManager.setMaxStarFilterAct(String.valueOf(max));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        SetAmountSeekBar();
        SetStarSeekBar();

    }

    private void ApplyFilter() {

        if (flag_Popularity) {
            sessionManager.setActSortingPosition("1");
        } else if (flag_high_to_Low) {
            sessionManager.setActSortingPosition("2");
        } else if (flag_low_to_high) {
            sessionManager.setActSortingPosition("3");
        }

        if (!flagfamily) {
            modelMainCategoryFilter.removeFilter(FAMILY);
            modelMainCategoryFilter.removeFilterID(FAMILYID);
        } else {
            modelMainCategoryFilter.addFilter(FAMILY);
            modelMainCategoryFilter.addFilterID(FAMILYID);
        }

        if (!flagadventure) {
            modelMainCategoryFilter.removeFilter(ADVENTURE);
            modelMainCategoryFilter.removeFilterID(ADVENTUREID);
        } else {
            modelMainCategoryFilter.addFilter(ADVENTURE);
            modelMainCategoryFilter.addFilterID(ADVENTUREID);
        }

        if (!flagkids) {
            modelMainCategoryFilter.removeFilter(KIDS);
            modelMainCategoryFilter.removeFilterID(KIDSID);
        } else {
            modelMainCategoryFilter.addFilter(KIDS);
            modelMainCategoryFilter.addFilterID(KIDSID);
        }

        if (!flagromantic) {
            modelMainCategoryFilter.removeFilter(ROMANTIC);
            modelMainCategoryFilter.removeFilterID(ROMANTICID);
        } else {
            modelMainCategoryFilter.addFilter(ROMANTIC);
            modelMainCategoryFilter.addFilterID(ROMANTICID);
        }

        if (!flagculturearts) {
            modelMainCategoryFilter.removeFilter(CULTURE);
            modelMainCategoryFilter.removeFilterID(CULTUREID);
        } else {
            modelMainCategoryFilter.addFilter(CULTURE);
            modelMainCategoryFilter.addFilterID(CULTUREID);
        }

        if (!flagluxury) {
            modelMainCategoryFilter.removeFilter(LUXARY);
            modelMainCategoryFilter.removeFilterID(LUXARYID);
        } else {
            modelMainCategoryFilter.addFilter(LUXARY);
            modelMainCategoryFilter.addFilterID(LUXARYID);
        }

        if (!flagjainfood) {
            modelMainCategoryFilter.removeFilterDining(JAINFOOD);
            modelMainCategoryFilter.removeFilterDiningID(JAINFOODID);
        } else {
            modelMainCategoryFilter.addFilterDining(JAINFOOD);
            modelMainCategoryFilter.addFilterDiningID(JAINFOODID);
        }

        if (!flagveg) {
            modelMainCategoryFilter.removeFilterDining(VEG);
            modelMainCategoryFilter.removeFilterDiningID(VEGID);

        } else {
            modelMainCategoryFilter.addFilterDining(VEG);
            modelMainCategoryFilter.addFilterDiningID(VEGID);
        }

        if (!flagnonveg) {
            modelMainCategoryFilter.removeFilterDining(NONVEG);
            modelMainCategoryFilter.removeFilterDiningID(NONVEGID);
        } else {
            modelMainCategoryFilter.addFilterDining(NONVEG);
            modelMainCategoryFilter.addFilterDiningID(NONVEGID);
        }

        if (!flaghalal) {
            modelMainCategoryFilter.removeFilterDining(HALAL);
            modelMainCategoryFilter.removeFilterDiningID(HALALID);
        } else {
            modelMainCategoryFilter.addFilterDining(HALAL);
            modelMainCategoryFilter.addFilterDiningID(HALALID);
        }

        if (!flagwestern) {
            modelMainCategoryFilter.removeFilterDining(WESTERN);
            modelMainCategoryFilter.removeFilterDiningID(WESTERNID);
        } else {
            modelMainCategoryFilter.addFilterDining(WESTERN);
            modelMainCategoryFilter.addFilterDiningID(WESTERNID);
        }

        if (!flagdining) {
            modelMainCategoryFilter.removeFilterDining(WITHOUTDINING);
            modelMainCategoryFilter.removeFilterDiningID(WITHOUTDININGID);
        } else {
            modelMainCategoryFilter.addFilterDining(WITHOUTDINING);
            modelMainCategoryFilter.addFilterDiningID(WITHOUTDININGID);
        }

        /*if (!flagexcursiononly) {
            modelMainCategoryFilter.removeFilter(EXCURSION);
            // modelMainCategoryFilter.removeFilterID(EXCURSIONID);
        } else {
            modelMainCategoryFilter.addFilter(EXCURSION);
            // modelMainCategoryFilter.addFilterID(EXCURSIONID);
        }

        if (!flagwithtransfer) {
            modelMainCategoryFilter.removeFilter(WITHTRANSFERS);
            //  modelMainCategoryFilter.removeFilterID(WITHTRANSFERSID);
        } else {
            modelMainCategoryFilter.addFilter(WITHTRANSFERS);
            // modelMainCategoryFilter.addFilterID(WITHTRANSFERSID);
        }*/

        try{
            String s = TextUtils.join(",", ModelMainCategoryFilter.filtersID);
            Log.e("Category Filter--->","("+s+")");
            sessionManager.setActCatFilter("("+s+")");
        }catch (Exception e){

        }

        try{
            String s = TextUtils.join(",", ModelMainCategoryFilter.filtersDiningID);
            Log.e("Dining Filter--->","("+s+")");
            sessionManager.setActDiningFilter("("+s+")");
        }catch (Exception e){

        }


    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.llPopularity:
                ivPopularity.setColorFilter(getResources().getColor(R.color.colorButton));
                tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                ivPriceLow.setColorFilter(getResources().getColor(R.color.colorBlack));
                tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorBlack));
                tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                flag_Popularity = true;
                flag_high_to_Low = false;
                flag_low_to_high = false;
                break;

            case R.id.llPriceLow:
                ivPopularity.setColorFilter(getResources().getColor(R.color.colorBlack));
                tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                ivPriceLow.setColorFilter(getResources().getColor(R.color.colorButton));
                tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorBlack));
                tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                flag_Popularity = false;
                flag_high_to_Low = true;
                flag_low_to_high = false;
                break;

            case R.id.ll_priceHigh:
                ivPopularity.setColorFilter(getResources().getColor(R.color.colorBlack));
                tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                ivPriceLow.setColorFilter(getResources().getColor(R.color.colorBlack));
                tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorButton));
                tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                flag_Popularity = false;
                flag_high_to_Low = false;
                flag_low_to_high = true;
                break;

            case R.id.iv_family:
            case R.id.tv_family:

                if (flagfamily) {
                    tv_family.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_family.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagfamily = false;
                } else {
                    tv_family.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_family.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagfamily = true;
                }
                break;
            case R.id.iv_adventure:
            case R.id.tv_adventure:
                if (flagadventure) {
                    tv_adventure.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_adventure.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagadventure = false;
                } else {
                    tv_adventure.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_adventure.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagadventure = true;
                }
                break;

            case R.id.iv_kids:
            case R.id.tv_kids:
                if (flagkids) {
                    tv_kids.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_kids.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagkids = false;
                } else {
                    tv_kids.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_kids.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagkids = true;
                }
                break;

            case R.id.iv_romantic:
            case R.id.tv_romantic:
                if (flagromantic) {
                    tv_romantic.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_romantic.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagromantic = false;
                } else {
                    tv_romantic.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_romantic.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagromantic = true;
                }
                break;

            case R.id.iv_culturearts:
            case R.id.tv_culture:
                if (flagculturearts) {
                    tv_culture.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_culturearts.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagculturearts = false;
                } else {
                    tv_culture.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_culturearts.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagculturearts = true;
                }
                break;

            case R.id.iv_luxury:
            case R.id.tv_luxary:
                if (flagluxury) {
                    tv_luxary.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_luxury.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagluxury = false;
                } else {
                    tv_luxary.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_luxury.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagluxury = true;
                }
                break;

            case R.id.iv_jainfood:
            case R.id.tv_jain:
                if (flagjainfood) {
                    tv_jain.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_jainfood.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagjainfood = false;
                } else {
                    tv_jain.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_jainfood.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagjainfood = true;
                }
                break;

            case R.id.iv_veg:
            case R.id.tv_veg:
                if (flagveg) {
                    tv_veg.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_veg.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagveg = false;
                } else {
                    tv_veg.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_veg.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagveg = true;
                }
                break;

            case R.id.iv_nonveg:
            case R.id.tv_nonveg:
                if (flagnonveg) {
                    tv_nonveg.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_nonveg.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagnonveg = false;
                } else {
                    tv_nonveg.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_nonveg.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagnonveg = true;
                }
                break;

            case R.id.iv_halal:
            case R.id.tv_halal:
                if (flaghalal) {
                    tv_halal.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_halal.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flaghalal = false;
                } else {
                    tv_halal.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_halal.setColorFilter(getResources().getColor(R.color.colorButton));
                    flaghalal = true;
                }
                break;

            case R.id.iv_western:
            case R.id.tv_western:
                if (flagwestern) {
                    tv_western.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_western.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagwestern = false;
                } else {
                    tv_western.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_western.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagwestern = true;
                }
                break;

            case R.id.iv_dining:
            case R.id.tv_dining:

                if (flagdining) {
                    tv_dining.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_dining.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagdining = false;
                } else {
                    tv_dining.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_dining.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagdining = true;
                }
                break;

            case R.id.iv_excursiononly:
            case R.id.tv_excursion:
                if (flagexcursiononly) {
                    tv_excursion.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_excursiononly.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagexcursiononly = false;
                    sessionManager.setTransferFilter("");
                } else {
                    tv_excursion.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_excursiononly.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagexcursiononly = true;
                    sessionManager.setTransferFilter(EXCURSION);

                    tv_withTransfer.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_withtransfer.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagwithtransfer = false;
                }
                break;

            case R.id.iv_withtransfer:
            case R.id.tv_withTransfer:
                if (flagwithtransfer) {
                    tv_withTransfer.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_withtransfer.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagwithtransfer = false;
                    sessionManager.setTransferFilter("");
                } else {
                    tv_withTransfer.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    iv_withtransfer.setColorFilter(getResources().getColor(R.color.colorButton));
                    flagwithtransfer = true;
                    sessionManager.setTransferFilter(WITHTRANSFERS);

                    tv_excursion.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    iv_excursiononly.setColorFilter(getResources().getColor(R.color.colorBlack));
                    flagexcursiononly = false;
                }
                break;
        }
    }

    public void SetAmountSeekBar(){
        try{

            try {
                try {
                    int min = (int) Double.parseDouble(db.getMinValueAct(sessionManager.getCountryCode()));
                    Log.e("MIN", "Value" + min);
                    rangeSeekbar.setMinValue(min);
                    int max = (int) Double.parseDouble(db.getMaxValueAct(sessionManager.getCountryCode()));
                    rangeSeekbar.setMaxValue(max);
                    Log.e("max", "Value" + max);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            rangeSeekbar.setMinStartValue(Float.parseFloat(sessionManager.getFilterMinAmtAct())).setMaxStartValue(Float.parseFloat(sessionManager.getFilterMaxAmtAct())).apply();

            //rangeSeekbar.setMinimumHeight(1);

            rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
                @Override
                public void valueChanged(Number minValue, Number maxValue) {
                    textMin.setText("USD " + String.valueOf(minValue));
                    textMax.setText("USD " + String.valueOf(maxValue));
                    sessionManager.setFilterMinAmtAct(String.valueOf(minValue));
                    sessionManager.setFilterMaxAmtAct(String.valueOf(maxValue));
                }
            });

            rangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
                @Override
                public void finalValue(Number minValue, Number maxValue) {
                    Log.e("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));

                }
            });

        }catch (Exception e){

        }


    }

    public void SetStarSeekBar(){
        try{
            try {
                try {
                    int min = 0;
                    Log.e("MIN", "Value" + min);
                    rangeSeekbarRatings.setMinValue(min);
                    int max =5;
                    rangeSeekbarRatings.setMaxValue(max);
                    Log.e("max", "Value" + max);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            rangeSeekbarRatings.setMinStartValue(Float.parseFloat(sessionManager.getFilterMinStarAct())).setMaxStartValue(Float.parseFloat(sessionManager.getFilterMaxStarAct())).apply();

            //rangeSeekbar.setMinimumHeight(1);

            rangeSeekbarRatings.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
                @Override
                public void valueChanged(Number minValue, Number maxValue) {
                    textMinRating.setText(String.valueOf(minValue)+" Star");
                    textMaxRating.setText(String.valueOf(maxValue)+" Star");
                    sessionManager.setMinStarFilterAct(String.valueOf(minValue));
                    sessionManager.setMaxStarFilterAct(String.valueOf(maxValue));
                }
            });

            rangeSeekbarRatings.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
                @Override
                public void finalValue(Number minValue, Number maxValue) {
                    Log.e("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));

                }
            });
        }catch (Exception e){

        }

    }
}
