package com.guiddoocn.holidays.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityFilter extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    //Populary
    @BindView(R.id.llPopularity)
    LinearLayout llPopularity;
    @BindView(R.id.ivPopularity)
    ImageView ivPopularity;
    @BindView(R.id.tvPopularity)
    TextView tvPopularity;

    // Price High to Low
    @BindView(R.id.llPriceLow)
    LinearLayout llPriceLow;
    @BindView(R.id.ivPriceLow)
    ImageView ivPriceLow;
    @BindView(R.id.tvPriceLow)
    TextView tvPriceLow;

    //Price Low to High
    @BindView(R.id.ll_priceHigh)
    LinearLayout ll_priceHigh;
    @BindView(R.id.iv_priceHigh)
    ImageView iv_priceHigh;
    @BindView(R.id.tv_PriceHigh)
    TextView tv_PriceHigh;

    //Sorting With MultiSelection

    // TV Selection  4  positon all 4 and multiselection True
    @BindView(R.id.llTv)
    LinearLayout ll_Tv;

    @BindView(R.id.ivTv)
    ImageView ivTv;

    @BindView(R.id.tvTv)
    TextView tvTv;

    @BindView(R.id.rangeSeekbar)
    CrystalRangeSeekbar rangeSeekbar;
    @BindView(R.id.textMin)
    TextView textMin;
    @BindView(R.id.textMax)
    TextView textMax;

    //Bath Hub selection 4
    @BindView(R.id.llBathtub)
    LinearLayout llBathtub;
    @BindView(R.id.ivBathtub)
    ImageView ivBathtub;
    @BindView(R.id.tvBathtub)
    TextView tvBathtub;

    // Gym Selection  4
    @BindView(R.id.llGym)
    LinearLayout llGym;
    @BindView(R.id.ivGym)
    ImageView ivGym;
    @BindView(R.id.tvGym)
    TextView tvGym;

    // Car Selection  4

    @BindView(R.id.llCar)
    LinearLayout llCar;
    @BindView(R.id.ivCar)
    ImageView ivCar;
    @BindView(R.id.tvCar)
    TextView tvCar;

    //Currency Selection 4
    @BindView(R.id.llCurrency)
    LinearLayout llCurrency;
    @BindView(R.id.ivCurrency)
    ImageView ivCurrency;
    @BindView(R.id.tvCurrency)
    TextView tvCurrency;

    // Free Shuttle Selection  4
    @BindView(R.id.llFreeShuttle)
    LinearLayout llFreeShuttle;
    @BindView(R.id.ivFreeShuttle)
    ImageView ivFreeShuttle;
    @BindView(R.id.tvFreeShuttle)
    TextView tvFreeShuttle;

    //Swimming Selection  4
    @BindView(R.id.llSwimming)
    LinearLayout llSwimming;
    @BindView(R.id.ivSwimming)
    ImageView ivSwimming;
    @BindView(R.id.tvSwimming)
    TextView tvSwimming;

    //Reset All
    @BindView(R.id.ll_resetAll)
    LinearLayout ll_resetAll;
    //Apply All
    @BindView(R.id.ll_applyAll)
    LinearLayout ll_applyAll;


    // Reting Star
    @BindView(R.id.llStarOne)
    LinearLayout llStarOne;
    @BindView(R.id.ivStarOne)
    ImageView ivStarOne;
    @BindView(R.id.tvStarOne)
    TextView tvStarOne;

    @BindView(R.id.llStarTwo)
    LinearLayout llStarTwo;
    @BindView(R.id.ivStarTwo)
    ImageView ivStarTwo;
    @BindView(R.id.tvStarTwo)
    TextView tvStarTwo;

    @BindView(R.id.llStarThree)
    LinearLayout llStarThree;
    @BindView(R.id.ivStarThree)
    ImageView ivStarThree;
    @BindView(R.id.tvStarThree)
    TextView tvStarThree;

    @BindView(R.id.llStarFour)
    LinearLayout llStarFour;
    @BindView(R.id.ivStarFour)
    ImageView ivStarFour;
    @BindView(R.id.tvStarFour)
    TextView tvStarFour;


    @BindView(R.id.llWifi)
    LinearLayout llWifi;
    @BindView(R.id.iv_wifi)
    ImageView iv_wifi;
    @BindView(R.id.tv_wifi)
    TextView tv_wifi;
    @BindView(R.id.ll_wellness_spa)
    LinearLayout ll_wellness_spa;
    @BindView(R.id.iv_wellness_spa)
    ImageView iv_wellness_spa;
    @BindView(R.id.tv_wellness_spa)
    TextView tv_wellness_spa;
    @BindView(R.id.ll_disco_nightclub)
    LinearLayout ll_disco_nightclub;
    @BindView(R.id.iv_disco_nightclub)
    ImageView iv_disco_nightclub;
    @BindView(R.id.tv_disco_nightclub)
    TextView tv_disco_nightclub;
    @BindView(R.id.ll_room_service)
    LinearLayout ll_room_service;
    @BindView(R.id.iv_service)
    ImageView iv_service;
    @BindView(R.id.tv_service)
    TextView tv_service;
    @BindView(R.id.llStarFive)
    LinearLayout llStarFive;
    @BindView(R.id.ivStarFive)
    ImageView ivStarFive;
    @BindView(R.id.tvStarFive)
    TextView tvStarFive;

    @BindView(R.id.rangeSeekbarRatings)
    CrystalRangeSeekbar rangeSeekbarRatings;
    @BindView(R.id.textMinRating)
    TextView textMinRating;
    @BindView(R.id.textMaxRating)
    TextView textMaxRating;

    Context mContext;
    private SessionManager sessionManager;

    private boolean flag_Popularity, flag_high_to_Low, flag_low_to_high;

    private SQLiteHandler db;
    private boolean flag_tv, flag_Bath, flag_Gym, flag_Car, flag_Currency, flag_Free_Shuttle, flag_Swimming, flag_Wifi, flag_Wellness_Spa, flag_Disco_Night_Club, flag_Room_Service;
    private boolean flag_One, flag_Two, flag_Three, flag_Four, flag_Five;
    private ArrayList<String> aminitiesFilter = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        mContext = this;
        sessionManager = new SessionManager(mContext);
        tv_title.setText(R.string.filter_title);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        db = new SQLiteHandler(mContext);
        CheckFilter();


        SetStarSeekBar();
        SetAmountSeekBar();


        ll_resetAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResetFilter();
            }
        });
        ll_applyAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplyFilter();
            }
        });

    }

    private void CheckFilter() {

        if (sessionManager.getSortingPosition().equalsIgnoreCase("1")) {
            ivPopularity.setColorFilter(getResources().getColor(R.color.colorButton));
            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Popularity = true;

        } else if (sessionManager.getSortingPosition().equalsIgnoreCase("2")) {
            ivPriceLow.setColorFilter(getResources().getColor(R.color.colorButton));
            tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_high_to_Low = true;
        } else if (sessionManager.getSortingPosition().equalsIgnoreCase("3")) {
            iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorButton));
            tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_low_to_high = true;
        }

        if (sessionManager.getTvFilter().equalsIgnoreCase("1")) {
            ivTv.setColorFilter(getResources().getColor(R.color.colorButton));
            tvTv.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_tv = true;
        }

        if (sessionManager.getBathtubFilter().equalsIgnoreCase("2")) {
            ivBathtub.setColorFilter(getResources().getColor(R.color.colorButton));
            tvBathtub.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Bath = true;
        }

        if (sessionManager.getSwimmingFilter().equalsIgnoreCase("3")) {
            ivSwimming.setColorFilter(getResources().getColor(R.color.colorButton));
            tvSwimming.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Swimming = true;
        }

        if (sessionManager.getGymFilter().equalsIgnoreCase("4")) {
            ivGym.setColorFilter(getResources().getColor(R.color.colorButton));
            tvGym.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Gym = true;
        }

        if (sessionManager.getFreeShuttleFilter().equalsIgnoreCase("5")) {
            ivFreeShuttle.setColorFilter(getResources().getColor(R.color.colorButton));
            tvFreeShuttle.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Free_Shuttle = true;
        }

        if (sessionManager.getCurrencyFilter().equalsIgnoreCase("6")) {
            ivCurrency.setColorFilter(getResources().getColor(R.color.colorButton));
            tvCurrency.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Currency = true;
        }

        if (sessionManager.getCarFilter().equalsIgnoreCase("7")) {
            ivCar.setColorFilter(getResources().getColor(R.color.colorButton));
            tvCar.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Car = true;
        }

        if (sessionManager.getWifiFilter().equalsIgnoreCase("8")) {
            iv_wifi.setColorFilter(getResources().getColor(R.color.colorButton));
            tv_wifi.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Wifi = true;
        }
        if (sessionManager.getWellnessSpaFilter().equalsIgnoreCase("9")) {
            iv_wellness_spa.setColorFilter(getResources().getColor(R.color.colorButton));
            tv_wellness_spa.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Wellness_Spa = true;
        }
        if (sessionManager.getDiscoNightClubFilter().equalsIgnoreCase("10")) {
            iv_disco_nightclub.setColorFilter(getResources().getColor(R.color.colorButton));
            tv_disco_nightclub.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Disco_Night_Club = true;
        }
        if (sessionManager.getRoomServiceFilter().equalsIgnoreCase("11")) {
            iv_service.setColorFilter(getResources().getColor(R.color.colorButton));
            tv_service.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Room_Service = true;
        }

        /// --------------- Star  Filter
        if (sessionManager.getOneFilter().equalsIgnoreCase("1")) {
            ivStarOne.setColorFilter(getResources().getColor(R.color.colorButton));
            tvStarOne.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_One = true;
        }
        if (sessionManager.getTwoFilter().equalsIgnoreCase("2")) {
            ivStarTwo.setColorFilter(getResources().getColor(R.color.colorButton));
            tvStarTwo.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Two = true;
        }
        if (sessionManager.getThreeFilter().equalsIgnoreCase("3")) {
            ivStarThree.setColorFilter(getResources().getColor(R.color.colorButton));
            tvStarThree.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Three = true;
        }
        if (sessionManager.getFourFilter().equalsIgnoreCase("4")) {
            ivStarFour.setColorFilter(getResources().getColor(R.color.colorButton));
            tvStarFour.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Four = true;
        }
        if (sessionManager.getFiveFilter().equalsIgnoreCase("5")) {
            ivStarFive.setColorFilter(getResources().getColor(R.color.colorButton));
            tvStarFive.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
            flag_Five = true;
        }
    }

    private void ApplyFilter() {

        sessionManager.setFilterHotel(true);

        if (flag_Popularity) {
            sessionManager.setSorting_Position("1");
        } else if (flag_high_to_Low) {
            sessionManager.setSorting_Position("2");
        } else if (flag_low_to_high) {
            sessionManager.setSorting_Position("3");
        }

        aminitiesFilter = new ArrayList<>();

        if (flag_tv) {
            sessionManager.setTvFilter("1");
            aminitiesFilter.add("1");
        } else {
            sessionManager.setTvFilter("");
        }

        if (flag_Bath) {
            sessionManager.setBathtubFilter("2");
            aminitiesFilter.add("2");
        } else {
            sessionManager.setBathtubFilter("");
        }

        if (flag_Swimming) {
            sessionManager.setSwimmingFilter("3");
            aminitiesFilter.add("3");
        } else {
            sessionManager.setSwimmingFilter("");
        }

        if (flag_Gym) {
            sessionManager.setGymFilter("4");
            aminitiesFilter.add("4");
        } else {
            sessionManager.setGymFilter("");
        }

        if (flag_Free_Shuttle) {
            sessionManager.setFreeShuttleFilter("5");
            aminitiesFilter.add("5");
        } else {
            sessionManager.setFreeShuttleFilter("");
        }

        if (flag_Currency) {
            sessionManager.setCurrencyFilter("6");
            aminitiesFilter.add("6");
        } else {
            sessionManager.setCurrencyFilter("");
        }

        if (flag_Car) {
            sessionManager.setCarFilter("7");
            aminitiesFilter.add("7");
        } else {
            sessionManager.setCarFilter("");
        }

        if (flag_Wifi) {
            sessionManager.setWifiFilter("8");
            aminitiesFilter.add("8");
        } else {
            sessionManager.setWifiFilter("");
        }

        if (flag_Wellness_Spa) {
            sessionManager.setWellnessSpaFilter("9");
            aminitiesFilter.add("9");
        } else {
            sessionManager.setWellnessSpaFilter("");
        }

        if (flag_Disco_Night_Club) {
            sessionManager.setDiscoNightClubFilter("10");
            aminitiesFilter.add("10");
        } else {
            sessionManager.setDiscoNightClubFilter("");
        }

        if (flag_Room_Service) {
            sessionManager.setRoomServiceFilter("11");
            aminitiesFilter.add("11");
        } else {
            sessionManager.setRoomServiceFilter("");
        }

        try{
            String s = TextUtils.join(",", aminitiesFilter);
            Log.e("setAminitiesFilter--->","("+s+")");
            sessionManager.setAminitiesFilter("("+s+")");
        }catch (Exception e){

        }


        if (flag_One) {
            sessionManager.setStarOneFilter(("1"));
        } else {
            sessionManager.setStarOneFilter((""));
        }
        if (flag_Two) {
            sessionManager.setStarTwoFilter(("2"));
        } else {
            sessionManager.setStarTwoFilter((""));
        }
        if (flag_Three) {
            sessionManager.setStarThreeFilter(("3"));
        } else {
            sessionManager.setStarThreeFilter((""));
        }
        if (flag_Four) {
            sessionManager.setStarFourFilter(("4"));
        } else {
            sessionManager.setStarFourFilter((""));
        }
        if (flag_Five) {
            sessionManager.setStarFiveFilter(("5"));
        } else {
            sessionManager.setStarFiveFilter((""));
        }

        finish();
    }

    private void ResetFilter() {
        try {


            sessionManager.setFilterHotel(false);
            sessionManager.setAminitiesFilter("(-)");

            // ----------- Sorting --------------
            ivPopularity.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            ivPriceLow.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorBlack));
            tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            // ---------- Setting Grid eg. TV,CAR, BATH------------
            ivTv.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvTv.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivBathtub.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvBathtub.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivGym.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvGym.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivCar.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvCar.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivCurrency.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvCurrency.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivFreeShuttle.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvFreeShuttle.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivSwimming.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvSwimming.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivStarOne.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvStarOne.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivStarTwo.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvStarTwo.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivStarThree.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvStarThree.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivStarFour.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvStarFour.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            ivStarFive.setColorFilter(getResources().getColor(R.color.colorBlack));
            tvStarFive.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            ///
            iv_wifi.setColorFilter(getResources().getColor(R.color.colorBlack));
            tv_wifi.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            iv_wellness_spa.setColorFilter(getResources().getColor(R.color.colorBlack));
            tv_wellness_spa.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            iv_disco_nightclub.setColorFilter(getResources().getColor(R.color.colorBlack));
            tv_disco_nightclub.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));

            iv_service.setColorFilter(getResources().getColor(R.color.colorBlack));
            tv_service.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));


            //-------Set Blank Object to Reset
            sessionManager.setTvFilter("");
            sessionManager.setBathtubFilter("");
            sessionManager.setGymFilter("");
            sessionManager.setCarFilter("");
            sessionManager.setCurrencyFilter("");
            sessionManager.setFreeShuttleFilter("");
            sessionManager.setSwimmingFilter("");
            sessionManager.setSorting_Position("0");
            sessionManager.setTvFilter("");
            sessionManager.setWifiFilter("");
            sessionManager.setWellnessSpaFilter("");
            sessionManager.setDiscoNightClubFilter("");
            sessionManager.setRoomServiceFilter("");

            sessionManager.setStarOneFilter("");
            sessionManager.setStarTwoFilter("");
            sessionManager.setStarThreeFilter("");
            sessionManager.setStarFourFilter("");
            sessionManager.setStarFiveFilter("");

            // Set Flag if Clear All Option In Sorting
            flag_Popularity = false;
            flag_high_to_Low = false;
            flag_low_to_high = false;

            flag_tv = false;
            flag_Bath = false;
            flag_Gym = false;
            flag_Car = false;
            flag_Currency = false;
            flag_Free_Shuttle = false;
            flag_Swimming = false;
            flag_Wifi = false;
            flag_Wellness_Spa = false;
            flag_Disco_Night_Club = false;
            flag_Room_Service = false;


            flag_One = false;
            flag_Two = false;
            flag_Three = false;
            flag_Four = false;
            flag_Five = false;

            try {
                try {
                    int min = (int) Double.parseDouble(db.getMinValue(sessionManager.getCountryCode()));
                    int max = (int) Double.parseDouble(db.getMaxValue(sessionManager.getCountryCode()));
                    sessionManager.setFilterMinAmt(String.valueOf(min));
                    sessionManager.setFilterMaxAmt(String.valueOf(max));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                try {
                    int min = 0;
                    int max =5;
                    sessionManager.setMinStarFilter(String.valueOf(min));
                    sessionManager.setMaxStarFilter(String.valueOf(max));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            SetStarSeekBar();
            SetAmountSeekBar();
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e("error", "msg" + ex.getMessage());
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.llPopularity:

                ivPopularity.setColorFilter(getResources().getColor(R.color.colorButton));
                tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                ivPriceLow.setColorFilter(getResources().getColor(R.color.colorBlack));
                tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorBlack));
                tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                flag_Popularity = true;
                flag_high_to_Low = false;
                flag_low_to_high = false;
                break;

            case R.id.llPriceLow:

                ivPopularity.setColorFilter(getResources().getColor(R.color.colorBlack));
                tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                ivPriceLow.setColorFilter(getResources().getColor(R.color.colorButton));
                tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorBlack));
                tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                flag_Popularity = false;
                flag_high_to_Low = true;
                flag_low_to_high = false;
                break;

            case R.id.ll_priceHigh:
                Log.e("Clicke", "Here =");
                ivPopularity.setColorFilter(getResources().getColor(R.color.colorBlack));
                tvPopularity.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                ivPriceLow.setColorFilter(getResources().getColor(R.color.colorBlack));
                tvPriceLow.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                iv_priceHigh.setColorFilter(getResources().getColor(R.color.colorButton));
                tv_PriceHigh.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                flag_Popularity = false;
                flag_high_to_Low = false;
                flag_low_to_high = true;
                break;

            case R.id.llTv:
                if (flag_tv) {
                    ivTv.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvTv.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_tv = false;
                } else {
                    ivTv.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvTv.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_tv = true;
                }


                break;
            case R.id.llBathtub:
                if (flag_Bath) {
                    ivBathtub.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvBathtub.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Bath = false;
                } else {
                    ivBathtub.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvBathtub.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Bath = true;
                }

                break;
            case R.id.llGym:
                if (flag_Gym) {
                    ivGym.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvGym.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Gym = false;
                } else {
                    ivGym.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvGym.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Gym = true;
                }


                break;
            case R.id.llCar:
                if (flag_Car) {
                    ivCar.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvCar.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Car = false;
                } else {
                    ivCar.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvCar.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Car = true;
                }
                break;
            case R.id.llCurrency:
                if (flag_Currency) {
                    ivCurrency.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvCurrency.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Currency = false;
                } else {
                    ivCurrency.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvCurrency.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Currency = true;
                }
                break;
            case R.id.llFreeShuttle:
                if (flag_Free_Shuttle) {
                    ivFreeShuttle.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvFreeShuttle.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Free_Shuttle = false;
                } else {
                    ivFreeShuttle.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvFreeShuttle.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Free_Shuttle = true;
                }
                break;
            case R.id.llSwimming:
                if (flag_Swimming) {
                    ivSwimming.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvSwimming.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Swimming = false;
                } else {
                    ivSwimming.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvSwimming.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Swimming = true;
                }
                break;

            case R.id.llWifi:
                if (flag_Wifi) {
                    iv_wifi.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tv_wifi.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Wifi = false;
                } else {
                    iv_wifi.setColorFilter(getResources().getColor(R.color.colorButton));
                    tv_wifi.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Wifi = true;
                }
                break;
            case R.id.ll_wellness_spa:
                if (flag_Wellness_Spa) {
                    iv_wellness_spa.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tv_wellness_spa.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Wellness_Spa = false;
                } else {
                    iv_wellness_spa.setColorFilter(getResources().getColor(R.color.colorButton));
                    tv_wellness_spa.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Wellness_Spa = true;
                }
                break;
            case R.id.ll_disco_nightclub:
                if (flag_Disco_Night_Club) {
                    iv_disco_nightclub.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tv_disco_nightclub.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Disco_Night_Club = false;
                } else {
                    iv_disco_nightclub.setColorFilter(getResources().getColor(R.color.colorButton));
                    tv_disco_nightclub.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Disco_Night_Club = true;
                }
                break;
            case R.id.ll_room_service:
                if (flag_Room_Service) {
                    iv_service.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tv_service.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Room_Service = false;
                } else {
                    iv_service.setColorFilter(getResources().getColor(R.color.colorButton));
                    tv_service.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Room_Service = true;
                }
                break;
            case R.id.llStarOne:
                if (flag_One) {
                    ivStarOne.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvStarOne.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_One = false;
                } else {
                    ivStarOne.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvStarOne.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_One = true;
                }
                break;
            case R.id.llStarTwo:
                if (flag_Two) {
                    ivStarTwo.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvStarTwo.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Two = false;
                } else {
                    ivStarTwo.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvStarTwo.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Two = true;
                }
                break;
            case R.id.llStarThree:
                if (flag_Three) {
                    ivStarThree.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvStarThree.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Three = false;
                } else {
                    ivStarThree.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvStarThree.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Three = true;
                }
                break;
            case R.id.llStarFour:
                if (flag_Four) {
                    ivStarFour.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvStarFour.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Four = false;
                } else {
                    ivStarFour.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvStarFour.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Four = true;
                }
                break;
            case R.id.llStarFive:
                if (flag_Five) {
                    ivStarFive.setColorFilter(getResources().getColor(R.color.colorBlack));
                    tvStarFive.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                    flag_Five = false;
                } else {
                    ivStarFive.setColorFilter(getResources().getColor(R.color.colorButton));
                    tvStarFive.setTextColor(ContextCompat.getColor(mContext, R.color.colorButton));
                    flag_Five = true;
                }
                break;


        }
    }


    public void SetAmountSeekBar(){
        try{

            try {
                try {
                    int min = (int) Double.parseDouble(db.getMinValue(sessionManager.getCountryCode()));
                    Log.e("MIN", "Value" + min);
                    rangeSeekbar.setMinValue(min);
                    int max = (int) Double.parseDouble(db.getMaxValue(sessionManager.getCountryCode()));
                    rangeSeekbar.setMaxValue(max);
                    Log.e("max", "Value" + max);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            rangeSeekbar.setMinStartValue(Float.parseFloat(sessionManager.getFilterMinAmt())).setMaxStartValue(Float.parseFloat(sessionManager.getFilterMaxAmt())).apply();

            //rangeSeekbar.setMinimumHeight(1);

            rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
                @Override
                public void valueChanged(Number minValue, Number maxValue) {
                    textMin.setText("USD " + String.valueOf(minValue));
                    textMax.setText("USD " + String.valueOf(maxValue));
                    sessionManager.setFilterMinAmt(String.valueOf(minValue));
                    sessionManager.setFilterMaxAmt(String.valueOf(maxValue));
                }
            });

            rangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
                @Override
                public void finalValue(Number minValue, Number maxValue) {
                    Log.e("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));

                }
            });

        }catch (Exception e){

        }


    }

    public void SetStarSeekBar(){
        try{
            try {
                try {
                    int min = 0;
                    Log.e("MIN", "Value" + min);
                    rangeSeekbarRatings.setMinValue(min);
                    int max =5;
                    rangeSeekbarRatings.setMaxValue(max);
                    Log.e("max", "Value" + max);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            rangeSeekbarRatings.setMinStartValue(Float.parseFloat(sessionManager.getFilterMinStar())).setMaxStartValue(Float.parseFloat(sessionManager.getFilterMaxStar())).apply();

            //rangeSeekbar.setMinimumHeight(1);

            rangeSeekbarRatings.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
                @Override
                public void valueChanged(Number minValue, Number maxValue) {
                    textMinRating.setText(String.valueOf(minValue)+" Star");
                    textMaxRating.setText(String.valueOf(maxValue)+" Star");
                    sessionManager.setMinStarFilter(String.valueOf(minValue));
                    sessionManager.setMaxStarFilter(String.valueOf(maxValue));
                }
            });

            rangeSeekbarRatings.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
                @Override
                public void finalValue(Number minValue, Number maxValue) {
                    Log.e("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));

                }
            });
        }catch (Exception e){

        }

    }


}
