package com.guiddoocn.holidays.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.ServiceUtility;
import com.guiddoocn.holidays.utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityPackageConfirmation extends AppCompatActivity {
    private static final String TAG = "ActivityPackageConfirmation";

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.tv_dateSelection)
    TextView tv_dateSelection;
    @BindView(R.id.tv_bookingNumber)
    TextView tv_bookingNumber;
    @BindView(R.id.tv_billingName)
    TextView tv_billingName;
    @BindView(R.id.tv_billingEmail)
    TextView tv_billingEmail;
    @BindView(R.id.tv_destination)
    TextView tv_destination;
    @BindView(R.id.tv_referenceNumber)
    TextView tv_referenceNumber;
    @BindView(R.id.tv_packageName)
    TextView tv_packageName;
    @BindView(R.id.tv_Packages_Name)
    TextView tv_Packages_Name;//

    @BindView(R.id.tv_total_Cost)
    TextView tv_total_Cost;//tv_total_Cost

    @BindView(R.id.bt_send)
    Button bt_send;
    int Pacakge_Amount = 0, Package_ID;
    String mPackage_Name, mCurrency, Current_Date, mEmail, mName;
    Context mContext;
    private SessionManager sessionManager;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_confirmation);
        ButterKnife.bind(this);
        mContext = this;
        sessionManager = new SessionManager(mContext);
        Bundle bundleExtra = getIntent().getExtras();
        if (bundleExtra != null) {
            try {

                try {
                    FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
                } catch (NullPointerException e) {

                }


                mCurrency = getIntent().getStringExtra("Currency");//both
                mEmail = getIntent().getStringExtra("Email");//GenerateQuoteDetailsActivity
                mName = getIntent().getStringExtra("Name");//GenerateQuoteDetailsActivity




            } catch (Exception e) {
                Toast.makeText(ActivityPackageConfirmation.this, "Something went wrong. Please try again.", Toast.LENGTH_LONG);
            }

        } else {
            Toast.makeText(ActivityPackageConfirmation.this, "Something went wrong. Please try again.", Toast.LENGTH_LONG);

        }
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_title.setText("Package Confirmation");
       // tv_bookingNumber.setText(String.valueOf(ServiceUtility.randInt(0, 999999)));
        tv_billingName.setText(mName);
        tv_billingEmail.setText(mEmail);
        tv_Packages_Name.setText(getIntent().getStringExtra("itinerary_reference_no"));
        tv_destination.setText(sessionManager.getCountryName());
        tv_referenceNumber.setText(getIntent().getStringExtra("itinerary_reference_no"));
        tv_dateSelection.setText(sessionManager.getCheckInDate()+"-"+sessionManager.getCheckOutDate());
        tv_packageName.setText("Standard Package");
        tv_total_Cost.setText(getIntent().getStringExtra("Currency")+" "+getIntent().getStringExtra("Total_Cost"));


        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityBooking.class);
                startActivity(intent);
                finish();
            }
        });


    }
}
