package com.guiddoocn.holidays.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.GPSTracker;
import com.guiddoocn.holidays.utils.LocaleHelper;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySplash extends AppCompatActivity {

    @BindView(R.id.tv_Version)
    TextView tv_Version;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private String[] AppPermissions = {Manifest.permission.ACCESS_FINE_LOCATION};

    private Context mContext;
    private int SPLASH_TIME_OUT = 2000, count =0;
    private View v;
    private SessionManager sessionManager;
    private LocationManager locationManager;
    private GPSTracker gpsTracker;
    private SQLiteHandler db;
    private boolean FlagPerFlow = false,FlagPermissionFlow = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = this;
        ButterKnife.bind(this);
        v = findViewById(R.id.view);
        sessionManager = new SessionManager(mContext);
        gpsTracker = new GPSTracker(mContext,this);
        db = new SQLiteHandler(mContext);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }

        sessionManager.setBaseUrl("https://prodapi.wayrtoo.com/Wayrtoo_Service.svc/");
        sessionManager.setBaseUrlGuiddoo("http://prodapi.guiddooworld.com/guiddoo_service.svc/");
        sessionManager.setNavigation("");
        sessionManager.setIsmargin("0");



    }

    private void LoadPages(){

        if(sessionManager.getLogedIn()){
            Intent mainIntent = new Intent(ActivitySplash.this, ActivityHome.class);
            mainIntent.putExtra("Flag", "false");
            startActivity(mainIntent);
            finish();
        }else{
            Intent mainIntent = new Intent(ActivitySplash.this, ActivityLogin.class);
            startActivity(mainIntent);
            finish();
        }
    }



    boolean checkAppPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(FlagPermissionFlow){
            if (requestCode == 102 && FlagPermissionFlow) {
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    FlagPermissionFlow=false;
                    Log.e("Allow------>","0000000");
                    //getAllData();
                }else{
                    Log.e("Denai------>","111111");

                }
            }

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(sessionManager.getLanguage().equalsIgnoreCase("Cn")){
            LocaleHelper.persist(mContext,"zh");
            setLocales(LocaleHelper.getLanguage(mContext));
        }else{
            LocaleHelper.persist(mContext,"en");
            setLocales(LocaleHelper.getLanguage(mContext));
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            gpsTracker.showSettingsAlert(ActivitySplash.this);

        } else {
            if (checkAppPermissions(mContext, AppPermissions)) {
                Log.e("Allow------>","0000000");
                getAllData();

            } else if(FlagPermissionFlow){
                ActivityCompat.requestPermissions(this, AppPermissions, 102);
            }
        }

    }

    public void setLocales(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        tv_Version.setText(R.string.version);
    }

    public void getAllData(){
        if (sessionManager.isNetworkAvailable()) {
            getTimeStampRequest();
        } else {
            Snackbar.make(v, "Please check your internet connection.", Snackbar.LENGTH_LONG).setAction("Action", null).show();

            Log.e("size of city list", String.valueOf(db.getCityResponce().size()));

            if (db.getCityResponce().size()>0 && db.getCityResponce()!=null) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LoadPages();
                    }
                }, SPLASH_TIME_OUT);

            } else {
                progressBar.setVisibility(View.GONE);
                final Dialog dialogMsg = new Dialog(ActivitySplash.this);
                dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogMsg.setContentView(R.layout.no_internet_dialog);
                dialogMsg.setCancelable(false);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialogMsg.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.gravity = Gravity.CENTER;
                dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogMsg.getWindow().setAttributes(lp);
                dialogMsg.show();

                TextView btn_text = (TextView) dialogMsg.findViewById(R.id.btn_text);

                btn_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogMsg.cancel();
                        finish();
                    }
                });

            }

        }
    }

    private void getTimeStampRequest() {
        count++;

        RequestQueue queue = Volley.newRequestQueue(this);

        final String url = sessionManager.getBaseUrl() + "utils/gettimestamp?api_key=0D1067102F935B4CC31E082BD45014D469E35268";

        Log.e("get timestamp url--->",url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,


                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response

                        Log.e("TimeStamp Responce-->", response.toString());
                        try {

                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONObject jsonData = jsonObject.getJSONObject("data");
                            JSONObject jsonStatus = jsonData.getJSONObject("status");

                            if (jsonStatus.getString("Success").equalsIgnoreCase("true")) {
                                sessionManager.setNewTimeStamp(jsonData.getString("timestamp"));
                                JSONArray currencies = jsonData.getJSONArray("currencies");

                                if (sessionManager.getOldTimeStamp().equalsIgnoreCase(sessionManager.getNewTimeStamp())) {
                                    Log.e("TimeStamp", "TimeStamp not Updated");
                                    sessionManager.setOldTimeStamp(jsonData.getString("timestamp"));
                                    for (int k = 0; k < currencies.length(); k++) {
                                        JSONObject activityJSON = currencies.getJSONObject(k);
                                        db.updateCurrencyData(activityJSON.getString("id"), activityJSON.getString("usd_value"));
                                    }
                                    //db.deleteDestinationData();
                                    //getCityAPILIST();
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            LoadPages();
                                        }
                                    }, SPLASH_TIME_OUT);
                                } else {
                                    Log.e("TimeStamp", "TimeStamp is Updated");
                                    db.deleteCurrencyData();
                                    db.deleteDestinationData();
                                    db.deleteCalendarData();
                                    db.deletePromoData();

                                    db.deleteHotelsData();   // Delete data base values hotels
                                    db.deleteAmenityData();  // Delete data base values Amenity
                                    db.deleteRoomsData(); // Delete data base values Rooms

                                    for (int k = 0; k < currencies.length(); k++) {
                                        JSONObject activityJSON = currencies.getJSONObject(k);
                                        db.setCurrencyData(activityJSON.getString("id"), activityJSON.getString("usd_value"));
                                    }

                                    getCityAPILIST();
                                }

                            } else {
                                Snackbar.make(v, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }

                        } catch (JSONException e) {
                            Snackbar.make(v, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(count<=3){
                            getTimeStampRequest();
                        }else if(count==2){
                            Snackbar.make(v, "Something went wrong.try again later.", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }else if(count==4){
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Snackbar.make(v, "Something went wrong.please contact with admin.", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();
                                }
                            }, 7000);

                        }


                    }
                }
        );

        queue.add(getRequest);

    }

    private void getCityAPILIST() {

        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = sessionManager.getBaseUrl() + "utils/cities?api_key=0D1067102F935B4CC31E082BD45014D469E35268";
        Log.e("City URL-->", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("City Response-->", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONObject jsonData = jsonObject.getJSONObject("data");
                            JSONObject jsonStatus = jsonData.getJSONObject("status");

                            if (jsonStatus.getString("Success").equalsIgnoreCase("true")) {
                                Log.e("Update City List", "---------->");
                                sessionManager.setOldTimeStamp(sessionManager.getNewTimeStamp());
                                JSONArray cityArray = jsonData.getJSONArray("cities");
                                for(int i=0;cityArray.length()>i;i++){
                                    JSONObject jobject = cityArray.getJSONObject(i);
                                    db.setCityData(jobject.getString("city_id"), jobject.getString("city_name"), jobject.getString("country_id"), jobject.getString("country_name"), jobject.getString("featured_image"),jobject.getString("flag"),jobject.getString("available_for_customize"),jobject.getString("available_for_standard"));
                                }

                                LoadPages();

                            } else {
                                Snackbar.make(v, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }

                        } catch (JSONException e) {
                            Snackbar.make(v, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getCityAPILIST();
                    }
                }
        );

        queue.add(getRequest);
    }


}
