package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterCancellationPoliciesList;
import com.guiddoocn.holidays.adapter.AdapterInclusionList;
import com.guiddoocn.holidays.adapter.AdapterMonthList;
import com.guiddoocn.holidays.adapter.AdapterOtherInclusionList;
import com.guiddoocn.holidays.adapter.AdapterPackageDayWiseList;
import com.guiddoocn.holidays.adapter.AdapterPackageHotelList;
import com.guiddoocn.holidays.models.ModelCancellationPoliciesList;
import com.guiddoocn.holidays.models.ModelHotelList;
import com.guiddoocn.holidays.models.ModelInclusionList;
import com.guiddoocn.holidays.models.ModelMonthList;
import com.guiddoocn.holidays.models.ModelPackGalleryImagesList;
import com.guiddoocn.holidays.models.ModelPackageDetailsList;
import com.guiddoocn.holidays.models.ModelPackagesDayWiseList;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityPackageDetails extends AppCompatActivity {
    private static final String TAG = "ActivityPackageDetails";

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.slider)
    SliderLayout sliderLayout;

    @BindView(R.id.tv_Packages_Name)
    TextView tv_Packages_Name;//tv_category

    @BindView(R.id.tv_category)
    TextView tv_category;//tv_category

    @BindView(R.id.tv_City_Cover)
    TextView tv_City_Cover;

    @BindView(R.id.tv_City_Name)
    TextView tv_City_Name;//

    @BindView(R.id.tv_Nights)
    TextView tv_Nights;

    @BindView(R.id.tv_Valid_from)
    TextView tv_Valid_from;

    @BindView(R.id.tv_Description)
    TextView tv_Description;

    @BindView(R.id.tv_long_Description)
    TextView tv_long_Description;//

    @BindView(R.id.tv_Highlights)
    TextView tv_Highlights;//tv_Packages_Name

    @BindView(R.id.bt_search)
    Button bt_search;//proced

    Context mContext;
    Activity activity;

    @BindView(R.id.rv_hotelList)
    RecyclerView rv_hotelList;

    @BindView(R.id.ll_day_wise_List)
    LinearLayout ll_day_wise_List;

    ArrayList<ModelHotelList> modelHotelLists = new ArrayList<>();

    AdapterPackageHotelList adapterHotelsList;

    ArrayList<ModelPackageDetailsList> mainPackageList = new ArrayList<>();

    @BindView(R.id.rv_InclusionList)
    RecyclerView rv_InclusionList;

    ArrayList<ModelInclusionList> modelInclusionLists = new ArrayList<>();
    ArrayList<ModelInclusionList> modelOtherInclusionLists = new ArrayList<>();
    private ArrayList<ModelPackagesDayWiseList> pack_itinerary_ArrayList = new ArrayList<>();
    private ArrayList<ModelPackagesDayWiseList> SortListModels = new ArrayList<>();
    String mPackage_Name,mCurrency;


    //MonthList
    @BindView(R.id.rv_MonthList)
    RecyclerView rv_MonthList;
    AdapterMonthList adapterMonthList;
    ArrayList<ModelMonthList> monthList = new ArrayList<>();

    //CancellationList polices

    @BindView(R.id.rv_CancellationList)
    RecyclerView rv_CancellationList;
    AdapterCancellationPoliciesList adapterCancellationPoliciesList;
    ArrayList<ModelCancellationPoliciesList> cancelList = new ArrayList<>();
    private ArrayList<ModelPackGalleryImagesList> pack_galleryImages_ArrayList = new ArrayList<>();

    @BindView(R.id.rv_OtherinclusionList)
    RecyclerView rv_OtherinclusionList;


    private int Total_Days = 0, Package_ID = 0,Pacakge_Amount=0;
    private CustomResponseDialog dialog;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packages_details);
        ButterKnife.bind(this);

        mContext = this;
        activity = this;
        dialog = new CustomResponseDialog(mContext);
        sessionManager = new SessionManager(mContext);


        Bundle bundleExtra = getIntent().getExtras();
        if (bundleExtra != null) {
            try {

                try {
                    FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
                } catch (NullPointerException e) {

                }


                //Total_Days = getIntent().getIntExtra("Total_Days", 1);
                Package_ID = getIntent().getIntExtra("Package_ID", 1);//AdapterAllListPackages
                Pacakge_Amount = getIntent().getIntExtra("Amount", 1);//AdapterAllListPackages
                Log.e("Amount","Amt "+Pacakge_Amount);////AdapterAllListPackages
                Log.e("Package_ID","Package_ID  "+Pacakge_Amount);//AdapterAllListPackages
                mPackage_Name= getIntent().getStringExtra("Package_Name");//AdapterAllListPackages
                mCurrency= getIntent().getStringExtra("Currency");//AdapterAllListPackages
                Log.e("currency",""+mCurrency+"jjjj=="+getIntent().getStringExtra("Currency"));

            } catch (Exception e) {
                showSnackBar(ActivityPackageDetails.this, "Something went wrong. Please try again.");
            }

        } else {
            showSnackBar(ActivityPackageDetails.this, "Something went wrong. Please try again.");
        }

        tv_title.setText(getIntent().getStringExtra("Package_Name"));

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        GetPackageDetail();

        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityPackageDetails.this, ActivityPackageBooking.class);
                intent.putExtra("Amount", Pacakge_Amount);
                intent.putExtra("Package_ID", Package_ID);
                intent.putExtra("Package_Name", mPackage_Name);
                intent.putExtra("Currency", mCurrency);
                startActivity(intent);
            }
        });

    }

    private void setDayWiseList() {
        String duration = "0";
        final LinearLayout l0 = (LinearLayout) findViewById(R.id.ll_day_wise_List);
        if (((LinearLayout) l0).getChildCount() > 0)
            ((LinearLayout) l0).removeAllViews();
        for (int i = 1; Total_Days >= i; i++) {


            TextView tv0 = new TextView(mContext);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            CardView card0 = new CardView(mContext);
            final LinearLayout ltop = new LinearLayout(mContext);
            ltop.setPadding(0, 10, 0, 10);


            SortListModels = new ArrayList<>();
            SortListModels.clear();


            for (int j = 0; pack_itinerary_ArrayList.size() > j; j++) {

                if (pack_itinerary_ArrayList.get(j).getDay_series().equalsIgnoreCase(String.valueOf(i))) {
                    ModelPackagesDayWiseList daysMainList = pack_itinerary_ArrayList.get(j);
                    SortListModels.add(daysMainList);
                }


            }


            card0.setLayoutParams(params);
            card0.setRadius(2);
            card0.setCardBackgroundColor(Color.parseColor("#929292"));
            card0.setMaxCardElevation(1);
            card0.setCardElevation(4);
            int pos = i;
            try {
                tv0.setText("Day " + i + " - " + SortListModels.get(0).getTitle());
            } catch (Exception e) {
                tv0.setText("Day " + i);
            }

            tv0.setGravity(Gravity.LEFT | Gravity.CENTER);
            tv0.setTextSize(12);
            tv0.setTextColor(Color.parseColor("#FFFFFF"));
            tv0.setSingleLine(true);
            tv0.setBackgroundColor(Color.parseColor("#929292"));
            tv0.setPadding(10, 0, 0, 0);
            card0.addView(tv0);

            ltop.setOrientation(LinearLayout.VERTICAL);
            ltop.setGravity(Gravity.LEFT);


            LinearLayout.LayoutParams lp0 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60);

            lp0.setMargins(0, 5, 0, 5);
            card0.setLayoutParams(lp0);
            ltop.addView(card0, lp0);
            l0.addView(ltop);


            final LinearLayout LLlist = (LinearLayout) findViewById(R.id.ll_day_wise_List);
            final LinearLayout lList = new LinearLayout(mContext);
            lList.setPadding(10, 10, 10, 10);
            RecyclerView rv_flight_list = new RecyclerView(mContext);
            LinearLayout.LayoutParams lppList = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            lList.addView(rv_flight_list, lppList);
            LLlist.addView(lList);

            try {

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                rv_flight_list.setLayoutManager(mLayoutManager);
                rv_flight_list.setItemAnimator(new DefaultItemAnimator());
                //rv_flight_list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));


                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                AdapterPackageDayWiseList adapterInclusionList = new AdapterPackageDayWiseList(mContext, SortListModels, activity, fragmentManager1);
                rv_flight_list.setLayoutManager(manager1);
                rv_flight_list.setAdapter(adapterInclusionList);


            } catch (Exception e) {
                Log.e("----inside--->", "Recycler adapter");
            }


        }
    }

    private void GetPackageDetail() {
        dialog.showCustomDialog();
        String url = sessionManager.getBaseUrlGuiddoo() + "GetPackageDetail?api_key=0D1067102F935B4CC31E082BD45014D469E35268&package_id=" + Package_ID;
        Log.e(TAG, "GetPackageDetail URL---->" + url);


        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.hideCustomeDialog();
                Log.e(TAG, "GetPackageDetail Response---->" + response);

                mainPackageList = new ArrayList<>();
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject jsonData = jsonObject.getJSONObject("data");

                    JSONObject jsonStatus = jsonData.getJSONObject("status");
                    if (jsonStatus.getString("status_code").equalsIgnoreCase("200")) {
                        ModelPackageDetailsList mainModel = new ModelPackageDetailsList();

                        try {
                            mainModel.setPackage_id(jsonData.getString("Package_id"));
                            mainModel.setThumbnail_image(jsonData.getString("thumbnail_image"));
                            mainModel.setPackage_name(jsonData.getString("Package_name"));
                            mainModel.setCoupon_code(jsonData.getString("Coupon_code"));
                            mainModel.setDicount_MaxCap(jsonData.getString("Dicount_MaxCap"));
                            mainModel.setDiscount(jsonData.getString("Discount"));
                            mainModel.setCities_covered(jsonData.getString("cities_covered"));
                            mainModel.setCities_covered_names(jsonData.getString("cities_covered_names"));
                            mainModel.setCities_nights(jsonData.getString("cities_nights"));
                            mainModel.setCountry_id(jsonData.getString("country_id"));
                            mainModel.setCountry_name(jsonData.getString("country_name"));
                            mainModel.setCurrency(jsonData.getString("currency"));
                            mainModel.setDescription(jsonData.getString("description"));
                            mainModel.setDuration(jsonData.getString("duration"));
                            mainModel.setHighlights(jsonData.getString("highlights"));
                            mainModel.setPreferred_months(jsonData.getString("preferred_months"));
                            mainModel.setShortDescription(jsonData.getString("shortDescription"));
                            mainModel.setRating(jsonData.getString("rating"));
                            mainModel.setValid_from(jsonData.getString("valid_from"));
                            mainModel.setValid_to(jsonData.getString("valid_to"));
                            mainModel.setVisa_included(jsonData.getString("visa_included"));
                        } catch (JSONException ee) {

                        }

                        try {
                            JSONArray jsonArray1 = jsonData.getJSONArray("pack_categories");
                            ArrayList<String> stringArray = new ArrayList<String>();
                            for (int j = 0, count = jsonArray1.length(); j < count; j++) {
                                try {
                                    JSONObject jsonObjects = jsonArray1.getJSONObject(j);
                                    stringArray.add(jsonObjects.getString("Category_name"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            String s = TextUtils.join(", ", stringArray);
                            mainModel.setPack_categories(s);
                        } catch (Exception e) {

                        }


                        try {
                            JSONArray jsonArrayCancelation = jsonData.getJSONArray("pack_cancellation");
                            if (jsonArrayCancelation.length() > 0) {
                                /* Cancellation  List*/
                                cancelList.add(new ModelCancellationPoliciesList("From", "To", "Deduction"));

                                for (int i = 0; jsonArrayCancelation.length() > i; i++) {
                                    JSONObject jsonCancel = jsonArrayCancelation.getJSONObject(i);
                                    cancelList.add(new ModelCancellationPoliciesList(jsonCancel.getString("from_hr"), jsonCancel.getString("to_hr"), jsonCancel.getString("charge") + "%"));
                                }

                                if (cancelList.size() > 0) {
                                    GridLayoutManager manager3 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                                    FragmentManager fragmentManager3 = getSupportFragmentManager();
                                    adapterCancellationPoliciesList = new AdapterCancellationPoliciesList(mContext, cancelList, activity, fragmentManager3);
                                    rv_CancellationList.setLayoutManager(manager3);
                                    rv_CancellationList.setAdapter(adapterCancellationPoliciesList);
                                }

                            }
                            //mainModel.setPack_cancellation_ArrayList(cancelList);
                        } catch (Exception e) {

                        }


                        try {
                            JSONArray jsonArrayGalleryImages = jsonData.getJSONArray("pack_galleryImages");
                            pack_galleryImages_ArrayList = new ArrayList<>();
                            for (int i = 0; jsonArrayGalleryImages.length() > i; i++) {
                                JSONObject jsonGallery = jsonArrayGalleryImages.getJSONObject(i);
                                ModelPackGalleryImagesList modelGallery = new ModelPackGalleryImagesList();
                                modelGallery.setImage_path(jsonGallery.getString("image_path"));
                                modelGallery.setTitle(jsonGallery.getString("title"));
                                pack_galleryImages_ArrayList.add(modelGallery);
                            }
                            //mainModel.setPack_galleryImages_ArrayList(pack_galleryImages_ArrayList);

                            if (pack_galleryImages_ArrayList.size() > 0) {

                                for (int i = 0; i < pack_galleryImages_ArrayList.size(); i++) {
                                    final int finalI = i;
                                    DefaultSliderView defaultSliderView = new DefaultSliderView(mContext);
                                    defaultSliderView .image(pack_galleryImages_ArrayList.get(i).getImage_path())
                                            .error(R.drawable.place_holder)
                                            .setScaleType(BaseSliderView.ScaleType.Fit);
                                    sliderLayout.addSlider(defaultSliderView);

                                }
                                sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                                sliderLayout.setCustomAnimation(new DescriptionAnimation());
                                sliderLayout.getPagerIndicator().setVisibility(View.VISIBLE);
                                sliderLayout.setDuration(8000);
                            }
                        } catch (Exception e) {

                        }


                        try {
                            JSONArray jsonArrayPack_Hotel = jsonData.getJSONArray("pack_hotel");
                            modelHotelLists = new ArrayList<>();
                            if (jsonArrayPack_Hotel.length() > 0) {
                                for (int i = 0; jsonArrayPack_Hotel.length() > i; i++) {
                                    JSONObject jsonHotel = jsonArrayPack_Hotel.getJSONObject(i);
                                    modelHotelLists.add(new ModelHotelList(jsonHotel.getString("Hotel_Name"), jsonHotel.getString("city_name"), jsonHotel.getString("hotel_id"), jsonHotel.getString("no_ofNights")));
                                }
                            }
                            //mainModel.setPack_hotel_ArrayList(modelHotelLists);

                            /* Hotel list */
                            GridLayoutManager manager = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            adapterHotelsList = new AdapterPackageHotelList(mContext, modelHotelLists, activity, fragmentManager);
                            rv_hotelList.setLayoutManager(manager);
                            rv_hotelList.setAdapter(adapterHotelsList);
                        } catch (Exception e) {

                        }


                        try {

                            /* Inclusion List*/

                            JSONArray jsonArrayPack_Inclusion = jsonData.getJSONArray("pack_inclusion");
                            for (int i = 0; jsonArrayPack_Inclusion.length() > i; i++) {
                                JSONObject jsonInclusion = jsonArrayPack_Inclusion.getJSONObject(i);
                                ModelInclusionList list = new ModelInclusionList();
                                list.setAddress(jsonInclusion.getString("address"));
                                list.setCity(jsonInclusion.getString("city"));
                                list.setFeatured_image(jsonInclusion.getString("featured_image"));
                                list.setInclusion(jsonInclusion.getString("inclusion"));
                                list.setTitle(jsonInclusion.getString("title"));

                                JSONObject jsonDuration = jsonInclusion.getJSONObject("duration");
                                list.setDay(jsonDuration.getString("day"));
                                list.setHour(jsonDuration.getString("hour"));
                                list.setMin(jsonDuration.getString("min"));
                                if (jsonInclusion.getString("address").equalsIgnoreCase("") && jsonInclusion.getString("title").equalsIgnoreCase("") && jsonInclusion.getString("city").equalsIgnoreCase("")) {
                                    modelOtherInclusionLists.add(list);
                                } else {
                                    modelInclusionLists.add(list);
                                }
                            }

                            if (modelInclusionLists.size() > 0) {
                                GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                                FragmentManager fragmentManager1 = getSupportFragmentManager();
                                AdapterInclusionList adapterInclusionList = new AdapterInclusionList(mContext, modelInclusionLists, activity, fragmentManager1);
                                rv_InclusionList.setLayoutManager(manager1);
                                rv_InclusionList.setAdapter(adapterInclusionList);
                            }


                            if (modelOtherInclusionLists.size() > 0) {
                                GridLayoutManager manager2 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                                FragmentManager fragmentManager2 = getSupportFragmentManager();
                                AdapterOtherInclusionList adapterInclusionList2 = new AdapterOtherInclusionList(mContext, modelOtherInclusionLists, activity, fragmentManager2);
                                rv_OtherinclusionList.setLayoutManager(manager2);
                                rv_OtherinclusionList.setAdapter(adapterInclusionList2);
                            }
                        } catch (Exception e) {

                        }


                        try {

                            /* MOnth List*/
                            monthList.clear();
                            String monthString = jsonData.getString("preferred_months");
                            List<String> items = Arrays.asList(monthString.split("\\s*,\\s*"));
                            for (int j = 0; items.size() > j; j++) {
                                int i = 0;
                                i = Integer.parseInt(items.get(j).toString());
                                if (i == 1) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " January"));
                                } else if (i == 2) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " February"));
                                } else if (i == 3) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " March"));
                                } else if (i == 4) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " April"));
                                } else if (i == 5) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " May"));
                                } else if (i == 6) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " June"));
                                } else if (i == 7) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " July"));
                                } else if (i == 8) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " August"));
                                } else if (i == 9) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " September"));
                                } else if (i == 10) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " October"));
                                } else if (i == 11) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " November"));
                                } else if (i == 12) {
                                    monthList.add(new ModelMonthList(R.drawable.ic_launcher_background, " December"));
                                }
                            }

                            GridLayoutManager manager2 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
                            FragmentManager fragmentManager2 = getSupportFragmentManager();
                            adapterMonthList = new AdapterMonthList(mContext, monthList, activity, fragmentManager2);
                            rv_MonthList.setLayoutManager(manager2);
                            rv_MonthList.setAdapter(adapterMonthList);

                        } catch (Exception e) {

                        }


                        try {
                            /*Day wise Itinerary*/
                            JSONArray jsonArrayPack_itinerary = jsonData.getJSONArray("pack_itinerary");
                            pack_itinerary_ArrayList = new ArrayList<>();
                            if (jsonArrayPack_itinerary.length() > 0) {
                                for (int i = 0; jsonArrayPack_itinerary.length() > i; i++) {
                                    JSONObject jsonDayWise = jsonArrayPack_itinerary.getJSONObject(i);
                                    pack_itinerary_ArrayList.add(new ModelPackagesDayWiseList(jsonDayWise.getString("city_id"), jsonDayWise.getString("day_series"), jsonDayWise.getString("title"), jsonDayWise.getString("itinerary_id"), jsonDayWise.getString("description"), jsonDayWise.getString("image")));
                                }
                            }
                            //mainModel.setPack_itinerary_ArrayList(pack_itinerary_ArrayList);
                        } catch (Exception e) {

                        }

                        Total_Days = pack_itinerary_ArrayList.size();



                        mainPackageList.add(mainModel);
                    }

                    tv_Packages_Name.setText(mainPackageList.get(0).getPackage_name());
                    tv_category.setText("Best for "+mainPackageList.get(0).getPack_categories());
                    tv_City_Cover.setText("Cities Covered "+mainPackageList.get(0).getCities_covered_names());
                    tv_City_Name.setText(mainPackageList.get(0).getCountry_name());
                    tv_Nights.setText(mainPackageList.get(0).getCities_nights()+" Nights");
                    tv_Valid_from.setText("Package Valid from "+mainPackageList.get(0).getValid_from()+" to "+mainPackageList.get(0).getValid_to());


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tv_Description.setText(Html.fromHtml(mainPackageList.get(0).getShortDescription(), Html.FROM_HTML_MODE_COMPACT));
                    }else{
                        tv_Description.setText(Html.fromHtml(mainPackageList.get(0).getShortDescription()));
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tv_long_Description.setText(Html.fromHtml(mainPackageList.get(0).getDescription(), Html.FROM_HTML_MODE_COMPACT));
                    }else{
                        tv_long_Description.setText(Html.fromHtml(mainPackageList.get(0).getDescription()));
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tv_long_Description.setText(Html.fromHtml(mainPackageList.get(0).getDescription(), Html.FROM_HTML_MODE_COMPACT));
                    }else{
                        tv_long_Description.setText(Html.fromHtml(mainPackageList.get(0).getDescription()));
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tv_Highlights.setText(Html.fromHtml(mainPackageList.get(0).getHighlights(), Html.FROM_HTML_MODE_COMPACT));
                    } else {
                        tv_Highlights.setText(Html.fromHtml(mainPackageList.get(0).getHighlights()));
                    }

                    setDayWiseList();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.hideCustomeDialog();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                }
            }
        }) /*{

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //params.put("Content-Type", "application/json");
                //params.put("Parameters", String.valueOf(noOfDays));
                //params.put("Authorization", sessionManager.getHashkey());
                //..add other headers
                return params;
            }

            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        }*/;
        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }

    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 1500).show();
    }
}
