package com.guiddoocn.holidays.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterCountry;
import com.guiddoocn.holidays.adapter.AdapterHotelsItineraryList;
import com.guiddoocn.holidays.adapter.AdapterHotelsItineraryQuotetionList;
import com.guiddoocn.holidays.models.ModelDestinationLists;
import com.guiddoocn.holidays.models.ModelItineraryActivitiesList;
import com.guiddoocn.holidays.models.ModelItineraryHotelList;
import com.guiddoocn.holidays.models.ModelRoomPriceList;
import com.guiddoocn.holidays.utils.AppConstants;
import com.guiddoocn.holidays.utils.AvenuesParams;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.ServiceUtility;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.ParserConfigurationException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class GenerateQuoteDetailsActivity extends AppCompatActivity {

    private static final String TAG = "GenerateQuoteDetails";
    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.ll_hotel_Dialog)
    LinearLayout ll_hotel_Dialog;

    @BindView(R.id.tv_hotel_Name)
    TextView tv_hotel_Name;

    @BindView(R.id.ll_source_Dialog)
    LinearLayout ll_source_Dialog;

    @BindView(R.id.tv_source_Name)
    TextView tv_source_Name;

    @BindView(R.id.sp_sharingType)
    Spinner sp_sharingType;
    @BindView(R.id.sp_roomType)
    Spinner sp_roomType;

    @BindView(R.id.sp_mealType)
    Spinner sp_mealType;

    @BindView(R.id.sp_marginType)
    Spinner sp_marginType;

    @BindView(R.id.sp_Option)
    Spinner sp_Option;

    @BindView(R.id.et_In)
    FormEditText et_In;
    @BindView(R.id.et_InAmount)
    FormEditText et_InAmount;
    @BindView(R.id.et_Name)
    FormEditText et_Name;
    @BindView(R.id.et_Email)
    FormEditText et_Email;
    @BindView(R.id.tv_dateSelection)
    TextView tv_dateSelection;
    @BindView(R.id.tv_Packages_Name)
    TextView tv_Packages_Name;

    Context mContext;
    @BindView(R.id.et_booking_Check_In)
    EditText et_booking_Check_In;
    @BindView(R.id.et_booking_Check_Out)
    EditText et_booking_Check_Out;
    @BindView(R.id.et_Currency)
    EditText et_Currency;
    @BindView(R.id.et_Amount)
    FormEditText et_Amount;
    @BindView(R.id.et_numberOfRoom)
    FormEditText et_numberOfRoom;
    @BindView(R.id.et_confirmationNumber)
    EditText et_confirmationNumber;

    @BindView(R.id.tv_hotelAmount)
    TextView tv_hotelAmount;
    @BindView(R.id.tv_activityAmount)
    TextView tv_activityAmount;
    @BindView(R.id.tv_cardAmount)
    TextView tv_cardAmount;
    @BindView(R.id.tv_total_costes)
    TextView tv_total_costes;
    @BindView(R.id.ll_visa_view)
    LinearLayout ll_visa_view;

    @BindView(R.id.final_Amt)
    TextView final_Amt;//

    @BindView(R.id.rv_allListHotel)
    RecyclerView rv_allListHotel;

    @BindView(R.id.ll_add_new_activity_List)
    LinearLayout ll_add_new_activity_List;

    @BindView(R.id.tv_my_itinerary_view_all)
    TextView tv_my_itinerary_view_all;
    @BindView(R.id.tv_travellers)
    TextView tv_travellers;

    @BindView(R.id.ll_option)
    LinearLayout ll_option;
    @BindView(R.id.tv_option_Hotels)
    TextView tv_option_Hotels;

    @BindView(R.id.ll_option1)
    LinearLayout ll_option1;
    @BindView(R.id.tv_option1)
    TextView tv_option1;

    @BindView(R.id.ll_option2)
    LinearLayout ll_option2;
    @BindView(R.id.tv_option2)
    TextView tv_option2;

    @BindView(R.id.ll_option3)
    LinearLayout ll_option3;
    @BindView(R.id.tv_option3)
    TextView tv_option3;



    Activity activity;

    private int  number_of_adults = 1, number_of_child = 0, NoOfRooms = 1;
    private double roomAmt = 0.0;
    private Calendar check_in_Calendar, check_out_Calendar;
    private DatePickerDialog.OnDateSetListener Fromdate_Check_In, Fromdate_Check_Out;
    private SQLiteHandler db;
    private CustomResponseDialog dialog;


    ArrayList<ModelDestinationLists> modelHotelLists = new ArrayList<>();
    ArrayList<ModelDestinationLists> modelsourceList = new ArrayList<>();
    ArrayList<ModelDestinationLists> modelSharingTypeList = new ArrayList<>();
    ArrayList<ModelDestinationLists> modelRoomTypeList = new ArrayList<>();
    ArrayList<ModelDestinationLists> modelMealTypeList = new ArrayList<>();
    ArrayList<ModelDestinationLists> modelmarginLists = new ArrayList<>();
    ArrayList<ModelDestinationLists> modelOptionLists = new ArrayList<>();
    ArrayList<ModelRoomPriceList> roomItineraryData = new ArrayList<>();
    ArrayList<ModelRoomPriceList> finalroomItineraryData = new ArrayList<>();

    ArrayList<ModelItineraryHotelList> modelHotelAllLists = new ArrayList<>();
    ArrayList<ModelItineraryActivitiesList> modelActivityLists = new ArrayList<>();

    ArrayList<ModelItineraryHotelList> modelHotelMainLists = new ArrayList<>();
    ArrayList<ModelItineraryHotelList> modelHotelAddedLists = new ArrayList<>();

    private JSONArray jsonArrayHotel =new JSONArray();
    private JSONArray jsonArrayActivity =new JSONArray();

    @BindView(R.id.bt_sendItinerary)
    Button bt_sendItinerary;
    @BindView(R.id.bt_saveItinerary)
    Button bt_saveItinerary;


    @BindView(R.id.ll_customerDetails)
    LinearLayout ll_customerDetails;
    String mCheckIn, mCheckOut,is_visa_included,visa_type,margin_type="1",marginValues,booking_code="",itinerary_reference_no,sharing_Type_ID="1",meal_Type_ID="1",sharing_Type="",meal_Type="",source_id="0",source_name="";
    private boolean flagMargin=false;

    private SessionManager sessionManager;
    String hotel_ID,hotel_Name,room_ID,room_Name,mCurrency, mEmail, mName, mDate, mConfirmationNumber,hotel_cost,activity_cost,option="1";
    private float total_hotel_cost = 0,Final_Cost=0,Total_Cost=0,Pay_Cost=0;
    private float total_hotel_cost_option1 = 0,total_hotel_cost_option2 = 0,total_hotel_cost_option3 = 0, total_activity_cost = 0,total_visa_cost=0, activity_Adult_cost=0,total_activity_Adult_cost=0,activity_Child_cost=0,total_activity_Child_cost=0,Transport_Charges_Adult=0, Total_transport_cost_adult =0, Total_transport_cost_child =0,Total_Voucher_cost=0;
    private String selected_Option="1",mainHint="",selected_Option1="",selected_Option2="",selected_Option3="",tv_hotel_cost_per_Adult,tv_hotel_cost_per_Child,tv_total_cost_per_Adult,tv_total_cost_per_Child,tv_excursion_cost_per_Adult,tv_excursion_cost_per_Child;
    private boolean flag_Option=false,flag_Option1=false,flag_Option2=false,flag_Option3=false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_quote_details);
        ButterKnife.bind(this);
        mContext = this;
        activity = this;
        db=new SQLiteHandler(mContext);
        tv_title.setText("Review Itinerary");
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        check_in_Calendar = Calendar.getInstance();
        check_out_Calendar = Calendar.getInstance();
        sessionManager = new SessionManager(mContext);
        dialog= new CustomResponseDialog(mContext);
        mCurrency = sessionManager.getCountryName();

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");

        } catch (NullPointerException e) {

        }


        getAllHotelsMaster();

        et_Email.setText(sessionManager.getEmailId());
        et_Name.setText(sessionManager.getContactPerson());

        mEmail = et_Email.getText().toString().trim();
        mName = et_Name.getText().toString().trim();

        /* jsonObject.put("hotel_cost_adult",new BigDecimal(getIntent().getStringExtra("hotel_cost_adult")));
            jsonObject.put("hotel_cost_child",new BigDecimal(getIntent().getStringExtra("hotel_cost_child")));
            jsonObject.put("inclusion_cost_adult",new BigDecimal(getIntent().getStringExtra("inclusion_cost_adult")));
            jsonObject.put("inclusion_cost_child",new BigDecimal(getIntent().getStringExtra("inclusion_cost_child")));
            jsonObject.put("total_cost_adult",new BigDecimal(getIntent().getStringExtra("total_cost_adult")));
            jsonObject.put("total_cost_child",new BigDecimal(getIntent().getStringExtra("total_cost_child")));*/

        et_booking_Check_In.setText(sessionManager.getCheckInDate());
        et_booking_Check_Out.setText(sessionManager.getCheckOutDate());
        tv_dateSelection.setText("Your Departure Date - " + sessionManager.getCheckInDate() + " To " + sessionManager.getCheckOutDate());


        try{
            Final_Cost = Float.parseFloat(getIntent().getStringExtra("Final_Cost"));
            activity_cost = getIntent().getStringExtra("Total_activity_cost");
            is_visa_included= getIntent().getStringExtra("is_visa_included");
            total_visa_cost= Float.parseFloat(getIntent().getStringExtra("visa_cost"));
            visa_type= getIntent().getStringExtra("visa_type");


            Double visa= Double.valueOf(total_visa_cost);
            if(visa>0){
                ll_visa_view.setVisibility(View.VISIBLE);
            }else{
                ll_visa_view.setVisibility(View.GONE);
            }




            int dateDifference = 0;
            String check_in_Date = et_booking_Check_In.getText().toString();
            String check_out_Date = et_booking_Check_Out.getText().toString();

            dateDifference = (int) getDateDiff(new SimpleDateFormat("dd-MM-yyyy"), check_in_Date, check_out_Date);
            int tmp = dateDifference;

            int Count = Integer.parseInt(sessionManager.getAdultCount()) + Integer.parseInt(sessionManager.getChildCount());
            tv_Packages_Name.setText(sessionManager.getCountryName() + "," + dateDifference+" NIGHTS "+  ++tmp +" DAYS, "+Count+" PAX");
        }catch (Exception e){

        }


        modelHotelAllLists=new ArrayList<>();
        modelHotelAllLists = db.getItineraryHotelsData(sessionManager.getCountryCode());

        if(modelHotelAllLists.size()>0){
            for (int i = 0; modelHotelAllLists.size() > i; i++) {
                total_hotel_cost = total_hotel_cost + Float.valueOf(modelHotelAllLists.get(i).getFinal_cost());
            }
            Total_Cost = Final_Cost + total_hotel_cost;
            //final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
           // tv_hotelAmount.setText(String.valueOf(String.format("%.02f", total_hotel_cost)));
            addHotelListShow();
        }


        modelActivityLists = new ArrayList<>();
        modelActivityLists = db.getItineraryActivityData(sessionManager.getCountryCode());

        for (int i = 0; modelActivityLists.size() > i; i++) {
            total_activity_cost = total_activity_cost + Float.valueOf(modelActivityLists.get(i).getFinal_cost());

            if(!modelActivityLists.get(i).getTransfer_Charges().equalsIgnoreCase("0")){
                Double temp = Double.valueOf(modelActivityLists.get(i).getTransfer_Charges()) / Double.valueOf(modelActivityLists.get(i).getAdult());
                Double Main = temp +  Double.valueOf(modelActivityLists.get(i).getAdult_Amt());
                String t = String.valueOf(Main);
                Total_transport_cost_adult = Total_transport_cost_adult + Float.valueOf(t);
                if(Float.valueOf(modelActivityLists.get(i).getChild())>0){
                    Total_transport_cost_child = Total_transport_cost_child + Float.valueOf(modelActivityLists.get(i).getChild_Amt());
                }else{
                    Total_transport_cost_child = 0;
                }
            }

            if(!modelActivityLists.get(i).getVouchers_Charges().equalsIgnoreCase("0")){
                if(!modelActivityLists.get(i).getIsPack().equalsIgnoreCase("false")){
                    Float temp = Float.valueOf(modelActivityLists.get(i).getAdult()) + Float.valueOf(modelActivityLists.get(i).getChild());
                    Total_Voucher_cost=Total_Voucher_cost + (Float.valueOf(modelActivityLists.get(i).getVouchers_Charges())/temp);
                }else{
                    Total_Voucher_cost=Total_Voucher_cost + Float.valueOf(modelActivityLists.get(i).getVouchers_Charges());
                }
            }
            if(modelActivityLists.get(i).getTransfer_Charges().equalsIgnoreCase("0") && modelActivityLists.get(i).getVouchers_Charges().equalsIgnoreCase("0")){
                if(!modelActivityLists.get(i).getIsPack().equalsIgnoreCase("false")){
                    Float temp = Float.valueOf(modelActivityLists.get(i).getAdult()) + Float.valueOf(modelActivityLists.get(i).getChild());
                    activity_Adult_cost=activity_Adult_cost + (Float.valueOf(modelActivityLists.get(i).getFinal_cost())/temp);
                    if(Float.valueOf(modelActivityLists.get(i).getChild())>0){
                        activity_Child_cost=activity_Child_cost + (Float.valueOf(modelActivityLists.get(i).getFinal_cost())/temp);
                    }

                }else{
                    activity_Adult_cost=activity_Adult_cost + Float.valueOf(modelActivityLists.get(i).getAdult_Amt());
                    if(Float.valueOf(modelActivityLists.get(i).getChild())>0){
                        activity_Child_cost=activity_Child_cost + Float.valueOf(modelActivityLists.get(i).getChild_Amt());
                    }
                }
            }
        }



        try {

            total_activity_Adult_cost = Total_Voucher_cost + Total_transport_cost_adult + activity_Adult_cost;
            total_activity_Child_cost =  Total_Voucher_cost + Total_transport_cost_child + activity_Child_cost;
            tv_excursion_cost_per_Adult=(String.format("%.02f", total_activity_Adult_cost));
            tv_excursion_cost_per_Child=(String.format("%.02f", total_activity_Child_cost));
            Total_Cost = total_hotel_cost + total_activity_cost + total_visa_cost;

        }catch (Exception e){

        }




        if(total_hotel_cost>0){
            tv_option_Hotels.setVisibility(View.GONE);
            Select_Option();
        }else{
            tv_option_Hotels.setVisibility(View.GONE);
        }


        tv_my_itinerary_view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateQuoteDetailsActivity.this, ActivityItineraryViewAll.class);
                intent.putExtra("visa_type",visa_type);
                startActivity(intent);
            }
        });

        ll_option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_option1.setBackgroundResource(R.drawable.custom_bg_red_option);
                tv_option1.setTextColor(Color.parseColor("#FFFFFF"));
                ll_option2.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option2.setTextColor(Color.parseColor("#F66961"));
                ll_option3.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option3.setTextColor(Color.parseColor("#F66961"));
                selected_Option="1";
                Select_Option();
            }
        });

        ll_option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_option1.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option1.setTextColor(Color.parseColor("#F66961"));
                ll_option2.setBackgroundResource(R.drawable.custom_bg_red_option);
                tv_option2.setTextColor(Color.parseColor("#FFFFFF"));
                ll_option3.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option3.setTextColor(Color.parseColor("#F66961"));
                selected_Option="2";
                Select_Option();
            }
        });

        ll_option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_option1.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option1.setTextColor(Color.parseColor("#F66961"));
                ll_option2.setBackgroundResource(R.drawable.custom_bg_white_box_book);
                tv_option2.setTextColor(Color.parseColor("#F66961"));
                ll_option3.setBackgroundResource(R.drawable.custom_bg_red_option);
                tv_option3.setTextColor(Color.parseColor("#FFFFFF"));
                selected_Option="3";
                Select_Option();
            }
        });

        et_InAmount.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0){
                    float temp = Total_Cost;
                    temp = temp + Float.valueOf(String.valueOf(s));
                    marginValues = String.valueOf(s);
                    final_Amt.setText("Your Payment Total: $"+String.format("%.02f", temp));
                    Pay_Cost = temp;
                }else{
                    final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                    Pay_Cost = Total_Cost;
                }

            }
        });

        et_In.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0){
                    float temp = Total_Cost;
                    float per = (temp / 100) * Float.valueOf(String.valueOf(s));
                    marginValues = String.valueOf(s);
                    temp = temp + Float.valueOf(String.valueOf(per));
                    final_Amt.setText("Your Payment Total: $"+String.format("%.02f", temp));
                    Pay_Cost = temp;
                }else{
                    final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                    Pay_Cost = Total_Cost;
                }

            }
        });



        // Creating  Check in Check Out
        et_booking_Check_In.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // tv_dateSelection.setText("Your Departure Date :" + et_booking_Check_In.getText().toString().trim() + " To " + et_booking_Check_Out.getText().toString().trim());
                DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate_Check_In, check_in_Calendar
                        .get(Calendar.YEAR), check_in_Calendar.get(Calendar.MONTH),
                        check_in_Calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.setTime(sdf.parse(sessionManager.getCheckInDate()));
                    String sDate = sdf.format(startCalendar.getTime());
                    startDate = sdf.parse(sDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                mDate.show();

            }
        });
        Fromdate_Check_In = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                check_in_Calendar.set(Calendar.YEAR, year);
                check_in_Calendar.set(Calendar.MONTH, monthOfYear);
                check_in_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy";
                final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                mCheckIn = sdf.format(check_in_Calendar.getTime()).toString();
                et_booking_Check_In.setText(sdf.format(check_in_Calendar.getTime()));
                //tv_dateSelection.setText("Your Departure Date :" + mCheckIn + " To " + et_booking_Check_Out.getText().toString().trim());

            }

        };


        et_booking_Check_Out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_dateSelection.setText("Your Departure Date :" + et_booking_Check_In.getText().toString().trim() + " To " + et_booking_Check_Out.getText().toString().trim());
                DatePickerDialog mDate = new DatePickerDialog(mContext, Fromdate_Check_Out, check_out_Calendar
                        .get(Calendar.YEAR), check_out_Calendar.get(Calendar.MONTH),
                        check_out_Calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date startDate = null;
                Date endDate = null;
                try {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(sdf.parse(et_booking_Check_In.getText().toString().trim()));
                    calendar.add(Calendar.DATE, 1);
                    String destDate = sdf.format(calendar.getTime());
                    startDate = sdf.parse(destDate);
                    mDate.getDatePicker().setMinDate(startDate.getTime());
                    mDate.show();

                    Calendar endCalendar = Calendar.getInstance();
                    endCalendar.setTime(sdf.parse(sessionManager.getCheckOutDate()));
                    String eDate = sdf.format(endCalendar.getTime());
                    endDate = sdf.parse(eDate);
                    mDate.getDatePicker().setMaxDate(endDate.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                mDate.show();

            }
        });
        Fromdate_Check_Out = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                check_out_Calendar.set(Calendar.YEAR, year);
                check_out_Calendar.set(Calendar.MONTH, monthOfYear);
                check_out_Calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy";
                final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                mCheckOut = sdf.format(check_out_Calendar.getTime()).toString();
                et_booking_Check_Out.setText(sdf.format(check_out_Calendar.getTime()));
                //tv_dateSelection.setText("Your Departure Date :" + et_booking_Check_In.getText().toString().trim() + " To " + mCheckOut);

            }

        };

        ModelDestinationLists marginTypeList0 = new ModelDestinationLists();
        ModelDestinationLists smarginTypeList1 = new ModelDestinationLists();
        marginTypeList0.setCountryName("In%");
        smarginTypeList1.setCountryName("In Amount");

        modelmarginLists.add(marginTypeList0);
        modelmarginLists.add(smarginTypeList1);

        AdapterCountry adaptermarginTypelLists = new AdapterCountry(GenerateQuoteDetailsActivity.this, R.layout.spiner_country_list, R.id.title, modelmarginLists);
        sp_marginType.setAdapter(adaptermarginTypelLists);

        sp_marginType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                String margin = modelmarginLists.get(pos).getCountryName();
                Log.e(TAG, "mar " + margin);
                if (margin.equalsIgnoreCase("In%")) {
                    Log.e(TAG, "In  " + margin);
                    margin_type ="1";
                    et_InAmount.setVisibility(View.GONE);
                    et_In.setVisibility(View.VISIBLE);
                    final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                    et_In.setText("");
                    et_InAmount.setText("");
                }
                if (margin.equalsIgnoreCase("In Amount")) {
                    Log.e(TAG, "InA  " + margin);
                    margin_type ="2";
                    et_In.setVisibility(View.GONE);
                    et_InAmount.setVisibility(View.VISIBLE);
                    final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                    et_In.setText("");
                    et_InAmount.setText("");
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        try{
            modelOptionLists = new ArrayList<>();
            modelOptionLists.add( new ModelDestinationLists("1","Opt 1"));
            modelOptionLists.add( new ModelDestinationLists("2","Opt 2"));
            modelOptionLists.add( new ModelDestinationLists("3","Opt 3"));
            modelOptionLists.add( new ModelDestinationLists("0","Inc all"));

            AdapterCountry adapterMealTypeList = new AdapterCountry(GenerateQuoteDetailsActivity.this, R.layout.spiner_country_list, R.id.title, modelOptionLists);
            sp_Option.setAdapter(adapterMealTypeList);

            sp_Option.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                    option = modelOptionLists.get(pos).getCityId();
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }catch (Exception e){


        }




        ll_hotel_Dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleSearchDialogCompat dialog = new SimpleSearchDialogCompat(activity, "Search Hotels Name...",
                        "Search Hotels Name...?", null, modelHotelLists,
                        new SearchResultListener<ModelDestinationLists>() {
                            @Override
                            public void onSelected(
                                    BaseSearchDialogCompat dialog,
                                    ModelDestinationLists item, int position
                            ) {
                                hotel_ID = item.getCityId();
                                hotel_Name = item.getCountryName();
                                tv_hotel_Name.setText(item.getCountryName());
                                dialog.dismiss();
                            }
                        }
                );
                dialog.show();
            }
        });


        ll_source_Dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleSearchDialogCompat dialog = new SimpleSearchDialogCompat(activity, "Search Source Name...",
                        "Search Source Name...?", null, modelsourceList,
                        new SearchResultListener<ModelDestinationLists>() {
                            @Override
                            public void onSelected(
                                    BaseSearchDialogCompat dialog,
                                    ModelDestinationLists item, int position
                            ) {
                                source_id = item.getCityId();
                                source_name = item.getCountryName();
                                tv_source_Name.setText(item.getCountryName());
                                dialog.dismiss();
                            }
                        }
                );
                dialog.show();
            }
        });

        bt_saveItinerary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_customerDetails.setVisibility(View.VISIBLE);
                if(margin_type.equalsIgnoreCase("1")){
                    if(et_In.getText().toString().equalsIgnoreCase("")){
                        flagMargin=false;
                    }else{
                        flagMargin=true;
                    }
                }else if(margin_type.equalsIgnoreCase("2")){
                    if(et_InAmount.getText().toString().equalsIgnoreCase("")){
                        flagMargin=false;
                    }else{
                        flagMargin=true;
                    }
                }

                if (getValidate() == true) {
                    if(flagMargin){

                        jsonArrayHotel = new JSONArray();
                        jsonArrayActivity = new JSONArray();

                        modelHotelAllLists=new ArrayList<>();
                        modelActivityLists=new ArrayList<>();
                        roomItineraryData =new ArrayList<>();

                        modelHotelAllLists = db.getItineraryHotelsData(sessionManager.getCountryCode());
                        total_hotel_cost=0;
                        if(modelHotelAllLists.size()>0){
                            for (int i = 0; modelHotelAllLists.size() > i; i++) {
                                total_hotel_cost = total_hotel_cost + Float.valueOf(modelHotelAllLists.get(i).getFinal_cost());
                                try {

                                    try{
                                        Log.e("Total Room Id",modelHotelAllLists.get(i).getHotelsTotalRooms());
                                        List<String> items = new ArrayList<>();
                                        List<String> itemsHash = new ArrayList<>();
                                        items = Arrays.asList(modelHotelAllLists.get(i).getHotelsTotalRooms().split("\\s*,\\s*"));
                                        HashSet<String> hashSet = new HashSet<String>();
                                        hashSet.addAll(items);
                                        items = new ArrayList<>();
                                        items.addAll(hashSet);
                                        Log.e("Total Room hash-->",items.toString());
                                         //Log.e("Room Data",outputStudents(roomItineraryData));
                                        for(int j=0;items.size()>j;j++){
                                            roomItineraryData =new ArrayList<>();
                                            roomItineraryData= db.getRoomItineraryData(Integer.parseInt(items.get(j).toString()),modelHotelAllLists.get(i).getHotel_id(),sessionManager.getCountryCode());
                                           // Log.e("Room number",items.get(j).toString());
                                           // Log.e("Room size",String.valueOf(roomItineraryData.size()));

                                            if(roomItineraryData.size()>1){
                                                for(int k=0;roomItineraryData.size()>k;k++){
                                                    JSONObject jsonObject1 = new JSONObject();
                                                    jsonObject1.put("hotel_id", new BigDecimal(modelHotelAllLists.get(i).getHotel_id()));
                                                    jsonObject1.put("hotel_name", modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle());
                                                    jsonObject1.put("hotel_extra",roomItineraryData.get(k).getHotel_extra());
                                                    //jsonObject1.put("room_id", new BigDecimal(modelHotelAllLists.get(i).getHotelsLists().get(0).getHotelRoomsLists().get(0).getRoom_id()));
                                                    if(Boolean.parseBoolean(roomItineraryData.get(k).getIs_additional_hotel())){
                                                        jsonObject1.put("room_id", new BigDecimal("0"));
                                                    }else{
                                                        jsonObject1.put("room_id", new BigDecimal(items.get(j).toString()));
                                                    }
                                                    jsonObject1.put("sharing_type_id",roomItineraryData.get(k).getSharing_type_id());
                                                    jsonObject1.put("meal_type_id",new BigDecimal(roomItineraryData.get(k).getMeal_type_id()));
                                                    jsonObject1.put("room_amt",roomItineraryData.get(k).getHotel_Amt());
                                                    jsonObject1.put("check_in_date", modelHotelAllLists.get(i).getCheck_in_date());
                                                    jsonObject1.put("check_out_date", modelHotelAllLists.get(i).getCheck_out_date());
                                                    jsonObject1.put("is_additional_hotel",Boolean.parseBoolean(roomItineraryData.get(k).getIs_additional_hotel()));
                                                    jsonObject1.put("extra_bed_included",Boolean.parseBoolean(roomItineraryData.get(k).getExtra_bed_included()));
                                                    jsonObject1.put("supplier",roomItineraryData.get(k).getSupplier());
                                                    jsonObject1.put("room_type_id",modelHotelAllLists.get(i).getHotel_id());
                                                    // jsonObject1.put("room_id", new BigDecimal(modelHotelAllLists.get(i).getHotelsTotalRooms()));

                                                    jsonArrayHotel.put(jsonObject1);
                                                    //Log.e("Room Object",jsonObject1.toString());
                                                }

                                            }else{
                                                //Log.e("Room Data",outputStudents(roomItineraryData));
                                                JSONObject jsonObject1 = new JSONObject();
                                                jsonObject1.put("hotel_id", new BigDecimal(modelHotelAllLists.get(i).getHotel_id()));
                                                jsonObject1.put("hotel_name", modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle());
                                                jsonObject1.put("hotel_extra",roomItineraryData.get(0).getHotel_extra());
                                                //jsonObject1.put("room_id", new BigDecimal(modelHotelAllLists.get(i).getHotelsLists().get(0).getHotelRoomsLists().get(0).getRoom_id()));
                                                if(Boolean.parseBoolean(roomItineraryData.get(0).getIs_additional_hotel())){
                                                    jsonObject1.put("room_id", new BigDecimal("0"));
                                                }else{
                                                    jsonObject1.put("room_id", new BigDecimal(items.get(0).toString()));
                                                }
                                                jsonObject1.put("sharing_type_id",roomItineraryData.get(0).getSharing_type_id());
                                                jsonObject1.put("meal_type_id",new BigDecimal(roomItineraryData.get(0).getMeal_type_id()));
                                               // jsonObject1.put("meal_type_id",roomItineraryData.get(0).getMeal_type_id());
                                                jsonObject1.put("room_amt",roomItineraryData.get(0).getHotel_Amt());
                                                jsonObject1.put("check_in_date", modelHotelAllLists.get(i).getCheck_in_date());
                                                jsonObject1.put("check_out_date", modelHotelAllLists.get(i).getCheck_out_date());
                                                jsonObject1.put("is_additional_hotel",Boolean.parseBoolean(roomItineraryData.get(0).getIs_additional_hotel()));
                                                jsonObject1.put("extra_bed_included",Boolean.parseBoolean(roomItineraryData.get(0).getExtra_bed_included()));
                                                jsonObject1.put("supplier",roomItineraryData.get(0).getSupplier());
                                                jsonObject1.put("room_type_id",modelHotelAllLists.get(i).getHotel_id());

                                                // jsonObject1.put("room_id", new BigDecimal(modelHotelAllLists.get(i).getHotelsTotalRooms()));

                                                jsonArrayHotel.put(jsonObject1);
                                                //Log.e("Room Object",jsonObject1.toString());
                                            }

                                        }
                                    }catch (JSONException e){

                                    }

                                }catch (Exception e) {

                                }
                            }
                            Log.e("All Room Details",jsonArrayHotel.toString());
                        }

                        modelActivityLists = new ArrayList<>();
                        modelActivityLists = db.getItineraryActivityData(sessionManager.getCountryCode());

                        for (int i = 0; modelActivityLists.size() > i; i++) {
                            try {
                                JSONObject jsonObject2 = new JSONObject();
                                jsonObject2.put("activity_date", modelActivityLists.get(i).getCheck_in_date());
                                jsonObject2.put("activity_id", new BigDecimal(modelActivityLists.get(i).getActivity_id()));
                                jsonObject2.put("activity_name",modelActivityLists.get(i).getModelActivitiesLists().get(0).getName());
                                jsonObject2.put("activity_time", modelActivityLists.get(i).getCheck_out_date());
                                jsonObject2.put("no_of_adults", new BigDecimal(modelActivityLists.get(i).getAdult()));
                                jsonObject2.put("no_of_child", new BigDecimal(modelActivityLists.get(i).getChild()));
                                jsonArrayActivity.put(jsonObject2);

                            }catch (JSONException e) {

                            }
                        }

                        getRequest();

                    }else{
                        Toast.makeText(mContext, "Please enter margin", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    getValidate();
                }

            }
        });


        ll_add_new_activity_List.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getHotelValidate()==true){
                    roomAmt = Double.parseDouble(et_Amount.getText().toString().trim());
                    NoOfRooms = Integer.parseInt(et_numberOfRoom.getText().toString().trim());
                    mConfirmationNumber = et_confirmationNumber.getText().toString().trim();
                    int dateDifference = 0;
                    double final_Cost = 0.0,temp=0.0;
                    String check_in_Date = et_booking_Check_In.getText().toString();
                    String check_out_Date = et_booking_Check_Out.getText().toString();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date convertedDate = new Date();
                    Date convertedDate2 = new Date();
                    try{
                        dateDifference = (int) getDateDiff(new SimpleDateFormat("dd-MM-yyyy"), check_in_Date, check_out_Date);
                        Log.e("Date different--->", String.valueOf(dateDifference +" no of night "+NoOfRooms +" Amt "+roomAmt));

                        if(number_of_adults>0 && number_of_child==0){
                            temp = roomAmt * number_of_adults;
                            final_Cost = temp * NoOfRooms;
                            final_Cost = final_Cost * dateDifference;
                        }else if(number_of_adults>0 && number_of_child>0){
                            double count = number_of_adults + number_of_child;
                            temp = roomAmt * count;
                            final_Cost = temp * NoOfRooms;
                            final_Cost = final_Cost * dateDifference;
                        }
                        Log.e("Final cost--->", String.valueOf(final_Cost));
                        db.setHotelsData(hotel_ID, hotel_Name, sessionManager.getCountryName(),"0", roomAmt, "", "USD", sessionManager.getCountryCode(), sessionManager.getCountryName(),"", "",sessionManager.getCountryCode());
                        //db.setAmenityData(modelAllHotelLists.get(i).getHotelAmnetiesLists().get(j).getID(), modelAllHotelLists.get(i).getHotelAmnetiesLists().get(j).getName(), modelAllHotelLists.get(i).getID());
                        db.setRoomsData(room_ID,room_Name,"",String.valueOf(roomAmt), meal_Type, String.valueOf(roomAmt), "USD","Flexible","" ,"","","",hotel_ID,sessionManager.getCountryCode());

                        for(int j=1;NoOfRooms>=j;j++){
                            try{
                                db.setRoomItineraryData(room_ID,hotel_ID,sessionManager.getCountryCode(),dateDifference,roomAmt,sharing_Type_ID,meal_Type_ID,room_Name+","+sharing_Type+","+meal_Type,"false","true",source_id);
                                /*if(db.CheckIsAvailableRoomItinerary(room_ID,hotel_ID,sessionManager.getCountryCode(),sharing_Type,meal_Type)){
                                    db.updateItineraryData(dateDifference,roomAmt,room_ID,hotel_ID,sessionManager.getCountryCode(),sharing_Type,meal_Type);
                                }else{
                                    db.setRoomItineraryData(room_ID,hotel_ID,sessionManager.getCountryCode(),dateDifference,roomAmt,sharing_Type,meal_Type);
                                }*/
                            }catch (Exception e){
                            }

                        }
                    }catch (Exception e){

                    }
                    try {
                        convertedDate = dateFormat.parse(check_in_Date);
                        convertedDate2 = dateFormat.parse(check_out_Date);
                        if (convertedDate2.after(convertedDate)) {
                            if(db.CheckIsAvailable(hotel_ID,sessionManager.getCountryCode())){
                                //Update
                                db.updateItineraryHotelsData(Integer.parseInt(hotel_ID),check_in_Date,check_out_Date,String.valueOf(NoOfRooms),sessionManager.getAdultMainCount(),sessionManager.getChildMainCount(),String.valueOf(dateDifference), String.valueOf(roomAmt),String.valueOf(final_Cost),sessionManager.getCountryCode(),room_ID);
                            }else{
                                //Insert
                                db.setItineraryHotelsData(hotel_ID,check_in_Date,check_out_Date,String.valueOf(NoOfRooms),sessionManager.getAdultMainCount(),sessionManager.getChildMainCount(),String.valueOf(dateDifference),String.valueOf(roomAmt),String.valueOf(final_Cost),sessionManager.getCountryCode(),room_ID,"1",option);
                            }
                            Toast.makeText(mContext, "Hotel added in your itinerary", Toast.LENGTH_LONG).show();
                            finish();
                            overridePendingTransition( 0, 0);
                            startActivity(getIntent());
                            overridePendingTransition( 0, 0);
                        } else {
                            Toast.makeText(mContext, "Check-Out date older than Check-In date", Toast.LENGTH_LONG).show();
                        }
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }else {
                    getHotelValidate();
                }
            }
        });


        bt_sendItinerary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_customerDetails.setVisibility(View.VISIBLE);
                AdapterCountry adaptermarginTypelLists = new AdapterCountry(GenerateQuoteDetailsActivity.this, R.layout.spiner_country_list, R.id.title, modelmarginLists);
                sp_marginType.setAdapter(adaptermarginTypelLists);

                sp_marginType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                        String margin = modelmarginLists.get(pos).getCountryName();
                        Log.e(TAG, "mar " + margin);
                        if (margin.equalsIgnoreCase("In%")) {
                            Log.e(TAG, "In  " + margin);
                            margin_type ="1";
                            et_InAmount.setVisibility(View.GONE);
                            et_In.setVisibility(View.VISIBLE);
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                            et_In.setText("");
                            et_InAmount.setText("");
                        }
                        if (margin.equalsIgnoreCase("In Amount")) {
                            Log.e(TAG, "InA  " + margin);
                            margin_type ="2";
                            et_In.setVisibility(View.GONE);
                            et_InAmount.setVisibility(View.VISIBLE);
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                            et_In.setText("");
                            et_InAmount.setText("");
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });


            }
        });

    }

    public void Select_Option(){
        modelHotelAllLists=new ArrayList<>();
        modelHotelAllLists = db.getItineraryHotelsData(sessionManager.getCountryCode());

        selected_Option1="";selected_Option2="";selected_Option3="";
        total_hotel_cost_option1 = 0;total_hotel_cost_option2 = 0;total_hotel_cost_option3 = 0;
        flag_Option1=false; flag_Option2=false ;flag_Option3=false;
        Total_Cost=0;

        for (int i = 0; modelHotelAllLists.size() > i; i++) {

            if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("1")){
                flag_Option1=true;
            }else if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("2")){
                flag_Option2=true;
            }else if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("3")){
                flag_Option3=true;
            }else if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("0")){
                flag_Option1=true;
            }
        }
        flag_Option=true;

        if(selected_Option.equalsIgnoreCase("1")){
            mainHint="** Option 1 includes ";
        }else if(selected_Option.equalsIgnoreCase("2")){
            mainHint="** Option 2 includes ";
        }else if(selected_Option.equalsIgnoreCase("3")){
            mainHint="** Option 3 includes ";
        }

        if(flag_Option1 && flag_Option2 && flag_Option3){
            ll_option.setVisibility(View.VISIBLE);
            ll_option1.setVisibility(View.VISIBLE);
            ll_option2.setVisibility(View.VISIBLE);
            ll_option3.setVisibility(View.VISIBLE);
            flag_Option=true;
        }else if(flag_Option1 && flag_Option2 && !flag_Option3){
            ll_option.setVisibility(View.VISIBLE);
            ll_option1.setVisibility(View.VISIBLE);
            ll_option2.setVisibility(View.VISIBLE);
            flag_Option=true;
        }else if(flag_Option1 && !flag_Option2 && !flag_Option3){
            ll_option.setVisibility(View.GONE);
            for (int i = 0; modelHotelAllLists.size() > i; i++) {

                if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("0")){
                    db.updateOptionColumn(modelHotelAllLists.get(i).getHotel_id(),modelHotelAllLists.get(i).getCountry_id(),"1");
                    flag_Option=false;
                }
            }
        }else if(flag_Option1 && !flag_Option2 && flag_Option3){
            ll_option.setVisibility(View.VISIBLE);
            ll_option1.setVisibility(View.VISIBLE);
            ll_option2.setVisibility(View.VISIBLE);
            for (int i = 0; modelHotelAllLists.size() > i; i++) {

                if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("3")){
                    db.updateOptionColumn(modelHotelAllLists.get(i).getHotel_id(),modelHotelAllLists.get(i).getCountry_id(),"2");
                    flag_Option=false;
                }
            }

        }else if(!flag_Option1 && flag_Option2 && !flag_Option3){
            ll_option.setVisibility(View.GONE);
            for (int i = 0; modelHotelAllLists.size() > i; i++) {
                if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("2")){
                    db.updateOptionColumn(modelHotelAllLists.get(i).getHotel_id(),modelHotelAllLists.get(i).getCountry_id(),"1");
                    flag_Option=false;
                }
            }

        }else if(!flag_Option1 && !flag_Option2 && flag_Option3){
            ll_option.setVisibility(View.GONE);
            for (int i = 0; modelHotelAllLists.size() > i; i++) {

                if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("3")){
                    db.updateOptionColumn(modelHotelAllLists.get(i).getHotel_id(),modelHotelAllLists.get(i).getCountry_id(),"1");
                    flag_Option=false;
                }
            }

        }else if(!flag_Option1 && flag_Option2 && flag_Option3){
            ll_option.setVisibility(View.VISIBLE);
            ll_option1.setVisibility(View.VISIBLE);
            ll_option2.setVisibility(View.VISIBLE);
            for (int i = 0; modelHotelAllLists.size() > i; i++) {

                if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("2")){
                    db.updateOptionColumn(modelHotelAllLists.get(i).getHotel_id(),modelHotelAllLists.get(i).getCountry_id(),"1");
                    flag_Option=false;
                }else if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("3")){
                    db.updateOptionColumn(modelHotelAllLists.get(i).getHotel_id(),modelHotelAllLists.get(i).getCountry_id(),"2");
                    flag_Option=false;
                }
            }

        }

        if(flag_Option){

            for (int i = 0; modelHotelAllLists.size() > i; i++) {

                if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("1") || modelHotelAllLists.get(i).getOption().equalsIgnoreCase("0")){

                    if(flag_Option1){
                        total_hotel_cost_option1 = total_hotel_cost_option1 + Float.valueOf(modelHotelAllLists.get(i).getFinal_cost());

                        if(i==0){
                            String Temp = modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle();
                            selected_Option1 =selected_Option1 + Temp;
                            tv_option_Hotels.setText(mainHint+selected_Option1);
                            selected_Option1=selected_Option1+",";

                        }else{
                            if(modelHotelAllLists.size()-1 == i){
                                String Temp = modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle();
                                selected_Option1 =selected_Option1 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option1);
                            }else{
                                String Temp = modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle()+",";
                                selected_Option1 =selected_Option1 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option1);
                            }
                        }
                    }

                }

                if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("2") || modelHotelAllLists.get(i).getOption().equalsIgnoreCase("0")){

                    if(flag_Option2){
                        total_hotel_cost_option2 = total_hotel_cost_option2 + Float.valueOf(modelHotelAllLists.get(i).getFinal_cost());

                        ll_option.setVisibility(View.VISIBLE);
                        ll_option1.setVisibility(View.VISIBLE);
                        ll_option2.setVisibility(View.VISIBLE);


                        if(i==0){
                            String Temp = modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle();
                            selected_Option2 =selected_Option2 + Temp;
                            tv_option_Hotels.setText(mainHint+selected_Option2);
                            selected_Option2=selected_Option2+",";
                        }else{
                            if(modelHotelAllLists.size()-1 == i){
                                String Temp = modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle();
                                selected_Option2 =selected_Option2 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option2);
                            }else{
                                String Temp = modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle()+",";
                                selected_Option2 =selected_Option2 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option2);
                            }
                        }
                    }

                }
                if(modelHotelAllLists.get(i).getOption().equalsIgnoreCase("3") || modelHotelAllLists.get(i).getOption().equalsIgnoreCase("0")){

                    if(flag_Option3){
                        total_hotel_cost_option3 = total_hotel_cost_option3 + Float.valueOf(modelHotelAllLists.get(i).getFinal_cost());

                        ll_option.setVisibility(View.VISIBLE);
                        ll_option3.setVisibility(View.VISIBLE);



                        if(i==0){
                            String Temp = modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle();
                            selected_Option3 =selected_Option3 + Temp;
                            tv_option_Hotels.setText(mainHint+selected_Option3);
                            selected_Option3=selected_Option3+",";
                        }else{
                            if(modelHotelAllLists.size()-1 == i){
                                String Temp = modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle();
                                selected_Option3 =selected_Option3 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option3);
                            }else{
                                String Temp = modelHotelAllLists.get(i).getHotelsLists().get(0).getTitle()+",";
                                selected_Option3 =selected_Option3 + Temp;
                                tv_option_Hotels.setText(mainHint+selected_Option3);
                            }
                        }
                    }
                }
            }


            if(selected_Option.equalsIgnoreCase("1")){
                if (selected_Option1.endsWith(",")) {
                    selected_Option1 = selected_Option1.substring(0, selected_Option1.length() - 1);
                }
                tv_option_Hotels.setText(mainHint+selected_Option1);
                try {

                    List<String> items = new ArrayList<>();
                    items = Arrays.asList(sessionManager.getChildAge().replaceAll("[a-zA-Z]", "").replaceAll(" ", "").split("\\s*,\\s*"));

                    double Hotel_Cost_Adult = total_hotel_cost_option1 ;/** 80 / 100;*/
                    double Hotel_Cost_Child = total_hotel_cost_option1 ;/** 20 / 100;*/
                    if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                        tv_hotel_cost_per_Adult = (String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                        tv_hotel_cost_per_Child = (String.format("%.02f", 0.00));
                    }else{
                        double Hotel_Cost_Adults = total_hotel_cost_option1 * 80 / 100;
                        double Hotel_Cost_Childs = total_hotel_cost_option1 * 20 / 100;
                        boolean flagTemp=false;
                        int count=0;
                        for(int j = 0;items.size()>j;j++){
                            if(Integer.parseInt(items.get(j))>2){
                                count=count+1;
                                flagTemp=true;
                            }
                        }
                        if(flagTemp){
                            tv_hotel_cost_per_Adult=(String.format("%.02f", Hotel_Cost_Adults / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child=(String.format("%.02f", Hotel_Cost_Childs / count /*Float.parseFloat(sessionManager.getChildMainCount())*/));
                        }else {
                            tv_hotel_cost_per_Adult=(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child=(String.format("%.02f", 0.00));
                        }
                    }
                    Total_Cost = total_hotel_cost_option1 + total_activity_cost + total_visa_cost;
                    try{
                        tv_total_cost_per_Adult= (String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Adult) + Float.parseFloat(tv_excursion_cost_per_Adult)));
                        tv_total_cost_per_Child= (String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Child) + Float.parseFloat(tv_excursion_cost_per_Child)));
                    }catch (Exception  e){

                    }
                }catch (Exception e){

                }

                if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                    tv_travellers.setText("Travellers - "+sessionManager.getAdultMainCount() +" Adults");
                    tv_hotelAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_hotel_cost_per_Adult+" = $"+String.valueOf(String.format("%.02f", total_hotel_cost_option1)));
                    tv_activityAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_excursion_cost_per_Adult+" = $"+String.valueOf(activity_cost));
                    tv_cardAmount.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("visa_cost_adult")+" = $"+total_visa_cost);
                    tv_total_costes.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("total_cost_adult")+" = $"+String.format("%.02f", Total_Cost));
                }else{
                    tv_travellers.setText("Travellers - "+sessionManager.getAdultMainCount() +" Adults & "+sessionManager.getChildMainCount() +" Child ("+sessionManager.getChildAge()+")");
                    tv_hotelAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_hotel_cost_per_Adult + " + "+sessionManager.getChildMainCount() +"C x $"+tv_hotel_cost_per_Child+ " = $"+String.valueOf(String.format("%.02f", total_hotel_cost_option1)));
                    tv_activityAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_excursion_cost_per_Adult + " + "+sessionManager.getChildMainCount() +"C x $"+tv_excursion_cost_per_Child+ " = $"+String.valueOf(activity_cost));
                    tv_cardAmount.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("visa_cost_adult") + " + "+sessionManager.getChildMainCount() +"C x $"+getIntent().getStringExtra("visa_cost_child")+ " = $"+total_visa_cost);
                    tv_total_costes.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("total_cost_adult") + " + "+sessionManager.getChildMainCount() +"C x $"+getIntent().getStringExtra("total_cost_child")+ " = $"+String.format("%.02f", Total_Cost));
                }


                //final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));

                if(margin_type.equalsIgnoreCase("1")){
                    if(et_In.getText().length() != 0){
                        float temp = Total_Cost;
                        float per = (temp / 100) * Float.valueOf(String.valueOf(et_In.getText().toString()));
                        marginValues = String.valueOf(et_In.getText().toString());
                        temp = temp + Float.valueOf(String.valueOf(per));
                        final_Amt.setText("Your Payment Total: $"+String.format("%.02f", temp));
                        Pay_Cost = temp;
                    }else{
                        final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                        Pay_Cost = Total_Cost;
                    }
                }else{

                    Log.e("Amount lenght", String.valueOf(et_InAmount.getText().length()));
                    Log.e("Amount val", String.valueOf(et_InAmount.getText().toString()));
                    if(et_InAmount.getText().length() != 0){
                        float temp = Total_Cost;
                        temp = temp + Float.valueOf(String.valueOf(et_InAmount.getText()));
                        marginValues = String.valueOf(et_InAmount.getText());
                        final_Amt.setText("Your Payment Total: $"+String.format("%.02f", temp));
                        Pay_Cost = temp;
                    }else{
                        final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                        Pay_Cost = Total_Cost;
                    }
                }

            }else if(selected_Option.equalsIgnoreCase("2")){

                if (selected_Option2.endsWith(",")) {
                    selected_Option2 = selected_Option2.substring(0, selected_Option2.length() - 1);
                }
                tv_option_Hotels.setText(mainHint+selected_Option2);
                try {

                    List<String> items = new ArrayList<>();
                    items = Arrays.asList(sessionManager.getChildAge().replaceAll("[a-zA-Z]", "").replaceAll(" ", "").split("\\s*,\\s*"));

                    double Hotel_Cost_Adult = total_hotel_cost_option2 ;/** 80 / 100;*/
                    double Hotel_Cost_Child = total_hotel_cost_option2 ;/** 20 / 100;*/
                    if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                        tv_hotel_cost_per_Adult=(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                        tv_hotel_cost_per_Child=(String.format("%.02f", 0.00));
                    }else{
                        double Hotel_Cost_Adults = total_hotel_cost_option2 * 80 / 100;
                        double Hotel_Cost_Childs = total_hotel_cost_option2 * 20 / 100;
                        boolean flagTemp=false;
                        int count=0;
                        for(int j = 0;items.size()>j;j++){
                            if(Integer.parseInt(items.get(j))>2){
                                count=count+1;
                                flagTemp=true;
                            }
                        }
                        if(flagTemp){
                            tv_hotel_cost_per_Adult=(String.format("%.02f", Hotel_Cost_Adults / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child=(String.format("%.02f", Hotel_Cost_Childs / count /*Float.parseFloat(sessionManager.getChildMainCount())*/));
                        }else {
                            tv_hotel_cost_per_Adult=(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child=(String.format("%.02f", 0.00));
                        }
                    }
                    Total_Cost = total_hotel_cost_option2 + total_activity_cost + total_visa_cost;


                    try{
                        tv_total_cost_per_Adult= (String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Adult) + Float.parseFloat(tv_excursion_cost_per_Adult)));
                        tv_total_cost_per_Child= (String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Child) + Float.parseFloat(tv_excursion_cost_per_Child)));
                    }catch (Exception  e){

                    }

                    if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                        tv_travellers.setText("Travellers - "+sessionManager.getAdultMainCount() +" Adults");
                        tv_hotelAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_hotel_cost_per_Adult+" = $"+String.valueOf(String.format("%.02f", total_hotel_cost_option2)));
                        tv_activityAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_excursion_cost_per_Adult+" = $"+String.valueOf(activity_cost));
                        tv_cardAmount.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("visa_cost_adult")+" = $"+total_visa_cost);
                        tv_total_costes.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("total_cost_adult")+" = $"+String.format("%.02f", Total_Cost));
                    }else{
                        tv_travellers.setText("Travellers - "+sessionManager.getAdultMainCount() +" Adults & "+sessionManager.getChildMainCount() +" Child ("+sessionManager.getChildAge()+")");
                        tv_hotelAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_hotel_cost_per_Adult + " + "+sessionManager.getChildMainCount() +"C x $"+tv_hotel_cost_per_Child+ " = $"+String.valueOf(String.format("%.02f", total_hotel_cost_option2)));
                        tv_activityAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_excursion_cost_per_Adult + " + "+sessionManager.getChildMainCount() +"C x $"+tv_excursion_cost_per_Child+ " = $"+String.valueOf(activity_cost));
                        tv_cardAmount.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("visa_cost_adult") + " + "+sessionManager.getChildMainCount() +"C x $"+getIntent().getStringExtra("visa_cost_child")+ " = $"+total_visa_cost);
                        tv_total_costes.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("total_cost_adult") + " + "+sessionManager.getChildMainCount() +"C x $"+getIntent().getStringExtra("total_cost_child")+ " = $"+String.format("%.02f", Total_Cost));
                    }

                    // final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                    if(margin_type.equalsIgnoreCase("1")){
                        if(et_In.getText().length() != 0){
                            float temp = Total_Cost;
                            float per = (temp / 100) * Float.valueOf(String.valueOf(et_In.getText().toString()));
                            marginValues = String.valueOf(et_In.getText().toString());
                            temp = temp + Float.valueOf(String.valueOf(per));
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", temp));
                            Pay_Cost = temp;
                        }else{
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                            Pay_Cost = Total_Cost;
                        }
                    }else{

                        if(et_InAmount.getText().length() != 0){
                            float temp = Total_Cost;
                            temp = temp + Float.valueOf(String.valueOf(et_InAmount.getText()));
                            marginValues = String.valueOf(et_InAmount.getText());
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", temp));
                            Pay_Cost = temp;
                        }else{
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                            Pay_Cost = Total_Cost;
                        }
                    }

                }catch (Exception e){

                }

            }else if(selected_Option.equalsIgnoreCase("3")){
                if (selected_Option3.endsWith(",")) {
                    selected_Option3 = selected_Option3.substring(0, selected_Option3.length() - 1);
                }
                tv_option_Hotels.setText(mainHint+selected_Option3);
                try {

                    List<String> items = new ArrayList<>();
                    items = Arrays.asList(sessionManager.getChildAge().replaceAll("[a-zA-Z]", "").replaceAll(" ", "").split("\\s*,\\s*"));

                    double Hotel_Cost_Adult = total_hotel_cost_option3 ;/** 80 / 100;*/
                    double Hotel_Cost_Child = total_hotel_cost_option3 ;/** 20 / 100;*/
                    if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                        tv_hotel_cost_per_Adult=(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                        tv_hotel_cost_per_Child=(String.format("%.02f", 0.00));
                    }else{
                        double Hotel_Cost_Adults = total_hotel_cost_option3 * 80 / 100;
                        double Hotel_Cost_Childs = total_hotel_cost_option3 * 20 / 100;
                        boolean flagTemp=false;
                        int count=0;
                        for(int j = 0;items.size()>j;j++){
                            if(Integer.parseInt(items.get(j))>2){
                                count=count+1;
                                flagTemp=true;
                            }
                        }
                        if(flagTemp){
                            tv_hotel_cost_per_Adult=(String.format("%.02f", Hotel_Cost_Adults / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child=(String.format("%.02f", Hotel_Cost_Childs / count /*Float.parseFloat(sessionManager.getChildMainCount())*/));
                        }else {
                            tv_hotel_cost_per_Adult=(String.format("%.02f", Hotel_Cost_Adult / Float.parseFloat(sessionManager.getAdultMainCount())));
                            tv_hotel_cost_per_Child=(String.format("%.02f", 0.00));
                        }
                    }
                    Total_Cost = total_hotel_cost_option3 + total_activity_cost + total_visa_cost;


                    try{
                        tv_total_cost_per_Adult= (String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Adult) + Float.parseFloat(tv_excursion_cost_per_Adult)));
                        tv_total_cost_per_Child= (String.format("%.02f", Float.parseFloat(tv_hotel_cost_per_Child) + Float.parseFloat(tv_excursion_cost_per_Child)));
                    }catch (Exception  e){

                    }

                    if(sessionManager.getChildMainCount().equalsIgnoreCase("0")){
                        tv_travellers.setText("Travellers - "+sessionManager.getAdultMainCount() +" Adults");
                        tv_hotelAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_hotel_cost_per_Adult+" = $"+String.valueOf(String.format("%.02f", total_hotel_cost_option2)));
                        tv_activityAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_excursion_cost_per_Adult+" = $"+String.valueOf(activity_cost));
                        tv_cardAmount.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("visa_cost_adult")+" = $"+total_visa_cost);
                        tv_total_costes.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("total_cost_adult")+" = $"+String.format("%.02f", Total_Cost));
                    }else{
                        tv_travellers.setText("Travellers - "+sessionManager.getAdultMainCount() +" Adults & "+sessionManager.getChildMainCount() +" Child ("+sessionManager.getChildAge()+")");
                        tv_hotelAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_hotel_cost_per_Adult + " + "+sessionManager.getChildMainCount() +"C x $"+tv_hotel_cost_per_Child+ " = $"+String.valueOf(String.format("%.02f", total_hotel_cost_option2)));
                        tv_activityAmount.setText(sessionManager.getAdultMainCount() +"A x $"+tv_excursion_cost_per_Adult + " + "+sessionManager.getChildMainCount() +"C x $"+tv_excursion_cost_per_Child+ " = $"+String.valueOf(activity_cost));
                        tv_cardAmount.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("visa_cost_adult") + " + "+sessionManager.getChildMainCount() +"C x $"+getIntent().getStringExtra("visa_cost_child")+ " = $"+total_visa_cost);
                        tv_total_costes.setText(sessionManager.getAdultMainCount() +"A x $"+getIntent().getStringExtra("total_cost_adult") + " + "+sessionManager.getChildMainCount() +"C x $"+getIntent().getStringExtra("total_cost_child")+ " = $"+String.format("%.02f", Total_Cost));
                    }

                    //final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                    if(margin_type.equalsIgnoreCase("1")){
                        if(et_In.getText().length() != 0){
                            float temp = Total_Cost;
                            float per = (temp / 100) * Float.valueOf(String.valueOf(et_In.getText().toString()));
                            marginValues = String.valueOf(et_In.getText().toString());
                            temp = temp + Float.valueOf(String.valueOf(per));
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", temp));
                            Pay_Cost = temp;
                        }else{
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                            Pay_Cost = Total_Cost;
                        }
                    }else{

                        if(et_InAmount.getText().length() != 0){
                            float temp = Total_Cost;
                            temp = temp + Float.valueOf(String.valueOf(et_InAmount.getText()));
                            marginValues = String.valueOf(et_InAmount.getText());
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", temp));
                            Pay_Cost = temp;
                        }else{
                            final_Amt.setText("Your Payment Total: $"+String.format("%.02f", Total_Cost));
                            Pay_Cost = Total_Cost;
                        }
                    }

                }catch (Exception e){

                }
            }


            if(selected_Option2.equalsIgnoreCase("") && selected_Option3.equalsIgnoreCase("")){
                ll_option.setVisibility(View.GONE);
            }

            if(selected_Option2.equalsIgnoreCase("")){
                ll_option2.setVisibility(View.GONE);
            }else{
                ll_option2.setVisibility(View.VISIBLE);
            }
            if(selected_Option3.equalsIgnoreCase("")){
                ll_option3.setVisibility(View.GONE);
            }else{
                ll_option3.setVisibility(View.VISIBLE);
            }



        }else{
            //modelHotelAllLists=new ArrayList<>();
            //modelHotelAllLists = db.getItineraryHotelsData(sessionManager.getCountryCode());

            Select_Option();
        }



    }


    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private void getAllHotelsMaster() {

        RequestQueue queue = Volley.newRequestQueue(this);
        //http://prodapi.wayrtoo.com/Wayrtoo_Service.svc/ta/additionalhotelsmaster?country_id={COUNTRY_ID}&api_key={API_KEY}
        final String url = sessionManager.getBaseUrl() + "ta/additionalhotelsmaster?country_id="+sessionManager.getCountryCode()+"&api_key=0D1067102F935B4CC31E082BD45014D469E35268";
        Log.e("getAllHotels URL-->", url);


        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("getAllHotels Resp-->", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONObject jsonData = jsonObject.getJSONObject("data");
                            JSONObject jsonStatus = jsonData.getJSONObject("status");

                            if (jsonStatus.getString("Success").equalsIgnoreCase("true")) {
                                //modelHotelLists = db.getAllHotelNameData(sessionManager.getCountryCode());
                                modelHotelLists = new ArrayList<>();
                                JSONArray cityArray = jsonData.getJSONArray("hotels");
                                for(int i=0;cityArray.length()>i;i++){
                                    JSONObject hotelsobject = cityArray.getJSONObject(i);
                                    ModelDestinationLists hotelList = new ModelDestinationLists();
                                    hotelList.setCityId(hotelsobject.getString("id"));
                                    hotelList.setCountryName(hotelsobject.getString("name"));
                                    modelHotelLists.add(hotelList);
                                }

                                modelsourceList = new ArrayList<>();
                                JSONArray sourceArray = jsonData.getJSONArray("suppliers");
                                for(int i=0;sourceArray.length()>i;i++){
                                    JSONObject hotelsobject = sourceArray.getJSONObject(i);
                                    ModelDestinationLists hotelList = new ModelDestinationLists();
                                    hotelList.setCityId(hotelsobject.getString("id"));
                                    hotelList.setCountryName(hotelsobject.getString("name"));
                                    modelsourceList.add(hotelList);
                                }

                                modelSharingTypeList = new ArrayList<>();
                                JSONArray sharingArray = jsonData.getJSONArray("sharing_types");
                                for(int i=0;sharingArray.length()>i;i++){
                                    JSONObject hotelsobject = sharingArray.getJSONObject(i);
                                    ModelDestinationLists hotelList = new ModelDestinationLists();
                                    hotelList.setCityId(hotelsobject.getString("id"));
                                    hotelList.setCountryName(hotelsobject.getString("name"));
                                    modelSharingTypeList.add(hotelList);
                                }

                                modelMealTypeList = new ArrayList<>();
                                JSONArray mealArray = jsonData.getJSONArray("meal_types");
                                for(int i=0;mealArray.length()>i;i++){
                                    JSONObject hotelsobject = mealArray.getJSONObject(i);
                                    ModelDestinationLists hotelList = new ModelDestinationLists();
                                    hotelList.setCityId(hotelsobject.getString("id"));
                                    hotelList.setCountryName(hotelsobject.getString("name"));
                                    modelMealTypeList.add(hotelList);
                                }

                                modelRoomTypeList = new ArrayList<>();
                                JSONArray roomsArray = jsonData.getJSONArray("room_types");
                                for(int i=0;roomsArray.length()>i;i++){
                                    JSONObject hotelsobject = roomsArray.getJSONObject(i);
                                    ModelDestinationLists hotelList = new ModelDestinationLists();
                                    hotelList.setCityId(hotelsobject.getString("id"));
                                    hotelList.setCountryName(hotelsobject.getString("name"));
                                    modelRoomTypeList.add(hotelList);
                                }


                                if(modelHotelLists.size()>0&&modelHotelLists!=null){

                                    hotel_ID = modelHotelLists.get(0).getCityId();
                                    hotel_Name = modelHotelLists.get(0).getCountryName();
                                    tv_hotel_Name.setText(modelHotelLists.get(0).getCountryName());
                                }

                                if(modelsourceList.size()>0&&modelsourceList!=null){

                                    source_id = modelsourceList.get(0).getCityId();
                                    source_name = modelsourceList.get(0).getCountryName();
                                    tv_source_Name.setText(modelsourceList.get(0).getCountryName());
                                }


                                // Creating Sharing for spinner
                                AdapterCountry adapterSharingTypeList = new AdapterCountry(GenerateQuoteDetailsActivity.this, R.layout.spiner_country_list, R.id.title, modelSharingTypeList);
                                sp_sharingType.setAdapter(adapterSharingTypeList);

                                sp_sharingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                                        sharing_Type_ID = modelSharingTypeList.get(pos).getCityId();
                                        sharing_Type = modelSharingTypeList.get(pos).getCountryName();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                                // Creating meals for spinner
                                AdapterCountry adapterMealTypeList = new AdapterCountry(GenerateQuoteDetailsActivity.this, R.layout.spiner_country_list, R.id.title, modelMealTypeList);
                                sp_mealType.setAdapter(adapterMealTypeList);
                                sp_mealType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                                        meal_Type_ID = modelMealTypeList.get(pos).getCityId();
                                        meal_Type = modelMealTypeList.get(pos).getCountryName();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                    }
                                });

                                // Creating rooms for spinner
                                AdapterCountry adapterRoomTypeList = new AdapterCountry(GenerateQuoteDetailsActivity.this, R.layout.spiner_country_list, R.id.title, modelRoomTypeList);
                                sp_roomType.setAdapter(adapterRoomTypeList);

                                sp_roomType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                                        room_ID = modelRoomTypeList.get(pos).getCityId();
                                        room_Name = modelRoomTypeList.get(pos).getCountryName();
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                    }
                                });


                            } else {
                                Snackbar.make(tv_title, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }

                        } catch (JSONException e) {
                            Snackbar.make(tv_title, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar.make(tv_title, "Something went wrong. Please check your internet connection or try again later.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                }
        );

        queue.add(getRequest);
    }


    private void addHotelListShow(){
        GridLayoutManager manager1 = new GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false);
        FragmentManager fragmentManager1 = getSupportFragmentManager();
        AdapterHotelsItineraryQuotetionList adapterListHotels = new AdapterHotelsItineraryQuotetionList(activity, modelHotelAllLists, activity, fragmentManager1);
        rv_allListHotel.setLayoutManager(manager1);
        rv_allListHotel.setAdapter(adapterListHotels);
        adapterListHotels.notifyDataSetChanged();
    }

    private boolean getValidate() {
        boolean Flag = false;
        FormEditText[] fields = new FormEditText[0];
        boolean allValid = true;
        if(margin_type.equalsIgnoreCase("1")){
             fields = new FormEditText[]{et_Email, et_Name,et_In};
        }else if(margin_type.equalsIgnoreCase("2")){
             fields = new FormEditText[]{et_Email, et_Name,et_InAmount};
        }
        //FormEditText[] fields = {et_Email, et_Name};
        for (FormEditText field : fields) {
            if (field.testValidity() && allValid) {
                allValid = field.testValidity() && allValid;
                Flag = true;
            } else {
                field.requestFocus();
                allValid = false;
                Flag = false;
                break;
            }
        }

        if (allValid) {
            Flag = true;
        } else {
            Flag = false;
        }


        return Flag;
    }


    private boolean getHotelValidate() {
        boolean Flag = false;

        boolean allValid = true;
        FormEditText[] fields = {et_Amount, et_numberOfRoom};
        for (FormEditText field : fields) {
            if (field.testValidity() && allValid) {
                allValid = field.testValidity() && allValid;
                Flag = true;
            } else {
                field.requestFocus();
                allValid = false;
                Flag = false;
                break;
            }
        }

        if (allValid) {
            Flag = true;
        } else {
            Flag = false;
        }


        return Flag;
    }



    private void getRequest() {
        dialog.showCustomDialog();
        //http://prodapi.wayrtoo.com/Wayrtoo_Service.svc/ta/itinerary/create
        //https://prodapi.wayrtoo.com/Wayrtoo_Service.svc/ta/itinerary/create
        String url = sessionManager.getBaseUrl() + "ta/itinerary/create";
        Log.e("URL", "Custom Booking Create---->" + url);
        try{
            itinerary_reference_no = "TA" + sessionManager.getUserId() + "-" + sessionManager.getCountryName() + "-" + ServiceUtility.randInt(0, 999999);
            int dateDifference = 0;
            int daysCount = 0;
            int total_people=0;
            String check_in_Date = sessionManager.getCheckInDate();
            String check_out_Date = sessionManager.getCheckOutDate();
            String Nights = "",Days="";

            try {
                dateDifference = (int) getDateDiff(new SimpleDateFormat("dd-MM-yyyy"), check_in_Date, check_out_Date);
                Nights = String.valueOf(dateDifference);
                daysCount = dateDifference + 1;
                Days = String.valueOf(daysCount);
                total_people= (int) (Double.parseDouble(sessionManager.getAdultMainCount()) + Double.parseDouble(sessionManager.getChildMainCount()));
                booking_code = "TA" + sessionManager.getUserId() + "-" + sessionManager.getCountryName() + ", " +Nights +" Nights"+ ", " +Days +" Days, "+String.valueOf(total_people)+" Pax-" + ServiceUtility.randInt(0, 999999);

            }catch (Exception e){

            }
        }catch (Exception  e){
        }


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("websiteid","");
            jsonObject.put("booking_code",itinerary_reference_no);
            jsonObject.put("agent_id",new BigDecimal(sessionManager.getUserId()));
            jsonObject.put("check_in",sessionManager.getCheckInDate());
            jsonObject.put("check_out",sessionManager.getCheckOutDate());
            jsonObject.put("country_id",new BigDecimal(sessionManager.getCountryCode()));
            jsonObject.put("city", sessionManager.getCityName());
            jsonObject.put("city_id",new BigDecimal(sessionManager.getCityCode()));
            jsonObject.put("currency","USD");
            jsonObject.put("customer_email",mEmail);
            jsonObject.put("customer_name",mName);

            jsonObject.put("hotel_cost",new BigDecimal(String.format("%.02f", total_hotel_cost)));
            jsonObject.put("hotels", jsonArrayHotel);

            jsonObject.put("inclusion_cost",new BigDecimal(activity_cost));
            jsonObject.put("inclusions", jsonArrayActivity);

            jsonObject.put("is_visa_included",Boolean.parseBoolean(is_visa_included));//true and false
            jsonObject.put("itinerary_reference_no",booking_code);
            jsonObject.put("itinerary_title",itinerary_reference_no);
            jsonObject.put("margin",new BigDecimal(marginValues));
            //jsonObject.put("margin_type",new BigDecimal(margin_type));
            jsonObject.put("margin_type",margin_type);
            jsonObject.put("no_of_adults",new BigDecimal(sessionManager.getAdultCount()));
            jsonObject.put("no_of_child",new BigDecimal(sessionManager.getChildCount()));//
            jsonObject.put("child_age",sessionManager.getChildAge().replaceAll("[a-zA-Z]", "").replaceAll(" ", ""));
            jsonObject.put("status","Sent");
            jsonObject.put("total_cost",new BigDecimal(String.format("%.02f", Pay_Cost)));
            jsonObject.put("visa_cost",new BigDecimal(total_visa_cost));
            jsonObject.put("visa_type",visa_type);
            jsonObject.put("hotel_cost_adult",new BigDecimal(getIntent().getStringExtra("hotel_cost_adult")));
            jsonObject.put("hotel_cost_child",new BigDecimal(getIntent().getStringExtra("hotel_cost_child")));
            jsonObject.put("inclusion_cost_adult",new BigDecimal(getIntent().getStringExtra("inclusion_cost_adult")));
            jsonObject.put("inclusion_cost_child",new BigDecimal(getIntent().getStringExtra("inclusion_cost_child")));
            jsonObject.put("total_cost_adult",new BigDecimal(getIntent().getStringExtra("total_cost_adult")));
            jsonObject.put("total_cost_child",new BigDecimal(getIntent().getStringExtra("total_cost_child")));
            jsonObject.put("save_status","");//travel_agent_id
            jsonObject.put("travel_agent_id",new BigDecimal("0"));//travel_agent_id


        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e("API Parameters ", "Para---->  " + jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.hideCustomeDialog();
                        Log.e("Succecss response ", "Status---->  " + response.toString());
                        String Status_message;
                        try {
                            /*{"Status_message":"Itinerary saved successfully!","Success":true,"status_code":200}*/
                            JSONObject jsonObject = new JSONObject(response.toString());
                            //JSONObject jsonStatus = jsonObject.getJSONObject("status");

                            Status_message = jsonObject.getString("Status_message");
                            if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {

                                Toast.makeText(GenerateQuoteDetailsActivity.this, Status_message, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(GenerateQuoteDetailsActivity.this, ActivityPackageConfirmation.class);
                                intent.putExtra("Amount", String.valueOf(String.format("%.02f", total_hotel_cost)));
                                intent.putExtra("Email", mEmail);
                                intent.putExtra("itinerary_reference_no", itinerary_reference_no);
                                intent.putExtra("Name", mName);
                                intent.putExtra("Currency", mCurrency);
                                intent.putExtra("Total_Cost", String.valueOf(Total_Cost));
                                startActivity(intent);

                            }else{
                                Toast.makeText(GenerateQuoteDetailsActivity.this, Status_message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //OnError();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.hideCustomeDialog();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(mContext, "Unable to connect web service.", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(mContext, "Please Check Network Connectivity", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("X-Authorization", "0D1067102F935B4CC31E082BD45014D469E35268");
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20*3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(mContext);
        queue.add(request);

    }


    public void deleteHotel(String hotel_id,String country_code,ModelItineraryHotelList model){
        try{
            db.DeleteHotelItinery(hotel_id,country_code);

            Log.e("Total Room Id",model.getHotelsTotalRooms());
            List<String> items = Arrays.asList(model.getHotelsTotalRooms().split("\\s*,\\s*"));
            for(int j=0;items.size()>j;j++){
                try{
                    Log.e("Room number",items.get(j).toString());
                    db.DeleteRoomItineraryData(items.get(j).toString(),hotel_id,country_code);
                }catch (Exception e){}

            }

            Toast.makeText(mContext, "Hotel deleted successfully", Toast.LENGTH_LONG).show();
            finish();
            overridePendingTransition( 0, 0);
            startActivity(getIntent());
            overridePendingTransition( 0, 0);
        }catch (Exception e){}

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(GenerateQuoteDetailsActivity.this,ActivityItinerary.class);
        startActivity(intent);
        finish();
    }
}
