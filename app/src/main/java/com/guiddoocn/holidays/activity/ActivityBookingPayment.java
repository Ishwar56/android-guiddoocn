package com.guiddoocn.holidays.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterCancellationPolicy;
import com.guiddoocn.holidays.models.ModelActivitiesCancellatioPolicy;
import com.guiddoocn.holidays.models.ModelActivitiesDetails;
import com.guiddoocn.holidays.models.ModelMyBooking;
import com.guiddoocn.holidays.utils.AvenuesParams;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.GPSTracker;
import com.guiddoocn.holidays.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityBookingPayment extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView tv_back;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_image)
    ImageView iv_image;

    @BindView(R.id.tv_city)
    TextView tv_city;

    @BindView(R.id.cv_discount)
    CardView cv_discount;

    @BindView(R.id.tv_discount)
    TextView tv_discount;

    @BindView(R.id.tv_name)
    TextView tv_event_name;

    @BindView(R.id.tv_duration)
    TextView tv_duration;

    @BindView(R.id.rb_rating)
    RatingBar rb_rating;

    @BindView(R.id.tv_price)
    TextView tv_price;

    @BindView(R.id.tv_old_price)
    TextView tv_old_price;

    @BindView(R.id.tv_date_time)
    TextView tv_date_time;

    @BindView(R.id.tv_no_of_people_title)
    TextView tv_no_of_people_title;


    @BindView(R.id.tv_no_of_people)
    TextView tv_no_of_people;

    @BindView(R.id.tv_total)
    TextView tv_total;//

    @BindView(R.id.fetFullName)
    FormEditText fetFullName;

    @BindView(R.id.fetEmailAddress)
    FormEditText fetEmailAddress;

    @BindView(R.id.fetMobile)
    FormEditText fetMobile;

    @BindView(R.id.tv_final_amount)
    TextView tv_final_amount;

    @BindView(R.id.rd_group)
    RadioGroup rd_group;

    @BindView(R.id.rb_razorpay)
    RadioButton rb_razorpay;

    @BindView(R.id.rb_ccavenue)
    RadioButton rb_ccavenue;

    @BindView(R.id.rb_crypto)
    RadioButton rb_crypto;//

    @BindView(R.id.ll_ccavenue)
    LinearLayout ll_ccavenue;

    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;

    private Context mContext;
    private LocationManager locationManager;
    private String Select_Gender, checkInDate, checkInTime, Amount, noOfAdult, noOfChild, noOfInfant, noOfPerson, Currency, tourId, tourName, USDAmount, payment_Type = "0";
    private SessionManager sessionManager;
    private GPSTracker gpsTracker;
    private ArrayList<ModelActivitiesCancellatioPolicy> cancellatioPolicyModels;
    private ArrayList<ModelActivitiesDetails> activitiesDetailstModels;
    private boolean FlagPayment = false,PaymentGatway = true;
    private ArrayList<ModelMyBooking> arrayModelMyBookings = new ArrayList<>();
    private int Final_Amount;
    private View v;
    private SQLiteHandler db;
    private double Starting_price;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_payment);

        setSupportActionBar(toolbar);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        mContext = this;
        ButterKnife.bind(this);
        db = new SQLiteHandler(mContext);
        sessionManager = new SessionManager(mContext);
        v = findViewById(R.id.tv_title);
        gpsTracker = new GPSTracker(mContext, ActivityBookingPayment.this);
        Bundle b = getIntent().getExtras();

        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }


        FlagPayment = true;
        PaymentGatway = true;
        ll_ccavenue.setVisibility(View.VISIBLE);
        payment_Type = "2";

        if (b != null) {

            tv_date_time.setText(getIntent().getStringExtra("Date") + " " + getIntent().getStringExtra("Time"));
            activitiesDetailstModels = (ArrayList<ModelActivitiesDetails>) getIntent().getSerializableExtra("ListDetailModels");
            // tv_duration.setText("Duration : " + getIntent().getStringExtra("Duration").replace("Duration -",""));
            if (getIntent().getStringExtra("No_of_Unit").equalsIgnoreCase("0")) {
                tv_no_of_people_title.setText("No of People");

                if (getIntent().getStringExtra("No_of_Child").equalsIgnoreCase("0")) {
                    if (getIntent().getStringExtra("No_of_Infant").equalsIgnoreCase("0")) {
                        tv_no_of_people.setText(getIntent().getStringExtra("No_of_Adult") + " Adult");
                    } else {
                        tv_no_of_people.setText(getIntent().getStringExtra("No_of_Adult") + " Adult & " + getIntent().getStringExtra("No_of_Infant") + " Infant");
                    }

                } else if (getIntent().getStringExtra("No_of_Infant").equalsIgnoreCase("0")) {
                    if (getIntent().getStringExtra("No_of_Child").equalsIgnoreCase("0")) {
                        tv_no_of_people.setText(getIntent().getStringExtra("No_of_Adult") + " Adult");
                    } else {
                        tv_no_of_people.setText(getIntent().getStringExtra("No_of_Adult") + " Adult & " + getIntent().getStringExtra("No_of_Child") + " Child");
                    }

                } else {
                    tv_no_of_people.setText(getIntent().getStringExtra("No_of_Adult") + " Adult & " + getIntent().getStringExtra("No_of_Child") + " Child &" + getIntent().getStringExtra("No_of_Infant") + " Infant");
                }

            }else{
                tv_no_of_people_title.setText("No of Unit");
                tv_no_of_people.setText(getIntent().getStringExtra("No_of_Unit") + " Unit(s)");
            }

            tv_event_name.setText(activitiesDetailstModels.get(0).getName());
            rb_rating.setRating(Float.parseFloat(activitiesDetailstModels.get(0).getRating()));
            //holder.rb_rating.setRating(Float.parseFloat("2"));
            if (activitiesDetailstModels.get(0).getDay().equalsIgnoreCase("00")) {
                tv_duration.setText("Duration - " + activitiesDetailstModels.get(0).getHour() + " hr " + activitiesDetailstModels.get(0).getMin() + " min");
            } else {
                tv_duration.setText("Duration - " + activitiesDetailstModels.get(0).getDay() + " day " + activitiesDetailstModels.get(0).getHour() + " hr " + activitiesDetailstModels.get(0).getMin() + " min");
            }

            tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + activitiesDetailstModels.get(0).getStarting_from_price());

            try{
                Glide.with(mContext)
                        .load(activitiesDetailstModels.get(0).getFeatured_image())
                        .placeholder(R.drawable.place_holder)
                        .error(R.drawable.place_holder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .animate(R.anim.fade_in)
                        .into(iv_image);
            }catch (Exception e){
                Glide.with(mContext)
                        .load(R.drawable.place_holder)
                        .placeholder(R.drawable.place_holder)
                        .error(R.drawable.place_holder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .animate(R.anim.fade_in)
                        .into(iv_image);
            }

            tv_city.setText(activitiesDetailstModels.get(0).getCity());

            Starting_price = Double.parseDouble(activitiesDetailstModels.get(0).getStarting_from_price());

            if (activitiesDetailstModels.get(0).getDiscount() == null || "".equalsIgnoreCase(activitiesDetailstModels.get(0).getDiscount()) || "0".equalsIgnoreCase(activitiesDetailstModels.get(0).getDiscount()) || activitiesDetailstModels.get(0).getDiscount().isEmpty()) {
                cv_discount.setVisibility(View.GONE);
                tv_old_price.setVisibility(View.GONE);
                sessionManager.setNewprice("0");

                try {
                    int intpart = (int) Starting_price;
                    float decpart = (float) (Starting_price - intpart);
                    if (decpart == 0.0f) {
                        tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + activitiesDetailstModels.get(0).getStarting_from_price());
                    } else {
                        tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f", Starting_price));
                    }
                } catch (Exception e) {
                    tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + activitiesDetailstModels.get(0).getStarting_from_price());

                }

            } else {

                try{
                    cv_discount.setVisibility(View.VISIBLE);
                    tv_old_price.setVisibility(View.VISIBLE);
                    Double Temp = Double.parseDouble(activitiesDetailstModels.get(0).getStarting_from_price()) * Double.parseDouble(activitiesDetailstModels.get(0).getDiscount()) /100;
                    Double Amt = Temp + Double.parseDouble(activitiesDetailstModels.get(0).getStarting_from_price());
                    tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                    tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Amt));
                    tv_discount.setText(Math.round(Float.parseFloat(activitiesDetailstModels.get(0).getDiscount())) + "% Off ");
                    tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                    sessionManager.setNewprice(String.valueOf(Starting_price));
                }catch (Exception e){

                }
                /*try{

                    double discount =  Double.parseDouble(activitiesDetailstModels.get(0).getDiscount());
                    double discount_cap = Double.parseDouble(activitiesDetailstModels.get(0).getDiscount_cap());
                    double Old_price = Double.parseDouble(activitiesDetailstModels.get(0).getStarting_from_price());
                    double New_price = Old_price -(Old_price/100 * discount) ;

                    double Difference = Old_price - New_price;
                    Log.e("Old Price-->",String.valueOf(Old_price));
                    Log.e("New Price-->",String.valueOf(New_price));
                    Log.e("New Price vv-->",String.valueOf(Math.round(New_price)));
                    Log.e("Difference vv-->",String.valueOf(String.format("%.2f",New_price)));
                    Log.e("Difference-->",String.valueOf(Difference));
                    Log.e("Difference vv-->",String.valueOf(String.format("%.2f",Difference)));
                    Log.e("discount_cap-->",String.valueOf(discount_cap));
                    Log.e("discount_cap-->","---------------------------------");

                    if(discount_cap > 0 ){

                        if(discount_cap < Difference){
                            Double Discount_Cap_Price = Old_price - discount_cap;
                            tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + Discount_Cap_Price);
                            tv_discount.setText(Math.round(discount) + "% Off ");
                            tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                            tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                            sessionManager.setNewprice(String.valueOf(Discount_Cap_Price));

                        }else{
                            tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",New_price));
                            tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                            tv_discount.setText(Math.round(discount) + "% Off ");
                            tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        }
                    }else{
                        tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",New_price));
                        tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                        tv_discount.setText(Math.round(discount) + "% Off ");
                        tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                    }
                }catch(Exception e){

                }*/

            }



            tv_total.setText(getIntent().getStringExtra("Total_Cost"));
            tv_final_amount.setText(getIntent().getStringExtra("Total_Cost"));
            cancellatioPolicyModels = (ArrayList<ModelActivitiesCancellatioPolicy>) getIntent().getSerializableExtra("CancellationPolicyModels");

            checkInDate = getIntent().getStringExtra("Date");
            checkInTime = getIntent().getStringExtra("Time");
            Amount = getIntent().getStringExtra("Amount");
            Currency = getIntent().getStringExtra("Currency");
            tourId = getIntent().getStringExtra("tourId");
            tourName = getIntent().getStringExtra("tourName");
            noOfAdult = getIntent().getStringExtra("No_of_Adult");
            noOfChild = getIntent().getStringExtra("No_of_Child");
            noOfInfant = getIntent().getStringExtra("No_of_Infant");
            noOfPerson = getIntent().getStringExtra("No_of_Unit");
            /*int total = Integer.valueOf(noOfAdult) + Integer.valueOf(noOfChild);
            noOfPerson = String.valueOf(total);
           */

            //getCurrencyConvertRequest();

            Log.e("All Data", "---------->Date:" + checkInDate + " Time:" + checkInTime + " Amount:" + Amount + " Currency:" + Currency + " Tour ID:" + tourId + " tour Name:" + tourName + " no Of Adult:" + noOfAdult + " no of Child:" + noOfChild + " No of Person:" + noOfPerson);

        } else {
            Snackbar.make(findViewById(R.id.tv_title), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
        }

        if (sessionManager.getLogedIn()) {
            fetFullName.setText(sessionManager.getUserName());
            fetEmailAddress.setText(sessionManager.getEmailId());
            fetMobile.setText(sessionManager.getUserMobileNo());
        }

        tv_title.setText("Booking Details");

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



       /* rd_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton rb = (RadioButton) radioGroup.findViewById(checkedId);
                Toast.makeText(ActivityBookingPayment.this, rb.getText(), Toast.LENGTH_LONG).show();
            }
            });*/


       if(db.getCurrencyValue(Currency)!=null){
           Double usd_value = Double.valueOf(db.getCurrencyValue(Currency));
           Double current_Amount = Double.valueOf(Amount);
           Log.e("Currency_value", String.valueOf(usd_value));
           Log.e("current_Amount", String.valueOf(current_Amount));
           USDAmount = String.valueOf(String.format("%.2f", usd_value * current_Amount));
           Log.e("Convert Amount", USDAmount);
       }else {
           Currency = getIntent().getStringExtra("Currency");
           USDAmount=Amount;
       }


        rb_razorpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getCurrencyConvertRequest();
                FlagPayment = true;
                PaymentGatway = true;
                ll_ccavenue.setVisibility(View.VISIBLE);

                payment_Type = "2";
            }
        });

        rb_ccavenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_final_amount.setText(activitiesDetailstModels.get(0).getCurrency() + " " + getIntent().getStringExtra("Amount"));
                FlagPayment = true;
                PaymentGatway = false;
                ll_ccavenue.setVisibility(View.VISIBLE);

                payment_Type = "0";
            }
        });

        rb_crypto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_final_amount.setText("USD " + USDAmount);
                FlagPayment = false;
                ll_ccavenue.setVisibility(View.GONE);

                payment_Type = "1";
            }
        });


    }

    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tv_cancellation_policy:

                if (cancellatioPolicyModels.size() > 0) {
                    final Dialog dialogMsg = new Dialog(ActivityBookingPayment.this);
                    dialogMsg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogMsg.setContentView(R.layout.dialog_event_detail_cancellation_policy);
                    dialogMsg.setCancelable(true);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialogMsg.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.gravity = Gravity.CENTER;
                    dialogMsg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogMsg.getWindow().setAttributes(lp);
                    dialogMsg.show();

                    RecyclerView rv_list = (RecyclerView) dialogMsg.findViewById(R.id.rv_list);
                    ImageView cardViewCancel = (ImageView) dialogMsg.findViewById(R.id.imageView_close);
                    rv_list.setNestedScrollingEnabled(false);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                    rv_list.setLayoutManager(mLayoutManager);
                    AdapterCancellationPolicy cancelAdapter = new AdapterCancellationPolicy(getApplicationContext(), cancellatioPolicyModels);
                    rv_list.setAdapter(cancelAdapter);

                    cardViewCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.cancel();
                        }
                    });

                } else {
                    Snackbar.make(findViewById(R.id.linear_layout_container), "Cancellation policy record not found. Please try again later.", Snackbar.LENGTH_LONG).show();
                }
                break;

            case R.id.pay_now:

                boolean allValid = true;
                FormEditText[] fields = {fetFullName, fetEmailAddress, fetMobile};
                for (FormEditText field : fields) {
                    if (field.testValidity() && allValid) {
                        allValid = field.testValidity() && allValid;
                    } else {
                        field.requestFocus();
                        allValid = false;
                        break;
                    }
                }
                if (allValid) {



                    if(fetMobile.getText().toString().length()< 6 || fetMobile.getText().toString().length()>15 ){
                        fetMobile.setError("Invalid mobile number");
                        fetMobile.requestFocus();
                    } else {

                        sessionManager.setUserMobile(fetMobile.getText().toString());

                        if (FlagPayment ) {

                            ModelMyBooking trip = new ModelMyBooking();

                            trip.setActivity_id(tourId);
                            trip.setActivity_name(tourName);
                            trip.setActivity_date(checkInDate);
                            trip.setActivity_time(checkInTime);
                            trip.setTotal_amount(Amount);
                            trip.setActual_currency(Currency);
                            trip.setNo_of_adult(noOfAdult);
                            trip.setNo_of_child(noOfChild);
                            trip.setNo_of_infant("0");
                            trip.setCart_id("0");
                            trip.setActivity_image(activitiesDetailstModels.get(0).getFeatured_image());
                            trip.setNo_of_units(noOfPerson);

                            arrayModelMyBookings.add(trip);


                            Intent i = new Intent(ActivityBookingPayment.this, ActivityMainRazorPayPayment.class);
                            i.putExtra(AvenuesParams.BILLING_NAME, fetFullName.getText().toString());
                            i.putExtra(AvenuesParams.BILLING_ADDRESS, gpsTracker.getFullAddress());
                            i.putExtra(AvenuesParams.BILLING_TEL, fetMobile.getText().toString());
                            i.putExtra(AvenuesParams.DELIVERY_TEL, fetMobile.getText().toString());
                            i.putExtra(AvenuesParams.BILLING_ZIP, gpsTracker.getPostalCode());
                            i.putExtra(AvenuesParams.DELIVERY_ZIP, gpsTracker.getPostalCode());
                            i.putExtra(AvenuesParams.BILLING_CITY, gpsTracker.getCityName());
                            i.putExtra(AvenuesParams.DELIVERY_CITY, gpsTracker.getCityName());
                            i.putExtra(AvenuesParams.DELIVERY_STATE, gpsTracker.getState());
                            i.putExtra(AvenuesParams.BILLING_STATE, gpsTracker.getState());
                            i.putExtra(AvenuesParams.DELIVERY_COUNTRY, gpsTracker.getCountyName());
                            i.putExtra(AvenuesParams.BILLING_COUNTRY, gpsTracker.getCountyName());
                            i.putExtra(AvenuesParams.BILLING_EMAIL, fetEmailAddress.getText().toString());
                            i.putExtra(AvenuesParams.DELIVERY_NAME, fetFullName.getText().toString());
                            i.putExtra(AvenuesParams.DELIVERY_ADDRESS, gpsTracker.getFullAddress());
                            i.putExtra("amount", String.valueOf(USDAmount) );
                            i.putExtra("curruncy", Currency);
                            i.putExtra("payment_Type", payment_Type);
                            i.putExtra("payment_way","0");
                            i.putExtra("CartListDetailModels", (ArrayList<ModelMyBooking>) arrayModelMyBookings);
                            startActivity(i);
                            arrayModelMyBookings.clear();
                        }
                        
                    }
                }
                break;


        }
    }

    private void getCurrencyConvertRequest() {

        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = "http://free.currencyconverterapi.com/api/v5/convert?q=USD_INR&compact=y";

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());
                        try {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.toString());
                                JSONObject jsonData = jsonObject.getJSONObject("USD_INR");
                                Double INR = Double.valueOf(jsonData.getString("val"));
                                String INRAmount = String.format("%.2f", INR);
                                Log.e("INR Amount--->",INRAmount);
                                Double roundVal = Double.valueOf(INRAmount);

                                Double Total_Cost = Double.parseDouble(activitiesDetailstModels.get(0).getStarting_from_price());
                                Double Amount =roundVal * Total_Cost;
                                Final_Amount = (int)Math.round(Amount);
                                Log.e("Total_Cost Amount--->", String.valueOf(Total_Cost));
                                Log.e("Final INR Amount --->", String.valueOf(Final_Amount));
                                if( FlagPayment && PaymentGatway){
                                    tv_final_amount.setText("INR "+String.valueOf(Final_Amount));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar.make(v, "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
                        //getCurrencyConvertRequest();
                    }
                }
        );

        queue.add(getRequest);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            gpsTracker.showSettingsAlert(ActivityBookingPayment.this);

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
