package com.guiddoocn.holidays.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.adapter.AdapterBookingTiming;
import com.guiddoocn.holidays.models.ModelActivitiesCancellatioPolicy;
import com.guiddoocn.holidays.models.ModelActivitiesDetails;
import com.guiddoocn.holidays.models.ModelPriceMain;
import com.guiddoocn.holidays.models.ModelPricesDetails;
import com.guiddoocn.holidays.models.ModelTiming;
import com.guiddoocn.holidays.utils.CustomResponseDialog;
import com.guiddoocn.holidays.utils.FontsOverride;
import com.guiddoocn.holidays.utils.GPSTracker;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityBookingPricing extends AppCompatActivity {

    @BindView(R.id.iv_back)
    ImageView tv_back;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.iv_image)
    ImageView iv_image;

    @BindView(R.id.tv_city)
    TextView tv_city;

    @BindView(R.id.cv_discount)
    CardView cv_discount;

    @BindView(R.id.tv_discount)
    TextView tv_discount;

    @BindView(R.id.tv_name)
    TextView tv_event_name;

    @BindView(R.id.tv_duration)
    TextView tv_duration;

    @BindView(R.id.rb_rating)
    RatingBar rb_rating;

    @BindView(R.id.tv_price)
    TextView tv_price;

    @BindView(R.id.tv_old_price)
    TextView tv_old_price;

    @BindView(R.id.btn_Pay_Now)
    CardView cardViewBooking;

    @BindView(R.id.tv_card_name)
    TextView tv_card_name;

    @BindView(R.id.book_nows)
    LinearLayout book_now;

    private String Event_ID, BookingFlag;
    private Context mContext;
    private SessionManager sessionManager;
    private GPSTracker gpsTracker;
    private CustomResponseDialog customResponseDialog;
    private ArrayList<ModelActivitiesDetails> activitiesDetailstModels;
    private ArrayList<ModelPriceMain> pricingDetailsModels;
    private ArrayList<ModelPricesDetails> pricesDetailsArrayList;
    private ArrayList<ModelActivitiesCancellatioPolicy> cancellatioPolicyModels;
    private ArrayList<ModelTiming> modelTimings;
    private AdapterBookingTiming activitiesListAdapter;
    private boolean setflagTiming,Flag=true,FlagResponce=false,FlagPermissionFlow = true;
    private String[] AppPermissions = {Manifest.permission.ACCESS_FINE_LOCATION};
    private View v;
    private double Starting_price;
    private LocationManager locationManager;

    private double latitude, longitude, Adult_Total_Val = 0.0, Child_Total_Val = 0.0, Infant_Total_Val = 0.0, Final_Cost = 0.0;
    private int total_count = 1, unit_count = 0, unit_minus = 0, adult_no = 1, child_no = 0, infant_no = 0,No_of_Unit_Count;

    private String Selected_Date, Selected_Time;
    private TextView tv_date_time, tv_time, tv_adult_price, tv_adult_minus, tv_adult_number, tv_adult_plus, tv_child_price, tv_child_minus, tv_child_number, tv_child_plus,
            tv_final_cost, tv_infant_price, tv_infant_minus, tv_infant_number, tv_infant_plus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_pricing);
        mContext = this;
        ButterKnife.bind(this);
        sessionManager = new SessionManager(mContext);
        customResponseDialog = new CustomResponseDialog(mContext);
        gpsTracker = new GPSTracker(mContext, this);
        v = findViewById(R.id.ll_total);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            FontsOverride.setDefaultFont(mContext, "SERIF", "font/proxima_nova_reg.ttf");
        } catch (NullPointerException e) {

        }

        Bundle b = getIntent().getExtras();
        try{
            if (b != null) {
                BookingFlag = getIntent().getStringExtra("BookingFlag");
                Event_ID = getIntent().getStringExtra("EventID");
                tv_title.setText(getIntent().getStringExtra("EventName"));
                Selected_Date = getIntent().getStringExtra("Selected_Date");
                Selected_Time = getIntent().getStringExtra("Selected_Time");
                cancellatioPolicyModels = (ArrayList<ModelActivitiesCancellatioPolicy>) getIntent().getSerializableExtra("CancellationPolicyModels");
                activitiesDetailstModels = (ArrayList<ModelActivitiesDetails>) getIntent().getSerializableExtra("ListDetailModels");
                pricingDetailsModels = (ArrayList<ModelPriceMain>) getIntent().getSerializableExtra("ModelPriceMain");
                pricesDetailsArrayList = (ArrayList<ModelPricesDetails>) getIntent().getSerializableExtra("pricesDetails");

                tv_event_name.setText(activitiesDetailstModels.get(0).getName());
                rb_rating.setRating(Float.parseFloat(activitiesDetailstModels.get(0).getRating()));
                //holder.rb_rating.setRating(Float.parseFloat("2"));

                tv_card_name.setText("Proceed");

                if (activitiesDetailstModels.get(0).getDay().equalsIgnoreCase("00")) {
                    tv_duration.setText("Duration - " + activitiesDetailstModels.get(0).getHour() + " hr " + activitiesDetailstModels.get(0).getMin() + " min");
                } else {
                    tv_duration.setText("Duration - " + activitiesDetailstModels.get(0).getDay() + " day " + activitiesDetailstModels.get(0).getHour() + " hr " + activitiesDetailstModels.get(0).getMin() + " min");
                }

                tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + activitiesDetailstModels.get(0).getStarting_from_price());

                Glide.with(mContext)
                        .load(activitiesDetailstModels.get(0).getFeatured_image())
                        .placeholder(R.drawable.place_holder)
                        .error(R.drawable.place_holder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .animate(R.anim.fade_in)
                        .into(iv_image);

                tv_city.setText(activitiesDetailstModels.get(0).getCity());

                Starting_price = Double.parseDouble(activitiesDetailstModels.get(0).getStarting_from_price());

                if (activitiesDetailstModels.get(0).getDiscount() == null || "".equalsIgnoreCase(activitiesDetailstModels.get(0).getDiscount()) || "0".equalsIgnoreCase(activitiesDetailstModels.get(0).getDiscount()) || activitiesDetailstModels.get(0).getDiscount().isEmpty()) {
                    cv_discount.setVisibility(View.GONE);
                    tv_old_price.setVisibility(View.GONE);

                    try {
                        int intpart = (int) Starting_price;
                        float decpart = (float) (Starting_price - intpart);
                        if (decpart == 0.0f) {
                            tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + activitiesDetailstModels.get(0).getStarting_from_price());
                        } else {
                            tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f", Starting_price));
                        }
                    } catch (Exception e) {
                        tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + activitiesDetailstModels.get(0).getStarting_from_price());

                    }

                } else {

                    try{
                        cv_discount.setVisibility(View.VISIBLE);
                        tv_old_price.setVisibility(View.VISIBLE);
                        Double Temp = Double.parseDouble(activitiesDetailstModels.get(0).getStarting_from_price()) * Double.parseDouble(activitiesDetailstModels.get(0).getDiscount()) /100;
                        Double Amt = Temp + Double.parseDouble(activitiesDetailstModels.get(0).getStarting_from_price());
                        tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                        tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Amt));
                        tv_discount.setText(Math.round(Float.parseFloat(activitiesDetailstModels.get(0).getDiscount())) + "% Off ");
                        tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        sessionManager.setNewprice(String.valueOf(Starting_price));
                    }catch (Exception e){

                    }

                    /*try{

                        double discount =  Double.parseDouble(activitiesDetailstModels.get(0).getDiscount());
                        double discount_cap = Double.parseDouble(activitiesDetailstModels.get(0).getDiscount_cap());
                        double Old_price = Double.parseDouble(activitiesDetailstModels.get(0).getStarting_from_price());
                        double New_price = Old_price -(Old_price/100 * discount) ;

                        double Difference = Old_price - New_price;

                        if(discount_cap > 0 ){

                            if(discount_cap < Difference){
                                Double Discount_Cap_Price = Old_price - discount_cap;
                                tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + Discount_Cap_Price);
                                tv_discount.setText(Math.round(discount) + "% Off ");
                                tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                                tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                                sessionManager.setNewprice(String.valueOf(Discount_Cap_Price));
                                activitiesDetailstModels.get(0).setOld_price(String.valueOf(Old_price));

                            }else{
                                tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",New_price));
                                tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                                tv_discount.setText(Math.round(discount) + "% Off ");
                                tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                                activitiesDetailstModels.get(0).setNew_price(String.valueOf(New_price));
                                activitiesDetailstModels.get(0).setOld_price(String.valueOf(Old_price));
                            }
                        }else{
                            tv_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",New_price));
                            tv_old_price.setText(activitiesDetailstModels.get(0).getCurrency() + " " + String.format("%.2f",Starting_price));
                            tv_discount.setText(Math.round(discount) + "% Off ");
                            tv_old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                            activitiesDetailstModels.get(0).setNew_price(String.valueOf(New_price));
                            activitiesDetailstModels.get(0).setOld_price(String.valueOf(Old_price));
                        }
                    }catch(Exception e){

                    }*/
                }

                tv_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       onBackPressed();
                    }
                });

                final LinearLayout ll_adult = (LinearLayout) findViewById(R.id.ll_adult);
                final LinearLayout ll_child = (LinearLayout) findViewById(R.id.ll_child);
                final LinearLayout ll_infant = (LinearLayout) findViewById(R.id.ll_infant);
                final LinearLayout ll_select_date = (LinearLayout) findViewById(R.id.ll_select_date);

                tv_date_time = (TextView) findViewById(R.id.tv_date_time);
                tv_time = (TextView) findViewById(R.id.tv_time);
                tv_duration = (TextView) findViewById(R.id.tv_duration);

                tv_adult_price = (TextView) findViewById(R.id.tv_adult_price);
                tv_adult_minus = (TextView) findViewById(R.id.tv_adult_minus);
                tv_adult_number = (TextView) findViewById(R.id.tv_adult_number);
                tv_adult_plus = (TextView) findViewById(R.id.tv_adult_plus);

                tv_child_price = (TextView) findViewById(R.id.tv_child_price);
                tv_child_minus = (TextView) findViewById(R.id.tv_child_minus);
                tv_child_number = (TextView) findViewById(R.id.tv_child_number);
                tv_child_plus = (TextView) findViewById(R.id.tv_child_plus);
                tv_final_cost = (TextView) findViewById(R.id.tv_final_cost);

                tv_infant_price = (TextView) findViewById(R.id.tv_infant_price);
                tv_infant_minus = (TextView) findViewById(R.id.tv_infant_minus);
                tv_infant_number = (TextView) findViewById(R.id.tv_infant_number);
                tv_infant_plus = (TextView) findViewById(R.id.tv_infant_plus);


                tv_date_time.setText(Selected_Date);
                tv_time.setText(Selected_Time);


               // pricesDetailsArrayList = pricingDetailsModels.get(0).getPricesArrayList().get(0).getPricingDetails();


                if (pricingDetailsModels.size() > 0) {
                    adult_no = Integer.parseInt(pricesDetailsArrayList.get(0).getFrom_pax_count());
                    tv_adult_number.setText(String.valueOf(adult_no));
                    if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                        child_no = 0;
                        infant_no = 0;
                        if (pricesDetailsArrayList.get(0).getAdult_price().equalsIgnoreCase("0")) {
                            ll_adult.setVisibility(View.GONE);
                        } else {
                            tv_adult_price.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price() + " per head");
                        }

                        if (pricesDetailsArrayList.get(0).getChild_price().equalsIgnoreCase("0")) {
                            ll_child.setVisibility(View.GONE);
                        } else {
                            tv_child_price.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getChild_price() + " per head");
                        }

                        if (pricesDetailsArrayList.get(0).getInfant_price().equalsIgnoreCase("0")) {
                            ll_infant.setVisibility(View.GONE);
                        } else {
                            tv_infant_price.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getInfant_price() + " per head");
                            ll_infant.setVisibility(View.GONE);

                        }
                    } else {
                       // adult_no = Integer.parseInt(activitiesDetailstModels.get(0).getMinimum_pax());

                        unit_count = Integer.parseInt(activitiesDetailstModels.get(0).getMaximum_pax());
                        tv_adult_number.setText(String.valueOf(adult_no));
                        if (pricesDetailsArrayList.get(0).getAdult_price().equalsIgnoreCase("0")) {
                            ll_adult.setVisibility(View.GONE);
                        } else {
                            tv_adult_price.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price() + " maximum " + unit_count + " person");
                        }

                        if (pricesDetailsArrayList.get(0).getChild_price().equalsIgnoreCase("0")) {
                            ll_child.setVisibility(View.GONE);
                        } else {
                            tv_child_price.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getChild_price() + " maximum " + unit_count + " person");
                        }

                        if (pricesDetailsArrayList.get(0).getInfant_price().equalsIgnoreCase("0")) {
                            ll_infant.setVisibility(View.GONE);
                        } else {
                            tv_infant_price.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getInfant_price() + " per head");
                            ll_infant.setVisibility(View.GONE);

                        }
                    }


                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + pricesDetailsArrayList.get(0).getAdult_price());

                    Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                    Adult_Total_Val = per_Adult_Price * adult_no;
                    String result = String.format("%.2f", Adult_Total_Val);
                    Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                    String final_result = String.format("%.2f", Final_Cost);
                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);

                    tv_adult_minus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                                if (adult_no > Integer.valueOf(pricesDetailsArrayList.get(0).getFrom_pax_count())) {
                                    adult_no--;
                                    tv_adult_number.setText(String.valueOf(adult_no));
                                    Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                                    Adult_Total_Val = per_Adult_Price * adult_no;
                                    Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                    String final_result = String.format("%.2f", Final_Cost);
                                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                                } else {
                                    ll_adult.startAnimation(shakeError());
                                }
                            } else {
                                if (adult_no > 1) {
                                    adult_no--;
                                    tv_adult_number.setText(String.valueOf(adult_no));
                                    float temp = (adult_no + child_no) / Float.parseFloat(activitiesDetailstModels.get(0).getMaximum_pax());
                                    Adult_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getAdult_price()));
                                    String final_result = String.format("%.2f", Adult_Total_Val);
                                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                                    Final_Cost = Double.parseDouble(final_result);
                                } else {
                                    ll_adult.startAnimation(shakeError());
                                }
                            }

                        }
                    });

                    tv_adult_plus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                                if (adult_no >= Integer.valueOf(pricesDetailsArrayList.get(0).getFrom_pax_count()) && adult_no < Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())) {
                                    adult_no++;
                                    tv_adult_number.setText(String.valueOf(adult_no));
                                    Double per_Adult_Price = Double.valueOf(pricesDetailsArrayList.get(0).getAdult_price());
                                    Adult_Total_Val = per_Adult_Price * adult_no;
                                    Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                    String final_result = String.format("%.2f", Final_Cost);
                                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                                } else {
                                    ll_adult.startAnimation(shakeError());
                                }
                            } else {
                                if (adult_no >= Integer.valueOf(pricesDetailsArrayList.get(0).getFrom_pax_count()) && adult_no < Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())) {
                                    adult_no++;
                                    tv_adult_number.setText(String.valueOf(adult_no));
                                    float temp = (adult_no + child_no) / Float.parseFloat(activitiesDetailstModels.get(0).getMaximum_pax());
                                    Adult_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getAdult_price()));
                                    String final_result = String.format("%.2f", Adult_Total_Val);
                                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                                    Final_Cost = Double.parseDouble(final_result);
                                } else {
                                    ll_adult.startAnimation(shakeError());
                                }
                            }

                        }
                    });

                    tv_child_minus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                                if (child_no > 0) {
                                    child_no--;
                                    tv_child_number.setText(String.valueOf(child_no));
                                    Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
                                    Child_Total_Val = per_Child_Price * child_no;
                                    Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                    String final_result = String.format("%.2f", Final_Cost);
                                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                                } else {
                                    ll_child.startAnimation(shakeError());
                                }
                            } else {
                                if (child_no > 0) {
                                    child_no--;
                                    tv_child_number.setText(String.valueOf(child_no));
                                    float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                                    Child_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getChild_price()));
                                    String final_result = String.format("%.2f", Child_Total_Val);
                                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                                    Final_Cost = Double.parseDouble(final_result);
                                } else {
                                    ll_child.startAnimation(shakeError());
                                }

                            }

                        }
                    });

                    tv_child_plus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                                if (child_no >= 0 && child_no < Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())) {
                                    child_no++;
                                    tv_child_number.setText(String.valueOf(child_no));
                                    Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getChild_price());
                                    Child_Total_Val = per_Child_Price * child_no;
                                    Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                    String final_result = String.format("%.2f", Final_Cost);
                                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                                } else {
                                    ll_child.startAnimation(shakeError());
                                }
                            } else {
                                if (child_no >= 0 && child_no < Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())) {
                                    child_no += 1;
                                    tv_child_number.setText(String.valueOf(child_no));
                                    float temp = (adult_no + child_no) / Float.parseFloat(pricesDetailsArrayList.get(0).getMaximum_pax());
                                    Child_Total_Val = (float) (Math.pow(2, (Math.ceil(temp) - 1)) * Float.parseFloat(pricesDetailsArrayList.get(0).getChild_price()));
                                    String final_result = String.format("%.2f", Child_Total_Val);
                                    tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                                    Final_Cost = Double.parseDouble(final_result);
                                } else {
                                    ll_child.startAnimation(shakeError());
                                }

                            }

                        }
                    });

                    tv_infant_minus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (infant_no > 0) {
                                infant_no--;
                                tv_infant_number.setText(String.valueOf(infant_no));
                                Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getInfant_price());
                                Infant_Total_Val = per_Child_Price * infant_no;
                                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                String final_result = String.format("%.2f", Final_Cost);
                                tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                            } else {
                                ll_infant.startAnimation(shakeError());
                            }
                        }
                    });

                    tv_infant_plus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (infant_no >= 0 && infant_no < Integer.valueOf(pricesDetailsArrayList.get(0).getTo_pax_count())) {
                                infant_no++;
                                tv_infant_number.setText(String.valueOf(infant_no));
                                Double per_Child_Price = Double.valueOf(pricesDetailsArrayList.get(0).getInfant_price());
                                Infant_Total_Val = per_Child_Price * infant_no;
                                String result = String.format("%.2f", Infant_Total_Val);
                                Final_Cost = Adult_Total_Val + Child_Total_Val + Infant_Total_Val;
                                String final_result = String.format("%.2f", Final_Cost);
                                tv_final_cost.setText(pricesDetailsArrayList.get(0).getCurrency() + " " + final_result);
                            } else {
                                ll_infant.startAnimation(shakeError());
                            }
                        }
                    });





                    cardViewBooking.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (pricingDetailsModels.get(0).getPricing_per_person().equalsIgnoreCase("true")) {
                                No_of_Unit_Count = 0;
                            }else{
                                int count = Integer.valueOf(tv_adult_number.getText().toString()) + Integer.valueOf(tv_child_number.getText().toString());

                                if(count>unit_count){
                                    int main_unit_count = count / unit_count ;
                                    int main_unit_base = count % unit_count ;
                                    if(main_unit_base==0){
                                        No_of_Unit_Count = main_unit_count;
                                        Log.e("Final --->", String.valueOf(No_of_Unit_Count));
                                    }else{
                                        No_of_Unit_Count = ++main_unit_count;
                                        Log.e("Final --->", String.valueOf(No_of_Unit_Count));
                                    }
                                }else{
                                    No_of_Unit_Count = 1;
                                }

                            }
                            Log.e("Date formate ----->",tv_date_time.getText().toString());

                            if(Flag){
                                Flag=false;
                                FlagPermissionFlow = true;
                                if (checkAppPermissions(mContext, AppPermissions)) {
                                    Intent intent = new Intent(mContext, ActivityBookingPayment.class);
                                    intent.putExtra("Titile", tv_title.getText().toString());
                                    intent.putExtra("Date", tv_date_time.getText().toString());
                                    intent.putExtra("Time", tv_time.getText().toString());
                                    intent.putExtra("No_of_Adult", tv_adult_number.getText().toString());
                                    intent.putExtra("No_of_Child", tv_child_number.getText().toString());
                                    intent.putExtra("No_of_Infant", tv_infant_number.getText().toString());
                                    intent.putExtra("No_of_Unit", String.valueOf(No_of_Unit_Count));
                                    intent.putExtra("Total_Cost", tv_final_cost.getText().toString());
                                    intent.putExtra("Amount", String.format("%.2f", Final_Cost));
                                    intent.putExtra("Currency", pricesDetailsArrayList.get(0).getCurrency());
                                    intent.putExtra("tourId", Event_ID);
                                    intent.putExtra("tourName", tv_title.getText().toString());
                                    intent.putExtra("CancellationPolicyModels", (ArrayList<ModelActivitiesCancellatioPolicy>) cancellatioPolicyModels);
                                    intent.putExtra("ListDetailModels", (ArrayList<ModelActivitiesDetails>) activitiesDetailstModels);
                                    startActivity(intent);
                                } else if(FlagPermissionFlow){
                                    ActivityCompat.requestPermissions(ActivityBookingPricing.this, AppPermissions, 102);
                                }
                            }

                        }
                    });

                } else {
                    Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
                    book_now.setVisibility(View.GONE);
                }

            } else {
                Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
                book_now.setVisibility(View.GONE);
            }

        }catch (Exception e){
            //Snackbar.make(findViewById(R.id.coordinate_container), "Something went wrong. Please try again.", Snackbar.LENGTH_LONG).show();
            //book_now.setVisibility(View.GONE);

        }


    }

    boolean checkAppPermissions(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(FlagPermissionFlow){
            if (requestCode == 102 && FlagPermissionFlow) {
                FlagPermissionFlow=false;
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(mContext, ActivityBookingPayment.class);
                    intent.putExtra("Titile", tv_title.getText().toString());
                    intent.putExtra("Date", tv_date_time.getText().toString());
                    intent.putExtra("Time", tv_time.getText().toString());
                    intent.putExtra("No_of_Adult", tv_adult_number.getText().toString());
                    intent.putExtra("No_of_Child", tv_child_number.getText().toString());
                    intent.putExtra("No_of_Infant", tv_infant_number.getText().toString());
                    intent.putExtra("No_of_Unit", String.valueOf(No_of_Unit_Count));
                    intent.putExtra("Total_Cost", tv_final_cost.getText().toString());
                    intent.putExtra("Amount", String.format("%.2f", Final_Cost));
                    intent.putExtra("Currency", pricesDetailsArrayList.get(0).getCurrency());
                    intent.putExtra("tourId", Event_ID);
                    intent.putExtra("tourName", tv_title.getText().toString());
                    intent.putExtra("CancellationPolicyModels", (ArrayList<ModelActivitiesCancellatioPolicy>) cancellatioPolicyModels);
                    intent.putExtra("ListDetailModels", (ArrayList<ModelActivitiesDetails>) activitiesDetailstModels);
                    startActivity(intent);
                }
            }else{
                FlagPermissionFlow=false;
                Toast.makeText(mContext, "Please allow location permission", Toast.LENGTH_LONG).show();
            }
        }

    }


    public TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }


    @Override
    protected void onResume() {
        super.onResume();
        Flag=true;
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            gpsTracker.showSettingsAlert(ActivityBookingPricing.this);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
