package com.guiddoocn.holidays.models;

public class ModelCancellationPoliciesList {

    private String from;
    private String to;
    private String deduction;

    public ModelCancellationPoliciesList(String from, String to, String deduction) {
        this.from = from;
        this.to = to;
        this.deduction = deduction;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDeduction() {
        return deduction;
    }

    public void setDeduction(String deduction) {
        this.deduction = deduction;
    }
}


