package com.guiddoocn.holidays.models;

public class ModelPackagesDayWiseList {


    private String city_id,day_series,title,itinerary_id,description,image;


    public ModelPackagesDayWiseList(String city_id, String day_series, String title, String itinerary_id, String description, String image) {
        this.city_id = city_id;
        this.day_series = day_series;
        this.title = title;
        this.itinerary_id = itinerary_id;
        this.description = description;
        this.image = image;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getDay_series() {
        return day_series;
    }

    public void setDay_series(String day_series) {
        this.day_series = day_series;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getItinerary_id() {
        return itinerary_id;
    }

    public void setItinerary_id(String itinerary_id) {
        this.itinerary_id = itinerary_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

