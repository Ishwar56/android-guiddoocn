package com.guiddoocn.holidays.models;

public class ModelPackGalleryImagesList {

    private String image_path;
    private String title;

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

