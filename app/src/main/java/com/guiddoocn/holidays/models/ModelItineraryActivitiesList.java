package com.guiddoocn.holidays.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ModelItineraryActivitiesList implements Serializable {

    private  String activity_id;
    private String check_in_date;
    private  String check_out_date;
    private  String total_rooms;
    private  String adult;
    private  String child;
    private  String total_night;
    private String room_cost;
    private  String final_cost;
    private String Country_id;
    private String Days;
    private String Adult_Amt;
    private String Child_Amt;
    private  String Transfer_Charges;
    private  String Vouchers_Charges;
    private String isPack;
    private int No_of_Days;
    private ArrayList<ModelActivitiesList> modelActivitiesLists = new ArrayList<>();

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String getCheck_in_date() {
        return check_in_date;
    }

    public void setCheck_in_date(String check_in_date) {
        this.check_in_date = check_in_date;
    }

    public String getCheck_out_date() {
        return check_out_date;
    }

    public void setCheck_out_date(String check_out_date) {
        this.check_out_date = check_out_date;
    }

    public String getTotal_rooms() {
        return total_rooms;
    }

    public void setTotal_rooms(String total_rooms) {
        this.total_rooms = total_rooms;
    }

    public String getAdult() {
        return adult;
    }

    public void setAdult(String adult) {
        this.adult = adult;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getTotal_night() {
        return total_night;
    }

    public void setTotal_night(String total_night) {
        this.total_night = total_night;
    }

    public String getFinal_cost() {
        return final_cost;
    }

    public void setFinal_cost(String final_cost) {
        this.final_cost = final_cost;
    }

    public String getCountry_id() {
        return Country_id;
    }

    public void setCountry_id(String country_id) {
        Country_id = country_id;
    }

    public String getDays() {
        return Days;
    }

    public void setDays(String days) {
        Days = days;
    }

    public int getNo_of_Days() {
        return No_of_Days;
    }

    public void setNo_of_Days(int no_of_Days) {
        No_of_Days = no_of_Days;
    }

    public ArrayList<ModelActivitiesList> getModelActivitiesLists() {
        return modelActivitiesLists;
    }

    public void setModelActivitiesLists(ArrayList<ModelActivitiesList> modelActivitiesLists) {
        this.modelActivitiesLists = modelActivitiesLists;
    }

    public String getRoom_cost() {
        return room_cost;
    }

    public String getAdult_Amt() {
        return Adult_Amt;
    }

    public void setAdult_Amt(String adult_Amt) {
        Adult_Amt = adult_Amt;
    }

    public String getChild_Amt() {
        return Child_Amt;
    }

    public void setChild_Amt(String child_Amt) {
        Child_Amt = child_Amt;
    }

    public String getTransfer_Charges() {
        return Transfer_Charges;
    }

    public void setTransfer_Charges(String transfer_Charges) {
        Transfer_Charges = transfer_Charges;
    }

    public String getVouchers_Charges() {
        return Vouchers_Charges;
    }

    public void setVouchers_Charges(String vouchers_Charges) {
        Vouchers_Charges = vouchers_Charges;
    }

    public String getIsPack() {
        return isPack;
    }

    public void setIsPack(String isPack) {
        this.isPack = isPack;
    }

    public void setRoom_cost(String room_cost) {
        this.room_cost = room_cost;
    }
}
