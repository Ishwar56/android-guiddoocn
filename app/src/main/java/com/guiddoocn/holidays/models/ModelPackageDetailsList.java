package com.guiddoocn.holidays.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ModelPackageDetailsList implements Serializable {

    private String Package_id;
    private String thumbnail_image;
    private String Package_name;
    private String Coupon_code;
    private String Dicount_MaxCap;
    private String Discount;
    private String cities_covered;
    private String cities_covered_names;
    private String cities_nights;
    private String country_id;
    private String country_name;
    private String currency;
    private String description;
    private String duration;
    private String highlights;
    private String preferred_months;
    private  String shortDescription;
    private  String rating;
    private String valid_from;
    private  String valid_to;
    private  String visa_included;
    private String pack_categories;

    private ArrayList<ModelHotelList> pack_hotel_ArrayList =new ArrayList<>();
    private ArrayList<ModelInclusionList> pack_inclusion_ArrayList = new ArrayList<>();
    private ArrayList<ModelCancellationPoliciesList> pack_cancellation_ArrayList = new ArrayList<>();
    private ArrayList<ModelPackagesDayWiseList> pack_itinerary_ArrayList = new ArrayList<>();
    private ArrayList<ModelPackGalleryImagesList> pack_galleryImages_ArrayList = new ArrayList<>();

    public String getPackage_id() {
        return Package_id;
    }

    public void setPackage_id(String package_id) {
        Package_id = package_id;
    }

    public String getThumbnail_image() {
        return thumbnail_image;
    }

    public void setThumbnail_image(String thumbnail_image) {
        this.thumbnail_image = thumbnail_image;
    }

    public String getPackage_name() {
        return Package_name;
    }

    public void setPackage_name(String package_name) {
        Package_name = package_name;
    }

    public String getCoupon_code() {
        return Coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        Coupon_code = coupon_code;
    }

    public String getDicount_MaxCap() {
        return Dicount_MaxCap;
    }

    public void setDicount_MaxCap(String dicount_MaxCap) {
        Dicount_MaxCap = dicount_MaxCap;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getCities_covered() {
        return cities_covered;
    }

    public void setCities_covered(String cities_covered) {
        this.cities_covered = cities_covered;
    }

    public String getCities_covered_names() {
        return cities_covered_names;
    }

    public void setCities_covered_names(String cities_covered_names) {
        this.cities_covered_names = cities_covered_names;
    }

    public String getCities_nights() {
        return cities_nights;
    }

    public void setCities_nights(String cities_nights) {
        this.cities_nights = cities_nights;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getPreferred_months() {
        return preferred_months;
    }

    public void setPreferred_months(String preferred_months) {
        this.preferred_months = preferred_months;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getValid_from() {
        return valid_from;
    }

    public void setValid_from(String valid_from) {
        this.valid_from = valid_from;
    }

    public String getValid_to() {
        return valid_to;
    }

    public void setValid_to(String valid_to) {
        this.valid_to = valid_to;
    }

    public String getVisa_included() {
        return visa_included;
    }

    public void setVisa_included(String visa_included) {
        this.visa_included = visa_included;
    }

    public String getPack_categories() {
        return pack_categories;
    }

    public void setPack_categories(String pack_categories) {
        this.pack_categories = pack_categories;
    }

    public ArrayList<ModelHotelList> getPack_hotel_ArrayList() {
        return pack_hotel_ArrayList;
    }

    public void setPack_hotel_ArrayList(ArrayList<ModelHotelList> pack_hotel_ArrayList) {
        this.pack_hotel_ArrayList = pack_hotel_ArrayList;
    }

    public ArrayList<ModelInclusionList> getPack_inclusion_ArrayList() {
        return pack_inclusion_ArrayList;
    }

    public void setPack_inclusion_ArrayList(ArrayList<ModelInclusionList> pack_inclusion_ArrayList) {
        this.pack_inclusion_ArrayList = pack_inclusion_ArrayList;
    }

    public ArrayList<ModelCancellationPoliciesList> getPack_cancellation_ArrayList() {
        return pack_cancellation_ArrayList;
    }

    public void setPack_cancellation_ArrayList(ArrayList<ModelCancellationPoliciesList> pack_cancellation_ArrayList) {
        this.pack_cancellation_ArrayList = pack_cancellation_ArrayList;
    }

    public ArrayList<ModelPackagesDayWiseList> getPack_itinerary_ArrayList() {
        return pack_itinerary_ArrayList;
    }

    public void setPack_itinerary_ArrayList(ArrayList<ModelPackagesDayWiseList> pack_itinerary_ArrayList) {
        this.pack_itinerary_ArrayList = pack_itinerary_ArrayList;
    }

    public ArrayList<ModelPackGalleryImagesList> getPack_galleryImages_ArrayList() {
        return pack_galleryImages_ArrayList;
    }

    public void setPack_galleryImages_ArrayList(ArrayList<ModelPackGalleryImagesList> pack_galleryImages_ArrayList) {
        this.pack_galleryImages_ArrayList = pack_galleryImages_ArrayList;
    }
}

