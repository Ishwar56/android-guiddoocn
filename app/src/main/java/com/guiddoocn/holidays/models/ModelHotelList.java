package com.guiddoocn.holidays.models;

public class ModelHotelList {

    private int image;
    private String hotelName;
    private  String amount;
    private  String Hotel_Name;
    private  String city_name;
    private  String extra_info;
    private  String hotel_id;
    private  String no_ofNights;
    private  String currency;


    public ModelHotelList(int image, String hotelName) {
        this.image = image;
        this.hotelName = hotelName;
    }

    public ModelHotelList(int image, String hotelName, String amount, String currency) {
        this.image = image;
        this.hotelName = hotelName;
        this.amount = amount;
        this.currency = currency;
    }

    public ModelHotelList(String hotel_Name, String cityname,String hotelID, String noofNights) {

        this.Hotel_Name = hotel_Name;
        this.city_name = cityname;
        this.hotel_id = hotelID;
        this.no_ofNights = noofNights;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotel_Name() {
        return Hotel_Name;
    }

    public void setHotel_Name(String hotel_Name) {
        Hotel_Name = hotel_Name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public void setExtra_info(String extra_info) {
        this.extra_info = extra_info;
    }

    public String getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(String hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getNo_ofNights() {
        return no_ofNights;
    }

    public void setNo_ofNights(String no_ofNights) {
        this.no_ofNights = no_ofNights;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
