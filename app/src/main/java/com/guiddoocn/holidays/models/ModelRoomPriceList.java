package com.guiddoocn.holidays.models;

public class ModelRoomPriceList {

    private String room_id;
    private String room_name;
    private  String hotel_id;
    private  String Country_id;
    private  int noOFnight;
    private  double hotel_Amt;
    private  String meal_type_id;
    private  String sharing_type_id;
    private  String meal_type;
    private  String sharing_type;
    private String hotel_extra;
    private String is_additional_hotel;
    private String extra_bed_included;
    private String supplier;

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(String hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getCountry_id() {
        return Country_id;
    }

    public void setCountry_id(String country_id) {
        Country_id = country_id;
    }

    public int getNoOFnight() {
        return noOFnight;
    }

    public void setNoOFnight(int noOFnight) {
        this.noOFnight = noOFnight;
    }

    public double getHotel_Amt() {
        return hotel_Amt;
    }

    public void setHotel_Amt(double hotel_Amt) {
        this.hotel_Amt = hotel_Amt;
    }

    public String getMeal_type_id() {
        return meal_type_id;
    }

    public void setMeal_type_id(String meal_type_id) {
        this.meal_type_id = meal_type_id;
    }

    public String getSharing_type_id() {
        return sharing_type_id;
    }

    public void setSharing_type_id(String sharing_type_id) {
        this.sharing_type_id = sharing_type_id;
    }

    public String getMeal_type() {
        return meal_type;
    }

    public void setMeal_type(String meal_type) {
        this.meal_type = meal_type;
    }

    public String getSharing_type() {
        return sharing_type;
    }

    public void setSharing_type(String sharing_type) {
        this.sharing_type = sharing_type;
    }

    public String getHotel_extra() {
        return hotel_extra;
    }

    public void setHotel_extra(String hotel_extra) {
        this.hotel_extra = hotel_extra;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getIs_additional_hotel() {
        return is_additional_hotel;
    }

    public void setIs_additional_hotel(String is_additional_hotel) {
        this.is_additional_hotel = is_additional_hotel;
    }

    public String getExtra_bed_included() {
        return extra_bed_included;
    }

    public void setExtra_bed_included(String extra_bed_included) {
        this.extra_bed_included = extra_bed_included;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /*public double getHotel_Amt() {
        return hotel_Amt;
    }

    public void setHotel_Amt(double hotel_Amt) {
        this.hotel_Amt = hotel_Amt;
    }*/
}
