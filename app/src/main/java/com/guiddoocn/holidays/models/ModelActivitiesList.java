package com.guiddoocn.holidays.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ModelActivitiesList implements Serializable {

    private ArrayList<ModelSubDiningPreferences> modelSubDiningPreferences;
    private ArrayList<ModelSubCategoryPreferences> modelSubCategoryPreferences;
    private String discount_cap  ;
    private String tour_id  ;
    private String address;
    private String category_id;
    private String category;
    private String city_id  ;
    private String city  ;
    private String currency;
    private String day;
    private String hour;
    private String min;
    private String featured_image;
    private String name;
    private String rating;
    private String short_description;
    private String starting_from_price;
    private String old_price;
    private String new_price;
    private String latitude;
    private String longitude;
    private String discount;
    private boolean selected;
    private String transfer_included;
    private String from_time;
    private String to_time;
    private String is_pricing_per_pax;
    private String maximum_pax;
    private String minimum_pax;
    private String is_special_offer;
    private String added_in_itinerary;

    public String getAdded_in_itinerary() {
        return added_in_itinerary;
    }

    public void setAdded_in_itinerary(String added_in_itinerary) {
        this.added_in_itinerary = added_in_itinerary;
    }

    public String getIs_special_offer() {
        return is_special_offer;
    }

    public void setIs_special_offer(String is_special_offer) {
        this.is_special_offer = is_special_offer;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getIs_pricing_per_pax() {
        return is_pricing_per_pax;
    }

    public void setIs_pricing_per_pax(String is_pricing_per_pax) {
        this.is_pricing_per_pax = is_pricing_per_pax;
    }

    public String getMaximum_pax() {
        return maximum_pax;
    }

    public void setMaximum_pax(String maximum_pax) {
        this.maximum_pax = maximum_pax;
    }

    public String getMinimum_pax() {
        return minimum_pax;
    }

    public void setMinimum_pax(String minimum_pax) {
        this.minimum_pax = minimum_pax;
    }

    public String getFrom_time() {
        return from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getTo_time() {
        return to_time;
    }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }

    public String getTransfer_included() {
        return transfer_included;
    }

    public void setTransfer_included(String transfer_included) {
        this.transfer_included = transfer_included;
    }

    public String getTour_id() {
        return tour_id;
    }

    public void setTour_id(String tour_id) {
        this.tour_id = tour_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getFeatured_image() {
        return featured_image;
    }

    public void setFeatured_image(String featured_image) {
        this.featured_image = featured_image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getStarting_from_price() {
        return starting_from_price;
    }

    public void setStarting_from_price(String starting_from_price) {
        this.starting_from_price = starting_from_price;
    }

    public String getOld_price() {
        return old_price;
    }

    public void setOld_price(String old_price) {
        this.old_price = old_price;
    }

    public String getNew_price() {
        return new_price;
    }

    public void setNew_price(String new_price) {
        this.new_price = new_price;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


    public ArrayList<ModelSubDiningPreferences> getModelSubDiningPreferences() {
        return modelSubDiningPreferences;
    }

    public void setModelSubDiningPreferences(ArrayList<ModelSubDiningPreferences> modelSubDiningPreferences) {
        this.modelSubDiningPreferences = modelSubDiningPreferences;
    }

    public ArrayList<ModelSubCategoryPreferences> getModelSubCategoryPreferences() {
        return modelSubCategoryPreferences;
    }

    public void setModelSubCategoryPreferences(ArrayList<ModelSubCategoryPreferences> modelSubCategoryPreferences) {
        this.modelSubCategoryPreferences = modelSubCategoryPreferences;
    }

    public String getDiscount_cap() {
        return discount_cap;
    }

    public void setDiscount_cap(String discount_cap) {
        this.discount_cap = discount_cap;
    }

    public boolean isSelected() {
        return selected;
    }

}
