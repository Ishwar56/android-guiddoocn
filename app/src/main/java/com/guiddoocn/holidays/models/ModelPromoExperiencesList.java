package com.guiddoocn.holidays.models;

import java.io.Serializable;

public class ModelPromoExperiencesList implements Serializable {
    private String tour_id;
    private String image;
    private String city;
    private String name;
    private String category_id;
    private String category_name;
    private  String rating;
    private String currency;
    private String day;
    private String hour;
    private String min;
    private String amount;
    private String discount;
    private String discount_cap;


    public ModelPromoExperiencesList() {
        super();
    }


    public String getImage() {
        return image;
    }

    public String getTour_id() {
        return tour_id;
    }

    public void setTour_id(String tour_id) {
        this.tour_id = tour_id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String title) {
        this.city = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_cap() {
        return discount_cap;
    }

    public void setDiscount_cap(String discount_cap) {
        this.discount_cap = discount_cap;
    }
}
