package com.guiddoocn.holidays.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ModelItineraryHotelList implements Serializable {

    private  String hotel_id;
    private String check_in_date;
    private  String check_out_date;
    private  String total_rooms;
    private  String adult;
    private  String child;
    private  String total_night;
    private String room_cost;
    private  String final_cost;
    private String Country_id;
    private String HotelsTotalRooms;
    private String suplier;
    private String room_type_id;
    private String option;
    private ArrayList<ModelAllHotelList> hotelRoomsLists = new ArrayList<>();

    public String getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(String hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getCheck_in_date() {
        return check_in_date;
    }

    public void setCheck_in_date(String check_in_date) {
        this.check_in_date = check_in_date;
    }

    public String getCheck_out_date() {
        return check_out_date;
    }

    public void setCheck_out_date(String check_out_date) {
        this.check_out_date = check_out_date;
    }

    public String getTotal_rooms() {
        return total_rooms;
    }

    public void setTotal_rooms(String total_rooms) {
        this.total_rooms = total_rooms;
    }

    public String getAdult() {
        return adult;
    }

    public void setAdult(String adult) {
        this.adult = adult;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getTotal_night() {
        return total_night;
    }

    public void setTotal_night(String total_night) {
        this.total_night = total_night;
    }

    public String getFinal_cost() {
        return final_cost;
    }

    public void setFinal_cost(String final_cost) {
        this.final_cost = final_cost;
    }

    public String getCountry_id() {
        return Country_id;
    }

    public void setCountry_id(String country_id) {
        Country_id = country_id;
    }

    public ArrayList<ModelAllHotelList> getHotelsLists() {
        return hotelRoomsLists;
    }

    public void setHotelsLists(ArrayList<ModelAllHotelList> hotelRoomsLists) {
        this.hotelRoomsLists = hotelRoomsLists;
    }

    public String getRoom_cost() {
        return room_cost;
    }

    public void setRoom_cost(String room_cost) {
        this.room_cost = room_cost;
    }

    public String getHotelsTotalRooms() {
        return HotelsTotalRooms;
    }

    public void setHotelsTotalRooms(String hotelsTotalRooms) {
        HotelsTotalRooms = hotelsTotalRooms;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getRoom_type_id() {
        return room_type_id;
    }

    public void setRoom_type_id(String room_type_id) {
        this.room_type_id = room_type_id;
    }

    public String getSuplier() {
        return suplier;
    }

    public void setSuplier(String suplier) {
        this.suplier = suplier;
    }
}
