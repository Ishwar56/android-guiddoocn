package com.guiddoocn.holidays.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ModelHotelRoomsList implements Serializable {

    private String room_id;
    private String room_type;
    private String remarks;
    private String net_rate;
    private String meal_type;
    private String extra_bed_cost;
    private String currency;
    private String booking_type;
    private String batch_size;
    private String additional_tax;
    private String additional_surcharge;
    private String hotel_id;
    private String Country_id;
    private String block_dates;
    private ArrayList<ModelHotelRoomsPricingsList> roomsPricingsLists = new ArrayList<>();

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getNet_rate() {
        return net_rate;
    }

    public void setNet_rate(String net_rate) {
        this.net_rate = net_rate;
    }

    public String getSelect() {
        return meal_type;
    }

    public void setSelect(String meal_type) {
        this.meal_type = meal_type;
    }

    public String getExtra_bed_cost() {
        return extra_bed_cost;
    }

    public void setExtra_bed_cost(String extra_bed_cost) {
        this.extra_bed_cost = extra_bed_cost;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBooking_type() {
        return booking_type;
    }

    public void setBooking_type(String booking_type) {
        this.booking_type = booking_type;
    }

    public String getBatch_size() {
        return batch_size;
    }

    public void setBatch_size(String batch_size) {
        this.batch_size = batch_size;
    }

    public String getAdditional_tax() {
        return additional_tax;
    }

    public void setAdditional_tax(String additional_tax) {
        this.additional_tax = additional_tax;
    }

    public String getAdditional_surcharge() {
        return additional_surcharge;
    }

    public void setAdditional_surcharge(String additional_surcharge) {
        this.additional_surcharge = additional_surcharge;
    }

    public String getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(String hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getCountry_id() {
        return Country_id;
    }

    public void setCountry_id(String country_id) {
        Country_id = country_id;
    }

    public String getBlock_dates() {
        return block_dates;
    }

    public void setBlock_dates(String block_dates) {
        this.block_dates = block_dates;
    }

    public ArrayList<ModelHotelRoomsPricingsList> getRoomsPricingsLists() {
        return roomsPricingsLists;
    }

    public void setRoomsPricingsLists(ArrayList<ModelHotelRoomsPricingsList> roomsPricingsLists) {
        this.roomsPricingsLists = roomsPricingsLists;
    }
}

