package com.guiddoocn.holidays.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ModelAllHotelList implements Serializable {

    private String ID;
    private String image;
    private String title;
    private String hotelDetails;
    private String cityCountryName;
    private String city_ID;
    private String distance_from_centre;
    private String location;
    private String amount;
    private String address;
    private  String currency;
    private  String rating;
    private String success_rate;
    private String queries_received;
    private ArrayList<ModelHotelAmnetiesList> hotelAmnetiesLists = new ArrayList<>();
    private ArrayList<ModelHotelRoomsList> hotelRoomsLists = new ArrayList<>();

    public ArrayList<ModelHotelAmnetiesList> getHotelAmnetiesLists() {
        return hotelAmnetiesLists;
    }

    public void setHotelAmnetiesLists(ArrayList<ModelHotelAmnetiesList> hotelAmnetiesLists) {
        this.hotelAmnetiesLists = hotelAmnetiesLists;
    }

    public ArrayList<ModelHotelRoomsList> getHotelRoomsLists() {
        return hotelRoomsLists;
    }

    public void setHotelRoomsLists(ArrayList<ModelHotelRoomsList> hotelRoomsLists) {
        this.hotelRoomsLists = hotelRoomsLists;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHotelDetails() {
        return hotelDetails;
    }

    public void setHotelDetails(String hotelDetails) {
        this.hotelDetails = hotelDetails;
    }

    public String getCityCountryName() {
        return cityCountryName;
    }

    public void setCityCountryName(String cityCountryName) {
        this.cityCountryName = cityCountryName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSuccess_rate() {
        return success_rate;
    }

    public void setSuccess_rate(String success_rate) {
        this.success_rate = success_rate;
    }

    public String getQueries_received() {
        return queries_received;
    }

    public void setQueries_received(String queries_received) {
        this.queries_received = queries_received;
    }

    public String getCity_ID() {
        return city_ID;
    }

    public void setCity_ID(String city_ID) {
        this.city_ID = city_ID;
    }

    public String getDistance_from_centre() {
        return distance_from_centre;
    }

    public void setDistance_from_centre(String distance_from_centre) {
        this.distance_from_centre = distance_from_centre;
    }
}

