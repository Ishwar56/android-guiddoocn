package com.guiddoocn.holidays.models;

public class ModelQueryList {


    private String destination;
    private String number_of_pax;
    private String travel_agent;
    private String travel_date;


    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getNumber_of_pax() {
        return number_of_pax;
    }

    public void setNumber_of_pax(String number_of_pax) {
        this.number_of_pax = number_of_pax;
    }

    public String getTravel_agent() {
        return travel_agent;
    }

    public void setTravel_agent(String travel_agent) {
        this.travel_agent = travel_agent;
    }

    public String getTravel_date() {
        return travel_date;
    }

    public void setTravel_date(String travel_date) {
        this.travel_date = travel_date;
    }
}

