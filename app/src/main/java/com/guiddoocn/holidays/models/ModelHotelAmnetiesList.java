package com.guiddoocn.holidays.models;

import java.io.Serializable;

public class ModelHotelAmnetiesList implements Serializable {

    private String ID;
    private String Name;
    private String Icon;
    private String hotel_ID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public String getHotel_ID() {
        return hotel_ID;
    }

    public void setHotel_ID(String hotel_ID) {
        this.hotel_ID = hotel_ID;
    }
}

