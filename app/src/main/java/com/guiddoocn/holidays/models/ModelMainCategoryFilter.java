package com.guiddoocn.holidays.models;

import android.util.Log;

import java.io.Serializable;
import java.util.HashSet;

/**
 * Created by Ishwar on 23/12/2017.
 */
public class ModelMainCategoryFilter implements Serializable {

    static final String TAG = "LogFilter";

    public static HashSet<String> filters = new HashSet<String>();
    public static HashSet<String> filtersDining = new HashSet<String>();

    public static HashSet<String> filtersID = new HashSet<String>();
    public static HashSet<String> filtersDiningID = new HashSet<String>();

    public void addFilter(String filterName) {
        Log.d(TAG, "addFilter: ");
        filters.add(filterName);
    }

    public void removeFilter(String filterName) {
        filters.remove(filterName);
    }

    public void addFilterDining(String filterName) {
        Log.d(TAG, "addFilter: ");
        filtersDining.add(filterName);
    }

    public void removeFilterDining(String filterName) {
        filtersDining.remove(filterName);
    }

    public void clearFilters() {
        filters.clear();
    }

    public void addFilterID(String filterName) {
        Log.d(TAG, "addFilter: ");
        filtersID.add(filterName);
    }

    public void removeFilterID(String filterName) {
        filtersID.remove(filterName);
    }

    public void addFilterDiningID(String filterName) {
        Log.d(TAG, "addFilter: ");
        filtersDiningID.add(filterName);
    }

    public void removeFilterDiningID(String filterName) {
        filtersDiningID.remove(filterName);
    }

}
