package com.guiddoocn.holidays.models;

import java.io.Serializable;

/**
 * Created by Ishwar on 23/12/2017.
 */
public class ModelStandardBookingPayment implements Serializable{

    private String package_id  ;
    private  String Package_Name;
    private String package_type  ;
    private String partner_id  ;
    private String booking_reference_no;
    private String confirmation_no;
    private String no_of_adult;
    private String no_of_child;
    private String no_of_infant;
    private String status;
    private String currency;
    private String total_amount;
    private String amount_payable;
    private String travel_date;
    private String voucher_link;
    private String transaction_id;


    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getPackage_type() {
        return package_type;
    }

    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getBooking_reference_no() {
        return booking_reference_no;
    }

    public void setBooking_reference_no(String booking_reference_no) {
        this.booking_reference_no = booking_reference_no;
    }

    public String getConfirmation_no() {
        return confirmation_no;
    }

    public void setConfirmation_no(String confirmation_no) {
        this.confirmation_no = confirmation_no;
    }

    public String getNo_of_adult() {
        return no_of_adult;
    }

    public void setNo_of_adult(String no_of_adult) {
        this.no_of_adult = no_of_adult;
    }

    public String getNo_of_child() {
        return no_of_child;
    }

    public void setNo_of_child(String no_of_child) {
        this.no_of_child = no_of_child;
    }

    public String getNo_of_infant() {
        return no_of_infant;
    }

    public void setNo_of_infant(String no_of_infant) {
        this.no_of_infant = no_of_infant;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getAmount_payable() {
        return amount_payable;
    }

    public void setAmount_payable(String amount_payable) {
        this.amount_payable = amount_payable;
    }

    public String getTravel_date() {
        return travel_date;
    }

    public void setTravel_date(String travel_date) {
        this.travel_date = travel_date;
    }

    public String getVoucher_link() {
        return voucher_link;
    }

    public void setVoucher_link(String voucher_link) {
        this.voucher_link = voucher_link;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getPackage_Name() {
        return Package_Name;
    }

    public void setPackage_Name(String package_Name) {
        Package_Name = package_Name;
    }
}

