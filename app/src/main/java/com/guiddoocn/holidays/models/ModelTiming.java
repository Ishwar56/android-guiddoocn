package com.guiddoocn.holidays.models;

import java.io.Serializable;

/**
 * Created by Ishwar on 23/12/2017.
 */
public class ModelTiming implements Serializable{


    private String timing;

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

}

