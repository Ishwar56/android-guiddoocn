package com.guiddoocn.holidays.models;

public class ModelCardDetailsList {

    private String id;
    private String cardType;
    private String currency;
    private String Amount;

    public ModelCardDetailsList(String id, String cardType, String currency, String amount) {
        this.id = id;
        this.cardType = cardType;
        this.currency = currency;
        Amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}

