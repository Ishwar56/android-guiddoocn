package com.guiddoocn.holidays.models;

public class ModelContactUs {

    private String title;
    private String address;
    private String contact;
    private String email;
    private String latitude;
    private String longitude;

    public ModelContactUs()
    {

    }
    public ModelContactUs(String title, String address, String contact, String email, String latitude, String longitude) {
        this.title = title;
        this.address = address;
        this.contact = contact;
        this.email = email;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
