package com.guiddoocn.holidays.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ishwar on 23/12/2017.
 */
public class ModelPricingList implements Serializable{


    private String currency ;
    private String from_date;
    private String to_date;
    private ArrayList<ModelPricesDetails> pricingDetails;


    public ArrayList<ModelPricesDetails> getPricingDetails() {
        return pricingDetails;
    }

    public void setPricingDetails(ArrayList<ModelPricesDetails> prcingDetails) {
        this.pricingDetails = prcingDetails;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }
}

