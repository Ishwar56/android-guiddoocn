package com.guiddoocn.holidays.models;

public class ModelNewsArticalList {

    private String title,desription,detailURL;
    private int image;


    public ModelNewsArticalList(String title, String desription, String detailURL, int image) {
        this.title = title;
        this.desription = desription;
        this.detailURL = detailURL;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getDesription() {
        return desription;
    }

    public String getDetailURL() {
        return detailURL;
    }

    public int getImage() {
        return image;
    }
}

