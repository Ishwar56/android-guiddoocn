package com.guiddoocn.holidays.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ishwar on 23/12/2017.
 */
public class ModelPriceMain implements Serializable{

    private String pricing_per_person;
    private ArrayList<ModelActivitiesOperationTiming> operationTimingModels;

    private ArrayList<ModelPricingList> pricesArrayList;

    public ArrayList<ModelPricingList> getPricesArrayList() {
        return pricesArrayList;
    }

    public void setPricesArrayList(ArrayList<ModelPricingList> pricesArrayList) {
        this.pricesArrayList = pricesArrayList;
    }

    public ArrayList<ModelActivitiesOperationTiming> getOperationTimingModels() {
        return operationTimingModels;
    }

    public void setOperationTimingModels(ArrayList<ModelActivitiesOperationTiming> operationTimingModels) {
        this.operationTimingModels = operationTimingModels;
    }

    public String getPricing_per_person() {
        return pricing_per_person;
    }

    public void setPricing_per_person(String pricing_per_person) {
        this.pricing_per_person = pricing_per_person;
    }


}

