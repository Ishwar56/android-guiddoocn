package com.guiddoocn.holidays.models;

import android.support.annotation.Nullable;

import ir.mirrajabi.searchdialog.core.Searchable;

public class ModelDestinationLists implements Searchable {

    private String cityId;
    private String cityName;
    private String countryName;
    private String countryID;
    private String featured_Image;
    private String flag;
    private String available_for_customize;
    private String available_for_standard;

    public ModelDestinationLists() {
    }

    public ModelDestinationLists(String cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getFeatured_Image() {
        return featured_Image;
    }

    public void setFeatured_Image(String featured_Image) {
        this.featured_Image = featured_Image;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAvailable_for_customize() {
        return available_for_customize;
    }

    public void setAvailable_for_customize(String available_for_customize) {
        this.available_for_customize = available_for_customize;
    }

    public String getAvailable_for_standard() {
        return available_for_standard;
    }

    public void setAvailable_for_standard(String available_for_standard) {
        this.available_for_standard = available_for_standard;
    }

    @Override
    public String toString() {
        return countryName;
    }


    @Override
    public String getTitle() {
        return countryName;
    }
}

