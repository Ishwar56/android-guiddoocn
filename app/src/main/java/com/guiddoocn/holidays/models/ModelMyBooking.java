package com.guiddoocn.holidays.models;

import java.io.Serializable;

/**
 * Created by Ishwar on 23/12/2017.
 */
public class ModelMyBooking implements Serializable{

    private String activity_id  ;
    private String cart_id  ;
    private String city  ;
    private String activity_date  ;
    private String activity_name;
    private String activity_image;
    private String activity_time;
    private String booking_id  ;
    private String booking_note;
    private String booking_reference_no;
    private String confirmation_no;
    private String crypto_value;
    private String cryptocurrency;
    private String no_of_adult;
    private String no_of_child;
    private String no_of_infant;
    private String no_of_units;
    private String payment_mode;
    private String status;
    private String currency;
    private String total_amount;
    private String actual_amount;
    private String actual_currency;
    private String voucher_link;
    private String duration_day;
    private String duration_hr;
    private String duration_min;
    private String rating;

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public String getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(String actual_amount) {
        this.actual_amount = actual_amount;
    }

    public String getActual_currency() {
        return actual_currency;
    }

    public void setActual_currency(String actual_currency) {
        this.actual_currency = actual_currency;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getActivity_date() {
        return activity_date;
    }

    public void setActivity_date(String activity_date) {
        this.activity_date = activity_date;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public String getActivity_time() {
        return activity_time;
    }

    public void setActivity_time(String activity_time) {
        this.activity_time = activity_time;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getBooking_note() {
        return booking_note;
    }

    public void setBooking_note(String booking_note) {
        this.booking_note = booking_note;
    }

    public String getBooking_reference_no() {
        return booking_reference_no;
    }

    public void setBooking_reference_no(String booking_reference_no) {
        this.booking_reference_no = booking_reference_no;
    }

    public String getConfirmation_no() {
        return confirmation_no;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setConfirmation_no(String confirmation_no) {
        this.confirmation_no = confirmation_no;
    }

    public String getActivity_image() {
        return activity_image;
    }

    public void setActivity_image(String activity_image) {
        this.activity_image = activity_image;
    }

    public String getCrypto_value() {
        return crypto_value;
    }

    public void setCrypto_value(String crypto_value) {
        this.crypto_value = crypto_value;
    }

    public String getCryptocurrency() {
        return cryptocurrency;
    }

    public void setCryptocurrency(String cryptocurrency) {
        this.cryptocurrency = cryptocurrency;
    }

    public String getNo_of_adult() {
        return no_of_adult;
    }

    public void setNo_of_adult(String no_of_adult) {
        this.no_of_adult = no_of_adult;
    }

    public String getNo_of_child() {
        return no_of_child;
    }

    public void setNo_of_child(String no_of_child) {
        this.no_of_child = no_of_child;
    }

    public String getNo_of_infant() {
        return no_of_infant;
    }

    public void setNo_of_infant(String no_of_infant) {
        this.no_of_infant = no_of_infant;
    }

    public String getNo_of_units() {
        return no_of_units;
    }

    public void setNo_of_units(String no_of_units) {
        this.no_of_units = no_of_units;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getVoucher_link() {
        return voucher_link;
    }

    public void setVoucher_link(String voucher_link) {
        this.voucher_link = voucher_link;
    }

    public String getDuration_day() {
        return duration_day;
    }

    public void setDuration_day(String duration_day) {
        this.duration_day = duration_day;
    }

    public String getDuration_hr() {
        return duration_hr;
    }

    public void setDuration_hr(String duration_hr) {
        this.duration_hr = duration_hr;
    }

    public String getDuration_min() {
        return duration_min;
    }

    public void setDuration_min(String duration_min) {
        this.duration_min = duration_min;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}

