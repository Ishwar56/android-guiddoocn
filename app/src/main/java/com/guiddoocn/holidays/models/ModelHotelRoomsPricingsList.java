package com.guiddoocn.holidays.models;

import java.io.Serializable;

public class ModelHotelRoomsPricingsList implements Serializable {

    private String meal_type;
    private String meal_type_id;
    private String price;
    private String sharing_type;
    private String sharing_type_id;
    private String validity_from;
    private String validity_to;
    private String room_id;
    private String hotel_id;
    private String Country_id;

    public String getMeal_type() {
        return meal_type;
    }

    public void setMeal_type(String meal_type) {
        this.meal_type = meal_type;
    }

    public String getMeal_type_id() {
        return meal_type_id;
    }

    public void setMeal_type_id(String meal_type_id) {
        this.meal_type_id = meal_type_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSharing_type() {
        return sharing_type;
    }

    public void setSharing_type(String sharing_type) {
        this.sharing_type = sharing_type;
    }

    public String getSharing_type_id() {
        return sharing_type_id;
    }

    public void setSharing_type_id(String sharing_type_id) {
        this.sharing_type_id = sharing_type_id;
    }

    public String getValidity_from() {
        return validity_from;
    }

    public void setValidity_from(String validity_from) {
        this.validity_from = validity_from;
    }

    public String getValidity_to() {
        return validity_to;
    }

    public void setValidity_to(String validity_to) {
        this.validity_to = validity_to;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(String hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getCountry_id() {
        return Country_id;
    }

    public void setCountry_id(String country_id) {
        Country_id = country_id;
    }
}

