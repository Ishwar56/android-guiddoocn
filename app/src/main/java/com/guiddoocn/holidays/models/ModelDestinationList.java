package com.guiddoocn.holidays.models;

public class ModelDestinationList {
    private int image;
    private String cityId;
    private String cityName;
    private String countryName;
    private String countryID;


    /*"city_id":5,
"city_name":"Abu Dhabi",
"country_id":2,
"country_name":"UAE",
"featured_image":"https:\/\/wayrtoos3.s3-ap-southeast-1.amazonaws.com\/Abu_Dhabi_thumb.jpg",
"flag":"https:\/\/wayrtoos3.s3-ap-southeast-1.amazonaws.com\/uae.png"*/




    public ModelDestinationList(int image, String cityId,  String cityName, String countryName, String countryID) {
        this.image=image;
        this.cityId = cityId;

        this.cityName = cityName;
        this.countryName = countryName;



    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }


}

