package com.guiddoocn.holidays.models;

import java.io.Serializable;

/**
 * Created by Ishwar on 23/12/2017.
 */
public class ModelSubDiningPreferences implements Serializable{

    private String subdiningid;
    private String subdiningName;
    private String tour_id;
    private String Country_id;
    private String Count;

    public String getTour_id() {
        return tour_id;
    }

    public void setTour_id(String tour_id) {
        this.tour_id = tour_id;
    }

    public String getCountry_id() {
        return Country_id;
    }

    public void setCountry_id(String country_id) {
        Country_id = country_id;
    }

    public String getSubdiningid() {
        return subdiningid;
    }

    public void setSubdiningid(String subdiningid) {
        this.subdiningid = subdiningid;
    }

    public String getSubdiningName() {
        return subdiningName;
    }

    public void setSubdiningName(String subdiningName) {
        this.subdiningName = subdiningName;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }
}

