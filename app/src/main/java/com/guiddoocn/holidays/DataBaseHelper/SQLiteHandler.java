package com.guiddoocn.holidays.DataBaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.guiddoocn.holidays.models.ModelActivitiesList;
import com.guiddoocn.holidays.models.ModelAllHotelList;
import com.guiddoocn.holidays.models.ModelDestinationLists;
import com.guiddoocn.holidays.models.ModelHotelAmnetiesList;
import com.guiddoocn.holidays.models.ModelHotelRoomsList;
import com.guiddoocn.holidays.models.ModelHotelRoomsPricingsList;
import com.guiddoocn.holidays.models.ModelItineraryActivitiesList;
import com.guiddoocn.holidays.models.ModelItineraryHotelList;
import com.guiddoocn.holidays.models.ModelPromoExperiencesList;
import com.guiddoocn.holidays.models.ModelRoomPriceList;
import com.guiddoocn.holidays.models.ModelSubCategoryPreferences;
import com.guiddoocn.holidays.models.ModelSubDiningPreferences;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;


/**
 * Created by Ishwar on 28-06-2019.
 */

public class SQLiteHandler extends SQLiteOpenHelper {
    private static final String TAG = SQLiteHandler.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Holidays";
    private SessionManager session;


    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        session = new SessionManager(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            String DESTINATION_TABLE, PROMO_TABLE, ACTIVITY_TABLE, SUBDINING_TABLE,SUBCATEGORY_TABLE,HOTEL_TABLE, CURRENCY_TABLE, CALENDAR_TABLE, AMNETIES_TABLE, ROOMDATA_TABLE,HOTEL_ITINERARY_TABLE,ACTIVITY_ITINERARY_TABLE,HOTELS_ROOMS_TABLE,HOTELS_ROOMS_PRICING_TABLE,HOTELS_ROOMS_ITINERARY_TABLE;


            ACTIVITY_TABLE = "CREATE TABLE ACTIVITY (city_id TEXT ,city_name Text NOT NULL,category_id Text NOT NULL,category_name Text NOT NULL,currency Text NOT NULL,discount Text,discount_cap Text" +
                    ",day Text NOT NULL,hr Text NOT NULL,min Text NOT NULL,featured_image Text NOT NULL,from_time Text ,to_time Text ,is_pricing_per_pax Text ,is_special_offer Text ,latitude Text , longitude Text ,maximum_pax Text, name Text NOT NULL, rating Text NOT NULL,tour_id Text NOT NULL,starting_from_price REAL NOT NULL,address Text NOT NULL,transfer_included Text,Country_id TEXT NOT NULL,Selected TEXT)";
            db.execSQL(ACTIVITY_TABLE);
            Log.d(TAG, "ACTIVITY table is created");

            SUBDINING_TABLE = "CREATE TABLE SUBDINING (id Text NOT NULL,name Text NOT NULL,count Text,tour_id Text NOT NULL)";
            db.execSQL(SUBDINING_TABLE);
            Log.d(TAG, "SUBDINING table is created");

            SUBCATEGORY_TABLE = "CREATE TABLE SUBCATEGORY(id Text NOT NULL,name Text NOT NULL,count Text,tour_id Text NOT NULL)";
            db.execSQL(SUBCATEGORY_TABLE);
            Log.d(TAG, "SUBCATEGORY table is created");


            DESTINATION_TABLE = "CREATE TABLE DESTINATION (city_id TEXT NOT NULL ,country_id Text NOT NULL, city_name Text NOT NULL,country_name Text NOT NULL,featured_image Text NOT NULL, flag Text NOT NULL,available_for_customize Text,available_for_standard Text)";
            db.execSQL(DESTINATION_TABLE);
            Log.d(TAG, "WAYRTOO table is created");

            PROMO_TABLE = "CREATE TABLE PROMO (city_name Text NOT NULL,cat_name Text NOT NULL,category_id Text NOT NULL, currency Text NOT NULL" +
                    ",day Text NOT NULL,hr Text NOT NULL,min Text NOT NULL,featured_image Text NOT NULL, name Text NOT NULL, rating Text NOT NULL,tour_id Text NOT NULL,starting_from_price Text ,discount Text,discount_cap Text)";
            db.execSQL(PROMO_TABLE);
            Log.d(TAG, "PROMO table is created");



            HOTEL_TABLE = "CREATE TABLE HOTEL (hotel_id TEXT NOT NULL,hotel_name TEXT NOT NULL,location TEXT NOT NULL,rating TEXT NOT NULL,starting_from_price REAL NOT NULL,featured_image TEXT NOT NULL" +
                    ",currency TEXT NOT NULL,city_id TEXT NOT NULL,city TEXT NOT NULL,distance_from_centre TEXT NOT NULL,address TEXT NOT NULL,Country_id TEXT NOT NULL)";
            db.execSQL(HOTEL_TABLE);
            Log.d(TAG, "HOTEL table is created");

            CURRENCY_TABLE = "CREATE TABLE CURRENCY (currency_name Text NOT NULL,currency_value Text NOT NULL)";
            db.execSQL(CURRENCY_TABLE);
            Log.d(TAG, "CURRENCY table is created");

            CALENDAR_TABLE = "CREATE TABLE CALENDAR (day Text NOT NULL,booking_timings Text NOT NULL)";
            db.execSQL(CALENDAR_TABLE);
            Log.d(TAG, "CALENDAR table is created");

            AMNETIES_TABLE = "CREATE TABLE AMNETIES (id Text NOT NULL,amnety Text NOT NULL,hotel_id Text NOT NULL)";
            db.execSQL(AMNETIES_TABLE);
            Log.d(TAG, "AMNETIES table is created");

            ROOMDATA_TABLE = "CREATE TABLE ROOMSDATA(room_id Text NOT NULL,room_type Text NOT NULL,remarks Text NOT NULL,net_rate Text NOT NULL,meal_type Text NOT NULL,extra_bed_cost Text NOT NULL,currency Text NOT NULL,booking_type Text NOT NULL,batch_size Text NOT NULL,block_dates Text ,additional_tax Text ,additional_surcharge Text ,hotel_id Text NOT NULL,Country_id TEXT)";
            db.execSQL(ROOMDATA_TABLE);
            Log.d(TAG, "HOTELDATA table is created");

            HOTEL_ITINERARY_TABLE = "CREATE TABLE HOTEL_ITINERARY (hotel_id Text PRIMARY KEY,check_in_date Text NOT NULL,check_out_date Text ,total_rooms Text,adult Text,child Text,total_night,room_cost Text,final_cost Text,Country_id TEXT,HotelsTotalRooms Text,supplier Text,option Text)";
            db.execSQL(HOTEL_ITINERARY_TABLE);
            Log.d(TAG, "HOTEL_ITINERARY table is created");

            ACTIVITY_ITINERARY_TABLE = "CREATE TABLE ACTIVITY_ITINERARY (activity_id Text PRIMARY KEY,check_in_date Text NOT NULL,check_out_date Text ,total_rooms Text,adult Text,child Text,total_night,room_cost Text,final_cost Text,Country_id TEXT,Days Text,no_of_Days Int,Adult_Amt Text,Child_Amt Text,Transfer_Charges Text,Vouchers_Charges Text,isPack Text)";
            db.execSQL(ACTIVITY_ITINERARY_TABLE);
            Log.d(TAG, "ACTIVITY_ITINERARY table is created");

            HOTELS_ROOMS_TABLE = "CREATE TABLE ROOM (room_id Text NOT NULL,noOFnight Int ,hotel_Amt REAL,hotel_id Text,Country_id Text,meal_type_id Text,sharing_type_id Text,meal_type Text,sharing_type Text,room_name Text,extra_bed_included Text,is_additional_hotel Text,supplier Text)";
            db.execSQL(HOTELS_ROOMS_TABLE);
            Log.d(TAG, "HOTELS_ROOMS_TABLE table is created");

            HOTELS_ROOMS_PRICING_TABLE = "CREATE TABLE ROOM_PRICING (meal_type Text,meal_type_id Text,price Text,sharing_type Text,sharing_type_id Text,validity_from Text, validity_to Text, room_id Text,hotel_id Text,Country_id Text)";
            db.execSQL(HOTELS_ROOMS_PRICING_TABLE);
            Log.d(TAG, "HOTELS_ROOMS_PRICING_TABLE table is created");

            HOTELS_ROOMS_ITINERARY_TABLE = "CREATE TABLE ROOM_ITINERARY (room_id Text NOT NULL,noOFnight Int ,hotel_Amt REAL,hotel_id Text,Country_id Text,meal_type_id Text,sharing_type_id Text,hotel_extra Text,extra_bed_included Text,is_additional_hotel Text,supplier Text)";
            db.execSQL(HOTELS_ROOMS_ITINERARY_TABLE);
            Log.d(TAG, "HOTELS_ROOMS_ITINERARY_TABLE table is created");

        } catch (Exception e1) {

        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        session.setLogedIn(false);
        db.execSQL("DROP TABLE IF EXISTS DESTINATION");
        onCreate(db);
    }



    /*---------------------City-------------------------------*/


//city_id TEXT NOT NULL ,country_id Text NOT NULL, city_name Text NOT NULL,country_name Text NOT NULL,featured_image Text NOT NULL,flag Text NOT NULL
//available_for_customize Text,available_for_standard Text
    public boolean setCityData(String city_id, String city_name, String country_id, String country_name, String featured_image, String flag,String available_for_customize,String available_for_standard) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!city_name.isEmpty() && !city_id.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("city_id", city_id);
                values.put("city_name", city_name);//country_name
                values.put("country_id", country_id);
                values.put("country_name", country_name);
                values.put("featured_image", featured_image);
                values.put("flag", flag);
                values.put("available_for_customize", available_for_customize);
                values.put("available_for_standard", available_for_standard);
                long id = db.insert("DESTINATION", null, values);
                db.close();
                Log.e(TAG, "Insert Data in DESTINATION table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }

    public ArrayList<ModelDestinationLists> getCityResponce() {
        try {
            //String selectQuery = "SELECT * FROM DESTINATION WHERE city_id='"+city_id+"'",data = "";
            String selectQuery = "SELECT * FROM DESTINATION";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelDestinationLists> result = new ArrayList<ModelDestinationLists>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelDestinationLists cityList = new ModelDestinationLists();
                cityList.setCityId(c.getString(c.getColumnIndex("city_id")));
                cityList.setCityName(c.getString(c.getColumnIndex("city_name")));
                cityList.setCountryID(c.getString(c.getColumnIndex("country_id")));
                cityList.setCountryName(c.getString(c.getColumnIndex("country_name")));
                cityList.setFeatured_Image(c.getString(c.getColumnIndex("featured_image")));
                cityList.setFlag(c.getString(c.getColumnIndex("flag")));
                cityList.setAvailable_for_customize(c.getString(c.getColumnIndex("available_for_customize")));
                cityList.setAvailable_for_standard(c.getString(c.getColumnIndex("available_for_standard")));

                result.add(cityList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public ArrayList<ModelDestinationLists> getCountryResponce(String flagCheck) {
        try {
            String selectQuery = "";
            if(flagCheck.equalsIgnoreCase("C")){
                selectQuery = "SELECT DISTINCT country_name,country_id FROM DESTINATION WHERE available_for_customize='"+true+"'";
            }else if(flagCheck.equalsIgnoreCase("S")){
                selectQuery = "SELECT DISTINCT country_name,country_id FROM DESTINATION WHERE available_for_standard='"+true+"'";
            }else{
                selectQuery = "SELECT DISTINCT country_name,country_id FROM DESTINATION ";
            }

           // String selectQuery = "SELECT DISTINCT country_name,country_id FROM DESTINATION;";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelDestinationLists> result = new ArrayList<ModelDestinationLists>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelDestinationLists cityList = new ModelDestinationLists();
                //cityList.setCityId(c.getString(c.getColumnIndex("city_id")));
                //cityList.setCityName(c.getString(c.getColumnIndex("city_name")));
                cityList.setCountryID(c.getString(c.getColumnIndex("country_id")));
                cityList.setCountryName(c.getString(c.getColumnIndex("country_name")));
                //cityList.setFeatured_Image(c.getString(c.getColumnIndex("featured_image")));
                //cityList.setFlag(c.getString(c.getColumnIndex("flag")));

                result.add(cityList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public void deleteDestinationData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("DESTINATION", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from DESTINATION Table");
    }




    /*--------------------------------------Promo-------------------------------------------*/

/*
PROMO_TABLE = "CREATE TABLE PROMO (city_name Text NOT NULL,cat_name Text NOT NULL,category_id Text NOT NULL,currency Text NOT NULL" +
                    ",day Text NOT NULL,hr Text NOT NULL,min Text NOT NULL,featured_image Text NOT NULL, name Text NOT NULL, rating Text NOT NULL,tour_id Text NOT NULL,starting_from_price Text NOT NULL)";
*/

    public boolean setPromoData(String tour_id, String city_name, String name, String category_id, String cat_name, String day, String hr, String min, String rating, String featured_image, String currency, String starting_from_price,String discount,String discount_cap) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!name.isEmpty() && !tour_id.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("tour_id", tour_id);
                values.put("city_name", city_name);
                values.put("name", name);
                values.put("category_id", category_id);
                values.put("cat_name", cat_name);
                values.put("day", day);
                values.put("hr", hr);
                values.put("min", min);
                values.put("rating", rating);
                values.put("featured_image", featured_image);
                values.put("currency", currency);
                values.put("starting_from_price", starting_from_price);
                values.put("discount", discount);
                values.put("discount_cap", discount_cap);
                long id = db.insert("PROMO", null, values);
                db.close();
                Log.e(TAG, "Insert Data in PROMO table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }

    public ArrayList<ModelPromoExperiencesList> getPromoData() {
        try {
            //String selectQuery = "SELECT * FROM DESTINATION WHERE city_id='"+city_id+"'",data = "";
            String selectQuery = "SELECT * FROM PROMO";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelPromoExperiencesList> result = new ArrayList<ModelPromoExperiencesList>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelPromoExperiencesList promoList = new ModelPromoExperiencesList();
                promoList.setTour_id(c.getString(c.getColumnIndex("tour_id")));
                promoList.setCity(c.getString(c.getColumnIndex("city_name")));
                promoList.setName(c.getString(c.getColumnIndex("name")));
                promoList.setCategory_id(c.getString(c.getColumnIndex("category_id")));
                promoList.setCategory_name(c.getString(c.getColumnIndex("cat_name")));
                promoList.setRating(c.getString(c.getColumnIndex("rating")));
                promoList.setCurrency(c.getString(c.getColumnIndex("currency")));
                promoList.setAmount(c.getString(c.getColumnIndex("starting_from_price")));
                promoList.setImage(c.getString(c.getColumnIndex("featured_image")));
                promoList.setDiscount(c.getString(c.getColumnIndex("discount")));
                promoList.setDiscount_cap(c.getString(c.getColumnIndex("discount_cap")));

                promoList.setDay(c.getString(c.getColumnIndex("day")));
                promoList.setHour(c.getString(c.getColumnIndex("hr")));
                promoList.setMin(c.getString(c.getColumnIndex("min")));

                result.add(promoList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public void deletePromoData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("PROMO", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from PROMO Table");
    }



    /*-------------------------------------Currency-----------------------------------------*/

    public boolean setCurrencyData(String currency_name, String currency_value) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!currency_name.isEmpty() && !currency_value.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("currency_name", currency_name);
                values.put("currency_value", currency_value);
                long id = db.insert("CURRENCY", null, values);
                db.close();
                Log.e(TAG, "Insert Data in CURRENCY table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }

    public String getCurrencyValue(String currency_name) {
        try {
            String selectQuery = "SELECT currency_value FROM CURRENCY WHERE currency_name='" + currency_name + "'", data = null;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();

            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        data = cursor.getString(cursor.getColumnIndex("currency_value"));
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();

            return data;
        } catch (Exception e1) {
            return "";
        }

    }

    public boolean updateCurrencyData(String currency_name, String currency_value) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            if (!currency_name.isEmpty() && !currency_value.isEmpty()) {
                String sql = "UPDATE WAYRTOO SET responce = '" + currency_value + "' WHERE currency_name = " + currency_name;
                ContentValues cv = new ContentValues();
                cv.put("currency_value", currency_value);
                db.update("CURRENCY", cv, "currency_name" + "= ?", new String[]{currency_name});
                db.close();
                Log.e(TAG, "Column is updated ----->" + currency_name);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }

    public void deleteCurrencyData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("CURRENCY", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from CURRENCY Table");
    }


    /*----------------------------CALENDAR-----------------------------------------*/

    public boolean setCalendarData(String day, String booking_timings) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!day.isEmpty() && !booking_timings.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("day", day);
                values.put("booking_timings", booking_timings);
                long id = db.insert("CALENDAR", null, values);
                db.close();
                Log.e(TAG, "Insert Data in CALENDAR table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }

    public String getCalendarValue(String day) {
        try {
            String selectQuery = "SELECT booking_timings FROM CALENDAR WHERE day='" + day + "'", data = "";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();

            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        data = cursor.getString(cursor.getColumnIndex("booking_timings"));
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();

            return data;
        } catch (Exception e1) {
            return "";
        }

    }

    public boolean updateCalendarData(String day, String booking_timings) {
        boolean isSetInDB = false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (!day.isEmpty() && !booking_timings.isEmpty()) {
            String sql = "UPDATE CALENDAR SET booking_timings = '" + booking_timings + "' WHERE day = " + day;
            ContentValues cv = new ContentValues();
            cv.put("booking_timings", booking_timings);
            db.update("CALENDAR", cv, "day" + "= ?", new String[]{day});
            db.close();
            Log.e(TAG, "Column is updated ----->" + day);
            isSetInDB = true;
        }
        return isSetInDB;
    }

    public void deleteCalendarData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("CALENDAR", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from CALENDAR Table");
    }


    /*--------------------------------------Hotels-------------------------------------------*/

/*
  HOTEL_TABLE = "CREATE TABLE HOTEL (hotel_id TEXT NOT NULL,hotel_name TEXT NOT NULL,location TEXT NOT NULL,rating TEXT NOT NULL,starting_from_price TEXT NOT NULL,featured_image TEXT NOT NULL" +
                    ",currency TEXT NOT NULL,city_id TEXT NOT NULL,city TEXT NOT NULL,distance_from_centre TEXT NOT NULL,address TEXT NOT NULL)";
*/

    public boolean setHotelsData(String hotel_id, String hotel_name, String location, String rating, double starting_from_price, String featured_image, String currency, String city_id, String city, String distance_from_centre, String address, String Country_id) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!hotel_name.isEmpty() && !hotel_id.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("hotel_id", hotel_id);
                values.put("hotel_name", hotel_name);
                values.put("location", location);
                values.put("rating", rating);
                values.put("starting_from_price", starting_from_price);
                values.put("featured_image", featured_image);
                values.put("currency", currency);
                values.put("city_id", city_id);
                values.put("city", city);
                values.put("distance_from_centre", distance_from_centre);
                values.put("address", address);
                values.put("Country_id", Country_id);

                long id = db.insert("HOTEL", null, values);
                db.close();
                Log.e(TAG, "Insert Data in HOTEL table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }

    public ArrayList<ModelAllHotelList> getHotelAllData(String Country_id) {
        try {
            String selectQuery = "SELECT * FROM HOTEL WHERE Country_id='" + Country_id + "'", data = "";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelAllHotelList> result = new ArrayList<ModelAllHotelList>();

            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelAllHotelList promoList = new ModelAllHotelList();
                promoList.setID(c.getString(c.getColumnIndex("hotel_id")));
                promoList.setTitle(c.getString(c.getColumnIndex("hotel_name")));
                promoList.setLocation(c.getString(c.getColumnIndex("location")));
                promoList.setRating(c.getString(c.getColumnIndex("rating")));
                promoList.setAmount(c.getString(c.getColumnIndex("starting_from_price")));
                promoList.setImage(c.getString(c.getColumnIndex("featured_image")));
                promoList.setCurrency(c.getString(c.getColumnIndex("currency")));
                promoList.setCity_ID(c.getString(c.getColumnIndex("city_id")));
                promoList.setCityCountryName(c.getString(c.getColumnIndex("city")));
                promoList.setDistance_from_centre(c.getString(c.getColumnIndex("distance_from_centre")));
                promoList.setAddress(c.getString(c.getColumnIndex("address")));

                ArrayList<ModelHotelAmnetiesList> aminitiesList = new ArrayList<ModelHotelAmnetiesList>();
                aminitiesList = getAmenityAllData(c.getString(c.getColumnIndex("hotel_id")));
                promoList.setHotelAmnetiesLists(aminitiesList);

                ArrayList<ModelHotelRoomsList> roomsLists = new ArrayList<ModelHotelRoomsList>();
                roomsLists = getRoomsData(c.getString(c.getColumnIndex("hotel_id")),Country_id);
                promoList.setHotelRoomsLists(roomsLists);

                result.add(promoList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }


    public ArrayList<ModelAllHotelList> getHotelAllData(String hotel_id,String Country_id) {
        try {
            String selectQuery = "SELECT * FROM HOTEL WHERE hotel_id='" + hotel_id + "' AND Country_id='" + Country_id + "'", data = "";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelAllHotelList> result = new ArrayList<ModelAllHotelList>();

            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelAllHotelList promoList = new ModelAllHotelList();
                promoList.setID(c.getString(c.getColumnIndex("hotel_id")));
                promoList.setTitle(c.getString(c.getColumnIndex("hotel_name")));
                promoList.setLocation(c.getString(c.getColumnIndex("location")));
                promoList.setRating(c.getString(c.getColumnIndex("rating")));
                promoList.setAmount(c.getString(c.getColumnIndex("starting_from_price")));
                promoList.setImage(c.getString(c.getColumnIndex("featured_image")));
                promoList.setCurrency(c.getString(c.getColumnIndex("currency")));
                promoList.setCity_ID(c.getString(c.getColumnIndex("city_id")));
                promoList.setCityCountryName(c.getString(c.getColumnIndex("city")));
                promoList.setDistance_from_centre(c.getString(c.getColumnIndex("distance_from_centre")));
                promoList.setAddress(c.getString(c.getColumnIndex("address")));

                ArrayList<ModelHotelAmnetiesList> aminitiesList = new ArrayList<ModelHotelAmnetiesList>();
                aminitiesList = getAmenityAllData(c.getString(c.getColumnIndex("hotel_id")));
                promoList.setHotelAmnetiesLists(aminitiesList);

                ArrayList<ModelHotelRoomsList> roomsLists = new ArrayList<ModelHotelRoomsList>();
                roomsLists = getRoomsData(c.getString(c.getColumnIndex("hotel_id")),Country_id);
                promoList.setHotelRoomsLists(roomsLists);

                result.add(promoList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public ArrayList<ModelDestinationLists> getAllHotelNameData(String Country_id) {
        try {
            String selectQuery = "SELECT hotel_id,hotel_name FROM HOTEL WHERE Country_id='" + Country_id + "'", data = "";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelDestinationLists> result = new ArrayList<ModelDestinationLists>();

            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelDestinationLists promoList = new ModelDestinationLists();
                promoList.setCityId(c.getString(c.getColumnIndex("hotel_id")));
                promoList.setCountryName(c.getString(c.getColumnIndex("hotel_name")));
                result.add(promoList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }


    public ArrayList<ModelAllHotelList> getHotelFilterData(String Country_id,String Sorting_Position,String minAmt,String maxAmt,String min,String max,String Aminities) {
        try {
            String selectQuery="";
            if(Sorting_Position.equalsIgnoreCase("0")){
                 selectQuery = "SELECT * FROM HOTEL WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ")";
            }else if(Sorting_Position.equalsIgnoreCase("1")){
                 selectQuery = "SELECT * FROM HOTEL WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND rating order by rating desc";
            }if(Sorting_Position.equalsIgnoreCase("2")){
                 selectQuery = "SELECT * FROM HOTEL WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price desc";
            }if(Sorting_Position.equalsIgnoreCase("3")){
                 selectQuery = "SELECT * FROM HOTEL WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price asc";
            }
            Log.e("Query --->",selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelAllHotelList> result = new ArrayList<ModelAllHotelList>();

            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelAllHotelList promoList = new ModelAllHotelList();
                promoList.setID(c.getString(c.getColumnIndex("hotel_id")));
                promoList.setTitle(c.getString(c.getColumnIndex("hotel_name")));
                promoList.setLocation(c.getString(c.getColumnIndex("location")));
                promoList.setRating(c.getString(c.getColumnIndex("rating")));
                promoList.setAmount(c.getString(c.getColumnIndex("starting_from_price")));
                promoList.setImage(c.getString(c.getColumnIndex("featured_image")));
                promoList.setCurrency(c.getString(c.getColumnIndex("currency")));
                promoList.setCity_ID(c.getString(c.getColumnIndex("city_id")));
                promoList.setCityCountryName(c.getString(c.getColumnIndex("city")));
                promoList.setDistance_from_centre(c.getString(c.getColumnIndex("distance_from_centre")));
                promoList.setAddress(c.getString(c.getColumnIndex("address")));

                ArrayList<ModelHotelRoomsList> roomsLists = new ArrayList<ModelHotelRoomsList>();
                roomsLists = getRoomsData(c.getString(c.getColumnIndex("hotel_id")),Country_id);
                promoList.setHotelRoomsLists(roomsLists);

                ArrayList<ModelHotelAmnetiesList> aminitiesList = new ArrayList<ModelHotelAmnetiesList>();
                ArrayList<ModelHotelAmnetiesList> aminitiesFilterList = new ArrayList<ModelHotelAmnetiesList>();

                if(Aminities.equalsIgnoreCase("()")){
                    aminitiesList = getAmenityAllData(c.getString(c.getColumnIndex("hotel_id")));
                    promoList.setHotelAmnetiesLists(aminitiesList);
                    result.add(promoList);
                }else{
                    aminitiesFilterList = getAmenityAllFilterData(c.getString(c.getColumnIndex("hotel_id")),Aminities);
                    if(aminitiesFilterList.size()>0){
                        aminitiesList = getAmenityAllData(c.getString(c.getColumnIndex("hotel_id")));
                        promoList.setHotelAmnetiesLists(aminitiesList);
                        result.add(promoList);
                    }
                }
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }


    public String getMinValue(String Country_id) {
        String minValue = "0";
        try {

            String selectQuery = "SELECT starting_from_price, min(starting_from_price) FROM HOTEL WHERE Country_id='" + Country_id + "'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                minValue = c.getString(c.getColumnIndex("starting_from_price"));
                c.moveToNext();
            }
            return minValue;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return minValue;
    }


    public String getMaxValue(String Country_id) {
        String maxValue = "0";
        try {

            String selectQuery = "SELECT starting_from_price, max(starting_from_price) FROM HOTEL WHERE Country_id='" + Country_id + "'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                maxValue = c.getString(c.getColumnIndex("starting_from_price"));
                c.moveToNext();
            }
            return maxValue;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return maxValue;
    }


    public void deleteHotelsData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("HOTEL", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from HOTEL Table");
    }



    /*--------------------------------------Activity-------------------------------------------*/


/*
 ACTIVITY_TABLE = "CREATE TABLE ACTIVITY (city_id TEXT NOT NULL ,city_name Text NOT NULL,category_id Text NOT NULL,category_name Text NOT NULL,currency Text NOT NULL,discount Text,discount_cap Text" +
                    ",day Text NOT NULL,hr Text NOT NULL,min Text NOT NULL,featured_image Text NOT NULL,from_time Text ,to_time Text ,is_pricing_per_pax Text ,is_special_offer Text ,latitude Text , longitude Text ,maximum_pax Text, name Text NOT NULL, rating Text NOT NULL,tour_id Text NOT NULL,starting_from_price Text NOT NULL,address Text NOT NULL,transfer_included Text,Country_id TEXT NOT NULL)";
*/

    public boolean setActivityData(String tour_id, String name, String category_id, String category_name, String discount,String discount_cap, String day,String hr,String min, String rating, double starting_from_price, String featured_image, String currency, String city_id, String city_name, String from_time,String to_time, String address,String is_pricing_per_pax,String is_special_offer,String latitude,String longitude,String maximum_pax,String transfer_included, String Country_id,String selected) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!name.isEmpty() && !tour_id.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("tour_id", tour_id);
                values.put("name", name);
                values.put("category_id",category_id);
                values.put("category_name",category_name);
                values.put("discount",discount);
                values.put("discount_cap",discount_cap);
                values.put("day", day);
                values.put("hr", hr);
                values.put("min", min);
                values.put("rating", rating);
                values.put("starting_from_price", starting_from_price);
                values.put("featured_image", featured_image);
                values.put("currency", currency);
                values.put("city_id", city_id);
                values.put("city_name", city_name);
                values.put("from_time", from_time);
                values.put("to_time", to_time);
                values.put("is_pricing_per_pax", is_pricing_per_pax);
                values.put("is_special_offer",is_special_offer);
                values.put("latitude",latitude);
                values.put("longitude",longitude);
                values.put("maximum_pax",maximum_pax);
                values.put("transfer_included",transfer_included);
                values.put("address", address);
                values.put("Country_id", Country_id);
                values.put("Selected",selected);

                long id = db.insert("ACTIVITY", null, values);
                db.close();
                Log.e(TAG, "Insert Data in ACTIVITY table " + id);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }

    public ArrayList<ModelActivitiesList> getActivityAllData(String Country_id) {
        try {
            String selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "'", data = "";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelActivitiesList> result = new ArrayList<ModelActivitiesList>();

            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelActivitiesList actList = new ModelActivitiesList();
                actList.setTour_id(c.getString(c.getColumnIndex("tour_id")));
                actList.setName(c.getString(c.getColumnIndex(  "name")));
                actList.setCategory_id(c.getString(c.getColumnIndex(  "category_id")));
                actList.setCategory(c.getString(c.getColumnIndex(  "category_name")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount_cap")));
                actList.setDay(c.getString(c.getColumnIndex(  "day")));
                actList.setHour(c.getString(c.getColumnIndex(  "hr")));
                actList.setMin(c.getString(c.getColumnIndex(  "min")));
                actList.setRating(c.getString(c.getColumnIndex(  "rating")));
                actList.setStarting_from_price(c.getString(c.getColumnIndex(  "starting_from_price")));
                actList.setFeatured_image(c.getString(c.getColumnIndex(  "featured_image")));
                actList.setCurrency(c.getString(c.getColumnIndex(  "currency")));
                actList.setCity_id(c.getString(c.getColumnIndex(  "city_id")));
                actList.setCity(c.getString(c.getColumnIndex(  "city_name")));
                actList.setFrom_time(c.getString(c.getColumnIndex(  "from_time")));
                actList.setTo_time(c.getString(c.getColumnIndex(  "to_time")));
                actList.setIs_pricing_per_pax(c.getString(c.getColumnIndex(  "is_pricing_per_pax")));
                actList.setIs_special_offer(c.getString(c.getColumnIndex(  "is_special_offer")));
                actList.setLatitude(c.getString(c.getColumnIndex(  "latitude")));
                actList.setLongitude(c.getString(c.getColumnIndex(  "longitude")));
                actList.setMaximum_pax(c.getString(c.getColumnIndex(  "maximum_pax")));
                actList.setTransfer_included(c.getString(c.getColumnIndex(  "transfer_included")));
                actList.setAddress(c.getString(c.getColumnIndex(  "address")));
                actList.setAdded_in_itinerary(c.getString(c.getColumnIndex(  "Selected")));

                ArrayList<ModelSubDiningPreferences> subDiningLists = new ArrayList<ModelSubDiningPreferences>();
                subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                actList.setModelSubDiningPreferences(subDiningLists);

                ArrayList<ModelSubCategoryPreferences> subCatLists = new ArrayList<ModelSubCategoryPreferences>();
                subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                actList.setModelSubCategoryPreferences(subCatLists);

                result.add(actList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public ArrayList<ModelActivitiesList> getActivityCatData(String Country_id,String cat_id) {
        try {
            String selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id ='" + cat_id + "'", data = "";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelActivitiesList> result = new ArrayList<ModelActivitiesList>();

            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelActivitiesList actList = new ModelActivitiesList();
                actList.setTour_id(c.getString(c.getColumnIndex("tour_id")));
                actList.setName(c.getString(c.getColumnIndex(  "name")));
                actList.setCategory_id(c.getString(c.getColumnIndex(  "category_id")));
                actList.setCategory(c.getString(c.getColumnIndex(  "category_name")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount_cap")));
                actList.setDay(c.getString(c.getColumnIndex(  "day")));
                actList.setHour(c.getString(c.getColumnIndex(  "hr")));
                actList.setMin(c.getString(c.getColumnIndex(  "min")));
                actList.setRating(c.getString(c.getColumnIndex(  "rating")));
                actList.setStarting_from_price(c.getString(c.getColumnIndex(  "starting_from_price")));
                actList.setFeatured_image(c.getString(c.getColumnIndex(  "featured_image")));
                actList.setCurrency(c.getString(c.getColumnIndex(  "currency")));
                actList.setCity_id(c.getString(c.getColumnIndex(  "city_id")));
                actList.setCity(c.getString(c.getColumnIndex(  "city_name")));
                actList.setFrom_time(c.getString(c.getColumnIndex(  "from_time")));
                actList.setTo_time(c.getString(c.getColumnIndex(  "to_time")));
                actList.setIs_pricing_per_pax(c.getString(c.getColumnIndex(  "is_pricing_per_pax")));
                actList.setIs_special_offer(c.getString(c.getColumnIndex(  "is_special_offer")));
                actList.setLatitude(c.getString(c.getColumnIndex(  "latitude")));
                actList.setLongitude(c.getString(c.getColumnIndex(  "longitude")));
                actList.setMaximum_pax(c.getString(c.getColumnIndex(  "maximum_pax")));
                actList.setTransfer_included(c.getString(c.getColumnIndex(  "transfer_included")));
                actList.setAddress(c.getString(c.getColumnIndex(  "address")));
                actList.setAdded_in_itinerary(c.getString(c.getColumnIndex(  "Selected")));

                ArrayList<ModelSubDiningPreferences> subDiningLists = new ArrayList<ModelSubDiningPreferences>();
                subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                actList.setModelSubDiningPreferences(subDiningLists);

                ArrayList<ModelSubCategoryPreferences> subCatLists = new ArrayList<ModelSubCategoryPreferences>();
                subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                actList.setModelSubCategoryPreferences(subCatLists);

                result.add(actList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }


    public ArrayList<ModelActivitiesList> getActivityItineraryData(String tour_id,String Country_id) {
        try {
            String selectQuery = "SELECT * FROM ACTIVITY WHERE tour_id='" + tour_id + "' AND Country_id='" + Country_id + "'", data = "";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelActivitiesList> result = new ArrayList<ModelActivitiesList>();

            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelActivitiesList actList = new ModelActivitiesList();
                actList.setTour_id(c.getString(c.getColumnIndex("tour_id")));
                actList.setName(c.getString(c.getColumnIndex(  "name")));
                actList.setCategory_id(c.getString(c.getColumnIndex(  "category_id")));
                actList.setCategory(c.getString(c.getColumnIndex(  "category_name")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount_cap")));
                actList.setDay(c.getString(c.getColumnIndex(  "day")));
                actList.setHour(c.getString(c.getColumnIndex(  "hr")));
                actList.setMin(c.getString(c.getColumnIndex(  "min")));
                actList.setRating(c.getString(c.getColumnIndex(  "rating")));
                actList.setStarting_from_price(c.getString(c.getColumnIndex(  "starting_from_price")));
                actList.setFeatured_image(c.getString(c.getColumnIndex(  "featured_image")));
                actList.setCurrency(c.getString(c.getColumnIndex(  "currency")));
                actList.setCity_id(c.getString(c.getColumnIndex(  "city_id")));
                actList.setCity(c.getString(c.getColumnIndex(  "city_name")));
                actList.setFrom_time(c.getString(c.getColumnIndex(  "from_time")));
                actList.setTo_time(c.getString(c.getColumnIndex(  "to_time")));
                actList.setIs_pricing_per_pax(c.getString(c.getColumnIndex(  "is_pricing_per_pax")));
                actList.setIs_special_offer(c.getString(c.getColumnIndex(  "is_special_offer")));
                actList.setLatitude(c.getString(c.getColumnIndex(  "latitude")));
                actList.setLongitude(c.getString(c.getColumnIndex(  "longitude")));
                actList.setMaximum_pax(c.getString(c.getColumnIndex(  "maximum_pax")));
                actList.setTransfer_included(c.getString(c.getColumnIndex(  "transfer_included")));
                actList.setAddress(c.getString(c.getColumnIndex(  "address")));
                actList.setAdded_in_itinerary(c.getString(c.getColumnIndex(  "Selected")));

               /* ArrayList<ModelSubDiningPreferences> subDiningLists = new ArrayList<ModelSubDiningPreferences>();
                subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                actList.setModelSubDiningPreferences(subDiningLists);

                ArrayList<ModelSubCategoryPreferences> subCatLists = new ArrayList<ModelSubCategoryPreferences>();
                subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                actList.setModelSubCategoryPreferences(subCatLists);*/

                result.add(actList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }


    public ArrayList<ModelActivitiesList> getActivityCustFilterData(String Country_id,String Sorting_Position,String minAmt,String maxAmt,String min,String max,String DiningFilter,String CatFilter,String Transfer,int tabID) {
        try {
            String selectQuery="";

            if(tabID==0){
                if(Transfer.equalsIgnoreCase("")) {
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ")";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price asc";
                    }
                }else{
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "'";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price asc";
                    }
                }
            }else{
                if(Transfer.equalsIgnoreCase("")) {
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ")";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price asc";
                    }
                }else{
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "'";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price asc";
                    }
                }
            }


            /*if (tabID == 8) {
                if(Transfer.equalsIgnoreCase("")) {
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ")";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price asc";
                    }
                }else{
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "'";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price asc";
                    }
                }
            }else if(tabID==0){
                if(Transfer.equalsIgnoreCase("")) {
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ")";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price asc";
                    }
                }else{
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "'";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price asc";
                    }
                }
            }else if(tabID==6){
                if(Transfer.equalsIgnoreCase("")) {
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ")";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price asc";
                    }
                }else{
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "'";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price asc";
                    }
                }
            }else{
                if(Transfer.equalsIgnoreCase("")) {
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ")";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price asc";
                    }
                }else{
                    if (Sorting_Position.equalsIgnoreCase("0")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "'";
                    } else if (Sorting_Position.equalsIgnoreCase("1")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND rating order by rating desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("2")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price desc";
                    }
                    if (Sorting_Position.equalsIgnoreCase("3")) {
                        selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND category_id='" + tabID + "'AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price asc";
                    }
                }
            }*/


            Log.e("Query --->",selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelActivitiesList> result = new ArrayList<ModelActivitiesList>();

            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelActivitiesList actList = new ModelActivitiesList();
                actList.setTour_id(c.getString(c.getColumnIndex("tour_id")));
                actList.setName(c.getString(c.getColumnIndex(  "name")));
                actList.setCategory_id(c.getString(c.getColumnIndex(  "category_id")));
                actList.setCategory(c.getString(c.getColumnIndex(  "category_name")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount_cap")));
                actList.setDay(c.getString(c.getColumnIndex(  "day")));
                actList.setHour(c.getString(c.getColumnIndex(  "hr")));
                actList.setMin(c.getString(c.getColumnIndex(  "min")));
                actList.setRating(c.getString(c.getColumnIndex(  "rating")));
                actList.setStarting_from_price(c.getString(c.getColumnIndex(  "starting_from_price")));
                actList.setFeatured_image(c.getString(c.getColumnIndex(  "featured_image")));
                actList.setCurrency(c.getString(c.getColumnIndex(  "currency")));
                actList.setCity_id(c.getString(c.getColumnIndex(  "city_id")));
                actList.setCity(c.getString(c.getColumnIndex(  "city_name")));
                actList.setFrom_time(c.getString(c.getColumnIndex(  "from_time")));
                actList.setTo_time(c.getString(c.getColumnIndex(  "to_time")));
                actList.setIs_pricing_per_pax(c.getString(c.getColumnIndex(  "is_pricing_per_pax")));
                actList.setIs_special_offer(c.getString(c.getColumnIndex(  "is_special_offer")));
                actList.setLatitude(c.getString(c.getColumnIndex(  "latitude")));
                actList.setLongitude(c.getString(c.getColumnIndex(  "longitude")));
                actList.setMaximum_pax(c.getString(c.getColumnIndex(  "maximum_pax")));
                actList.setTransfer_included(c.getString(c.getColumnIndex(  "transfer_included")));
                actList.setAddress(c.getString(c.getColumnIndex(  "address")));
                actList.setAdded_in_itinerary(c.getString(c.getColumnIndex(  "Selected")));



                ArrayList<ModelSubDiningPreferences> subDiningLists = new ArrayList<ModelSubDiningPreferences>();
                ArrayList<ModelSubDiningPreferences> subDiningFilterList = new ArrayList<ModelSubDiningPreferences>();

                ArrayList<ModelSubCategoryPreferences> subCatLists = new ArrayList<ModelSubCategoryPreferences>();
                ArrayList<ModelSubCategoryPreferences> subCatFilterList = new ArrayList<ModelSubCategoryPreferences>();

                if(DiningFilter.equalsIgnoreCase("()") && CatFilter.equalsIgnoreCase("()")){
                    subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                    actList.setModelSubDiningPreferences(subDiningLists);

                    subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                    actList.setModelSubCategoryPreferences(subCatLists);

                    result.add(actList);

                }else if(!DiningFilter.equalsIgnoreCase("()") && CatFilter.equalsIgnoreCase("()")){

                    subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                    actList.setModelSubCategoryPreferences(subCatLists);

                    subDiningFilterList = getSubDinningFilterData(c.getString(c.getColumnIndex("tour_id")),DiningFilter);
                    if(subDiningFilterList.size()>0){
                        subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                        actList.setModelSubDiningPreferences(subDiningLists);
                        result.add(actList);
                    }


                }else if(DiningFilter.equalsIgnoreCase("()") && !CatFilter.equalsIgnoreCase("()")){
                    subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                    actList.setModelSubDiningPreferences(subDiningLists);

                    subCatFilterList = getSubCategoryFilterData(c.getString(c.getColumnIndex("tour_id")),CatFilter);
                    if(subCatFilterList.size()>0){
                        subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                        actList.setModelSubCategoryPreferences(subCatLists);
                        result.add(actList);
                    }

                }else if(!DiningFilter.equalsIgnoreCase("()") && !CatFilter.equalsIgnoreCase("()")){

                    subDiningFilterList = getSubDinningFilterData(c.getString(c.getColumnIndex("tour_id")),DiningFilter);
                    if(subDiningFilterList.size()>0){
                        subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                        actList.setModelSubDiningPreferences(subDiningLists);
                    }

                    subCatFilterList = getSubCategoryFilterData(c.getString(c.getColumnIndex("tour_id")),CatFilter);
                    if(subCatFilterList.size()>0){
                        subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                        actList.setModelSubCategoryPreferences(subCatLists);
                    }
                    result.add(actList);
                }

                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public ArrayList<ModelActivitiesList> getActivityFilterData(String Country_id,String Sorting_Position,String minAmt,String maxAmt,String min,String max,String DiningFilter,String CatFilter,String Transfer) {
        try {
            String selectQuery="";
            if(Transfer.equalsIgnoreCase("")) {
                if (Sorting_Position.equalsIgnoreCase("0")) {
                    selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ")";
                } else if (Sorting_Position.equalsIgnoreCase("1")) {
                    selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND rating order by rating desc";
                }
                if (Sorting_Position.equalsIgnoreCase("2")) {
                    selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price desc";
                }
                if (Sorting_Position.equalsIgnoreCase("3")) {
                    selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND starting_from_price order by starting_from_price asc";
                }
            }else{
                if (Sorting_Position.equalsIgnoreCase("0")) {
                    selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "'";
                } else if (Sorting_Position.equalsIgnoreCase("1")) {
                    selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND rating order by rating desc";
                }
                if (Sorting_Position.equalsIgnoreCase("2")) {
                    selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price desc";
                }
                if (Sorting_Position.equalsIgnoreCase("3")) {
                    selectQuery = "SELECT * FROM ACTIVITY WHERE Country_id='" + Country_id + "' AND (starting_from_price BETWEEN '" + minAmt + "' AND '" + maxAmt + "') AND (rating BETWEEN " + min + " AND " + max + ") AND transfer_included='" + Transfer + "' AND starting_from_price order by starting_from_price asc";
                }
            }

            Log.e("Query --->",selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelActivitiesList> result = new ArrayList<ModelActivitiesList>();

            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelActivitiesList actList = new ModelActivitiesList();
                actList.setTour_id(c.getString(c.getColumnIndex("tour_id")));
                actList.setName(c.getString(c.getColumnIndex(  "name")));
                actList.setCategory_id(c.getString(c.getColumnIndex(  "category_id")));
                actList.setCategory(c.getString(c.getColumnIndex(  "category_name")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount")));
                actList.setDiscount(c.getString(c.getColumnIndex(  "discount_cap")));
                actList.setDay(c.getString(c.getColumnIndex(  "day")));
                actList.setHour(c.getString(c.getColumnIndex(  "hr")));
                actList.setMin(c.getString(c.getColumnIndex(  "min")));
                actList.setRating(c.getString(c.getColumnIndex(  "rating")));
                actList.setStarting_from_price(c.getString(c.getColumnIndex(  "starting_from_price")));
                actList.setFeatured_image(c.getString(c.getColumnIndex(  "featured_image")));
                actList.setCurrency(c.getString(c.getColumnIndex(  "currency")));
                actList.setCity_id(c.getString(c.getColumnIndex(  "city_id")));
                actList.setCity(c.getString(c.getColumnIndex(  "city_name")));
                actList.setFrom_time(c.getString(c.getColumnIndex(  "from_time")));
                actList.setTo_time(c.getString(c.getColumnIndex(  "to_time")));
                actList.setIs_pricing_per_pax(c.getString(c.getColumnIndex(  "is_pricing_per_pax")));
                actList.setIs_special_offer(c.getString(c.getColumnIndex(  "is_special_offer")));
                actList.setLatitude(c.getString(c.getColumnIndex(  "latitude")));
                actList.setLongitude(c.getString(c.getColumnIndex(  "longitude")));
                actList.setMaximum_pax(c.getString(c.getColumnIndex(  "maximum_pax")));
                actList.setTransfer_included(c.getString(c.getColumnIndex(  "transfer_included")));
                actList.setAddress(c.getString(c.getColumnIndex(  "address")));
                actList.setAdded_in_itinerary(c.getString(c.getColumnIndex(  "Selected")));


                ArrayList<ModelSubDiningPreferences> subDiningLists = new ArrayList<ModelSubDiningPreferences>();
                ArrayList<ModelSubDiningPreferences> subDiningFilterList = new ArrayList<ModelSubDiningPreferences>();

                ArrayList<ModelSubCategoryPreferences> subCatLists = new ArrayList<ModelSubCategoryPreferences>();
                ArrayList<ModelSubCategoryPreferences> subCatFilterList = new ArrayList<ModelSubCategoryPreferences>();

                if(DiningFilter.equalsIgnoreCase("()") && CatFilter.equalsIgnoreCase("()")){
                    subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                    actList.setModelSubDiningPreferences(subDiningLists);

                    subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                    actList.setModelSubCategoryPreferences(subCatLists);

                    result.add(actList);

                }else if(!DiningFilter.equalsIgnoreCase("()") && CatFilter.equalsIgnoreCase("()")){

                    subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                    actList.setModelSubCategoryPreferences(subCatLists);

                    subDiningFilterList = getSubDinningFilterData(c.getString(c.getColumnIndex("tour_id")),DiningFilter);
                    if(subDiningFilterList.size()>0){
                        subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                        actList.setModelSubDiningPreferences(subDiningLists);
                        result.add(actList);
                    }


                }else if(DiningFilter.equalsIgnoreCase("()") && !CatFilter.equalsIgnoreCase("()")){
                    subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                    actList.setModelSubDiningPreferences(subDiningLists);

                    subCatFilterList = getSubCategoryFilterData(c.getString(c.getColumnIndex("tour_id")),CatFilter);
                    if(subCatFilterList.size()>0){
                        subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                        actList.setModelSubCategoryPreferences(subCatLists);
                        result.add(actList);
                    }

                }else if(!DiningFilter.equalsIgnoreCase("()") && !CatFilter.equalsIgnoreCase("()")){

                    subDiningFilterList = getSubDinningFilterData(c.getString(c.getColumnIndex("tour_id")),DiningFilter);
                    if(subDiningFilterList.size()>0){
                        subDiningLists = getSubDinningData(c.getString(c.getColumnIndex("tour_id")));
                        actList.setModelSubDiningPreferences(subDiningLists);
                    }

                    subCatFilterList = getSubCategoryFilterData(c.getString(c.getColumnIndex("tour_id")),CatFilter);
                    if(subCatFilterList.size()>0){
                        subCatLists = getSubCategoryData(c.getString(c.getColumnIndex("tour_id")));
                        actList.setModelSubCategoryPreferences(subCatLists);
                    }
                    result.add(actList);
                }

                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }


    public String getMinValueAct(String Country_id) {
        String minValue = "0";
        try {

            String selectQuery = "SELECT starting_from_price, min(starting_from_price) FROM ACTIVITY WHERE Country_id='" + Country_id + "'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                minValue = c.getString(c.getColumnIndex("starting_from_price"));
                c.moveToNext();
            }
            return minValue;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return minValue;
    }


    public String getMaxValueAct(String Country_id) {
        String maxValue = "0";
        try {

            String selectQuery = "SELECT starting_from_price, max(starting_from_price) FROM ACTIVITY WHERE Country_id='" + Country_id + "'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                maxValue = c.getString(c.getColumnIndex("starting_from_price"));
                c.moveToNext();
            }
            return maxValue;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return maxValue;
    }
    public boolean updateOneColumn(String tour_id,String Country_id,String selected) {
        try {
            String selectQuery = "UPDATE ACTIVITY SET Selected = '" + selected + "' WHERE tour_id='" + tour_id + "' AND Country_id='" + Country_id + "'", data = "";
            Log.e("Update query",selectQuery);
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            Log.e(TAG, "update Data in ACTIVITY table ");
            if(cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            cursor.close();
            return true;
        }catch (Exception e){
            return false;
        }

    }

    public boolean updateAllColumn(String Old,String New) {
        try {
            String selectQuery = "UPDATE ACTIVITY SET Selected = '" + New + "' WHERE Selected = '" + Old + "' ", data = "";
            Log.e("Update query",selectQuery);
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            Log.e(TAG, "update Data in ACTIVITY table ");
            if(cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            cursor.close();
            return true;
        }catch (Exception e){
            return false;
        }

    }

    public void deleteActivityData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("ACTIVITY", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from ACTIVITY Table");
    }

   /* public void updateOneColumn(String tour_id,String Country_id,String selected){
        try {
            String selectQuery = "UPDATE ACTIVITY SET Selected = '" + selected + "' WHERE tour_id='" + tour_id + "' AND Country_id='" + Country_id + "'", data = "";
            // String sql = "UPDATE ACTIVITY SET Selected = "+selected+" WHERE tour_id = "+tour_id+" AND Country_id = "+Country_id;
            SQLiteDatabase db = this.getWritableDatabase();
            db.beginTransaction();
            Cursor c = db.rawQuery(selectQuery, null);

            Log.e(TAG, "update Data in ACTIVITY_ITINERARY table ");

        }catch (Exception e){

        }


    }*/




    /*--------------------------------------SUBDINING_TABLE-------------------------------------------*/
    //SUBDINING_TABLE = "CREATE TABLE SUBDINING (id Text NOT NULL,name Text NOT NULL,count Text,tour_id Text NOT NULL)";

    public boolean setSubDinningData(String id, String name, String count,String tour_id) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!id.isEmpty() && !name.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("id", id);
                values.put("name", name);
                values.put("count", count);
                values.put("tour_id", tour_id);

                long ids = db.insert("SUBDINING", null, values);
                db.close();
                Log.e(TAG, "Insert Data in SUBDINING table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }

    public ArrayList<ModelSubDiningPreferences> getSubDinningData(String tour_id) {
        try {
            String selectQuery = "SELECT * FROM SUBDINING WHERE tour_id='" + tour_id + "'", data = "";
            //String selectQuery = "SELECT * FROM AMNETIES";
            Log.e("SUBDINING Query --->",selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelSubDiningPreferences> result = new ArrayList<ModelSubDiningPreferences>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelSubDiningPreferences promoList = new ModelSubDiningPreferences();
                promoList.setSubdiningid(c.getString(c.getColumnIndex("id")));
                promoList.setSubdiningName(c.getString(c.getColumnIndex("name")));
                promoList.setCount(c.getString(c.getColumnIndex("count")));
                promoList.setTour_id(c.getString(c.getColumnIndex("tour_id")));

                result.add(promoList);
                c.moveToNext();
            }
            c.close();
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public ArrayList<ModelSubDiningPreferences> getSubDinningFilterData(String tour_id,String DiningFilter) {
        try {
            String selectQuery = "SELECT * FROM SUBDINING WHERE tour_id='" + tour_id + "' AND id in "+DiningFilter, data = "";
            //String selectQuery = "SELECT * FROM AMNETIES";
            Log.e("Dining filter Query-->",selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelSubDiningPreferences> result = new ArrayList<ModelSubDiningPreferences>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelSubDiningPreferences promoList = new ModelSubDiningPreferences();
                promoList.setSubdiningid(c.getString(c.getColumnIndex("id")));
                promoList.setSubdiningName(c.getString(c.getColumnIndex("name")));
                promoList.setCount(c.getString(c.getColumnIndex("count")));
                promoList.setTour_id(c.getString(c.getColumnIndex("tour_id")));

                result.add(promoList);
                c.moveToNext();
            }
            c.close();
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public void deleteSubDiningData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("SUBDINING", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from SUBDINING Table");
    }



    /*--------------------------------------SUBDINING_TABLE-------------------------------------------*/
    //SUBDINING_TABLE = "CREATE TABLE SUBCATEGORY (id Text NOT NULL,name Text NOT NULL,count Text,tour_id Text NOT NULL)";

    public boolean setSubCategoryData(String id, String name, String count,String tour_id) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!id.isEmpty() && !name.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("id", id);
                values.put("name", name);
                values.put("count", count);
                values.put("tour_id", tour_id);

                long ids = db.insert("SUBCATEGORY", null, values);
                db.close();
                Log.e(TAG, "Insert Data in SUBCATEGORY table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }

    public ArrayList<ModelSubCategoryPreferences> getSubCategoryData(String tour_id) {
        try {
            String selectQuery = "SELECT * FROM SUBCATEGORY WHERE tour_id='" + tour_id + "'", data = "";
            //String selectQuery = "SELECT * FROM AMNETIES";
            Log.e("SUBDINING Query --->",selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelSubCategoryPreferences> result = new ArrayList<ModelSubCategoryPreferences>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelSubCategoryPreferences promoList = new ModelSubCategoryPreferences();
                promoList.setSubcategoryid(c.getString(c.getColumnIndex("id")));
                promoList.setSubcategoryName(c.getString(c.getColumnIndex("name")));
                promoList.setCount(c.getString(c.getColumnIndex("count")));
                promoList.setTour_id(c.getString(c.getColumnIndex("tour_id")));

                result.add(promoList);
                c.moveToNext();
            }
            c.close();
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public ArrayList<ModelSubCategoryPreferences> getSubCategoryFilterData(String tour_id,String CatFilter) {
        try {
            String selectQuery = "SELECT * FROM SUBCATEGORY WHERE tour_id='" + tour_id + "' AND id in "+CatFilter, data = "";
            //String selectQuery = "SELECT * FROM AMNETIES";
            Log.e("Dining filter Query-->",selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelSubCategoryPreferences> result = new ArrayList<ModelSubCategoryPreferences>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelSubCategoryPreferences promoList = new ModelSubCategoryPreferences();
                promoList.setSubcategoryid(c.getString(c.getColumnIndex("id")));
                promoList.setSubcategoryName(c.getString(c.getColumnIndex("name")));
                promoList.setCount(c.getString(c.getColumnIndex("count")));
                promoList.setTour_id(c.getString(c.getColumnIndex("tour_id")));

                result.add(promoList);
                c.moveToNext();
            }
            c.close();
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public void deleteSubCategoryData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("SUBCATEGORY", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from SUBCATEGORY Table");
    }


    /*--------------------------------------AMNETIES-------------------------------------------*/
    // AMNETIES_TABLE = "CREATE TABLE AMNETIES (id Text NOT NULL,amnety Text NOT NULL,hotel_id Text NOT NULL)";

    public boolean setAmenityData(String id, String amnety, String hotel_id) {
        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!id.isEmpty() && !amnety.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("id", id);
                values.put("amnety", amnety);
                values.put("hotel_id", hotel_id);

                long ids = db.insert("AMNETIES", null, values);
                db.close();
                Log.e(TAG, "Insert Data in AMNETIES table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }

    }


    public ArrayList<ModelHotelAmnetiesList> getAmenityAllData(String hotel_id) {
        try {
            String selectQuery = "SELECT * FROM AMNETIES WHERE hotel_id='" + hotel_id + "'", data = "";
            //String selectQuery = "SELECT * FROM AMNETIES";
            Log.e("Query Amneties --->",selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelHotelAmnetiesList> result = new ArrayList<ModelHotelAmnetiesList>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelHotelAmnetiesList promoList = new ModelHotelAmnetiesList();
                promoList.setID(c.getString(c.getColumnIndex("id")));
                promoList.setName(c.getString(c.getColumnIndex("amnety")));
                promoList.setHotel_ID(c.getString(c.getColumnIndex("hotel_id")));

                result.add(promoList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public ArrayList<ModelHotelAmnetiesList> getAmenityAllFilterData(String hotel_id,String Aminities) {
        try {
            String selectQuery = "SELECT * FROM AMNETIES WHERE hotel_id='" + hotel_id + "' AND id in "+Aminities, data = "";
            //String selectQuery = "SELECT * FROM AMNETIES";
            Log.e("Query Amneties --->",selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelHotelAmnetiesList> result = new ArrayList<ModelHotelAmnetiesList>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelHotelAmnetiesList promoList = new ModelHotelAmnetiesList();
                promoList.setID(c.getString(c.getColumnIndex("id")));
                promoList.setName(c.getString(c.getColumnIndex("amnety")));
                promoList.setHotel_ID(c.getString(c.getColumnIndex("hotel_id")));

                result.add(promoList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }


    }

    public void deleteAmenityData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("AMNETIES", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from AMNETIES Table");
    }


    //   selected hotel object data

    public boolean setRoomsData(String room_id, String room_type, String remarks, String net_rate, String meal_type, String extra_bed_cost, String currency, String booking_type, String batch_size, String block_dates,String additional_tax, String additional_surcharge, String hotel_id,String Country_id) {

        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!room_id.isEmpty() && !room_type.isEmpty()) {
                ContentValues values = new ContentValues();
                values.put("room_id", room_id);
                values.put("room_type", room_type);
                values.put("remarks", remarks);
                values.put("net_rate", net_rate);
                values.put("meal_type", meal_type);
                values.put("extra_bed_cost", extra_bed_cost);
                values.put("currency", currency);
                values.put("booking_type", booking_type);
                values.put("batch_size", batch_size);
                values.put("block_dates",block_dates);
                values.put("additional_tax", additional_tax);
                values.put("additional_surcharge", additional_surcharge);
                values.put("hotel_id", hotel_id);
                values.put("Country_id",Country_id);
                long ids = db.insert("ROOMSDATA", null, values);
                db.close();
                Log.e(TAG, "Insert Data in ROOMSDATA table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }


    }


    public ArrayList<ModelHotelRoomsList> getRoomsData(String hotel_id,String Country_id) {
        try {
            String selectQuery = "SELECT * FROM ROOMSDATA WHERE hotel_id='" + hotel_id + "' AND Country_id='" + Country_id + "'", data = "";
            //String selectQuery = "SELECT * FROM ROOMSDATA";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelHotelRoomsList> result = new ArrayList<ModelHotelRoomsList>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelHotelRoomsList roomsList = new ModelHotelRoomsList();
                roomsList.setRoom_id(c.getString(c.getColumnIndex("room_id")));
                roomsList.setRoom_type(c.getString(c.getColumnIndex("room_type")));
                roomsList.setRemarks(c.getString(c.getColumnIndex("remarks")));
                roomsList.setNet_rate(c.getString(c.getColumnIndex("net_rate")));
                roomsList.setSelect(c.getString(c.getColumnIndex("meal_type")));
                roomsList.setExtra_bed_cost(c.getString(c.getColumnIndex("extra_bed_cost")));
                roomsList.setCurrency(c.getString(c.getColumnIndex("currency")));
                roomsList.setBooking_type(c.getString(c.getColumnIndex("booking_type")));
                roomsList.setBatch_size(c.getString(c.getColumnIndex("batch_size")));
                roomsList.setBlock_dates(c.getString(c.getColumnIndex("block_dates")));
                roomsList.setAdditional_tax(c.getString(c.getColumnIndex("additional_tax")));
                roomsList.setAdditional_surcharge(c.getString(c.getColumnIndex("additional_surcharge")));
                roomsList.setHotel_id(c.getString(c.getColumnIndex("hotel_id")));
                roomsList.setCountry_id(c.getString(c.getColumnIndex("Country_id")));

                ArrayList<ModelHotelRoomsPricingsList> roomsLists = new ArrayList<ModelHotelRoomsPricingsList>();
                //String id = String.valueOf();
                roomsLists = getHotelRoomPricingData(c.getString(c.getColumnIndex("room_id")),c.getString(c.getColumnIndex("hotel_id")),Country_id);
                roomsList.setRoomsPricingsLists(roomsLists);

                result.add(roomsList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }
    }

    public boolean updateOneColumnHotelRooms(String hotel_id,String room_id,String selected) {
        try {
            String selectQuery = "UPDATE ROOMSDATA SET meal_type = '" + selected + "' WHERE hotel_id='" + hotel_id + "' AND room_id='" + room_id + "'", data = "";
            Log.e("Update query",selectQuery);
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            Log.e(TAG, "update Data in ROOMSDATA table ");
            if(cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            cursor.close();
            return true;
        }catch (Exception e){
            return false;
        }

    }

    public boolean updateAllColumnHotelRooms(String Old,String New) {
        try {
            String selectQuery = "UPDATE ROOMSDATA SET meal_type = '" + New + "' WHERE meal_type = '" + Old + "' ", data = "";
            Log.e("Update query",selectQuery);
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            Log.e(TAG, "update Data in ROOMSDATA table ");
            if(cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            cursor.close();
            return true;
        }catch (Exception e){
            return false;
        }

    }

    public void deleteRoomsData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("ROOMSDATA", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from ROOMSDATA Table");
    }


    /*-------------------------------------Itinerary Hotels List--------------------------------------*/


//HOTEL_ITINERARY (hotel_id Text NOT NULL,check_in_date Text NOT NULL,check_out_date Text ,total_rooms Text,adult Text,child Text,total_night,final_cost Text,Country_id)";
    public boolean setItineraryHotelsData(String hotel_id, String check_in_date, String check_out_date, String total_rooms, String adult, String child, String total_night, String room_cost,String final_cost,String Country_id,String TotalRooms,String supplier,String option) {

        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!Country_id.isEmpty() ) {
                ContentValues values = new ContentValues();
                values.put("hotel_id", hotel_id);
                values.put("check_in_date", check_in_date);
                values.put("check_out_date", check_out_date);
                values.put("total_rooms", total_rooms);
                values.put("adult", adult);
                values.put("child", child);
                values.put("total_night", total_night);
                values.put("room_cost",room_cost);
                values.put("final_cost", final_cost);
                values.put("Country_id", Country_id);
                values.put("HotelsTotalRooms",TotalRooms);
                values.put("supplier",supplier);
                values.put("option",option);

                long ids = db.insert("HOTEL_ITINERARY", null, values);
                db.close();
                Log.e(TAG, "Insert Data in HOTEL_ITINERARY table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }


    }


    public ArrayList<ModelItineraryHotelList> getItineraryHotelsData(String Country_id) {
        try {
            String selectQuery = "SELECT * FROM HOTEL_ITINERARY WHERE Country_id='" + Country_id + "'", data = "";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelItineraryHotelList> result = new ArrayList<ModelItineraryHotelList>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelItineraryHotelList roomsList = new ModelItineraryHotelList();
                roomsList.setHotel_id(c.getString(c.getColumnIndex("hotel_id")));
                roomsList.setCheck_in_date(c.getString(c.getColumnIndex("check_in_date")));
                roomsList.setCheck_out_date(c.getString(c.getColumnIndex("check_out_date")));
                roomsList.setTotal_rooms(c.getString(c.getColumnIndex("total_rooms")));
                roomsList.setAdult(c.getString(c.getColumnIndex("adult")));
                roomsList.setChild(c.getString(c.getColumnIndex("child")));
                roomsList.setTotal_night(c.getString(c.getColumnIndex("total_night")));
                roomsList.setFinal_cost(c.getString(c.getColumnIndex("final_cost")));
                roomsList.setRoom_cost(c.getString(c.getColumnIndex("room_cost")));
                roomsList.setCountry_id(c.getString(c.getColumnIndex("Country_id")));
                roomsList.setHotelsTotalRooms(c.getString(c.getColumnIndex("HotelsTotalRooms")));
                roomsList.setSuplier(c.getString(c.getColumnIndex("supplier")));
                roomsList.setOption(c.getString(c.getColumnIndex("option")));

                ArrayList<ModelAllHotelList> roomsLists = new ArrayList<ModelAllHotelList>();
                //String id = String.valueOf();
                roomsLists = getHotelAllData(c.getString(c.getColumnIndex("hotel_id")),Country_id);
                roomsList.setHotelsLists(roomsLists);

                result.add(roomsList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }
    }


    public boolean updateItineraryHotelsData(int hotel_id, String check_in_date, String check_out_date, String total_rooms, String adult, String child, String total_night, String room_cost,String final_cost,String Country_id,String HotelsTotalRooms) {

        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!Country_id.isEmpty() ) {
                ContentValues values = new ContentValues();
                values.put("hotel_id", hotel_id);
                values.put("check_in_date", check_in_date);
                values.put("check_out_date", check_out_date);
                values.put("total_rooms", total_rooms);
                values.put("adult", adult);
                values.put("child", child);
                values.put("total_night", total_night);
                values.put("room_cost",room_cost);
                values.put("final_cost", final_cost);
                values.put("Country_id", Country_id);
                values.put("HotelsTotalRooms", HotelsTotalRooms);

                long ids = db.update("HOTEL_ITINERARY", values, "", new String[] {});
                //long ids = db.update("HOTEL_ITINERARY", null, values);
                db.close();
                Log.e(TAG, "update Data in HOTEL_ITINERARY table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }


    }

    public boolean updateOptionColumn(String hotel_id,String Country_id,String option) {
        try {
            String selectQuery = "UPDATE HOTEL_ITINERARY SET option = '" + option + "' WHERE hotel_id='" + hotel_id + "' AND Country_id='" + Country_id + "'", data = "";
            Log.e("Update query",selectQuery);
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            Log.e(TAG, "update Data in HOTEL_ITINERARY table ");
            if(cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            cursor.close();
            return true;
        }catch (Exception e){
            return false;
        }

    }

    public boolean CheckIsAvailable( String hotel_id,String Country_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from HOTEL_ITINERARY where hotel_id='" + hotel_id + "' AND Country_id='" + Country_id + "'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void DeleteHotelItinery( String hotel_id,String Country_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM HOTEL_ITINERARY WHERE hotel_id='" + hotel_id + "' AND Country_id='" + Country_id + "'");
        db.close();
        Log.e(TAG, "Deleted record from HOTEL_ITINERARY Table");
    }

    public void deleteItineraryHotelsData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("HOTEL_ITINERARY", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from HOTEL_ITINERARY Table");
    }

    /*-------------------------------------Itinerary Activity List--------------------------------------*/


    //HOTEL_ITINERARY (hotel_id Text NOT NULL,check_in_date Text NOT NULL,check_out_date Text ,total_rooms Text,adult Text,child Text,total_night,final_cost Text,Country_id)";
    //Adult_Amt Text,Child_Amt Text,Transfer_Charges Text,Vouchers_Charges Text,isPack Text
    public boolean setItineraryActivityData(String activity_id, String check_in_date, String check_out_date, String total_rooms, String adult, String child, String total_night, String room_cost,String final_cost,String Country_id,String Days,int no_of_Days,String Adult_Amt,String Child_Amt,String Transfer_Charges,String Vouchers_Charges,String isPack) {

        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!Country_id.isEmpty() ) {
                ContentValues values = new ContentValues();
                values.put("activity_id", activity_id);
                values.put("check_in_date", check_in_date);
                values.put("check_out_date", check_out_date);
                values.put("total_rooms", total_rooms);
                values.put("adult", adult);
                values.put("child", child);
                values.put("total_night", total_night);
                values.put("room_cost",room_cost);
                values.put("final_cost", final_cost);
                values.put("Country_id", Country_id);
                values.put(("Days"),Days);
                values.put("no_of_Days",no_of_Days);
                values.put("Adult_Amt",Adult_Amt);
                values.put("Child_Amt",Child_Amt);
                values.put("Transfer_Charges",Transfer_Charges);
                values.put("Vouchers_Charges",Vouchers_Charges);
                values.put("isPack",isPack);

                long ids = db.insert("ACTIVITY_ITINERARY", null, values);
                db.close();
                Log.e(TAG, "Insert Data in ACTIVITY_ITINERARY table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }


    }


    public ArrayList<ModelItineraryActivitiesList> getItineraryActivityData(String Country_id) {
        try {
            String selectQuery = "SELECT * FROM ACTIVITY_ITINERARY WHERE Country_id='" + Country_id + "'", data = "";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelItineraryActivitiesList> result = new ArrayList<ModelItineraryActivitiesList>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelItineraryActivitiesList roomsList = new ModelItineraryActivitiesList();
                roomsList.setActivity_id(c.getString(c.getColumnIndex("activity_id")));
                roomsList.setCheck_in_date(c.getString(c.getColumnIndex("check_in_date")));
                roomsList.setCheck_out_date(c.getString(c.getColumnIndex("check_out_date")));
                roomsList.setTotal_rooms(c.getString(c.getColumnIndex("total_rooms")));
                roomsList.setAdult(c.getString(c.getColumnIndex("adult")));
                roomsList.setChild(c.getString(c.getColumnIndex("child")));
                roomsList.setTotal_night(c.getString(c.getColumnIndex("total_night")));
                roomsList.setFinal_cost(c.getString(c.getColumnIndex("final_cost")));
                roomsList.setRoom_cost(c.getString(c.getColumnIndex("room_cost")));
                roomsList.setCountry_id(c.getString(c.getColumnIndex("Country_id")));
                roomsList.setDays(c.getString(c.getColumnIndex("Days")));
                roomsList.setNo_of_Days(c.getInt(c.getColumnIndex("no_of_Days")));
                roomsList.setAdult_Amt(c.getString(c.getColumnIndex("Adult_Amt")));
                roomsList.setChild_Amt(c.getString(c.getColumnIndex("Child_Amt")));
                roomsList.setTransfer_Charges(c.getString(c.getColumnIndex("Transfer_Charges")));
                roomsList.setVouchers_Charges(c.getString(c.getColumnIndex("Vouchers_Charges")));
                roomsList.setIsPack(c.getString(c.getColumnIndex("isPack")));


                ArrayList<ModelActivitiesList> roomsLists = new ArrayList<ModelActivitiesList>();
                //String id = String.valueOf();
                roomsLists = getActivityItineraryData(c.getString(c.getColumnIndex("activity_id")),Country_id);
                roomsList.setModelActivitiesLists(roomsLists);

                result.add(roomsList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }
    }



    public boolean updateItineraryActivityData(int activity_id, String check_in_date, String check_out_date, String total_rooms, String adult, String child, String total_night, String room_cost,String final_cost,String Country_id,String Days,int no_of_Days,String Transfer_Charges,String Vouchers_Charges) {

        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!Country_id.isEmpty() ) {
                ContentValues values = new ContentValues();
                values.put("activity_id", activity_id);
                values.put("check_in_date", check_in_date);
                values.put("check_out_date", check_out_date);
                values.put("total_rooms", total_rooms);
                values.put("adult", adult);
                values.put("child", child);
                values.put("total_night", total_night);
                values.put("room_cost",room_cost);
                values.put("final_cost", final_cost);
                values.put("Country_id", Country_id);
                values.put("Days", Days);
                values.put("Days", no_of_Days);
                values.put("Transfer_Charges",Transfer_Charges);
                values.put("Vouchers_Charges",Vouchers_Charges);

                long ids = db.update("ACTIVITY_ITINERARY", values, "", new String[] {});
                //long ids = db.update("HOTEL_ITINERARY", null, values);
                db.close();
                Log.e(TAG, "update Data in ACTIVITY_ITINERARY table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }


    }

    public boolean CheckIsAvailableActivity( String activity_id,String Country_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from ACTIVITY_ITINERARY where activity_id='" + activity_id + "' AND Country_id='" + Country_id + "'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void DeleteHotelItineryActivity( String activity_id,String Country_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM ACTIVITY_ITINERARY WHERE activity_id='" + activity_id + "' AND Country_id='" + Country_id + "'");
        db.close();
        Log.e(TAG, "Deleted record from HOTEL_ITINERARY Table");
    }

    public void deleteItineraryActivityData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("ACTIVITY_ITINERARY", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from ACTIVITY_ITINERARY Table");
    }


    /*-------------------------------------Rooms Pricing List--------------------------------------*/


    //"CREATE TABLE ROOM (room_id Text NOT NULL,noOFnight Int ,hotel_Amt Int,hotel_id Text,Country_id Text,meal_type_id Text,sharing_type_id Text)";
    public boolean setRoomPriceData(String room_id, String hotel_id, String Country_id,int noOFnight,double hotel_Amt,String sharing_type_id ,String meal_type_id,String sharing_type,String meal_type,String room_name,String extra_bed_included,String is_additional_hotel,String supplier) {

        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!room_id.isEmpty() ) {
                ContentValues values = new ContentValues();
                values.put("room_id", room_id);
                values.put("hotel_id", hotel_id);
                values.put("Country_id", Country_id);
                values.put("noOFnight", noOFnight);
                values.put("hotel_Amt", hotel_Amt);
                values.put("meal_type_id", meal_type_id);
                values.put("sharing_type_id", sharing_type_id);
                values.put("meal_type", meal_type);
                values.put("sharing_type", sharing_type);
                values.put("room_name",room_name);
                values.put("extra_bed_included",extra_bed_included);
                values.put("is_additional_hotel",is_additional_hotel);
                values.put("supplier",supplier);

                long ids = db.insert("ROOM", null, values);
                db.close();
                Log.e(TAG, "Insert Data in ROOM table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }


    }


    public ArrayList<ModelRoomPriceList> getRoomPriceData(/*String room_id,String hotel_id,*/String Country_id) {
        try {
            String selectQuery = "SELECT * FROM ROOM WHERE Country_id='" + Country_id +"'";
            //String selectQuery = "SELECT * FROM ROOM WHERE room_id='" + room_id + "' AND hotel_id='" + hotel_id + "' AND Country_id='" + Country_id +"'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelRoomPriceList> result = new ArrayList<ModelRoomPriceList>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelRoomPriceList roomsList = new ModelRoomPriceList();
                roomsList.setRoom_id(c.getString(c.getColumnIndex("room_id")));
                roomsList.setHotel_id(c.getString(c.getColumnIndex("hotel_id")));
                roomsList.setCountry_id(c.getString(c.getColumnIndex("Country_id")));
                roomsList.setNoOFnight(c.getInt(c.getColumnIndex("noOFnight")));
                roomsList.setHotel_Amt(c.getDouble(c.getColumnIndex("hotel_Amt")));
                roomsList.setSharing_type_id(c.getString(c.getColumnIndex("sharing_type_id")));
                roomsList.setMeal_type_id(c.getString(c.getColumnIndex("meal_type_id")));
                roomsList.setSharing_type(c.getString(c.getColumnIndex("sharing_type")));
                roomsList.setMeal_type(c.getString(c.getColumnIndex("meal_type")));
                roomsList.setRoom_name(c.getString(c.getColumnIndex("room_name")));
                roomsList.setExtra_bed_included(c.getString(c.getColumnIndex("extra_bed_included")));
                roomsList.setIs_additional_hotel(c.getString(c.getColumnIndex("is_additional_hotel")));
                roomsList.setSupplier(c.getString(c.getColumnIndex("supplier")));

                result.add(roomsList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }
    }


    public boolean updateRoomPriceData(int noOFnight, double hotel_Amt,String room_id,String hotel_id,String Country_id,String sharing_type,String meal_type) {
        boolean isSetInDB = false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (!room_id.isEmpty() && !hotel_id.isEmpty()) {
            String sql = "UPDATE ROOM SET noOFnight = '" + noOFnight + "' AND hotel_Amt = '" + hotel_Amt + "' WHERE room_id = '" + room_id + "' AND hotel_id = = '" + hotel_id + "' AND Country_id = = '" + Country_id + "'AND sharing_type = = '" + sharing_type + "' AND meal_type = = '" + meal_type + "'" ;
            ContentValues cv = new ContentValues();
            cv.put("noOFnight", noOFnight);
            cv.put("hotel_Amt", hotel_Amt);
            db.update("ROOM", cv, "room_id" + "= ? AND hotel_id" + "= ? AND Country_id " + "= ? AND sharing_type_id" + "= ? AND meal_type_id " + "= ?", new String[]{room_id,hotel_id,Country_id,sharing_type,meal_type});
            db.close();
            Log.e(TAG, "Column is updated -----Room id -->" + room_id);
            isSetInDB = true;
        }
        return isSetInDB;
    }


    public boolean CheckIsAvailableRoom( String room_id,String hotel_id,String Country_id,String sharing_type,String meal_type) {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from ROOM where room_id='" + room_id + "' AND hotel_id='" + hotel_id + "'AND Country_id='" + Country_id +"'AND sharing_type_id='" + sharing_type + "'AND meal_type_id='" + meal_type +"'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void DeleteRoomPriceData( String room_id,String hotel_id,String Country_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM ROOM WHERE room_id='" + room_id + "' AND hotel_id='" + hotel_id + "' AND Country_id='" + Country_id +"'");
        db.close();
        Log.e(TAG, "Deleted record from ROOM Table");
    }

    public void deleteAllRoomPriceData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("ROOM", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from ROOM Table");
    }



    /*------------------------------ROOM PRICE----------------------------*/

    //"CREATE TABLE ROOM_PRICING (meal_type Text,meal_type_id Text,price Text,sharing_type Text,sharing_type_id Text,validity_from Text, validity_to Text, room_id Text,hotel_id Text,Country_id Text)";

    public boolean setHotelRoomPricingData(String meal_type,String meal_type_id,String price,String sharing_type,String sharing_type_id, String validity_from,String validity_to, String room_id, String hotel_id, String Country_id) {

        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!room_id.isEmpty() ) {
                ContentValues values = new ContentValues();
                values.put("meal_type", meal_type);
                values.put("meal_type_id", meal_type_id);
                values.put("price", price);
                values.put("sharing_type", sharing_type);
                values.put("sharing_type_id", sharing_type_id);
                values.put("validity_from", validity_from);
                values.put("validity_to", validity_to);
                values.put("room_id", room_id);
                values.put("hotel_id", hotel_id);
                values.put("Country_id", Country_id);


                long ids = db.insert("ROOM_PRICING", null, values);
                db.close();
                Log.e(TAG, "Insert Data in ROOM_PRICING table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }


    }


    public ArrayList<ModelHotelRoomsPricingsList> getHotelRoomPricingData(String room_id,String hotel_id,String Country_id) {
        try {
            String selectQuery = "SELECT * FROM ROOM_PRICING WHERE room_id='" + room_id + "' AND hotel_id='" + hotel_id + "' AND Country_id='" + Country_id +"'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelHotelRoomsPricingsList> result = new ArrayList<ModelHotelRoomsPricingsList>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelHotelRoomsPricingsList roomsList = new ModelHotelRoomsPricingsList();
                roomsList.setMeal_type(c.getString(c.getColumnIndex("meal_type")));
                roomsList.setMeal_type_id(c.getString(c.getColumnIndex("meal_type_id")));
                roomsList.setPrice(c.getString(c.getColumnIndex("price")));
                roomsList.setSharing_type(c.getString(c.getColumnIndex("sharing_type")));
                roomsList.setSharing_type_id(c.getString(c.getColumnIndex("sharing_type_id")));
                roomsList.setValidity_from(c.getString(c.getColumnIndex("validity_from")));
                roomsList.setValidity_to(c.getString(c.getColumnIndex("validity_to")));
                roomsList.setRoom_id(c.getString(c.getColumnIndex("room_id")));
                roomsList.setHotel_id(c.getString(c.getColumnIndex("hotel_id")));
                roomsList.setCountry_id(c.getString(c.getColumnIndex("Country_id")));

                result.add(roomsList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }
    }

    public String getRoomPricing(String room_id, String hotel_id, String Country_id,String meal_type_id,String sharing_type_id) {
        try {
            String selectQuery = "SELECT price FROM ROOM_PRICING WHERE room_id='" + room_id + "' AND hotel_id='" + hotel_id + "' AND Country_id='" + Country_id +"' AND meal_type_id='" + meal_type_id +"'AND sharing_type_id='" + sharing_type_id +"' ";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            String result = "";
           // c.moveToFirst();
            if (c != null) {
                c.moveToFirst();
                while (c.isAfterLast() == false) {
                    result = (c.getString(c.getColumnIndex("price")));
                    c.moveToNext();
                }
            }
            Log.e(TAG, "Room Price = "+result);
            return result;
        } catch (Exception e1) {
            return "0";
        }
    }


    public void deleteAllHotelRoomPricingData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("ROOM_PRICING", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from ROOM_PRICING Table");
    }


    /*-------------------------------------Rooms Itinerary List--------------------------------------*/


    //"CREATE TABLE ROOM (room_id Text NOT NULL,noOFnight Int ,hotel_Amt Int,hotel_id Text,Country_id Text,meal_type_id Text,sharing_type_id Text)";
    public boolean setRoomItineraryData(String room_id, String hotel_id, String Country_id,int noOFnight,double hotel_Amt,String sharing_type_id ,String meal_type_id,String hotel_extra,String extra_bed_included,String is_additional_hotel,String supplier) {

        try {
            boolean isSetInDB = false;
            SQLiteDatabase db = this.getWritableDatabase();
            if (!room_id.isEmpty() ) {
                ContentValues values = new ContentValues();
                values.put("room_id", room_id);
                values.put("hotel_id", hotel_id);
                values.put("Country_id", Country_id);
                values.put("noOFnight", noOFnight);
                values.put("hotel_Amt", hotel_Amt);
                values.put("meal_type_id", meal_type_id);
                values.put("sharing_type_id", sharing_type_id);
                values.put("hotel_extra", hotel_extra);
                values.put("extra_bed_included",extra_bed_included);
                values.put("is_additional_hotel",is_additional_hotel);
                values.put("supplier",supplier);

                long ids = db.insert("ROOM_ITINERARY", null, values);
                db.close();
                Log.e(TAG, "Insert Data in ROOM_ITINERARY table " + ids);
                isSetInDB = true;
            }
            return isSetInDB;
        } catch (Exception e1) {
            return false;
        }


    }


    public ArrayList<ModelRoomPriceList> getRoomItineraryData(int room_id,String hotel_id,String Country_id) {
        try {
            //String selectQuery = "SELECT * FROM ROOM_ITINERARY WHERE Country_id='" + Country_id +"'";
            String selectQuery = "SELECT * FROM ROOM_ITINERARY WHERE room_id=" + room_id + " AND hotel_id='" + hotel_id + "' AND Country_id='" + Country_id +"'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<ModelRoomPriceList> result = new ArrayList<ModelRoomPriceList>();
            c.moveToFirst();
            for (int i = 0; i < c.getCount(); i++) {
                ModelRoomPriceList roomsList = new ModelRoomPriceList();
                roomsList.setRoom_id(c.getString(c.getColumnIndex("room_id")));
                roomsList.setHotel_id(c.getString(c.getColumnIndex("hotel_id")));
                roomsList.setCountry_id(c.getString(c.getColumnIndex("Country_id")));
                roomsList.setNoOFnight(c.getInt(c.getColumnIndex("noOFnight")));
                roomsList.setHotel_Amt(c.getDouble(c.getColumnIndex("hotel_Amt")));
                roomsList.setSharing_type_id(c.getString(c.getColumnIndex("sharing_type_id")));
                roomsList.setMeal_type_id(c.getString(c.getColumnIndex("meal_type_id")));
                roomsList.setHotel_extra(c.getString(c.getColumnIndex("hotel_extra")));
                roomsList.setExtra_bed_included(c.getString(c.getColumnIndex("extra_bed_included")));
                roomsList.setIs_additional_hotel(c.getString(c.getColumnIndex("is_additional_hotel")));
                roomsList.setSupplier(c.getString(c.getColumnIndex("supplier")));

                result.add(roomsList);
                c.moveToNext();
            }
            return result;
        } catch (Exception e1) {
            return null;
        }
    }


    public boolean updateItineraryData(int noOFnight, double hotel_Amt,String room_id,String hotel_id,String Country_id,String sharing_type,String meal_type,String Supplier) {
        boolean isSetInDB = false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (!room_id.isEmpty() && !hotel_id.isEmpty()) {
            String sql = "UPDATE ROOM_ITINERARY SET noOFnight = '" + noOFnight + "' AND hotel_Amt = '" + hotel_Amt + "' WHERE room_id = '" + room_id + "' AND hotel_id = = '" + hotel_id + "' AND Country_id = = '" + Country_id + "'AND sharing_type = = '" + sharing_type + "' AND meal_type = = '" + meal_type + "'" ;
            ContentValues cv = new ContentValues();
            cv.put("noOFnight", noOFnight);
            cv.put("hotel_Amt", hotel_Amt);
            cv.put("sharing_type_id",sharing_type);
            cv.put("meal_type",meal_type);
            cv.put("supplier",Supplier);
            db.update("ROOM_ITINERARY", cv, "room_id" + "= ? AND hotel_id" + "= ? AND Country_id " + "= ? AND sharing_type_id" + "= ? AND meal_type_id " + "= ? AND supplier " + "= ?", new String[]{room_id,hotel_id,Country_id});
            db.close();
            Log.e(TAG, "Column is updated -----Room id -->" + room_id);
            isSetInDB = true;
        }
        return isSetInDB;
    }


    public boolean CheckIsAvailableRoomItinerary( String room_id,String hotel_id,String Country_id,String sharing_type,String meal_type) {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from ROOM_ITINERARY where room_id='" + room_id + "' AND hotel_id='" + hotel_id + "'AND Country_id='" + Country_id +"'AND sharing_type_id='" + sharing_type + "'AND meal_type_id='" + meal_type +"'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void DeleteRoomItineraryData( String room_id,String hotel_id,String Country_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM ROOM_ITINERARY WHERE room_id='" + room_id + "' AND hotel_id='" + hotel_id + "' AND Country_id='" + Country_id +"'");
        db.close();
        Log.e(TAG, "Deleted record from ROOM Table");
    }

    public void deleteAllRoomItineraryata() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("ROOM_ITINERARY", null, null);
        db.close();
        Log.e(TAG, "Deleted all saved data from ROOM_ITINERARY Table");
    }


}
