package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityCustomeEventDetails;
import com.guiddoocn.holidays.activity.ActivityMainEventDetails;
import com.guiddoocn.holidays.models.ModelActivitiesList;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;
import java.util.Locale;

public class AdapterAllListMainActivities extends RecyclerView.Adapter<AdapterAllListMainActivities.MyViewHolder> implements Filterable {
    private ArrayList<ModelActivitiesList> liabraryList;
    private ArrayList<ModelActivitiesList> arraylist;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;
    private SessionManager sessionManager;

    public AdapterAllListMainActivities(Context mContext, ArrayList<ModelActivitiesList> liabraryList, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.liabraryList = liabraryList;
        this.fragmentManager = fragmentManager;
        this.arraylist = new ArrayList<ModelActivitiesList>();
        this.arraylist.addAll(liabraryList);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                liabraryList = new ArrayList<>();
                String charString = charText.toString();

                if (charString.isEmpty()) {
                    liabraryList = arraylist;
                } else {

                    ModelActivitiesList modelAllList2 = new ModelActivitiesList();
                    for (ModelActivitiesList wp : arraylist) {
                        if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                            liabraryList.add(wp);
                        }
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = liabraryList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                liabraryList = (ArrayList<ModelActivitiesList>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Pic;
        TextView tv_City, tv_name,tv_Rating,tv_amt,tv_Selected;
        TextView tv_Duration;
        private LinearLayout ll_Click;
        RatingBar rb_Rating;


        public MyViewHolder(View view) {
            super(view);
            ll_Click = view.findViewById(R.id.ll_Click);
            iv_Pic = view.findViewById(R.id.iv_Contry);
            tv_City = view.findViewById(R.id.tv_City);
            tv_name = view.findViewById(R.id.tv_name);
            tv_Selected = view.findViewById(R.id.tv_Selected);
            tv_Rating = view.findViewById(R.id.tv_Rating);
            tv_Duration = view.findViewById(R.id.tv_Duration);
            tv_amt = view.findViewById(R.id.tv_amt);
            rb_Rating = view.findViewById(R.id.rb_Rating);
            sessionManager = new SessionManager(mContext);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_main_activities_list, parent, false);
        return new AdapterAllListMainActivities.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ModelActivitiesList model = liabraryList.get(holder.getAdapterPosition());

        holder.tv_Duration.setText("Duration - "+model.getDay()+" Days "+model.getHour()+" hrs "+model.getMin()+" min");
        if (!model.getDay().equalsIgnoreCase("00") && !model.getHour().equalsIgnoreCase("00") && !model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getDay() + " day " + model.getHour() + " hr " + model.getMin() + " min");
        } else if (model.getDay().equalsIgnoreCase("00") && !model.getHour().equalsIgnoreCase("00")
                && !model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getHour() + " hr " + model.getMin() + " min");

        }else if (model.getDay().equalsIgnoreCase("00") && model.getHour().equalsIgnoreCase("00")
                && !model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getMin() + " min ");

        }else if (model.getDay().equalsIgnoreCase("00") && !model.getHour().equalsIgnoreCase("00")
                && model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getHour() + " hr ");
        }else if (!model.getDay().equalsIgnoreCase("00") && model.getHour().equalsIgnoreCase("00")
                && model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getDay() + " day ");
        }

        holder.tv_City.setText(model.getCity());
        holder.tv_name.setText(model.getName());
        holder.tv_amt.setText(model.getCurrency() +" "+model.getStarting_from_price());
        holder.rb_Rating.setRating(Float.valueOf(model.getRating()));

        try{
            Glide.with(mContext)
                    .load(model.getFeatured_image())
                    //.placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.iv_Pic);
        }catch (Exception e){

        }

        try{
            holder.ll_Click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sessionManager.isNetworkAvailable()) {
                        Intent intent = new Intent(mContext, ActivityMainEventDetails.class);
                        intent.putExtra("EventID", model.getTour_id());
                        intent.putExtra("EventName", model.getName());
                        intent.putExtra("Discount", model.getDiscount());
                        intent.putExtra("Discount_cap", model.getDiscount_cap());
                        mContext.startActivity(intent);

                    } else {
                        Snackbar.make(vv, "Please Check Internet Connection !", Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        }catch (Exception e){
        }

    }

    @Override
    public int getItemCount() {

        return liabraryList.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}