package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelMonthList;
import com.guiddoocn.holidays.models.ModelQueryList;

import java.util.List;

public class AdapterQueryList extends RecyclerView.Adapter<AdapterQueryList.MyViewHolder> {
    private List<ModelQueryList> modelMonthLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterQueryList(Context mContext, List<ModelQueryList> modelMonthLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelMonthLists = modelMonthLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_QueryName;

        public MyViewHolder(View view) {
            super(view);
            tv_QueryName = view.findViewById(R.id.tv_QueryName);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_query_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelQueryList model = modelMonthLists.get(position);
        Log.e("Data", "" + model);
        holder.tv_QueryName.setText("New query received from "+model.getDestination()+". "+model.getNumber_of_pax()+" pax for "+model.getTravel_date());
    }

    @Override
    public int getItemCount() {

        return modelMonthLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}