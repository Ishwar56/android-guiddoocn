package com.guiddoocn.holidays.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelCardDetailsList;

import java.util.List;

public class AdapterCardDetails extends BaseAdapter {

    Context mContext;
    List<ModelCardDetailsList> cardTypeList;
    LayoutInflater inflter;

    public AdapterCardDetails(Context mContext, List<ModelCardDetailsList> cardTypeList) {
        this.mContext = mContext;
        this.cardTypeList = cardTypeList;
        inflter = (LayoutInflater.from(mContext));
    }

    @Override
    public int getCount() {
        return cardTypeList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ModelCardDetailsList model =cardTypeList.get(i);

        view = inflter.inflate(R.layout.adapter_card_type_list, null);

        TextView tv_CardType = (TextView) view.findViewById(R.id.tv_CardType);
        TextView tv_CardAmt = (TextView) view.findViewById(R.id.tv_CardAmt);

        tv_CardType.setText(model.getCardType());
        tv_CardAmt.setText(model.getCurrency()+model.getAmount());

        return view;

    }
}
