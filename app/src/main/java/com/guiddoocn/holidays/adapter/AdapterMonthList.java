package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelHotelList;
import com.guiddoocn.holidays.models.ModelMonthList;

import java.util.List;

public class AdapterMonthList extends RecyclerView.Adapter<AdapterMonthList.MyViewHolder> {
    private List<ModelMonthList> modelMonthLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterMonthList(Context mContext, List<ModelMonthList> modelMonthLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelMonthLists = modelMonthLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_MonthName;


        public MyViewHolder(View view) {
            super(view);
            tv_MonthName = view.findViewById(R.id.tv_MonthName);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_month_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelMonthList model = modelMonthLists.get(position);
        Log.e("Data", "" + model);
        holder.tv_MonthName.setText(model.getName());
    }

    @Override
    public int getItemCount() {

        return modelMonthLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}