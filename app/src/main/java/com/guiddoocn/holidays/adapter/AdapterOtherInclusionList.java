package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelInclusionList;

import java.util.List;

public class AdapterOtherInclusionList extends RecyclerView.Adapter<AdapterOtherInclusionList.MyViewHolder> {
    private List<ModelInclusionList> modelInclusionLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterOtherInclusionList(Context mContext, List<ModelInclusionList>  modelInclusionLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelInclusionLists = modelInclusionLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
       TextView tv_CityTitle;




        public MyViewHolder(View view) {
            super(view);
            tv_CityTitle=view.findViewById(R.id.tv_CityTitle);



        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_other_inclusion_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelInclusionList model = modelInclusionLists.get(position);
        Log.e("Data", "" + model);
        holder.tv_CityTitle.setText("-"+model.getInclusion());


    }

    @Override
    public int getItemCount() {

        return modelInclusionLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}