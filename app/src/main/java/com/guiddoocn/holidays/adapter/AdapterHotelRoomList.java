package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityAllListHotels;
import com.guiddoocn.holidays.models.ModelActivitiesList;
import com.guiddoocn.holidays.models.ModelHotelRoomsList;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class AdapterHotelRoomList extends RecyclerView.Adapter<AdapterHotelRoomList.MyViewHolder> {
    private List<ModelHotelRoomsList> modelInclusionLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int selectedPosition = -1,OldselectedPosition=-1;
    private FragmentManager fragmentManager;
    private boolean flag = true, flag_Add = false;
    private List<Integer> noRooms = new ArrayList<Integer>();
    private ArrayList<String> list_share_type = new ArrayList<String>();
    private ArrayList<String> list_meal_type = new ArrayList<String>();
    private ArrayList<String> temp_list_share_type = new ArrayList<String>();
    private ArrayList<String> temp_list_meal_type = new ArrayList<String>();
    private int NoOfRooms = 0,TotalNoOfRooms,item_pos;
    private double roomAmt = 0.0,TotalroomAmt=0.0;
    private String select_share_Type_ID="1",select_meal_Type_ID="1",selected_room_Amt,select_share_Type="",select_meal_Type="",extra_bed_included="false";
    private SQLiteHandler db;
    private SessionManager sessionManager;

    public AdapterHotelRoomList(Context mContext, List<ModelHotelRoomsList> modelInclusionLists, Activity activity, FragmentManager fragmentManager,List<Integer> noRoom) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelInclusionLists = modelInclusionLists;
        this.fragmentManager = fragmentManager;
        this.noRooms =noRoom;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_room_no, tv_room_type, tv_amt, tv_meal_type, tv_extra_info, tv_additional_tax, tv_remarks,tv_block_date,tv_Selected;
        LinearLayout ll_show;
        Spinner sp_no_of_rooms,sp_share_type,sp_meal_type;


        public MyViewHolder(View view) {
            super(view);
            tv_room_no = view.findViewById(R.id.tv_room_no);
            tv_room_type = view.findViewById(R.id.tv_room_type);
            tv_amt = view.findViewById(R.id.tv_amt);
            //tv_meal_type = view.findViewById(R.id.tv_meal_type);
            tv_extra_info = view.findViewById(R.id.tv_extra_info);
            tv_additional_tax = view.findViewById(R.id.tv_additional_tax);
            tv_remarks = view.findViewById(R.id.tv_remarks);
            ll_show = view.findViewById(R.id.ll_show);
            sp_no_of_rooms = view.findViewById(R.id.sp_no_of_rooms);
            tv_block_date = view.findViewById(R.id.tv_block_date);
            sp_share_type = view.findViewById(R.id.sp_share_type);
            sp_meal_type = view.findViewById(R.id.sp_meal_type);
            db=new SQLiteHandler(mContext);
            sessionManager = new SessionManager(mContext);
            tv_Selected = view.findViewById(R.id.tv_Selected);


            //   Night adapter for spinner
            ArrayAdapter<Integer> nighgtAdapter = new ArrayAdapter<Integer>(mContext, android.R.layout.simple_spinner_item, noRooms);
            sp_no_of_rooms.setAdapter(nighgtAdapter);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_hotel_roooms_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try{

            final ModelHotelRoomsList model = modelInclusionLists.get(position);
            item_pos=position;
            SetArrayList();
            Log.e("Data", "" + model);

            int pos = position;
            pos = pos + 1;
            holder.tv_room_no.setText("Room "+String.valueOf(pos));
            holder.tv_room_type.setText(model.getRoom_type());

            if(modelInclusionLists.get(item_pos).getRoomsPricingsLists().size()>0){
                holder.tv_amt.setText(model.getCurrency() + " " + String.format("%.02f", Float.parseFloat(model.getRoomsPricingsLists().get(0).getPrice())));
            }else{
                holder.tv_amt.setText(model.getCurrency() + " 0");
            }


            //holder.tv_meal_type.setText(model.getSelect());

            if(model.getBlock_dates().equalsIgnoreCase("")){
                holder.tv_block_date.setVisibility(View.GONE);
            }else{
                holder.tv_block_date.setVisibility(View.VISIBLE);
                holder.tv_block_date.setText("Block date-"+model.getBlock_dates());
            }

            holder.tv_additional_tax.setText(" * "+model.getAdditional_tax());
            holder.tv_remarks.setText(" * "+model.getRemarks());

            holder.tv_extra_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(model.getAdditional_tax().equalsIgnoreCase("")&&model.getRemarks().equalsIgnoreCase("")){
                        Toast.makeText(mContext, "extra information not available.", Toast.LENGTH_SHORT).show();
                    }else{
                        if (flag) {
                            flag = false;
                            holder.ll_show.setVisibility(View.VISIBLE);
                        } else {
                            flag = true;
                            holder.ll_show.setVisibility(View.GONE);
                        }
                    }


                }
            });



            AdapterCommenList share_typeAdapter = new AdapterCommenList(activity, R.layout.spiner_room_type_list,R.id.spiner_list, list_share_type);
            holder.sp_share_type.setAdapter(share_typeAdapter);

            AdapterCommenList meal_typeAdapter = new AdapterCommenList(activity, R.layout.spiner_room_type_list,R.id.spiner_list, list_meal_type);
            holder.sp_meal_type.setAdapter(meal_typeAdapter);

            for(int k=0;list_share_type.size()>k;k++){
                if(list_share_type.get(k).equalsIgnoreCase("Double")){
                    holder.sp_share_type.setSelection(k);
                }
            }

            try{
                holder.sp_share_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        select_share_Type_ID = model.getRoomsPricingsLists().get(position).getSharing_type_id();
                        select_share_Type = model.getRoomsPricingsLists().get(position).getSharing_type();
                        //holder.tv_amt.setText(model.getCurrency() +" "+ db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID));
                        holder.tv_amt.setText(model.getCurrency() + " " + String.format("%.02f", Float.parseFloat(db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID))));
                        selected_room_Amt = db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID);
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }catch (Exception e){
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
            }


            try{
                holder.sp_meal_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        select_meal_Type_ID = model.getRoomsPricingsLists().get(position).getMeal_type_id();
                        select_meal_Type = model.getRoomsPricingsLists().get(position).getMeal_type();
                        ((ActivityAllListHotels)mContext).HotelRoomsPrice(select_share_Type_ID,select_meal_Type_ID);
                       // holder.tv_amt.setText(model.getCurrency() +" "+ db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID));
                        holder.tv_amt.setText(model.getCurrency() + " " + String.format("%.02f", Float.parseFloat(db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID))));
                        selected_room_Amt = db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID);
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }catch (Exception e){
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
            }


            try{

                if(model.getSelect().equalsIgnoreCase("True")){
                    holder.tv_Selected.setText("Selected");
                    holder.tv_Selected.setBackgroundResource(R.drawable.custom_bg_bordered_green);
                }else{
                    holder.tv_Selected.setText("Select");
                    holder.tv_Selected.setBackgroundResource(R.drawable.custom_bg_bordered_red);
                }

                holder.tv_Selected.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(selectedPosition==holder.getAdapterPosition()){
                            if(flag_Add){
                                try {
                                    TotalroomAmt = TotalroomAmt - (Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)) * NoOfRooms);
                                    ((ActivityAllListHotels)mContext).SetHotelRoomsPrice(model.getRoom_id(),model.getHotel_id(),0, Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(position).getRoom_id(),model.getRoomsPricingsLists().get(position).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)),select_share_Type_ID,select_meal_Type_ID,select_share_Type,select_meal_Type,model.getRoom_type(),extra_bed_included);
                                    flag_Add=false;
                                    holder.tv_Selected.setText("Select");
                                    holder.tv_Selected.setBackgroundResource(R.drawable.custom_bg_bordered_red);
                                }catch (IndexOutOfBoundsException e){
                                    Toast.makeText(mContext, "Amount is 0,share type and meal type not available", Toast.LENGTH_LONG).show();
                                }

                            }else{
                                try {
                                    OldselectedPosition=holder.getAdapterPosition();
                                    TotalroomAmt = TotalroomAmt + (Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)) * NoOfRooms);
                                    ((ActivityAllListHotels)mContext).SetHotelRoomsPrice(model.getRoom_id(),model.getHotel_id(),1, Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(position).getRoom_id(),model.getRoomsPricingsLists().get(position).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)),select_share_Type_ID,select_meal_Type_ID,select_share_Type,select_meal_Type,model.getRoom_type(),extra_bed_included);
                                    flag_Add=true;
                                    holder.tv_Selected.setText("Selected");
                                    holder.tv_Selected.setBackgroundResource(R.drawable.custom_bg_bordered_green);
                                }catch (IndexOutOfBoundsException e){
                                    Toast.makeText(mContext, "Amount is 0,share type and meal type not available", Toast.LENGTH_LONG).show();
                                }
                            }
                        }else{


                            if(flag_Add){
                                try{
                                    final ModelHotelRoomsList model = modelInclusionLists.get(OldselectedPosition);
                                    TotalroomAmt = TotalroomAmt - (Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)) * NoOfRooms);
                                    ((ActivityAllListHotels)mContext).SetHotelRoomsPrice(model.getRoom_id(),model.getHotel_id(),0, Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(OldselectedPosition).getRoom_id(),model.getRoomsPricingsLists().get(OldselectedPosition).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)),select_share_Type_ID,select_meal_Type_ID,select_share_Type,select_meal_Type,model.getRoom_type(),extra_bed_included);
                                    //notifyDataSetChanged();
                                    notifyItemChanged(OldselectedPosition);
                                   // Toast.makeText(mContext, "remove All", Toast.LENGTH_SHORT).show();
                                    flag_Add=false;
                                   /* holder.tv_Selected.setText("Add");
                                    holder.tv_Selected.setBackgroundResource(R.drawable.custom_bg_bordered_red);
                                    flag_Add=false;*/
                                    selectedPosition=-1;
                                }catch (IndexOutOfBoundsException e){
                                    Toast.makeText(mContext, "Amount is 0,share type and meal type not available", Toast.LENGTH_LONG).show();
                                }
                                try{
                                    OldselectedPosition=holder.getAdapterPosition();
                                    TotalroomAmt = TotalroomAmt + (Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)) * NoOfRooms);
                                    ((ActivityAllListHotels)mContext).SetHotelRoomsPrice(model.getRoom_id(),model.getHotel_id(),1, Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(position).getRoom_id(),model.getRoomsPricingsLists().get(position).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)),select_share_Type_ID,select_meal_Type_ID,select_share_Type,select_meal_Type,model.getRoom_type(),extra_bed_included);
                                    selectedPosition=holder.getAdapterPosition();
                                    flag_Add=true;
                                    holder.tv_Selected.setText("Selected");
                                    holder.tv_Selected.setBackgroundResource(R.drawable.custom_bg_bordered_green);
                                }catch (IndexOutOfBoundsException e){
                                    Toast.makeText(mContext, "Amount is 0,share type and meal type not available", Toast.LENGTH_LONG).show();
                                }

                            }else{
                                try{
                                    OldselectedPosition=holder.getAdapterPosition();
                                    TotalroomAmt = TotalroomAmt + (Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)) * NoOfRooms);
                                    ((ActivityAllListHotels)mContext).SetHotelRoomsPrice(model.getRoom_id(),model.getHotel_id(),1, Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(position).getRoom_id(),model.getRoomsPricingsLists().get(position).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)),select_share_Type_ID,select_meal_Type_ID,select_share_Type,select_meal_Type,model.getRoom_type(),extra_bed_included);
                                    selectedPosition=holder.getAdapterPosition();
                                flag_Add=true;
                                holder.tv_Selected.setText("Selected");
                                holder.tv_Selected.setBackgroundResource(R.drawable.custom_bg_bordered_green);
                                }catch (IndexOutOfBoundsException e){
                                    Toast.makeText(mContext, "Amount is 0,share type and meal type not available", Toast.LENGTH_LONG).show();
                                }
                            }


                        }

                    }

                });

                /*holder.sp_no_of_rooms.setSelection(0,false);
                holder.sp_no_of_rooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        try{
                            if(position==0){
                                try{
                                    TotalNoOfRooms = TotalNoOfRooms - NoOfRooms;
                                    TotalroomAmt = TotalroomAmt - (Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)) * NoOfRooms);
                                    ((ActivityAllListHotels)mContext).SetHotelRoomsPrice(model.getRoom_id(),model.getHotel_id(),position, Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(position).getRoom_id(),model.getRoomsPricingsLists().get(position).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)),select_share_Type_ID,select_meal_Type_ID);
                                }catch (IndexOutOfBoundsException e){
                                    Toast.makeText(mContext, "Amount is 0,share type and meal type not available", Toast.LENGTH_LONG).show();
                                    holder.sp_no_of_rooms.setSelection(0);
                                }

                            }else{
                                try{
                                    NoOfRooms = position;
                                    TotalNoOfRooms = TotalNoOfRooms + NoOfRooms;
                                    TotalroomAmt = TotalroomAmt + (Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(0).getRoom_id(),model.getRoomsPricingsLists().get(0).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)) * NoOfRooms);
                                    ((ActivityAllListHotels)mContext).SetHotelRoomsPrice(model.getRoom_id(),model.getHotel_id(),position, Double.parseDouble(db.getRoomPricing(model.getRoomsPricingsLists().get(position).getRoom_id(),model.getRoomsPricingsLists().get(position).getHotel_id(),sessionManager.getCountryCode(),select_meal_Type_ID,select_share_Type_ID)),select_share_Type_ID,select_meal_Type_ID);
                                }catch (IndexOutOfBoundsException e){
                                    Toast.makeText(mContext, "Amount is 0,share type and meal type not available", Toast.LENGTH_LONG).show();
                                    holder.sp_no_of_rooms.setSelection(0);
                                }
                            }
                        }catch (IndexOutOfBoundsException e){
                            Toast.makeText(activity, "Something went wrong", Toast.LENGTH_LONG).show();
                            holder.sp_no_of_rooms.setSelection(0);
                        }


                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });*/

            }catch (IndexOutOfBoundsException e){
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
            }


        }catch (Exception e){
            Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
        }








    }

    @Override
    public int getItemCount() {

        return modelInclusionLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }

    public void SetArrayList(){
        if(modelInclusionLists.get(item_pos).getRoomsPricingsLists().size()>0){
            list_meal_type=new ArrayList<>();
            list_share_type=new ArrayList<>();
            for(int i=0;modelInclusionLists.get(item_pos).getRoomsPricingsLists().size()>i;i++){
                temp_list_meal_type.add(modelInclusionLists.get(item_pos).getRoomsPricingsLists().get(i).getMeal_type());
                temp_list_share_type.add(modelInclusionLists.get(item_pos).getRoomsPricingsLists().get(i).getSharing_type());
            }

            list_meal_type = new ArrayList<String>(new LinkedHashSet<String>(temp_list_meal_type));
            list_share_type = new ArrayList<String>(new LinkedHashSet<String>(temp_list_share_type));
        }

    }

    public void CheckHotelRooms(int meal_Type_ID,int share_Type_ID){


    }


}