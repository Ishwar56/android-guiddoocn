package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelHotelList;

import java.util.List;

public class AdapterPackageHotelList extends RecyclerView.Adapter<AdapterPackageHotelList.MyViewHolder> {
    private List<ModelHotelList> modelHotelLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterPackageHotelList(Context mContext, List<ModelHotelList>  modelHotelLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelHotelLists = modelHotelLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Hotel;
       TextView tv_Hotel,tv_Details,tv_ContryName;


        public MyViewHolder(View view) {
            super(view);
            iv_Hotel = view.findViewById(R.id.iv_Hotel);
            tv_Hotel= view.findViewById(R.id.tv_Hotel);
            tv_Details= view.findViewById(R.id.tv_Details);
            tv_ContryName= view.findViewById(R.id.tv_ContryName);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_package_hotels_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelHotelList model = modelHotelLists.get(position);
        Log.e("Data", "" + modelHotelLists.size());

        holder.tv_Hotel.setText(model.getHotel_Name());
        holder.tv_Details.setText("City : "+model.getCity_name());
        holder.tv_ContryName.setText("No of night : "+model.getNo_ofNights());    }

    @Override
    public int getItemCount() {

        return modelHotelLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}