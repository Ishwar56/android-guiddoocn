package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityAllListHotels;
import com.guiddoocn.holidays.models.ModelActivitiesList;
import com.guiddoocn.holidays.models.ModelAllHotelList;
import com.guiddoocn.holidays.models.ModelTiming;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterAllListHotels extends RecyclerView.Adapter<AdapterAllListHotels.MyViewHolder> implements Filterable {
    private List<ModelAllHotelList> modelAllHotelLists;
    private ArrayList<ModelAllHotelList> arraylist;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterAllListHotels(Context mContext, List<ModelAllHotelList> modelAllHotelLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelAllHotelLists = modelAllHotelLists;
        this.fragmentManager = fragmentManager;
        this.arraylist = new ArrayList<ModelAllHotelList>();
        this.arraylist.addAll(modelAllHotelLists);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                modelAllHotelLists = new ArrayList<>();
                String charString = charText.toString();

                if (charString.isEmpty()) {
                    modelAllHotelLists = arraylist;
                } else {

                    ModelActivitiesList modelAllList2 = new ModelActivitiesList();
                    for (ModelAllHotelList wp : arraylist) {
                        if (wp.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                            modelAllHotelLists.add(wp);
                        }
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = modelAllHotelLists;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                modelAllHotelLists = (ArrayList<ModelAllHotelList>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Hotel;
        TextView tv_Hotel, tv_Details, tv_ContryName, tv_Currency;
        ProgressBar progressBar;
        RatingBar rb_Rating;
        LinearLayout ll_Rooms,ll_tv,ll_bathtub,ll_swim,ll_dumbbel,ll_car,ll_currency,ll_bus,ll_wifi,ll_wellness,ll_Disco,ll_room_Service;

        public MyViewHolder(View view) {
            super(view);
            iv_Hotel = view.findViewById(R.id.iv_Hotel);
            tv_Hotel = view.findViewById(R.id.tv_Hotel);
            tv_Details = view.findViewById(R.id.tv_Details);
            tv_ContryName = view.findViewById(R.id.tv_ContryName);
            tv_Currency = view.findViewById(R.id.tv_Currency);
            progressBar = view.findViewById(R.id.progressBar);
            rb_Rating = view.findViewById(R.id.rb_Rating);
            ll_Rooms = view.findViewById(R.id.ll_Rooms);

            ll_tv = view.findViewById(R.id.ll_tv);
            ll_bathtub = view.findViewById(R.id.ll_bathtub);
            ll_swim = view.findViewById(R.id.ll_swim);
            ll_dumbbel = view.findViewById(R.id.ll_dumbbel);
            ll_car = view.findViewById(R.id.ll_car);
            ll_currency = view.findViewById(R.id.ll_currency);
            ll_bus = view.findViewById(R.id.ll_bus);
            ll_wifi = view.findViewById(R.id.ll_wifi);
            ll_wellness = view.findViewById(R.id.ll_wellness);
            ll_Disco = view.findViewById(R.id.ll_Disco);
            ll_room_Service = view.findViewById(R.id.ll_room_Service);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_all_hotels_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelAllHotelList model = modelAllHotelLists.get(position);
        Log.e("Data", "" + model);
        holder.tv_Hotel.setText(model.getTitle());
        holder.tv_Details.setText(model.getAddress());
        holder.tv_ContryName.setText(model.getCityCountryName() + "-" + model.getLocation());
        holder.tv_Currency.setText(model.getCurrency() + " " + model.getAmount());
        holder.rb_Rating.setRating(Float.valueOf(model.getRating()));

        for(int i=0;modelAllHotelLists.get(position).getHotelAmnetiesLists().size()>i;i++){
            if(1== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_tv.setVisibility(View.VISIBLE);
            }else if(2== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_bathtub.setVisibility(View.VISIBLE);
            }else if(3== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_swim.setVisibility(View.VISIBLE);
            }else if(4== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_dumbbel.setVisibility(View.VISIBLE);
            }else if(5== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_bus.setVisibility(View.VISIBLE);
            }else if(6== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_currency.setVisibility(View.VISIBLE);
            }else if(7== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_car.setVisibility(View.VISIBLE);
            }else if(8== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_wifi.setVisibility(View.VISIBLE);
            }else if(9== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_wellness.setVisibility(View.VISIBLE);
            }else if(10== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_Disco.setVisibility(View.VISIBLE);
            }else if(11== Integer.parseInt(modelAllHotelLists.get(position).getHotelAmnetiesLists().get(i).getID())){
                holder.ll_room_Service.setVisibility(View.VISIBLE);
            }

        }


        try {
            Glide.with(mContext)
                    .load(model.getImage())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.iv_Hotel.setImageResource(R.drawable.place_holder);
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                       boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.iv_Hotel);
        } catch (Exception e) {
            Glide.with(mContext)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .into(holder.iv_Hotel);
        }



        holder.ll_Rooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((ActivityAllListHotels)mContext).HotelRoomsDialog(position);

            }
        });


    }

    @Override
    public int getItemCount() {

        return modelAllHotelLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


    /**
     * Created by Ishwar on 26/12/2017.
     */

    public static class AdapterBookingPricing extends RecyclerView.Adapter<AdapterBookingPricing.MyViewHolder> {

        private static final String TAG = "AdapterBookingPricing";
        private List<ModelTiming> activityList;
        private Context mContext;
        private Activity activity;
        private View vv;
        private List<ModelTiming> items;
        private OnItemClickListener listener;
        private int row_index=-1;
        private  boolean flag;
        private int num = 1;

        public AdapterBookingPricing(Context mContext, ArrayList<ModelTiming> flightsListModels, boolean flag, OnItemClickListener onItemClickListener) {
            this.activity=activity;
            this.mContext=mContext;
            this.listener = onItemClickListener;
            this.items=flightsListModels;
            this.activityList = flightsListModels;
            this.flag = flag;
        }

        public interface OnItemClickListener {
            void onItemClick(ModelTiming item, LinearLayout layout);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView tv_booking_time;
            private LinearLayout ll_content_click;
            public MyViewHolder(View view) {
                super(view);
                ll_content_click = (LinearLayout)view.findViewById(R.id.ll_content_click);
                tv_booking_time = (TextView)view.findViewById(R.id.tv_booking_time);

            }

        }
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_timing_list, parent, false);
            return new MyViewHolder(itemView);
        }
        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {


            holder.tv_booking_time.setText(activityList.get(holder.getAdapterPosition()).getTiming());
            holder.tv_booking_time.setTextColor(Color.parseColor("#D51E01"));

            holder.ll_content_click.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(items.get(position),holder.ll_content_click);
                    row_index=position;
                    notifyDataSetChanged();
                }
            });

            if(row_index==position){
                holder.ll_content_click.setBackgroundColor(Color.parseColor("#D51E01"));
                holder.tv_booking_time.setTextColor(Color.parseColor("#ffffff"));
                Log.e(TAG, "onBindViewHolder: back red");
            }else{
                holder.ll_content_click.setBackgroundColor(Color.parseColor("#ffffff"));
                holder.tv_booking_time.setTextColor(Color.parseColor("#D51E01"));
                Log.e(TAG, "onBindViewHolder: text red");
            }

        }

        @Override
        public int getItemCount() {
            if(flag){
                return activityList.size();
            }else{
                if(num*1 > activityList.size()){
                    return activityList.size();
                }else{
                    return num*1;
                }
            }
            //return activityList.size();
        }
    }
}