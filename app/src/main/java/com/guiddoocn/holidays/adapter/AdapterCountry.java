package com.guiddoocn.holidays.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelDestinationLists;

import java.util.List;

public class AdapterCountry extends ArrayAdapter<ModelDestinationLists> {

    LayoutInflater flater;

    public AdapterCountry(Activity context, int resouceId, int textviewId, List<ModelDestinationLists> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ModelDestinationLists rowItem = getItem(position);

        View rowview = flater.inflate(R.layout.spiner_country_list,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.title);
        txtTitle.setText(rowItem.getCountryName());

        return rowview;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(R.layout.spiner_country_list,parent, false);
        }
        ModelDestinationLists rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setText(rowItem.getCountryName());
        return convertView;
    }
}
