package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityMainEventDetails;
import com.guiddoocn.holidays.models.ModelPromoExperiencesList;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.List;

public class AdapterPromoExperiencesMaxList extends RecyclerView.Adapter<AdapterPromoExperiencesMaxList.MyViewHolder> {
    private List<ModelPromoExperiencesList> liabraryList;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;
    private SessionManager sessionManager;

    public AdapterPromoExperiencesMaxList(Context mContext, List<ModelPromoExperiencesList> liabraryList, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.liabraryList = liabraryList;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Pic;
        TextView tv_City, tv_name,tv_Rating,tv_amt,tv_old_price;
        TextView tv_Duration;
        private LinearLayout ll_click;
        RatingBar rb_Rating;


        public MyViewHolder(View view) {
            super(view);
            iv_Pic = view.findViewById(R.id.iv_Contry);
            tv_City = view.findViewById(R.id.tv_City);
            tv_name = view.findViewById(R.id.tv_name);
            tv_Rating = view.findViewById(R.id.tv_Rating);
            tv_Duration = view.findViewById(R.id.tv_Duration);
            tv_amt = view.findViewById(R.id.tv_amt);
            rb_Rating = view.findViewById(R.id.rb_Rating);
            tv_old_price=view.findViewById(R.id.tv_old_price);
            sessionManager=new SessionManager(mContext);
            ll_click=view.findViewById(R.id.ll_click);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_promo_experience_max_list, parent, false);
        return new AdapterPromoExperiencesMaxList.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelPromoExperiencesList model = liabraryList.get(position);

        holder.tv_Duration.setText("Duration - "+model.getDay()+" Days "+model.getHour()+" hrs "+model.getMin()+" min");
        if (!model.getDay().equalsIgnoreCase("00") && !model.getHour().equalsIgnoreCase("00") && !model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getDay() + " day " + model.getHour() + " hr " + model.getMin() + " min");
        } else if (model.getDay().equalsIgnoreCase("00") && !model.getHour().equalsIgnoreCase("00")
                && !model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getHour() + " hr " + model.getMin() + " min");

        }else if (model.getDay().equalsIgnoreCase("00") && model.getHour().equalsIgnoreCase("00")
                && !model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getMin() + " min ");

        }else if (model.getDay().equalsIgnoreCase("00") && !model.getHour().equalsIgnoreCase("00")
                && model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getHour() + " hr ");
        }else if (!model.getDay().equalsIgnoreCase("00") && model.getHour().equalsIgnoreCase("00")
                && model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getDay() + " day ");
        }

        holder.tv_City.setText(model.getCity());
        holder.tv_name.setText(model.getName());
        holder.tv_amt.setText(model.getCurrency() +" "+model.getAmount());
        holder.rb_Rating.setRating(Float.valueOf(model.getRating()));
        if(model.getDiscount().equalsIgnoreCase("0")|| model.getDiscount().equalsIgnoreCase("null")){
            holder.tv_old_price.setVisibility(View.GONE);
        }else{
            holder.tv_old_price.setVisibility(View.VISIBLE);
            Double Temp = Double.parseDouble(model.getAmount()) * Double.parseDouble(model.getDiscount()) /100;
            Double Amt = Temp + Double.parseDouble(model.getAmount());
            String text = "<strike><font color='#727272'>"+model.getCurrency() +" "+String.format("%.2f", Amt)+"</font></strike>";
            holder.tv_old_price.setText(Html.fromHtml(text));
        }

        try{
            Glide.with(mContext)
                    .load(model.getImage())
                    //.placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.iv_Pic);
        }catch (Exception e){

        }

        try{
            holder.ll_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sessionManager.isNetworkAvailable()) {
                        Intent intent = new Intent(mContext, ActivityMainEventDetails.class);
                        intent.putExtra("EventID", model.getTour_id());
                        intent.putExtra("EventName", model.getName());
                        intent.putExtra("Discount", model.getDiscount());
                        intent.putExtra("Discount_cap", model.getDiscount_cap());
                        mContext.startActivity(intent);

                    } else {
                        Snackbar.make(vv, "Please Check Internet Connection !", Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        }catch (Exception e){
        }

    }

    @Override
    public int getItemCount() {

        return liabraryList.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}