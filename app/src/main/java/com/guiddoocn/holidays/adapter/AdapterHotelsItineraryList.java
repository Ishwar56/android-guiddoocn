package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityAllListHotels;
import com.guiddoocn.holidays.activity.ActivityItinerary;
import com.guiddoocn.holidays.models.ModelItineraryHotelList;
import com.guiddoocn.holidays.models.ModelRoomPriceList;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class AdapterHotelsItineraryList extends RecyclerView.Adapter<AdapterHotelsItineraryList.MyViewHolder> {
    private List<ModelItineraryHotelList> modelHotelLists;
    ArrayList<ModelRoomPriceList> roomItineraryData = new ArrayList<>();
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;
    private SessionManager sessionManager;
    private SQLiteHandler db;
    private List<String> optionsList = new ArrayList<String>();

    public AdapterHotelsItineraryList(Context mContext, List<ModelItineraryHotelList>  modelHotelLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelHotelLists = modelHotelLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Hotel;
        TextView tv_Hotel,tv_adult_child_count,tv_calculation,tv_Check_in,tv_Check_out,tv_Amount;
        RatingBar rb_Rating;
        LinearLayout ll_delete;
        Spinner sp_option;

        public MyViewHolder(View view) {
            super(view);
            iv_Hotel = view.findViewById(R.id.iv_Hotel);
            tv_Hotel= view.findViewById(R.id.tv_Hotel);
            tv_adult_child_count= view.findViewById(R.id.tv_adult_child_count);
            tv_calculation= view.findViewById(R.id.tv_calculation);
            tv_Check_in= view.findViewById(R.id.tv_Check_in);
            tv_Check_out= view.findViewById(R.id.tv_Check_out);
            tv_Amount = view.findViewById(R.id.tv_Amount);
            rb_Rating = view.findViewById(R.id.rb_Rating);
            ll_delete = view.findViewById(R.id.ll_delete);
            sp_option = view.findViewById(R.id.sp_option);
            sessionManager = new SessionManager(mContext);
            db=new SQLiteHandler(mContext);
            roomItineraryData =new ArrayList<>();

            optionsList = new ArrayList<>();
            optionsList.add("opt 1");
            optionsList.add("opt 2");
            optionsList.add("opt 3");
            optionsList.add("Inc all");




            AdapterCommenList share_typeAdapter = new AdapterCommenList(activity, R.layout.spiner_room_type_list,R.id.spiner_list, optionsList);
            sp_option.setAdapter(share_typeAdapter);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_hotels_itinerary_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelItineraryHotelList model = modelHotelLists.get(position);
        Log.e("Data", "" + modelHotelLists.size());
        try{
            List<String> items = new ArrayList<>();
            List<String> itemsHash = new ArrayList<>();
            items = Arrays.asList(model.getHotelsTotalRooms().split("\\s*,\\s*"));
            HashSet<String> hashSet = new HashSet<String>();
            hashSet.addAll(items);
            items = new ArrayList<>();
            items.addAll(hashSet);
            roomItineraryData =new ArrayList<>();
            roomItineraryData= db.getRoomItineraryData(Integer.parseInt(items.get(0).toString()),model.getHotel_id(),sessionManager.getCountryCode());

            if(roomItineraryData.size()>1){
                for(int k=0;roomItineraryData.size()>k;k++){
                }
            }
        }catch (Exception e){

        }

        holder.tv_Hotel.setText(model.getHotelsLists().get(0).getTitle());
        holder.tv_Amount.setText(model.getHotelsLists().get(0).getCurrency()+" "+model.getFinal_cost());
        holder.rb_Rating.setRating(Float.valueOf(model.getHotelsLists().get(0).getRating()));
        double roomCost = Double.parseDouble(model.getRoom_cost());
        if(!model.getChild().equalsIgnoreCase("0")){
            //holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults,"+model.getChild()+" Child,"+model.getTotal_night()+" Nights");
            holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults,"+model.getChild()+" Child ("+model.getTotal_night()+" nights x $"+String.format("%.02f", roomCost)+")");
        }else{
            //holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults,"+model.getTotal_night()+" Nights");
            holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults ("+model.getTotal_night()+" nights x $"+String.format("%.02f", roomCost)+")");
        }
       // holder.tv_calculation.setText(model.getHotelsLists().get(0).getCurrency()+" "+model.getRoom_cost());
        holder.tv_Check_in.setText("Check in : "+model.getCheck_in_date());
        holder.tv_Check_out.setText("Check out : "+model.getCheck_out_date());

       /* holder.tv_Check_in.setText(roomItineraryData.get(0).getHotel_extra());
        holder.tv_Check_out.setText("Check in:"+model.getCheck_in_date()+", Check out:"+model.getCheck_out_date());*/



        try {
            Glide.with(mContext)
                    .load(model.getHotelsLists().get(0).getImage())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.iv_Hotel.setImageResource(R.drawable.place_holder);
                            //holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                       boolean isFromMemoryCache, boolean isFirstResource) {
                            //holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.iv_Hotel);
        } catch (Exception e) {
            Glide.with(mContext)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .into(holder.iv_Hotel);
        }

        holder.ll_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityItinerary)mContext).deleteHotel(model.getHotel_id(),model.getCountry_id(),model);
            }
        });


        try{

            int pos = Integer.parseInt(model.getOption());
            if(pos==0){
                holder.sp_option.setSelection(3,false);
            }else{
                pos=pos-1;
                holder.sp_option.setSelection(pos,false);
            }

            holder.sp_option.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String option="";
                    if(position==0){
                        option="1";
                    }else if(position==1){
                        option="2";
                    }else if(position==2){
                        option="3";
                    }else if(position==3){
                        option="0";
                    }

                    ((ActivityItinerary)mContext).updateOption(model.getHotel_id(),model.getCountry_id(),option);
                }
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }catch (Exception e){
            Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public int getItemCount() {

        return modelHotelLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}