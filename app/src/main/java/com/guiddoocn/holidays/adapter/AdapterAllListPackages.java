package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityPackageDetails;
import com.guiddoocn.holidays.models.ModelAllHotelList;

import java.util.List;

public class AdapterAllListPackages extends RecyclerView.Adapter<AdapterAllListPackages.MyViewHolder> {
    private List<ModelAllHotelList> modelAllHotelLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;
    private int NoofNights;

    public AdapterAllListPackages(Context mContext, List<ModelAllHotelList> modelAllHotelLists, Activity activity, FragmentManager fragmentManager, int noofNight) {
        this.activity = activity;
        this.mContext = mContext;
        this.NoofNights = noofNight;
        this.modelAllHotelLists = modelAllHotelLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Hotel;
        TextView tv_Hotel, tv_query, tv_Rate;
        TextView tv_Details;
        TextView tv_ContryName;
        TextView tv_Currency;
        ProgressBar progressBar;
        RatingBar rb_Rating;
        RelativeLayout rl_rate;
        LinearLayout ll_click;

        public MyViewHolder(View view) {
            super(view);
            iv_Hotel = view.findViewById(R.id.iv_Hotel);
            tv_query = view.findViewById(R.id.tv_query);
            tv_Hotel = view.findViewById(R.id.tv_Hotel);
            tv_Details = view.findViewById(R.id.tv_Details);
            tv_ContryName = view.findViewById(R.id.tv_ContryName);
            tv_Currency = view.findViewById(R.id.tv_Currency);
            progressBar = view.findViewById(R.id.progressBar);
            rl_rate = view.findViewById(R.id.rl_rate);
            tv_Rate = view.findViewById(R.id.tv_Rate);
            rb_Rating = view.findViewById(R.id.rb_Rating);
            ll_click = view.findViewById(R.id.ll_click);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_all_package_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelAllHotelList model = modelAllHotelLists.get(position);
        Log.e("Data", "" + model);
        holder.tv_Hotel.setText(model.getTitle());
        holder.tv_Details.setText("Best for " + model.getAddress());
        holder.tv_ContryName.setText("Cities covered: " + model.getCityCountryName());
        holder.tv_Currency.setText(model.getCurrency() + " " + model.getAmount());
        holder.rb_Rating.setRating(Float.valueOf(model.getRating()));

        if (model.getQueries_received().equalsIgnoreCase("")) {
            holder.tv_query.setVisibility(View.GONE);
        } else {
            holder.tv_query.setText(model.getQueries_received() + " query received");
        }

        if (model.getSuccess_rate().equalsIgnoreCase("")) {
            holder.rl_rate.setVisibility(View.GONE);
        } else {
            holder.rl_rate.setVisibility(View.VISIBLE);
            holder.tv_Rate.setText(model.getSuccess_rate() + "% success rate");
        }


        try {
            Glide.with(mContext)
                    .load(model.getImage())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.iv_Hotel.setImageResource(R.drawable.place_holder);
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                       boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.iv_Hotel);
        } catch (Exception e) {
            Glide.with(mContext)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .into(holder.iv_Hotel);
        }

        holder.ll_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ActivityPackageDetails.class);
                intent.putExtra("Total_Days", NoofNights);
                intent.putExtra("Package_Name", model.getTitle());
                intent.putExtra("Package_ID", Integer.parseInt(model.getID()));
                intent.putExtra("Amount", Integer.parseInt(model.getAmount()));
                intent.putExtra("Currency", model.getCurrency());
                Log.e("amount","amtAll Adapter "+model.getCurrency());
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {

        return modelAllHotelLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}