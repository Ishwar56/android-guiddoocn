package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityItinerary;
import com.guiddoocn.holidays.activity.ActivityItineraryViewAll;
import com.guiddoocn.holidays.models.ModelItineraryActivitiesList;

import java.util.List;

public class AdapterItineraryActivitiesAllList extends RecyclerView.Adapter<AdapterItineraryActivitiesAllList.MyViewHolder> {
    private List<ModelItineraryActivitiesList> modelHotelLists;

    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterItineraryActivitiesAllList(Context mContext, List<ModelItineraryActivitiesList>  modelHotelLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelHotelLists = modelHotelLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Hotel;
        TextView tv_Hotel,tv_adult_child_count,tv_calculation,tv_calculation2,tv_Check_in,tv_Check_out,tv_Amount;
        RatingBar rb_Rating;
        LinearLayout ll_delete;

        public MyViewHolder(View view) {
            super(view);
            iv_Hotel = view.findViewById(R.id.iv_Hotel);
            tv_Hotel= view.findViewById(R.id.tv_Hotel);
            tv_adult_child_count= view.findViewById(R.id.tv_adult_child_count);
            tv_calculation= view.findViewById(R.id.tv_calculation);
            tv_calculation2= view.findViewById(R.id.tv_calculation2);
            tv_Check_in= view.findViewById(R.id.tv_Check_in);
            tv_Check_out= view.findViewById(R.id.tv_Check_out);
            tv_Amount = view.findViewById(R.id.tv_Amount);
            rb_Rating = view.findViewById(R.id.rb_Rating);
            ll_delete = view.findViewById(R.id.ll_delete);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_itinerary_all_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelItineraryActivitiesList model = modelHotelLists.get(position);
        Log.e("Data", "" + modelHotelLists.size());

        holder.tv_Hotel.setText(model.getModelActivitiesLists().get(0).getName());
        holder.tv_Amount.setText(model.getModelActivitiesLists().get(0).getCurrency()+" "+model.getFinal_cost());
        holder.rb_Rating.setRating(Float.valueOf(model.getModelActivitiesLists().get(0).getRating()));

        if(!model.getChild().equalsIgnoreCase("0")){
            if(model.getNo_of_Days()==0){
                holder.tv_adult_child_count.setText(model.getAdult()+" Adults, "+model.getChild()+" Child");
            }else{
                holder.tv_adult_child_count.setText(model.getAdult()+" Adults, "+model.getChild()+" Child, "+model.getDays());
            }

        }else{
            if(model.getNo_of_Days()==0){
                holder.tv_adult_child_count.setText(model.getAdult()+" Adults");
            }else{
                holder.tv_adult_child_count.setText(model.getAdult()+" Adults, "+model.getDays());
            }
        }

        if(!model.getTransfer_Charges().equalsIgnoreCase("0")){
            Double temp = Double.valueOf(model.getTransfer_Charges()) / Double.valueOf(model.getAdult());
            Double Main = temp +  Double.valueOf(model.getAdult_Amt());
           // Total_transport_cost_adult = Total_transport_cost_adult + Main;
            holder.tv_calculation.setText(model.getAdult()+" Adults x $"/*+model.getModelActivitiesLists().get(0).getCurrency()+" "*/+String.format("%.02f", Main));
            if(model.getChild().equalsIgnoreCase("0")){
                holder.tv_calculation2.setText("Transfer charges "+model.getTransfer_Charges());
            }else{
                holder.tv_calculation2.setText("Transfer charges "+model.getTransfer_Charges()+", "+model.getChild()+" Child x $"+model.getChild_Amt());
            }

        }

        if(!model.getVouchers_Charges().equalsIgnoreCase("0")){

           // holder.tv_calculation.setText(model.getAdult()+" Adults x $"+model.getAdult_Amt());
            if(!model.getIsPack().equalsIgnoreCase("false")){
                holder.tv_calculation2.setVisibility(View.GONE);
                holder.tv_calculation.setText(model.getIsPack()+" x $"+model.getFinal_cost());
            }else{
                holder.tv_calculation.setVisibility(View.GONE);
                if(model.getChild().equalsIgnoreCase("0")){
                    holder.tv_calculation2.setText(model.getAdult()+" Adults x $"+model.getAdult_Amt());
                }else{
                    holder.tv_calculation2.setText(model.getAdult()+" Adults x $"+model.getAdult_Amt()+", "+model.getChild()+" Child x $"+model.getChild_Amt());
                }
            }

        }

        if(model.getTransfer_Charges().equalsIgnoreCase("0") && model.getVouchers_Charges().equalsIgnoreCase("0")){
            holder.tv_adult_child_count.setVisibility(View.GONE);
            holder.tv_calculation.setVisibility(View.GONE);
            if(model.getChild().equalsIgnoreCase("0")){
                holder.tv_calculation2.setText(model.getAdult()+" Adults x $"+model.getAdult_Amt());
            }else{
                holder.tv_calculation2.setText(model.getAdult()+" Adults x $"+model.getAdult_Amt()+", "+model.getChild()+" Child x $"+model.getChild_Amt());
            }

        }

        if(model.getCheck_in_date().equalsIgnoreCase("")||model.getCheck_in_date().equalsIgnoreCase("null")){
            holder.tv_Check_in.setVisibility(View.GONE);
        }else{
            holder.tv_Check_in.setText("Booking Date : "+model.getCheck_in_date()+" "+model.getCheck_out_date());
        }


        holder.tv_Check_out.setText("Booking Time : "+model.getCheck_out_date());


        try {
            Glide.with(mContext)
                    .load(model.getModelActivitiesLists().get(0).getFeatured_image())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.iv_Hotel.setImageResource(R.drawable.place_holder);
                            //holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                       boolean isFromMemoryCache, boolean isFirstResource) {
                            //holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.iv_Hotel);
        } catch (Exception e) {
            Glide.with(mContext)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .into(holder.iv_Hotel);
        }

        holder.ll_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityItineraryViewAll)mContext).deleteActicity(model.getActivity_id(),model.getCountry_id());
            }
        });
    }

    @Override
    public int getItemCount() {

        return modelHotelLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}