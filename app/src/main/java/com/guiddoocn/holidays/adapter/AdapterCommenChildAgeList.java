package com.guiddoocn.holidays.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guiddoocn.holidays.R;

import java.util.List;

public class AdapterCommenChildAgeList extends ArrayAdapter<Integer> {

        LayoutInflater flater;

        public AdapterCommenChildAgeList(Activity context, int resouceId, int textviewId, List<Integer> list){

            super(context,resouceId,textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Integer rowItem = getItem(position);

            View rowview = flater.inflate(R.layout.spiner_child_age_list,null,true);

            TextView txtTitle = (TextView) rowview.findViewById(R.id.title);
            txtTitle.setText(String.valueOf(rowItem));

            return rowview;
        }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(R.layout.spiner_child_age_list,parent, false);
        }
        Integer rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setText(String.valueOf(rowItem));
        return convertView;
    }
    }

