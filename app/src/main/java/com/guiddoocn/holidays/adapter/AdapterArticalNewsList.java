package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.fragment.FragmentMediaKitDetails;
import com.guiddoocn.holidays.models.ModelNewsArticalList;

import java.util.List;

public class AdapterArticalNewsList extends RecyclerView.Adapter<AdapterArticalNewsList.MyViewHolder> {
    private List<ModelNewsArticalList> modelNewsArticalLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterArticalNewsList(Context mContext, List<ModelNewsArticalList> modelNewsArticalLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelNewsArticalLists = modelNewsArticalLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
       TextView tv_Title,tv_Description,tv_Read;

      ImageView iv_Wall;



        public MyViewHolder(View view) {
            super(view);
            tv_Title=view.findViewById(R.id.tv_Title);
            tv_Description=view.findViewById(R.id.tv_Description);
            tv_Read=view.findViewById(R.id.tv_Read);
            iv_Wall = view.findViewById(R.id.iv_Wall);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_news_artical_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelNewsArticalList model = modelNewsArticalLists.get(position);
        Log.e("Data", "" + model);

        holder.tv_Title.setText(model.getTitle());
        holder.tv_Description.setText(model.getDesription());
        holder.iv_Wall.setImageResource(model.getImage());

        holder.tv_Read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentMediaKitDetails destination =new FragmentMediaKitDetails();
                Bundle bundle = new Bundle();
                bundle.putString("URL",model.getDetailURL());
                destination.setArguments(bundle);
                changeFragment(R.id.fragment_container, destination, true);
            }
        });


    }

    @Override
    public int getItemCount() {

        return modelNewsArticalLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}