package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelHotelList;
import com.guiddoocn.holidays.models.ModelInclusionList;

import java.util.List;

public class AdapterInclusionList extends RecyclerView.Adapter<AdapterInclusionList.MyViewHolder> {
    private List<ModelInclusionList> modelInclusionLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterInclusionList(Context mContext, List<ModelInclusionList>  modelInclusionLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelInclusionLists = modelInclusionLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
       TextView tv_CityTitle;
       TextView tv_CityName;
       TextView tv_Address;
      TextView  tv_Duration;
      ImageView iv_litnary;



        public MyViewHolder(View view) {
            super(view);
            tv_CityTitle=view.findViewById(R.id.tv_CityTitle);
            tv_CityName=view.findViewById(R.id.tv_CityName);
            tv_Address=view.findViewById(R.id.tv_Address);
            tv_Duration=view.findViewById(R.id.tv_Duration);
            iv_litnary = view.findViewById(R.id.iv_litnary);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_inclusion_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelInclusionList model = modelInclusionLists.get(position);
        Log.e("Data", "" + model);
        holder.tv_CityTitle.setText(model.getTitle());
        holder.tv_CityName.setText(model.getCity());
        holder.tv_Address.setText(model.getAddress());
       // holder.tv_Duration.setText("Duration - "+model.getDay()+" Days "+model.getHour()+" hrs "+model.getMin()+" min");
        if (!model.getDay().equalsIgnoreCase("00") && !model.getHour().equalsIgnoreCase("00") && !model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getDay() + " day " + model.getHour() + " hr " + model.getMin() + " min");
        } else if (model.getDay().equalsIgnoreCase("00") && !model.getHour().equalsIgnoreCase("00")
                && !model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getHour() + " hr " + model.getMin() + " min");

        }else if (model.getDay().equalsIgnoreCase("00") && model.getHour().equalsIgnoreCase("00")
                && !model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getMin() + " min ");

        }else if (model.getDay().equalsIgnoreCase("00") && !model.getHour().equalsIgnoreCase("00")
                && model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getHour() + " hr ");
        }else if (!model.getDay().equalsIgnoreCase("00") && model.getHour().equalsIgnoreCase("00")
                && model.getMin().equalsIgnoreCase("00") ) {
            holder.tv_Duration.setText("Duration - " + model.getDay() + " day ");
        }
        try{
            Glide.with(mContext)
                    .load(model.getFeatured_image())
                    .error(R.drawable.place_holder)
                    .into(holder.iv_litnary);
        }catch (Exception e){

        }


    }

    @Override
    public int getItemCount() {

        return modelInclusionLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}