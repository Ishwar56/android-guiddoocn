package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityItinerary;
import com.guiddoocn.holidays.activity.ActivityItineraryViewAll;
import com.guiddoocn.holidays.models.ModelItineraryHotelList;
import com.guiddoocn.holidays.models.ModelRoomPriceList;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class AdapterHotelsItineraryAllList extends RecyclerView.Adapter<AdapterHotelsItineraryAllList.MyViewHolder> {
    private List<ModelItineraryHotelList> modelHotelLists;
    ArrayList<ModelRoomPriceList> roomItineraryData = new ArrayList<>();
    ArrayList<String> roomDetails= new ArrayList<>();
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;
    private SessionManager sessionManager;
    private SQLiteHandler db;

    public AdapterHotelsItineraryAllList(Context mContext, List<ModelItineraryHotelList>  modelHotelLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelHotelLists = modelHotelLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Hotel;
        TextView tv_Hotel,tv_adult_child_count,tv_calculation,tv_Check_in,tv_Check_out,tv_Amount,tv_room1,tv_room2,tv_room3,tv_room4,tv_room5,tv_room6,tv_room7,tv_option;
        RatingBar rb_Rating;
        LinearLayout ll_delete,ll_show_rooms;

        public MyViewHolder(View view) {
            super(view);
            iv_Hotel = view.findViewById(R.id.iv_Hotel);
            tv_Hotel= view.findViewById(R.id.tv_Hotel);
            tv_adult_child_count= view.findViewById(R.id.tv_adult_child_count);
            tv_calculation= view.findViewById(R.id.tv_calculation);
            tv_Check_in= view.findViewById(R.id.tv_Check_in);
            tv_Check_out= view.findViewById(R.id.tv_Check_out);
            tv_Amount = view.findViewById(R.id.tv_Amount);
            rb_Rating = view.findViewById(R.id.rb_Rating);
            ll_delete = view.findViewById(R.id.ll_delete);
            ll_show_rooms = view.findViewById(R.id.ll_show_rooms);
            sessionManager = new SessionManager(mContext);
            db=new SQLiteHandler(mContext);
            roomItineraryData =new ArrayList<>();
            tv_room1 = view.findViewById(R.id.tv_room1);
            tv_room2 = view.findViewById(R.id.tv_room2);
            tv_room3 = view.findViewById(R.id.tv_room3);
            tv_room4 = view.findViewById(R.id.tv_room4);
            tv_room5 = view.findViewById(R.id.tv_room5);
            tv_room6 = view.findViewById(R.id.tv_room6);
            tv_room7 = view.findViewById(R.id.tv_room7);
            tv_option = view.findViewById(R.id.tv_option);



        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_hotels_itinerary_all_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelItineraryHotelList model = modelHotelLists.get(position);
        Log.e("Data", "" + modelHotelLists.size());
        try{
            roomDetails= new ArrayList<>();
            List<String> items = new ArrayList<>();
            List<String> itemsHash = new ArrayList<>();
            items = Arrays.asList(model.getHotelsTotalRooms().split("\\s*,\\s*"));
            HashSet<String> hashSet = new HashSet<String>();
            hashSet.addAll(items);
            items = new ArrayList<>();
            items.addAll(hashSet);
            for(int j=0;items.size()>j;j++) {
                roomItineraryData = new ArrayList<>();
                roomItineraryData = db.getRoomItineraryData(Integer.parseInt(items.get(j).toString()), model.getHotel_id(), sessionManager.getCountryCode());
                roomDetails.add(roomItineraryData.get(0).getHotel_extra());
            }
                if (roomDetails.size() > 0) {
                    for (int k = 0; roomDetails.size() > k; k++) {
                        if (k == 0) {
                            holder.tv_room1.setVisibility(View.VISIBLE);
                            holder.tv_room1.setText("Room 1 - " + roomDetails.get(k));
                        } else if (k == 1) {
                            holder.tv_room2.setVisibility(View.VISIBLE);
                            holder.tv_room2.setText("Room 2 - " + roomDetails.get(k));
                        } else if (k == 2) {
                            holder.tv_room3.setVisibility(View.VISIBLE);
                            holder.tv_room3.setText("Room 3 - " + roomDetails.get(k));
                        } else if (k == 3) {
                            holder.tv_room4.setVisibility(View.VISIBLE);
                            holder.tv_room4.setText("Room 4 - " + roomDetails.get(k));
                        } else if (k == 4) {
                            holder.tv_room5.setVisibility(View.VISIBLE);
                            holder.tv_room5.setText("Room 5 - " + roomDetails.get(k));
                        } else if (k == 5) {
                            holder.tv_room6.setVisibility(View.VISIBLE);
                            holder.tv_room6.setText("Room 6 - " + roomDetails.get(k));
                        } else if (k == 6) {
                            holder.tv_room7.setVisibility(View.VISIBLE);
                            holder.tv_room7.setText("Room 7 - " + roomDetails.get(k));
                        }
                    }
                }

        }catch (Exception e){

        }

        holder.tv_Hotel.setText(model.getHotelsLists().get(0).getTitle());
        holder.tv_Amount.setText(model.getHotelsLists().get(0).getCurrency()+" "+model.getFinal_cost());
        holder.rb_Rating.setRating(Float.valueOf(model.getHotelsLists().get(0).getRating()));
        double roomCost = Double.parseDouble(model.getRoom_cost());

        int pos = Integer.parseInt(model.getOption());
        if(pos==0){
            holder.tv_option.setText("Inc all");
        }else if(pos==1){
            holder.tv_option.setText("Opt 1");
        }else if(pos==2){
            holder.tv_option.setText("Opt 2");
        }else if(pos==3){
            holder.tv_option.setText("Opt 3");
        }


        if(!model.getChild().equalsIgnoreCase("0")){
            //holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults,"+model.getChild()+" Child,"+model.getTotal_night()+" Nights");
            holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults,"+model.getChild()+" Child ("+model.getTotal_night()+" nights x $"+String.format("%.02f", roomCost)+")");
        }else{
            //holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults,"+model.getTotal_night()+" Nights");
            holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults ("+model.getTotal_night()+" nights x $"+String.format("%.02f", roomCost)+")");
        }

        holder.tv_Check_in.setText("Check in:"+model.getCheck_in_date());
        holder.tv_Check_out.setText("Check out:"+model.getCheck_out_date());
        //holder.tv_Check_out.setText("Check in:"+model.getCheck_in_date()+", Check out:"+model.getCheck_out_date());




        try {
            Glide.with(mContext)
                    .load(model.getHotelsLists().get(0).getImage())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.iv_Hotel.setImageResource(R.drawable.place_holder);
                            //holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                       boolean isFromMemoryCache, boolean isFirstResource) {
                            //holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.iv_Hotel);
        } catch (Exception e) {
            Glide.with(mContext)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .into(holder.iv_Hotel);
        }

        holder.ll_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityItineraryViewAll)mContext).deleteHotel(model.getHotel_id(),model.getCountry_id(),model);
            }
        });
    }

    @Override
    public int getItemCount() {

        return modelHotelLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}