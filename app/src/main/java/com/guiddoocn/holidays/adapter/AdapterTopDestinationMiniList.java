package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.guiddoocn.holidays.DataBaseHelper.SQLiteHandler;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityAllListMainActivities;
import com.guiddoocn.holidays.activity.ActivityBooking;
import com.guiddoocn.holidays.activity.ActivityHome;
import com.guiddoocn.holidays.models.ModelDestinationLists;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.List;


/**
 * Created by Ishwar on 26/12/2017.
 */

public class AdapterTopDestinationMiniList extends RecyclerView.Adapter<AdapterTopDestinationMiniList.MyViewHolder> {
    private List<ModelDestinationLists> liabraryList;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;
    private SessionManager sessionManager;
    private SQLiteHandler db;

    public AdapterTopDestinationMiniList(Context mContext, List<ModelDestinationLists> liabraryList, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.liabraryList = liabraryList;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_Contry;
        private TextView tv_Contry,tv_Amt,tv_City;
        private ProgressBar progressBar;
        private RelativeLayout rv_click;

        public MyViewHolder(View view) {
            super(view);
            iv_Contry=view.findViewById(R.id.iv_Contry1);
            tv_Contry=view.findViewById(R.id.tv_Contry);
            tv_City=view.findViewById(R.id.tv_City);
            tv_Amt=view.findViewById(R.id.tv_Amt);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);
            rv_click=view.findViewById(R.id.rv_click);
            sessionManager = new SessionManager(mContext);
            db = new SQLiteHandler(mContext);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_top_destination_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

         final ModelDestinationLists model=liabraryList.get(position);

        holder.tv_City.setText(model.getCityName());
         holder.tv_Contry.setText(" ("+model.getCountryName()+")");
         //holder.tv_Amt.setText(model.getCountryName());

        try {
            Glide.with(mContext)
                    .load(model.getFeatured_Image())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.iv_Contry.setImageResource(R.drawable.place_holder);
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                       boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.iv_Contry);
        } catch (Exception e) {
            Glide.with(mContext)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .into(holder.iv_Contry);
        }

        holder.rv_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.deleteActivityData();
                db.deleteSubDiningData();
                db.deleteSubCategoryData();
                Intent intent =new Intent(activity, ActivityAllListMainActivities.class);
                sessionManager.setCountryCode(model.getCountryID());
                activity.startActivity(intent);
            }
        });




    }

    @Override
    public int getItemCount() {

        return liabraryList.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){
        }
    }





}