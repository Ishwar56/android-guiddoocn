package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityMainEventDetails;
import com.guiddoocn.holidays.models.ModelPromoExperiencesList;
import com.guiddoocn.holidays.utils.SessionManager;

import java.util.List;

public class AdapterPromoExperiencesMiniList extends RecyclerView.Adapter<AdapterPromoExperiencesMiniList.MyViewHolder> {
    private List<ModelPromoExperiencesList> liabraryList;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;
    private SessionManager sessionManager;

    public AdapterPromoExperiencesMiniList(Context mContext, List<ModelPromoExperiencesList> liabraryList, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.liabraryList = liabraryList;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Contry2;
        TextView tv_City,tv_name,tv_Amt,tv_old_price;
        RatingBar rb_rating;
        private LinearLayout ll_click;
        public MyViewHolder(View view) {
            super(view);
            iv_Contry2=view.findViewById(R.id.iv_Contry2);
            tv_City=view.findViewById(R.id.tv_City);
            tv_name=view.findViewById(R.id.tv_name);
            tv_Amt=view.findViewById(R.id.tv_Amt);
            tv_old_price=view.findViewById(R.id.tv_old_price);
            rb_rating = view.findViewById(R.id.rb_rating);
            ll_click= view.findViewById(R.id.ll_click);
            sessionManager = new SessionManager(mContext);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_promo_experience_list, parent, false);
        return new AdapterPromoExperiencesMiniList.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelPromoExperiencesList model=liabraryList.get(position);

        holder.tv_City.setText(model.getCity());
        holder.tv_name.setText(model.getName());
        holder.tv_Amt.setText(model.getCurrency() +" "+model.getAmount());
        holder.rb_rating.setRating(Float.valueOf(model.getRating()));

        if(model.getDiscount().equalsIgnoreCase("0")|| model.getDiscount().equalsIgnoreCase("null")){
            holder.tv_old_price.setVisibility(View.GONE);
        }else{
            holder.tv_old_price.setVisibility(View.VISIBLE);
            Double Temp = Double.parseDouble(model.getAmount()) * Double.parseDouble(model.getDiscount()) /100;
            Double Amt = Temp + Double.parseDouble(model.getAmount());
            String text = "<strike><font color='#727272'>"+model.getCurrency() +" "+String.format("%.2f", Amt)+"</font></strike>";
            holder.tv_old_price.setText(Html.fromHtml(text));
        }

        try{
            Glide.with(mContext)
                    .load(model.getImage())
                    //.placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.iv_Contry2);
        }catch (Exception e){

        }


        try{
            holder.ll_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sessionManager.isNetworkAvailable()) {
                        Intent intent = new Intent(mContext, ActivityMainEventDetails.class);
                        intent.putExtra("EventID", model.getTour_id());
                        intent.putExtra("EventName", model.getName());
                        intent.putExtra("Discount", model.getDiscount());
                        intent.putExtra("Discount_cap", model.getDiscount_cap());
                        mContext.startActivity(intent);

                    } else {
                        Snackbar.make(vv, "Please Check Internet Connection !", Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        }catch (Exception e){
        }





    }

    @Override
    public int getItemCount() {

        return liabraryList.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try{
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if(flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        }catch (Exception e){
        }
    }





}