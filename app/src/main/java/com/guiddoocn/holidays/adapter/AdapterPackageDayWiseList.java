package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelInclusionList;
import com.guiddoocn.holidays.models.ModelPackagesDayWiseList;

import java.util.List;

public class AdapterPackageDayWiseList extends RecyclerView.Adapter<AdapterPackageDayWiseList.MyViewHolder> {
    private List<ModelPackagesDayWiseList> modelInclusionLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterPackageDayWiseList(Context mContext, List<ModelPackagesDayWiseList>  modelInclusionLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelInclusionLists = modelInclusionLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
       TextView tv_Title;
       TextView tv_Address;
      ImageView iv_litnary;



        public MyViewHolder(View view) {
            super(view);
            tv_Title=view.findViewById(R.id.tv_Title);
            tv_Address=view.findViewById(R.id.tv_Address);
            iv_litnary = view.findViewById(R.id.iv_litnary);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_package_day_wise_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelPackagesDayWiseList model = modelInclusionLists.get(position);
        Log.e("Data", "" + model);
        holder.tv_Title.setText(model.getTitle());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.tv_Address.setText(Html.fromHtml(model.getDescription(), Html.FROM_HTML_MODE_COMPACT));
            Log.e("Description","mode Desc "+model.getDescription());
        } else {
            holder.tv_Address.setText(Html.fromHtml(model.getDescription()));
        }

        try{
            Glide.with(mContext)
                    .load(model.getImage())
                    .error(R.drawable.place_holder)
                    .into(holder.iv_litnary);
        }catch (Exception e){

        }


    }

    @Override
    public int getItemCount() {

        return modelInclusionLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}