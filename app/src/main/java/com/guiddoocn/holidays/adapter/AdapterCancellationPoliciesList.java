package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.models.ModelCancellationPoliciesList;

import java.util.List;

public class AdapterCancellationPoliciesList extends RecyclerView.Adapter<AdapterCancellationPoliciesList.MyViewHolder> {
    private List<ModelCancellationPoliciesList> cancelList;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterCancellationPoliciesList(Context mContext, List<ModelCancellationPoliciesList> cancelList, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.cancelList = cancelList;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // LinearLayout ll_From ,ll_To ,ll_Deduction;
        TextView tv_From, tv_To, tv_Deduction;
        View vFT, vTD;

        public MyViewHolder(View view) {
            super(view);
            // ll_From=view.findViewById(R.id.ll_From);
            // ll_To=view.findViewById(R.id.ll_To);
            // ll_Deduction=view.findViewById(R.id.ll_Deduction);
            tv_From = view.findViewById(R.id.tv_From);
            tv_To = view.findViewById(R.id.tv_To);
            tv_Deduction = view.findViewById(R.id.tv_Deduction);
            vFT= view.findViewById(R.id.vFT);
            vTD= view.findViewById(R.id.vTD);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_cancel_policy_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelCancellationPoliciesList model = cancelList.get(position);
        Log.e("Data", "" + model);

        if (position == 0) {
            holder.tv_From.setBackgroundResource(R.color.colorNavyDark);
            holder.tv_To.setBackgroundResource(R.color.colorNavyDark);
            holder.tv_Deduction.setBackgroundResource(R.color.colorNavyDark);
            holder.tv_From.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            holder.tv_To.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            holder.tv_Deduction.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            holder.tv_From.setText(model.getFrom());
            holder.tv_To.setText(model.getTo());
            holder.tv_Deduction.setText(model.getDeduction());
            holder.vFT.setBackgroundResource(R.color.colorWhite);
            holder.vTD.setBackgroundResource(R.color.colorWhite);

        } else {

            holder.tv_From.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            holder.tv_To.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            holder.tv_Deduction.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            holder.tv_From.setText(model.getFrom());
            holder.tv_To.setText(model.getTo());
            holder.tv_Deduction.setText(model.getDeduction());
            holder.vFT.setVisibility(View.GONE);
            holder.vTD.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {

        return cancelList.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}