package com.guiddoocn.holidays.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.guiddoocn.holidays.R;
import com.guiddoocn.holidays.activity.ActivityItinerary;
import com.guiddoocn.holidays.activity.GenerateQuoteDetailsActivity;
import com.guiddoocn.holidays.models.ModelItineraryHotelList;

import java.util.List;

public class AdapterHotelsItineraryQuotetionList extends RecyclerView.Adapter<AdapterHotelsItineraryQuotetionList.MyViewHolder> {
    private List<ModelItineraryHotelList> modelHotelLists;
    private Context mContext;
    private Activity activity;
    private View vv;
    private int num = 1;
    private FragmentManager fragmentManager;

    public AdapterHotelsItineraryQuotetionList(Context mContext, List<ModelItineraryHotelList>  modelHotelLists, Activity activity, FragmentManager fragmentManager) {
        this.activity = activity;
        this.mContext = mContext;
        this.modelHotelLists = modelHotelLists;
        this.fragmentManager = fragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_Hotel;
        TextView tv_Hotel,tv_adult_child_count,tv_calculation,tv_Check_in,tv_Check_out,tv_Amount;
        RatingBar rb_Rating;
        LinearLayout ll_delete;


        public MyViewHolder(View view) {
            super(view);
            iv_Hotel = view.findViewById(R.id.iv_Hotel);
            tv_Hotel= view.findViewById(R.id.tv_Hotel);
            tv_adult_child_count= view.findViewById(R.id.tv_adult_child_count);
            tv_calculation= view.findViewById(R.id.tv_calculation);
            tv_Check_in= view.findViewById(R.id.tv_Check_in);
            tv_Check_out= view.findViewById(R.id.tv_Check_out);
            tv_Amount = view.findViewById(R.id.tv_Amount);
            rb_Rating = view.findViewById(R.id.rb_Rating);
            ll_delete = view.findViewById(R.id.ll_delete);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_hotels_itinerary_quotetion_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ModelItineraryHotelList model = modelHotelLists.get(position);
        Log.e("Data", "" + modelHotelLists.size());

        holder.tv_Hotel.setText(model.getHotelsLists().get(0).getTitle());
        holder.tv_Amount.setText(model.getHotelsLists().get(0).getCurrency()+" "+model.getFinal_cost());
        holder.rb_Rating.setRating(Float.valueOf(model.getHotelsLists().get(0).getRating()));

        if(!model.getChild().equalsIgnoreCase("0")){
            holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults,"+model.getChild()+" Child,"+model.getTotal_night()+" Nights");
        }else{
            holder.tv_adult_child_count.setText(model.getTotal_rooms()+" Rooms,"+model.getAdult()+" Adults,"+model.getTotal_night()+" Nights");
        }
        holder.tv_calculation.setText(model.getHotelsLists().get(0).getCurrency()+" "+model.getRoom_cost());
        holder.tv_Check_in.setText("Check in : "+model.getCheck_in_date());
        holder.tv_Check_out.setText("Check out : "+model.getCheck_out_date());

        if(model.getSuplier().equalsIgnoreCase("0")){
            holder.ll_delete.setVisibility(View.GONE);
        }else{
            holder.ll_delete.setVisibility(View.VISIBLE);
        }

        holder.ll_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GenerateQuoteDetailsActivity)mContext).deleteHotel(model.getHotel_id(),model.getCountry_id(),model);
            }
        });


        try {
            Glide.with(mContext)
                    .load(model.getHotelsLists().get(0).getImage())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.iv_Hotel.setImageResource(R.drawable.place_holder);
                            //holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                       boolean isFromMemoryCache, boolean isFirstResource) {
                            //holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.iv_Hotel);
        } catch (Exception e) {
            Glide.with(mContext)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .animate(R.anim.fade_in)
                    .into(holder.iv_Hotel);
        }


    }

    @Override
    public int getItemCount() {

        return modelHotelLists.size();
    }

    public void changeFragment(int id, Fragment fragment, boolean flag) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(id, fragment);
            if (flag)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


}